<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: common/targeting.proto

namespace Adsmanagement\Common;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>adsmanagement.common.RouteLengthTarget</code>
 */
class RouteLengthTarget extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>.google.protobuf.Int64Value min_m = 1;</code>
     */
    private $min_m = null;
    /**
     * Generated from protobuf field <code>.google.protobuf.Int64Value max_m = 2;</code>
     */
    private $max_m = null;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type \Google\Protobuf\Int64Value $min_m
     *     @type \Google\Protobuf\Int64Value $max_m
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Common\Targeting::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>.google.protobuf.Int64Value min_m = 1;</code>
     * @return \Google\Protobuf\Int64Value
     */
    public function getMinM()
    {
        return $this->min_m;
    }

    /**
     * Generated from protobuf field <code>.google.protobuf.Int64Value min_m = 1;</code>
     * @param \Google\Protobuf\Int64Value $var
     * @return $this
     */
    public function setMinM($var)
    {
        GPBUtil::checkMessage($var, \Google\Protobuf\Int64Value::class);
        $this->min_m = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>.google.protobuf.Int64Value max_m = 2;</code>
     * @return \Google\Protobuf\Int64Value
     */
    public function getMaxM()
    {
        return $this->max_m;
    }

    /**
     * Generated from protobuf field <code>.google.protobuf.Int64Value max_m = 2;</code>
     * @param \Google\Protobuf\Int64Value $var
     * @return $this
     */
    public function setMaxM($var)
    {
        GPBUtil::checkMessage($var, \Google\Protobuf\Int64Value::class);
        $this->max_m = $var;

        return $this;
    }

}

