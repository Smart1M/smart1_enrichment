<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: enums/targeting.proto

namespace Adsmanagement\Enums\TrafficLevelValue;

/**
 * Protobuf type <code>adsmanagement.enums.TrafficLevelValue.TrafficLevel</code>
 */
class TrafficLevel
{
    /**
     * Generated from protobuf enum <code>UNSPECIFIED = 0;</code>
     */
    const UNSPECIFIED = 0;
    /**
     * Generated from protobuf enum <code>UNKNOWN = 1;</code>
     */
    const UNKNOWN = 1;
    /**
     * Generated from protobuf enum <code>LIGHT_TRAFFIC = 2;</code>
     */
    const LIGHT_TRAFFIC = 2;
    /**
     * Generated from protobuf enum <code>MODERATE_TRAFFIC = 3;</code>
     */
    const MODERATE_TRAFFIC = 3;
    /**
     * Generated from protobuf enum <code>HEAVY_TRAFFIC = 4;</code>
     */
    const HEAVY_TRAFFIC = 4;
    /**
     * Generated from protobuf enum <code>STANDSTILL_TRAFFIC = 5;</code>
     */
    const STANDSTILL_TRAFFIC = 5;
    /**
     * Generated from protobuf enum <code>NORMAL_RED_LIGHT = 6;</code>
     */
    const NORMAL_RED_LIGHT = 6;
}

// Adding a class alias for backwards compatibility with the previous class name.
class_alias(TrafficLevel::class, \Adsmanagement\Enums\TrafficLevelValue_TrafficLevel::class);

