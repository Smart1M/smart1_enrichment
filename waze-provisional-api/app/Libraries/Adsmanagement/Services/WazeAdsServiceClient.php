<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Adsmanagement\Services;

/**
 */
class WazeAdsServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * Searches for entities and metrics, based on a query.
     * @param \Adsmanagement\Services\SearchWazeAdsRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function Search(\Adsmanagement\Services\SearchWazeAdsRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/adsmanagement.services.WazeAdsService/Search',
        $argument,
        ['\Adsmanagement\Services\SearchWazeAdsResponse', 'decode'],
        $metadata, $options);
    }

}
