<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: services/location_service.proto

namespace Adsmanagement\Services;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>adsmanagement.services.MutateLocationsRequest</code>
 */
class MutateLocationsRequest extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>.google.protobuf.StringValue account_id = 1;</code>
     */
    private $account_id = null;
    /**
     * Generated from protobuf field <code>repeated .adsmanagement.services.LocationOperation operations = 2;</code>
     */
    private $operations;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type \Google\Protobuf\StringValue $account_id
     *     @type \Adsmanagement\Services\LocationOperation[]|\Google\Protobuf\Internal\RepeatedField $operations
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Services\LocationService::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>.google.protobuf.StringValue account_id = 1;</code>
     * @return \Google\Protobuf\StringValue
     */
    public function getAccountId()
    {
        return $this->account_id;
    }

    /**
     * Generated from protobuf field <code>.google.protobuf.StringValue account_id = 1;</code>
     * @param \Google\Protobuf\StringValue $var
     * @return $this
     */
    public function setAccountId($var)
    {
        GPBUtil::checkMessage($var, \Google\Protobuf\StringValue::class);
        $this->account_id = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>repeated .adsmanagement.services.LocationOperation operations = 2;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getOperations()
    {
        return $this->operations;
    }

    /**
     * Generated from protobuf field <code>repeated .adsmanagement.services.LocationOperation operations = 2;</code>
     * @param \Adsmanagement\Services\LocationOperation[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setOperations($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::MESSAGE, \Adsmanagement\Services\LocationOperation::class);
        $this->operations = $arr;

        return $this;
    }

}

