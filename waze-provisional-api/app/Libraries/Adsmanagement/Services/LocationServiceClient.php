<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Adsmanagement\Services;

/**
 */
class LocationServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * Returns the requested Location,
     * @param \Adsmanagement\Services\GetLocationRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetLocation(\Adsmanagement\Services\GetLocationRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/adsmanagement.services.LocationService/GetLocation',
        $argument,
        ['\Adsmanagement\Resources\Location', 'decode'],
        $metadata, $options);
    }

    /**
     * Creates or updates Locations. Operation statuses are returned.
     * @param \Adsmanagement\Services\MutateLocationsRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function MutateLocations(\Adsmanagement\Services\MutateLocationsRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/adsmanagement.services.LocationService/MutateLocations',
        $argument,
        ['\Adsmanagement\Services\MutateLocationsResponse', 'decode'],
        $metadata, $options);
    }

}
