<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Adsmanagement\Services;

/**
 */
class CreativeServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * Returns the requested Creative,
     * @param \Adsmanagement\Services\GetCreativeRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetCreative(\Adsmanagement\Services\GetCreativeRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/adsmanagement.services.CreativeService/GetCreative',
        $argument,
        ['\Adsmanagement\Resources\Creative', 'decode'],
        $metadata, $options);
    }

    /**
     * Creates or updates Creative. Operation statuses are returned.
     * @param \Adsmanagement\Services\MutateCreativesRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function MutateCreatives(\Adsmanagement\Services\MutateCreativesRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/adsmanagement.services.CreativeService/MutateCreatives',
        $argument,
        ['\Adsmanagement\Services\MutateCreativesResponse', 'decode'],
        $metadata, $options);
    }

}
