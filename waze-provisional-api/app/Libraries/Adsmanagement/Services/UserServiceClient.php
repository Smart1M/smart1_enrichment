<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Adsmanagement\Services;

/**
 */
class UserServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param \Adsmanagement\Services\LoginRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function Login(\Adsmanagement\Services\LoginRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/adsmanagement.services.UserService/Login',
        $argument,
        ['\Adsmanagement\Services\LoginResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Adsmanagement\Services\CreateUserRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateUser(\Adsmanagement\Services\CreateUserRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/adsmanagement.services.UserService/CreateUser',
        $argument,
        ['\Adsmanagement\Services\CreateUserResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Adsmanagement\Services\ConnectUserRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ConnectUser(\Adsmanagement\Services\ConnectUserRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/adsmanagement.services.UserService/ConnectUser',
        $argument,
        ['\Adsmanagement\Services\ConnectUserResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Adsmanagement\Services\ResolveUserRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ResolveUser(\Adsmanagement\Services\ResolveUserRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/adsmanagement.services.UserService/ResolveUser',
        $argument,
        ['\Adsmanagement\Services\ResolveUserResponse', 'decode'],
        $metadata, $options);
    }

}
