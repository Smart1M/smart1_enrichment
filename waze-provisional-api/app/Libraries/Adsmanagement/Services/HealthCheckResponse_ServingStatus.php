<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: services/health_service.proto

namespace Adsmanagement\Services;

if (false) {
    /**
     * This class is deprecated. Use Adsmanagement\Services\HealthCheckResponse\ServingStatus instead.
     * @deprecated
     */
    class HealthCheckResponse_ServingStatus {}
}
class_exists(HealthCheckResponse\ServingStatus::class);
@trigger_error('Adsmanagement\Services\HealthCheckResponse_ServingStatus is deprecated and will be removed in the next major release. Use Adsmanagement\Services\HealthCheckResponse\ServingStatus instead', E_USER_DEPRECATED);

