<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Adsmanagement\Services;

/**
 */
class BudgetPlanServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * Returns the requested BudgetPlan,
     * @param \Adsmanagement\Services\GetBudgetPlanRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetBudgetPlan(\Adsmanagement\Services\GetBudgetPlanRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/adsmanagement.services.BudgetPlanService/GetBudgetPlan',
        $argument,
        ['\Adsmanagement\Resources\BudgetPlan', 'decode'],
        $metadata, $options);
    }

    /**
     * Creates or updates BudgetPlan. Operation statuses are returned.
     * @param \Adsmanagement\Services\MutateBudgetPlansRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function MutateBudgetPlans(\Adsmanagement\Services\MutateBudgetPlansRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/adsmanagement.services.BudgetPlanService/MutateBudgetPlans',
        $argument,
        ['\Adsmanagement\Services\MutateBudgetPlansResponse', 'decode'],
        $metadata, $options);
    }

}
