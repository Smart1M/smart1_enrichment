<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Adsmanagement\Services;

/**
 */
class CampaignServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * Returns the requested Campaign,
     * @param \Adsmanagement\Services\GetCampaignRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetCampaign(\Adsmanagement\Services\GetCampaignRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/adsmanagement.services.CampaignService/GetCampaign',
        $argument,
        ['\Adsmanagement\Resources\Campaign', 'decode'],
        $metadata, $options);
    }

    /**
     * Creates or updates Campaign. Operation statuses are returned.
     * @param \Adsmanagement\Services\MutateCampaignsRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function MutateCampaigns(\Adsmanagement\Services\MutateCampaignsRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/adsmanagement.services.CampaignService/MutateCampaigns',
        $argument,
        ['\Adsmanagement\Services\MutateCampaignsResponse', 'decode'],
        $metadata, $options);
    }

}
