<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Adsmanagement\Services;

/**
 * * Service for managing the current authenticated user. 
 */
class UserSelfServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * * Gets information about the logged in user. 
     * @param \Adsmanagement\Services\GetInfoRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetInfo(\Adsmanagement\Services\GetInfoRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/adsmanagement.services.UserSelfService/GetInfo',
        $argument,
        ['\Adsmanagement\Services\GetInfoResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * * Connect the logged in user to a Google account. 
     * @param \Adsmanagement\Services\ConnectSelfRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ConnectSelf(\Adsmanagement\Services\ConnectSelfRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/adsmanagement.services.UserSelfService/ConnectSelf',
        $argument,
        ['\Adsmanagement\Services\ConnectSelfResponse', 'decode'],
        $metadata, $options);
    }

}
