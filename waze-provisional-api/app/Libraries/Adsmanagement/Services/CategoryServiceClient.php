<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Adsmanagement\Services;

/**
 */
class CategoryServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param \Adsmanagement\Services\ListCategoriesRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListCategories(\Adsmanagement\Services\ListCategoriesRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/adsmanagement.services.CategoryService/ListCategories',
        $argument,
        ['\Adsmanagement\Services\ListCategoriesResponse', 'decode'],
        $metadata, $options);
    }

}
