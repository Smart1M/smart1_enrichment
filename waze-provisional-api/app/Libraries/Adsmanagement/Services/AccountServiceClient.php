<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Adsmanagement\Services;

/**
 */
class AccountServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * Returns the requested Account in full detail.
     * @param \Adsmanagement\Services\GetAccountRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetAccount(\Adsmanagement\Services\GetAccountRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/adsmanagement.services.AccountService/GetAccount',
        $argument,
        ['\Adsmanagement\Resources\Account', 'decode'],
        $metadata, $options);
    }

    /**
     * Creates or updates Account. Operation statuses are returned.
     * @param \Adsmanagement\Services\MutateAccountRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function MutateAccount(\Adsmanagement\Services\MutateAccountRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/adsmanagement.services.AccountService/MutateAccount',
        $argument,
        ['\Adsmanagement\Services\MutateAccountResponse', 'decode'],
        $metadata, $options);
    }

}
