<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: resources/campaign.proto

namespace Adsmanagement\Resources\Campaign;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>adsmanagement.resources.Campaign.SalesIo</code>
 */
class SalesIo extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>.google.protobuf.Int64Value sales_io_number = 1 [(.adsmanagement.common.Field.annotation) = {</code>
     */
    private $sales_io_number = null;
    /**
     * Generated from protobuf field <code>.google.type.Money sales_io_budget = 2 [(.adsmanagement.common.Field.annotation) = {</code>
     */
    private $sales_io_budget = null;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type \Google\Protobuf\Int64Value $sales_io_number
     *     @type \Google\Type\Money $sales_io_budget
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Resources\Campaign::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>.google.protobuf.Int64Value sales_io_number = 1 [(.adsmanagement.common.Field.annotation) = {</code>
     * @return \Google\Protobuf\Int64Value
     */
    public function getSalesIoNumber()
    {
        return $this->sales_io_number;
    }

    /**
     * Generated from protobuf field <code>.google.protobuf.Int64Value sales_io_number = 1 [(.adsmanagement.common.Field.annotation) = {</code>
     * @param \Google\Protobuf\Int64Value $var
     * @return $this
     */
    public function setSalesIoNumber($var)
    {
        GPBUtil::checkMessage($var, \Google\Protobuf\Int64Value::class);
        $this->sales_io_number = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>.google.type.Money sales_io_budget = 2 [(.adsmanagement.common.Field.annotation) = {</code>
     * @return \Google\Type\Money
     */
    public function getSalesIoBudget()
    {
        return $this->sales_io_budget;
    }

    /**
     * Generated from protobuf field <code>.google.type.Money sales_io_budget = 2 [(.adsmanagement.common.Field.annotation) = {</code>
     * @param \Google\Type\Money $var
     * @return $this
     */
    public function setSalesIoBudget($var)
    {
        GPBUtil::checkMessage($var, \Google\Type\Money::class);
        $this->sales_io_budget = $var;

        return $this;
    }

}

// Adding a class alias for backwards compatibility with the previous class name.
class_alias(SalesIo::class, \Adsmanagement\Resources\Campaign_SalesIo::class);

