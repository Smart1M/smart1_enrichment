<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: resources/location.proto

namespace Adsmanagement\Resources;

if (false) {
    /**
     * This class is deprecated. Use Adsmanagement\Resources\Location\RegionLocation\RegionTypeValue\RegionType instead.
     * @deprecated
     */
    class Location_RegionLocation_RegionTypeValue_RegionType {}
}
class_exists(Location\RegionLocation\RegionTypeValue\RegionType::class);
@trigger_error('Adsmanagement\Resources\Location_RegionLocation_RegionTypeValue_RegionType is deprecated and will be removed in the next major release. Use Adsmanagement\Resources\Location\RegionLocation\RegionTypeValue\RegionType instead', E_USER_DEPRECATED);

