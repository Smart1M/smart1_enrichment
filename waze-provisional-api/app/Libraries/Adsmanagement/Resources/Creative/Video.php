<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: resources/creative.proto

namespace Adsmanagement\Resources\Creative;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>adsmanagement.resources.Creative.Video</code>
 */
class Video extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>.google.protobuf.StringValue youtube_video_id = 1;</code>
     */
    private $youtube_video_id = null;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type \Google\Protobuf\StringValue $youtube_video_id
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Resources\Creative::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>.google.protobuf.StringValue youtube_video_id = 1;</code>
     * @return \Google\Protobuf\StringValue
     */
    public function getYoutubeVideoId()
    {
        return $this->youtube_video_id;
    }

    /**
     * Generated from protobuf field <code>.google.protobuf.StringValue youtube_video_id = 1;</code>
     * @param \Google\Protobuf\StringValue $var
     * @return $this
     */
    public function setYoutubeVideoId($var)
    {
        GPBUtil::checkMessage($var, \Google\Protobuf\StringValue::class);
        $this->youtube_video_id = $var;

        return $this;
    }

}

// Adding a class alias for backwards compatibility with the previous class name.
class_alias(Video::class, \Adsmanagement\Resources\Creative_Video::class);

