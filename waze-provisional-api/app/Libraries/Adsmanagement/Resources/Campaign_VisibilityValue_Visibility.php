<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: resources/campaign.proto

namespace Adsmanagement\Resources;

if (false) {
    /**
     * This class is deprecated. Use Adsmanagement\Resources\Campaign\VisibilityValue\Visibility instead.
     * @deprecated
     */
    class Campaign_VisibilityValue_Visibility {}
}
class_exists(Campaign\VisibilityValue\Visibility::class);
@trigger_error('Adsmanagement\Resources\Campaign_VisibilityValue_Visibility is deprecated and will be removed in the next major release. Use Adsmanagement\Resources\Campaign\VisibilityValue\Visibility instead', E_USER_DEPRECATED);

