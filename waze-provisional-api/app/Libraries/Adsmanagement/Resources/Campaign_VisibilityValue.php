<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: resources/campaign.proto

namespace Adsmanagement\Resources;

if (false) {
    /**
     * This class is deprecated. Use Adsmanagement\Resources\Campaign\VisibilityValue instead.
     * @deprecated
     */
    class Campaign_VisibilityValue {}
}
class_exists(Campaign\VisibilityValue::class);
@trigger_error('Adsmanagement\Resources\Campaign_VisibilityValue is deprecated and will be removed in the next major release. Use Adsmanagement\Resources\Campaign\VisibilityValue instead', E_USER_DEPRECATED);

