<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: resources/account.proto

namespace Adsmanagement\Resources\Account;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>adsmanagement.resources.Account.StatusValue</code>
 */
class StatusValue extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>.adsmanagement.resources.Account.StatusValue.Status value = 1;</code>
     */
    private $value = 0;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int $value
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Resources\Account::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>.adsmanagement.resources.Account.StatusValue.Status value = 1;</code>
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Generated from protobuf field <code>.adsmanagement.resources.Account.StatusValue.Status value = 1;</code>
     * @param int $var
     * @return $this
     */
    public function setValue($var)
    {
        GPBUtil::checkEnum($var, \Adsmanagement\Resources\Account_StatusValue_Status::class);
        $this->value = $var;

        return $this;
    }

}

// Adding a class alias for backwards compatibility with the previous class name.
class_alias(StatusValue::class, \Adsmanagement\Resources\Account_StatusValue::class);

