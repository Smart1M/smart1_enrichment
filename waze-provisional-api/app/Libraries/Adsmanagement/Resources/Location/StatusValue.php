<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: resources/location.proto

namespace Adsmanagement\Resources\Location;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>adsmanagement.resources.Location.StatusValue</code>
 */
class StatusValue extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>.adsmanagement.resources.Location.StatusValue.Status value = 1;</code>
     */
    private $value = 0;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int $value
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Resources\Location::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>.adsmanagement.resources.Location.StatusValue.Status value = 1;</code>
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Generated from protobuf field <code>.adsmanagement.resources.Location.StatusValue.Status value = 1;</code>
     * @param int $var
     * @return $this
     */
    public function setValue($var)
    {
        GPBUtil::checkEnum($var, \Adsmanagement\Resources\Location_StatusValue_Status::class);
        $this->value = $var;

        return $this;
    }

}

// Adding a class alias for backwards compatibility with the previous class name.
class_alias(StatusValue::class, \Adsmanagement\Resources\Location_StatusValue::class);

