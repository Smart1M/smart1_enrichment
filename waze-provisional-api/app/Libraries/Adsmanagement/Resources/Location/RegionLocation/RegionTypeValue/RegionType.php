<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: resources/location.proto

namespace Adsmanagement\Resources\Location\RegionLocation\RegionTypeValue;

/**
 * Protobuf type <code>adsmanagement.resources.Location.RegionLocation.RegionTypeValue.RegionType</code>
 */
class RegionType
{
    /**
     * Generated from protobuf enum <code>UNSPECIFIED = 0;</code>
     */
    const UNSPECIFIED = 0;
    /**
     * Generated from protobuf enum <code>UNKNOWN = 1;</code>
     */
    const UNKNOWN = 1;
    /**
     * Generated from protobuf enum <code>COUNTRY = 2;</code>
     */
    const COUNTRY = 2;
    /**
     * Generated from protobuf enum <code>STATE = 3;</code>
     */
    const STATE = 3;
    /**
     * Generated from protobuf enum <code>CITY = 4;</code>
     */
    const CITY = 4;
    /**
     * Generated from protobuf enum <code>METRO = 5;</code>
     */
    const METRO = 5;
}

// Adding a class alias for backwards compatibility with the previous class name.
class_alias(RegionType::class, \Adsmanagement\Resources\Location_RegionLocation_RegionTypeValue_RegionType::class);

