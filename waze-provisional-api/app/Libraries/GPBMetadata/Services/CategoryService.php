<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: services/category_service.proto

namespace GPBMetadata\Services;

class CategoryService
{
    public static $is_initialized = false;

    public static function initOnce() {
        $pool = \Google\Protobuf\Internal\DescriptorPool::getGeneratedPool();

        if (static::$is_initialized == true) {
          return;
        }
        \GPBMetadata\Resources\Category::initOnce();
        $pool->internalAddGeneratedFile(hex2bin(
            "0ad4020a1f73657276696365732f63617465676f72795f73657276696365" .
            "2e70726f746f12166164736d616e6167656d656e742e7365727669636573" .
            "22170a154c69737443617465676f7269657352657175657374224c0a164c" .
            "69737443617465676f72696573526573706f6e736512320a07726573756c" .
            "747318012003280b32212e6164736d616e6167656d656e742e7265736f75" .
            "726365732e43617465676f72793284010a0f43617465676f727953657276" .
            "69636512710a0e4c69737443617465676f72696573122d2e6164736d616e" .
            "6167656d656e742e73657276696365732e4c69737443617465676f726965" .
            "73526571756573741a2e2e6164736d616e6167656d656e742e7365727669" .
            "6365732e4c69737443617465676f72696573526573706f6e736522004223" .
            "0a1f636f6d2e77617a652e6164736d616e6167656d656e742e7365727669" .
            "6365735001620670726f746f33"
        ));

        static::$is_initialized = true;
    }
}

