<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    protected $address;
    protected $channel;
    protected $call_options;

    public function checkHealth(){
    	$this->address = 'wam-api.endpoints.waze-web-am.cloud.goog:443';
		$CREDENTIALS = \Grpc\ChannelCredentials::createSsl(file_get_contents(storage_path('certificates/wam-api-prod-esp.crt')));
		$this->channel = new \Grpc\Channel($this->address, [
		  'credentials' => $CREDENTIALS
		]);
		$this->call_option = array(
		  'x-api-key' =>  ['AIzaSyAPVtrUpxefeCB-mUsldPLPigEG5ULFAzg']
		);
		// BEGIN Health Check.
		$response = $this->healthCheck($this->call_options);
		echo 'Response: ' . $response->getStatus(), PHP_EOL, PHP_EOL;
    }
    public function healthCheck() {
	    $client = new \Adsmanagement\Services\HealthClient($this->address, null, $this->channel);
	    $request = new \Adsmanagement\Services\HealthCheckRequest();
	    list( $response , $status ) = $client->Check($request, $this->call_option)->wait();
	    return $response;
	}
}
