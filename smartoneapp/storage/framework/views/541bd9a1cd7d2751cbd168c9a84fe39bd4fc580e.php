<?php $__env->startSection('content'); ?>


<p>
<?php if(session('status')): ?>
    <div class="alert alert-success">
        <?php echo e(session('status')); ?>

    </div>
<?php endif; ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
        
        <ul id="horizontal-list">
            
            <li>
                <a href="<?php echo e(url('menu/add')); ?>" >
                <button type="button" class="btn btn-primary" name="create" class="">Menu <i class="fa fa-plus"></i></button>
                </a> 
            </li>
            
        </ul> 

        <table id="example" class="table table-bordered" style="width:100%;">
        <thead>
        <tr>
            <th></th>
            <th>Sl.No</th>
            <th>Name</th>
            <th>Menu Type</th>
            <th>Slug</th>
            <th>Permission</th>
            <th>Order</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>

        <?php $i=1; foreach ($menu as $key => $value) { ?>
        <tr id="<?php echo e($value->menu_id); ?>" >
            <td <?php if($value->menu_type==1 && $value->is_sub==1) { ?>class="details-control" <?php } ?>></td>
            <td><?php echo e($i); ?></td>
            <td><?php echo e($value->menu_name); ?></td>
            <td><?php if($value->menu_type==1): ?> Parent <?php else: ?> Sub <?php endif; ?></td>
            <td><?php echo e($value->menu_slug); ?></td>
            <td><?php echo e($value->name); ?></td>
            <td><?php echo e($value->menu_order); ?></td>
            <td><span id="menustatus_<?php echo e($value->menu_id); ?>"><?php if($value->active_status==0): ?> Active <?php else: ?> Inactive <?php endif; ?> </span></td>
            <td><a  href="<?php echo e(url('menu/edit/'.$value->menu_id)); ?>" title="Edit" style="text-decoration: none; color: inherit;"><i class="far fa-edit"></i></a>&nbsp; <a id="<?php echo e($value->menu_id); ?>" href="javascript:void(0)" title="Delete" style="text-decoration: none; color: inherit;" class="deletemenu"><i class="far fa-trash-alt"></i></a></td>
        </tr>

        <?php $i++; } ?>

        </tbody>
        </table>
            
        </div>
    </div>
</div>
</p>



<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.menulist', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\smart1_enrichment\smartoneapp\resources\views/menu.blade.php ENDPATH**/ ?>