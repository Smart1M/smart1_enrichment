<?php $__env->startSection('content'); ?>

<?php if($errors->any()): ?>
  <div class="alert alert-danger">
    <ul>
        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <li><?php echo e($error); ?></li>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ul>
  </div><br />
<?php endif; ?>
<div class="dash-main-body">
<div class="dash-main-form">
  <form action="<?php echo e(url('/users/update/'.$users->id)); ?>" method="post">
    <?php echo csrf_field(); ?>
      <div class="col-lg-12 row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Name <span style="color: red;">*</span>
                  </label> 
              <input type="text" name="name" id="name" required autocomplete="name" value="<?php echo e($users->name); ?>">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Role <span style="color: red;">*</span>
                  </label> 
              <select name="role_id" id="role_id" style="width: 100%;" required="">
                <option>Select a Role</option>
                <?php foreach ($roles as $key => $value) { ?>
                 <option value="<?php echo e($value->id); ?>" <?php if($value->id==$users->role_id) { ?>selected=""<?php } ?> ><?php echo e($value->name); ?></option>
               <?php } ?>
              </select>
            </div>
      </div>
      <div class="col-lg-12 row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Email <span style="color: red;">*</span>
                  </label> 
              <input type="text" name="email" id="email" value="<?php echo e($users->email); ?>" required autocomplete="email">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Mobile <span style="color: red;">*</span>
                  </label> 
              <input type="text" name="mobile" id="mobile" autocomplete="Mobile" value="<?php echo e($users->mobile); ?>">
            </div>
      </div>
      <div class="col-lg-12 row">

            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Password <span style="color: red;">*</span>
                  </label> 
              <input type="password" name="password" id="password" style="width: 100%;" autocomplete="new-password" value="<?php echo e($users->password); ?>">
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Confirm pasword <span style="color: red;">*</span>
                  </label> 
              <input id="password-confirm" type="password"  name="password_confirmation" required autocomplete="new-password" style="width: 100%" value="<?php echo e($users->password); ?>">
            </div>
      </div>
  <div class="col-lg-12 row">    
      <div class="col-lg-12">
            <input type="submit" value="Submit">
      </div>
  </div>
</form>
</div>
</div>

<?php $__env->stopSection(); ?>

<script type="text/javascript">

</script>

<?php echo $__env->make('layouts.userlist', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\smartoneapp\resources\views/editusers.blade.php ENDPATH**/ ?>