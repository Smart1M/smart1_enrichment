<div id="smartsidenav" class="sidenav-wrap">                  
      <div class="sidebar-head logo">
            <img src="<?php echo e(asset('css/images/logo.png')); ?>">
      </div>
      <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
      <div class="prof">
            <img class="prof-icon" src="<?php echo e(asset('css/images/prof.png')); ?>">
            <a href="javascript:void(0)"><span class="prof-label"><?php echo e(Auth::user()->name); ?></span></a>
      </div>
      <ul class="sidebar-menu">
            <li class="sidebar-menu-item">
                  <a href="<?php echo e(url('/home')); ?>" style="text-decoration: none; color: inherit;"><i class="fas fa-home"></i>Dashboard</a>
            </li>
            <li class="sidebar-menu-item">
                  <a href="<?php echo e(url('/roles')); ?>" style="text-decoration: none; color: inherit;"><i class="fas fa-file"></i>Roles</a>
            </li>
            <li class="sidebar-menu-item">
                  <a href="<?php echo e(url('/users')); ?>" style="text-decoration: none; color: inherit;"><i class="fas fa-user"></i>Users</a>
            </li>
            <li class="sidebar-menu-item">
                  <a href="<?php echo e(url('/company')); ?>" style="text-decoration: none; color: inherit;"><i class="fa fa-building"></i>Company</a>
            </li>
            <li class="sidebar-menu-item">
                  <a href="<?php echo e(url('/silktide')); ?>" style="text-decoration: none; color: inherit;"><i class="fa fa-wrench"></i>Silktide</a>
            </li>
            <li class="sidebar-menu-item">
                  <a href="<?php echo e(url('/brandfetch')); ?>" style="text-decoration: none; color: inherit;"><i class="fa fa-wrench"></i>Brand Fetch</a>
            </li>
            <li class="sidebar-menu-item">
              <a href="<?php echo e(route('logout')); ?>" style="text-decoration: none; color: inherit;" onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();"><i class="fas fa-power-off"></i>
                      <?php echo e(__('Logout')); ?></a>
            </li>
      </ul>
      
</div>


<div class="col-lg-2 col-md-4 col-sm-12 col-12 sidebar-wrap">

    <div class="sidebar-head logo">
          <img src="<?php echo e(asset('css/images/logo.png')); ?>">
    </div>
    <div class="prof">
          <img class="prof-icon" src="<?php echo e(asset('css/images/prof.png')); ?>">
          <a href="javascript:void(0)"><span class="prof-label"><?php echo e(Auth::user()->name); ?></span></a>
    </div>
    <ul class="sidebar-menu">
           <li class="sidebar-menu-item">
                <a href="<?php echo e(url('/home')); ?>" style="text-decoration: none; color: inherit;"><i class="fas fa-home"></i>Dashboard</a>
            </li>
            <li class="sidebar-menu-item">
                <a href="<?php echo e(url('/roles')); ?>" style="text-decoration: none; color: inherit;"><i class="fas fa-file"></i>Roles</a>
            </li>
            <li class="sidebar-menu-item">
                  <a href="<?php echo e(url('/users')); ?>" style="text-decoration: none; color: inherit;"><i class="fas fa-user"></i>Users</a>
            </li>
            <li class="sidebar-menu-item">
                  <a href="<?php echo e(url('/company')); ?>" style="text-decoration: none; color: inherit;"><i class="fas fa-building"></i>Company</a>
            </li>
            <li class="sidebar-menu-item">
                  <a href="<?php echo e(url('/silktide')); ?>" style="text-decoration: none; color: inherit;"><i class="fas fa-wrench"></i>Silktide</a>
            </li>
            <li class="sidebar-menu-item">
                  <a href="<?php echo e(url('/brandfetch')); ?>"style="text-decoration: none; color: inherit;"><i class="fas fa-wrench"></i>Brand Fetch</a>
            </li>
          <li class="sidebar-menu-item">
                <a href="<?php echo e(route('logout')); ?>" style="text-decoration: none; color: inherit;" onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();"><i class="fas fa-power-off"></i>
                          <?php echo e(__('Logout')); ?></a>
          </li>
    </ul>
    
</div>
<form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
        <?php echo csrf_field(); ?>
</form>

<?php /**PATH C:\xampp\htdocs\joshua\smartoneapp\resources\views/includes/sidebar.blade.php ENDPATH**/ ?>