<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo e(config('app.name', 'Laravel')); ?></title>

    <!-- Scripts -->
    <script src="<?php echo e(asset('js/app.js')); ?>" defer></script>
    <link rel="stylesheet" href="<?php echo e(asset('css/css/style.css')); ?>">
    <script src="https://kit.fontawesome.com/ef363a0a6e.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Fonts -->
    <!-- <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"> -->

    <!-- Styles -->
    <!-- <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet"> -->
</head>
<body>
    <div class="main-wrapper">
                        

            <?php echo $__env->make('includes.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <div class="col-lg-10 col-md-8 col-sm-12 col-12 dash-main">
                  <div class="dash-main-nav">
                        <span class="openbtn"  onclick="openNav()">&#9776;</span>
                        <div class="dash-main-nav-search">
                              <i class="fas fa-search"></i><input type="text" placeholder="Search...">
                        </div>
                        <div class="dash-main-nav-settings">
                              <a href="#"><i class="fas fa-cog"></i></a>
                        </div>
                  </div>
                  <div class="dash-main-body">
                    <?php echo $__env->yieldContent('content'); ?>
                  </div>
            </div>
      </div>
</body>
</html>
<?php echo $__env->make('includes.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\smartoneapp\resources\views/layouts/layout.blade.php ENDPATH**/ ?>