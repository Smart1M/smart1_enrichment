<?php $__env->startSection('content'); ?>

<?php if($errors->any()): ?>
  <div class="alert alert-danger">
    <ul>
        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <li><?php echo e($error); ?></li>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ul>
  </div><br />
<?php endif; ?>
<div class="dash-main-body">
<div class="dash-main-form">
  <form action="<?php echo e(url('company/save')); ?>" method="post">
    <?php echo csrf_field(); ?>
      <div class="col-lg-12 row">
            <div class="col-lg-12 col-md-6 col-sm-12 col-12">
                  <label>
                        Name <span style="color: red;">*</span>
                  </label> 
              <input type="text" name="cname" id="cname" required autocomplete="cname">
            </div>
      </div>
       <div class="col-lg-12 row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Address1 <span style="color: red;">*</span>
                  </label> 
                <input type="text" name="address1" id="address1" required autocomplete="address1">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Address2 <span style="color: red;">*</span>
                  </label> 
                <input type="text" name="address2" id="address2" required autocomplete="address2">
            </div>
      </div>
      <div class="col-lg-12 row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        City <span style="color: red;">*</span>
                  </label> 
              <input type="text" name="city" id="city" required autocomplete="city">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        State <span style="color: red;">*</span>
                  </label> 
                <input type="text" name="state" id="state" required autocomplete="state">
            </div>
      </div>
      <div class="col-lg-12 row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Zip <span style="color: red;">*</span>
                  </label> 
              <input type="text" name="zip" id="zip" required autocomplete="zip">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Country <span style="color: red;">*</span>
                  </label> 
                <input type="text" name="country" id="country" required autocomplete="country">
            </div>
      </div>
      <div class="col-lg-12 row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Email <span style="color: red;">*</span>
                  </label> 
              <input type="text" name="email" id="email" required autocomplete="email">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Phone <span style="color: red;">*</span>
                  </label> 
              <input type="text" name="phone" id="phone" autocomplete="phone">
            </div>
      </div>
      
  <div class="col-lg-12 row">    
      <div class="col-lg-12">
            <input type="submit" value="Submit">
      </div>
  </div>
</form>
</div>
</div>

<?php $__env->stopSection(); ?>

<script type="text/javascript">

</script>

<?php echo $__env->make('layouts.companylist', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\joshua\smartoneapp\resources\views/addcompany.blade.php ENDPATH**/ ?>