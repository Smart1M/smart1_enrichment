<?php $__env->startSection('content'); ?>

<?php if($errors->any()): ?>
  <div class="alert alert-danger">
    <ul>
        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <li><?php echo e($error); ?></li>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ul>
  </div><br />
<?php endif; ?>
<div class="dash-main-body">
<div class="dash-main-form">
  <form action="<?php echo e(url('/roles/save')); ?>" method="post">
     <?php echo csrf_field(); ?>
        <div class="col-lg-12 row">
              <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                    <label>
                          Role <span style="color: red;">*</span>
                    </label> 
                <input type="text" name="name" id="name" >
              </div>
        </div>
        
    <div class="col-lg-6 row">    
        <div class="col-lg-6">
              <input type="submit" value="Submit" style="float: left;">
        </div>
    </div>
  </form>
</div>
</div>

<?php $__env->stopSection(); ?>

<script type="text/javascript">

</script>

<?php echo $__env->make('layouts.list', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\smartoneapp\resources\views/addroles.blade.php ENDPATH**/ ?>