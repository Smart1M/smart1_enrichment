<?php $__env->startSection('content'); ?>


<p>
<?php if(session('status')): ?>
    <div class="alert alert-success">
        <?php echo e(session('status')); ?>

    </div>
<?php endif; ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
        
        <ul id="horizontal-list">
            <li>
                <a href="<?php echo e(url('users/add')); ?>" >
                <button type="button" class="btn btn-primary" name="create" class="">User <i class="fa fa-plus"></i></button>
                </a> 
            </li>
        </ul> 

        <table id="example" class="table table-striped table-bordered" style="width:100%;">
        <thead>
        <tr>
            <th>Sl.No</th>
            <th>Name</th>
            <th>Email</th>
            <th>Mobile</th>
            <th>Role</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>

        <?php $i=1; foreach ($users as $key => $value) { ?>
        <tr>
            <td><?php echo e($i); ?></td>
            <td><?php echo e($value->name); ?></td>
            <td><?php echo e($value->email); ?></td>
            <td><?php echo e($value->mobile); ?></td>
            <td><?php echo e($value->rolename); ?></td>
            <td><span id="status_<?php echo e($value->id); ?>"><?php if($value->ustatus==0): ?> Active <?php else: ?> Inactive <?php endif; ?> </span></td>
            <td><a  href="<?php echo e(url('users/edit/'.$value->id)); ?>" title="Edit" style="text-decoration: none; color: inherit;"><i class="far fa-edit"></i></a>&nbsp; <a id="<?php echo e($value->id); ?>" href="javascript:void(0)" title="Delete" style="text-decoration: none; color: inherit;" class="deleteusers"><i class="far fa-trash-alt"></i></a></td>
        </tr>

        <?php $i++; } ?>

        </tbody>
        </table>
            
        </div>
    </div>
</div>
</p>



<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.userlist', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\smart1_enrichment\smartoneapp\resources\views/users.blade.php ENDPATH**/ ?>