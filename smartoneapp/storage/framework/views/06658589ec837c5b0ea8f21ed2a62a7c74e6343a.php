<?php $__env->startSection('content'); ?>


<p>
<?php if(session('status')): ?>
    <div class="alert alert-success">
        <?php echo e(session('status')); ?>

    </div>
<?php endif; ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
        
        <ul id="horizontal-list">
            
                <a href="<?php echo e(url('cloudinarywidget')); ?>" >
                <button type="button" class="btn btn-primary" name="create" class="">Create <i class="fa fa-plus"></i></button>
                </a> 
            </li>
           
            <!-- <li>
                <a href="<?php echo e(url('customers')); ?>" >
                <button type="button" class="btn btn-primary" name="create" class="">Customer <i class="fa fa-user-plus"></i></button>
                </a> 
            </li> -->
        </ul> 

        <table id="example" class="table table-striped table-bordered" style="width:100%;">
        <thead>
        <tr>
            <th>Sl.No</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Website</th>
            <th>Code</th>
            <th>Widget</th>
        </tr>
        </thead>
        <tbody>

        <?php $i=1; foreach ($uploadw as $key => $value) {
            //$iframe_content = "<iframe id='myFrame' src=".base_url().'/'.$value->widget_url" max-width=100% width=100% height=650px frameborder='0'></iframe>";
        ?>
        <tr>
            <td><?php echo e($i); ?></td>
            <td><?php echo e($value->cl_name); ?></td>
            <td><?php echo e($value->cl_email); ?></td>
            <td><?php echo e($value->cl_phone); ?></td>
            <td><?php echo e($value->web_url); ?></td>
            <td><button type="button" class="btn btn-primary" onclick="showCode(<?php echo $value->cloud_id; ?>)">Code</button></td>
            <td><a href="<?php echo e(url($value->widget_url)); ?>" style="text-decoration: none;" target="_blank"><?php if($value->widget_type==1){ echo "Upload"; } else if($value->widget_type==2) { echo "Media"; } ?> &nbsp; <i class="fa fa-external-link"></i></a> </td>
        </tr>

        <?php $i++; } ?>

        </tbody>
        </table>
            
        </div>
    </div>
</div>
</p>



<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.cloudwidget', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\smart1_enrichment\smartoneapp\resources\views/uploadwidgetlist.blade.php ENDPATH**/ ?>