<style type="text/css">
  table {
    border-collapse: collapse;
    width: 100%;
  }
  
  table, th, td {
    border: 1px solid black;
  }
  th {
    text-align: center;
    height: 50px;
  }
  td {
    text-align: center;
    height: 25px;
    width: 25%;
  }
  
  </style>
  
  <h2><u>BrandFetch Data of <?php echo e($web_url); ?></u></h2>
  
  <h4>Site Content</h4>
  
  <table>
    <tbody>
        <tr>
          <td><b>Website</b></td>
          <td><?php echo e($web_url); ?></td>
        </tr>
        <tr>
          <td><b>Created Date</b></td>
          <td><?php echo e($created_at); ?></td>
        </tr>
        
    </tbody>
  </table>
  
  
  <br>
  <h4>Social Media</h4>

  <table>
    <tbody>
        <tr>
          <td><b>Twitter</b></td>
          <td><?php echo e($twitter); ?></td>
        </tr>
        <tr>
          <td><b>linkedin</b></td>
          <td><?php echo e($linkedin); ?></td>
        </tr>
        <tr>
          <td><b>github</b></td>
          <td><?php echo e($github); ?></td>
        </tr>
        <tr>
          <td><b>instagram</b></td>
          <td><?php echo e($instagram); ?></td>
        </tr>
        <tr>
          <td><b>facebook</b></td>
          <td><?php echo e($facebook); ?></td>
        </tr>
        <tr>
          <td><b>Youtube</b></td>
          <td><?php echo e($youtube); ?></td>
        </tr>
        <tr>
          <td><b>Pinterest</b></td>
          <td><?php echo e($pinterest); ?></td>
        </tr>
        <tr>
          <td><b>crunchbase</b></td>
          <td><?php echo e($crunchbase); ?></td>
        </tr>
        
    </tbody>
  </table>

  <br>
  <h4>Logo</h4>

  <table>
    <tbody>
        <tr>
          <td><b>Logo</b></td>
          <td><?php echo e($web_logo); ?></td>
        </tr>
        
        
    </tbody>
  </table>

  <br>
  <h4>Logo Background</h4>

  <table>
    <tbody>
        <tr>
          <td><b>Logo Background Colour</b></td>
          <td><?php echo e($bg_color); ?></td>
        </tr>
        
        
    </tbody>
  </table>
  
  <br>
  <h4>Logo Colour Category</h4>

  <table>
    <tbody>
        <tr>
          <td><b>Vibrant Colour</b></td>
          <td><?php echo e($vibrant); ?></td>
        </tr>

        <tr>
          <td><b>Dark Colour</b></td>
          <td><?php echo e($dark); ?></td>
        </tr>

        <tr>
          <td><b>Light Colour</b></td>
          <td><?php echo e($light); ?></td>
        </tr>
        
        
    </tbody>
  </table>

  <br>
  <h4>Logo Colour Raw</h4>

  <table>
    <tbody>
        <tr>
          <td><b>Colour Code</b></td>
          <td><?php echo e($l_colour_code); ?></td>
        </tr>

        <tr>
          <td><b>Colour Percentage</b></td>
          <td><?php echo e($l_percentage); ?></td>
        </tr>

        
        
        
    </tbody>
  </table>

  <br>
  <h4>Website Font</h4>

  <table>
    <tbody>
        <tr>
          <td><b>Title tag</b></td>
          <td><?php echo e($title_tag); ?></td>
        </tr>

        <tr>
          <td><b>Primary Font</b></td>
          <td><?php echo e($primary_font); ?></td>
        </tr>

        
        
        
    </tbody>
  </table>

  <br>
  <h4>Website Images</h4>

  <table>
    <tbody>
        <tr>
          <td><b>Image url</b></td>
          <td><?php echo e($image_url); ?></td>
        </tr>

        

        
        
        
    </tbody>
  </table>
  

  <br>
  <h4>Website Metadata</h4>

  <table>
    <tbody>
        <tr>
          <td><b>Title</b></td>
          <td><?php echo e($title); ?></td>
        </tr>
        <tr>
          <td><b>Summary</b></td>
          <td><?php echo e($summary); ?></td>
        </tr>
        <tr>
          <td><b>Description</b></td>
          <td><?php echo e($description); ?></td>
        </tr>
        <tr>
          <td><b>Keywords</b></td>
          <td><?php echo e($keywords); ?></td>
        </tr>
        <tr>
          <td><b>Language</b></td>
          <td><?php echo e($language); ?></td>
        </tr>

        

        
        
        
    </tbody>
  </table>
  
  <br>
  <h4>Website Screenshot</h4>

  <table>
    <tbody>
        <tr>
          <td><b>Screenshot url</b></td>
          <td><?php echo e($scn_url); ?></td>
        </tr>

        

        
        
        
    </tbody>
  </table>


  <br>
  <h4>Website Colour Category</h4>

  <table>
    <tbody>
        <tr>
          <td><b>Vibrant</b></td>
          <td><?php echo e($vibrant); ?></td>
        </tr>
        <tr>
          <td><b>Dark</b></td>
          <td><?php echo e($dark); ?></td>
        </tr>
        <tr>
          <td><b>Light</b></td>
          <td><?php echo e($light); ?></td>
        </tr>
        

        

        
        
        
    </tbody>
  </table>

  <br>
  <h4>Website Colour Raw</h4>

  <table>
    <tbody>
        <tr>
          <td><b>Colour Code</b></td>
          <td><?php echo e($colour_code); ?></td>
        </tr>
        <tr>
          <td><b>Percentage</b></td>
          <td><?php echo e($percentage); ?></td>
        </tr>
       
        

        

        
        
        
    </tbody>
  </table><?php /**PATH C:\xampp\htdocs\joshua\smartoneapp\resources\views/brandfetchpdf.blade.php ENDPATH**/ ?>