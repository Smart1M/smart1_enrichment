<?php $__env->startSection('content'); ?>


<p>
<?php if(session('status')): ?>
    <div class="alert alert-success">
        <?php echo e(session('status')); ?>

    </div>
<?php endif; ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            
        <ul id="horizontal-list">
            <li>
                <a href="<?php echo e(url('roles/add')); ?>" >
                <button type="button" class="btn btn-primary" name="create" class="">Role <i class="fa fa-plus"></i></button>
                </a> 
            </li>
            <li>
                <a href="<?php echo e(url('permissions')); ?>">
                <button type="button" name="create" class="btn btn-primary">Permissions <i class="fa fa-cog"></i></button>
                </a> 
            </li>
        </ul>      
        

        <table id="example" class="table table-striped table-bordered" style="width:100%;">
        <thead>
        <tr>
            <th>Sl.No</th>
            <th>Role</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>

        <?php $i=1; foreach ($roles as $key => $value) { ?>
        <tr>
            <td><?php echo e($i); ?></td>
            <td><?php echo e($value->name); ?></td>
            <td><span id="status_<?php echo e($value->id); ?>"><?php if($value->rstatus==0): ?> Active <?php else: ?> Inactive <?php endif; ?> </span></td>
            <td><a  href="<?php echo e(url('roles/edit/'.$value->id)); ?>" title="Edit" style="text-decoration: none; color: inherit;"><i class="far fa-edit"></i></a>&nbsp; <a id="<?php echo e($value->id); ?>" href="javascript:void(0)" title="Delete" style="text-decoration: none; color: inherit;" class="deleteroles"><i class="far fa-trash-alt"></i></a>&nbsp; <a  href="<?php echo e(url('permissions/assignpermission/'.$value->id)); ?>" title="Assign Permission" style="text-decoration: none; color: inherit;"><i class="fa fa-link"></i></a></td>
        </tr>

        <?php $i++; } ?>

        </tbody>
        </table>
            
        </div>
    </div>
</div>
</p>

<?php $__env->stopSection(); ?>

<script type="text/javascript">

</script>

<?php echo $__env->make('layouts.list', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\smartoneapp\resources\views/roles.blade.php ENDPATH**/ ?>