<div id="smartsidenav" class="sidenav-wrap">                  
      <div class="sidebar-head logo">
            <img src="<?php echo e(asset('css/images/logo.png')); ?>">
      </div>
      <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
      <div class="prof">
            <img class="prof-icon" src="<?php echo e(asset('css/images/prof.png')); ?>">
            <a href="javascript:void(0)"><span class="prof-label"><?php echo e(Auth::user()->name); ?></span></a>
      </div>

      <ul class="sidebar-menu">
            <li class="sidebar-menu-item">
                  <a href="<?php echo e(url('/home')); ?>" style="text-decoration: none; color: inherit; "> &nbsp; Dashboard</a>
            </li>
            <?php $menu = Menu::get_parentmenu(); $dropicon = ""; 
            foreach($menu as $mkey => $mval) { 
              if($mval->is_sub==1)  
              {
                $dropicon = 'dropdown';
              }
            ?>
             <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check($mval->name)): ?>

              <li class="sidebar-menu-item <?php echo e($dropicon); ?> ">
                  <a href="<?php echo e(url('/'.$mval->menu_slug.'')); ?>" data-toggle="<?php echo e($dropicon); ?>" style="text-decoration: none; color: inherit; display: inline-block; width: 100%;"> &nbsp; <?php echo e($mval->menu_name); ?> <?php if($mval->is_sub==1) { ?><i class="icon-arrow"></i><?php } ?></a>
                  <?php if($mval->is_sub==1) { 

                    $sub_menu = Menu::get_submenu($mval->menu_id);
                  ?>

                    <ul class="dropdown-menu">
                      <?php foreach($sub_menu as $smkey => $smval) { ?>

                        <li><a href="<?php echo e(url('/'.$smval->menu_slug.'')); ?>" style="text-decoration: none; color: inherit; display: inline-block; width: 100%;">&nbsp;<?php echo e($smval->menu_name); ?></a></li>

                      <?php } ?>
                     
                   </ul>

                  <?php } ?>
            </li>

            <?php endif; ?>

            <?php } ?>
            <?php if(auth()->check() && auth()->user()->hasRole('General|Manager')): ?>
            <li class="sidebar-menu-item dropdown">
              <a href="#" data-toggle="dropdown" style="text-decoration: none; color: inherit; display: inline-block; width: 100%;"><i class="icon-arrow"></i> &nbsp; Settings</a>

                  <ul class="dropdown-menu">
                        <li><a href="<?php echo e(url('/menu/add')); ?>" style="text-decoration: none; color: inherit; display: inline-block; width: 100%;"> &nbsp; Menu</a></li>
                   </ul>

            </li>
           <?php endif; ?>
            
            <li class="sidebar-menu-item">
            </li>
              <a href="<?php echo e(route('logout')); ?>" style="text-decoration: none; color: inherit; display: inline-block; width: 100%;" onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();"> &nbsp; <?php echo e(__('Logout')); ?></a>
            </li>
            
      </ul>
      
</div>


<div class="col-lg-2 col-md-4 col-sm-12 col-12 sidebar-wrap">

    <div class="sidebar-head logo">
          <img src="<?php echo e(asset('css/images/logo.png')); ?>">
    </div>
    <div class="prof">
          <img class="prof-icon" src="<?php echo e(asset('css/images/prof.png')); ?>">
          <a href="javascript:void(0)"><span class="prof-label"><?php echo e(Auth::user()->name); ?></span></a>
    </div>
    <!--   -->
    <ul class="sidebar-menu ">
           <li class="sidebar-menu-item dropdown">
                <a href="<?php echo e(url('/home')); ?>" data-toggle="dropdown" style="text-decoration: none; color: inherit; display: inline-block; width: 100%;"> &nbsp; Dashboard</a>
            </li>
            <?php $menu = Menu::get_parentmenu(); $dropicon = "";
            foreach($menu as $mkey => $mval) { 
              if($mval->is_sub==1)  
              {
                $dropicon = 'dropdown';
              }
            ?>
             <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check($mval->name)): ?>
              <li class="sidebar-menu-item dropdown ">
                  <a href="<?php echo e(url('/'.$mval->menu_slug.'')); ?>" data-toggle="dropdown" style="text-decoration: none; color: inherit; display: inline-block; width: 100%;"> &nbsp; <?php echo e($mval->menu_name); ?> <?php if($mval->is_sub==1) { ?><i class="icon-arrow"></i><?php } ?></a>
                  <?php if($mval->is_sub==1) { 

                    $sub_menu = Menu::get_submenu($mval->menu_id);
                  ?>

                    <ul class="dropdown-menu">
                      <?php foreach($sub_menu as $smkey => $smval) { ?>

                        <li><a href="<?php echo e(url('/'.$smval->menu_slug.'')); ?>" style="text-decoration: none; color: inherit; display: inline-block; width: 100%;">&nbsp;<?php echo e($smval->menu_name); ?></a></li>

                      <?php } ?>
                     
                   </ul>

                  <?php } ?>
            </li>
            <?php endif; ?>

            <?php } ?>

            
            <li class="sidebar-menu-item dropdown">
              <a href="#" data-toggle="dropdown" style="text-decoration: none; color: inherit; display: inline-block;
    width: 100%;"><i class="icon-arrow"></i> &nbsp; Settings</a>

                  <ul class="dropdown-menu">
                    <?php if(auth()->check() && auth()->user()->hasRole('Super Admin|Admin')): ?>
                        <li><a href="<?php echo e(url('/menu')); ?>" style="text-decoration: none; color: inherit; display: inline-block;
    width: 100%;">&nbsp;Menu</a></li>
                    <?php endif; ?>
                        <!-- <li><a href="<?php echo e(url('/changepassword')); ?>" style="text-decoration: none; color: inherit;">&nbspChange Password</a></li> -->
                   </ul>

            </li>
           
            
          <li class="sidebar-menu-item">
                <a href="<?php echo e(route('logout')); ?>" style="text-decoration: none; color: inherit; display: inline-block;
    width: 100%;" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> &nbsp; <?php echo e(__('Logout')); ?></a>
          </li>
          
    </ul>
    
</div>
<form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
        <?php echo csrf_field(); ?>
</form>

<?php /**PATH C:\xampp\htdocs\smart1_enrichment\smartoneapp\resources\views/includes/sidebar.blade.php ENDPATH**/ ?>