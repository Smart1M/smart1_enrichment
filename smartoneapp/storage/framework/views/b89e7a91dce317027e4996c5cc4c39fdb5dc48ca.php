<?php $__env->startSection('content'); ?>

<?php if($errors->any()): ?>
  <div class="alert alert-danger">
    <ul>
        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <li><?php echo e($error); ?></li>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ul>
  </div><br />
<?php endif; ?>
<div class="dash-main-body">
<div class="dash-main-form">
  <form action="<?php echo e(route('register')); ?>" method="post">
    <?php echo csrf_field(); ?>
      <div class="col-lg-12 row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Name <span style="color: red;">*</span>
                  </label> 
              <input type="text" name="name" id="name" required autocomplete="name">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Role <span style="color: red;">*</span>
                  </label> 
              <select name="role_id" id="role_id" class="js-example-basic-single" style="width: 100%;" required="">
                <option>Select a Role</option>
                <?php foreach ($roles as $key => $value) { ?>
                 <option value="<?php echo e($value->id); ?>"><?php echo e($value->name); ?></option>
               <?php } ?>
              </select>
            </div>
      </div>
      <div class="col-lg-12 row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Email <span style="color: red;">*</span>
                  </label> 
              <input type="text" name="email" id="email" required autocomplete="email">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Mobile <span style="color: red;">*</span>
                  </label> 
              <input type="text" name="mobile" id="mobile" autocomplete="Mobile">
            </div>
      </div>
      <div class="col-lg-12 row">

            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Password <span style="color: red;">*</span>
                  </label> 
              <input type="password" name="password" id="password" style="width: 100%;" autocomplete="new-password">
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Confirm pasword <span style="color: red;">*</span>
                  </label> 
              <input id="password-confirm" type="password"  name="password_confirmation" required autocomplete="new-password" style="width: 100%";>
            </div>
      </div>
  <div class="col-lg-12 row">    
      <div class="col-lg-12">
            <input type="submit" value="Submit">
      </div>
  </div>
</form>
</div>
</div>

<?php $__env->stopSection(); ?>

<script type="text/javascript">

</script>

<?php echo $__env->make('layouts.userlist', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\smart1_enrichment\smartoneapp\resources\views/addusers.blade.php ENDPATH**/ ?>