<?php $__env->startSection('content'); ?>

<?php if($errors->any()): ?>
  <div class="alert alert-danger">
    <ul>
        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <li><?php echo e($error); ?></li>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ul>
  </div><br />
<?php endif; ?>


<?php if(session('mailstatus')): ?>
    <div class="alert alert-success">
        <?php echo e(session('mailstatus')); ?>

    </div>
<?php endif; ?>
<div class="dash-main-body">
<div class="dash-main-form">
  <form action="<?php echo e(url('company/update/'.$company->company_id)); ?>" method="post" name="companyedit">
    <?php echo csrf_field(); ?>
    <input type="hidden" name="company_id" id="company_id" value="<?php echo e($company->company_id); ?>">
    <input type="hidden" name="company_name" id="company_name" value="<?php echo e($company->cname); ?>">
    <input type="hidden" name="unix_time" id="unix_time" value="<?php echo time(); ?>">
      <div class="col-lg-12 row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Name <span style="color: red;">*</span>
                  </label>
              <input type="text" name="cname" id="cname" value="<?php echo e($company->cname); ?>"  autocomplete="cname">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Email <span style="color: red;">*</span>
                  </label>
              <input type="text" name="email" id="email" value="<?php echo e($company->email); ?>" onchange="checkemailexist('<?php echo e($company->company_id); ?>')" autocomplete="email">
              <span style="color: red; font-size: 14px;" id="email_err"></span>
            </div>
      </div>
      <div class="col-lg-12 row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Address1 <span style="color: red;">*</span>
                  </label>
                <input type="text" name="address1" id="address1" value="<?php echo e($company->address1); ?>"  autocomplete="address1">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Address2
                  </label>
                <input type="text" name="address2" id="address2" value="<?php echo e($company->address2); ?>"  autocomplete="address2">
            </div>
      </div>
      <div class="col-lg-12 row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        City <span style="color: red;">*</span>
                  </label>
              <input type="text" name="city" id="city" value="<?php echo e($company->city); ?>"  autocomplete="city">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        State <span style="color: red;">*</span>
                  </label>

                <select name="state" id="state"  style="width: 100%; height: 30px;">
                  <option value="">Select State</option>
                  <?php foreach($states as $key => $value) { ?>
                      <option value="<?php echo $value->state_name; ?>" <?php if($company->state_name==$value->state_name) { ?> selected="" <?php } ?>><?php echo e($value->state_name); ?></option>
                  <?php } ?>
                </select>
            </div>
      </div>
      <div class="col-lg-12 row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Zip <span style="color: red;">*</span>
                  </label>
              <input type="text" name="zip" id="zip" value="<?php echo e($company->zip); ?>"  autocomplete="zip">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Country <span style="color: red;">*</span>
                  </label>
                <input type="text" name="country" id="country" value="<?php echo e($company->country); ?>"  autocomplete="country">
            </div>
      </div>
      <div class="col-lg-12 row">

            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Website URL <span style="color: red;">*</span>
                  </label>
              <input type="url" placeholder="https://example.com" pattern="https://.*" name="company_url" id="company_url"  autocomplete="company_url"  value="<?php echo e($company->company_url); ?>" onchange="validUrl()">
              <span id="urlError" style="color: Red; display: none; font-size: 14px;">Invalid URL</span>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Phone <span style="color: red;">*</span>
                  </label>
              <input type="text" name="phone" id="phone" autocomplete="phone" value="<?php echo e($company->phone); ?>" onchange="Validate()">
              <span id="spnError" style="color: Red; display: none">*Invalid phone number</span>
            </div>
      </div>

      <div class="col-lg-12 row">
      <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Media Partner <span style="color: red;">*</span>
                  </label>
              <select name="media_partner" id="media_partner"  style="width: 100%; height: 30px;">
                  <option value="">Select Media Partner</option>
                  <?php foreach($media_partner as $key => $value) { ?>
                      <option value="<?php echo $value->media_partner; ?>" <?php if($company->media_partner==$value->media_partner) { ?> selected="" <?php } ?>><?php echo e($value->media_partner); ?></option>
                  <?php } ?>
                </select>
            </div>
          </div>

  <div class="col-lg-12 row">
      <div class="col-lg-12">
            <input type="submit" name="submit" class="dash-main-form-btn" value="Submit">
            <a href="<?php echo e(url('/company')); ?>"><input type="button" class="dash-main-form-btn" value="Cancel" ></a>
      </div>
  </div>
</form>
</div>

<div class="container">
<a href="<?php echo e(url('/company/playgame/'.$company->company_id)); ?>" style="text-decoration: none;" class="btn btn-primary">Update&nbsp; <i class="fa fa-refresh"></i></a>


<a href="<?php echo e(url('/company')); ?>" style="text-decoration: none; float: right;" class="btn btn-default"><i class="fa fa-arrow-left"></i>&nbsp;Back to list</a>

</div>
<br>

<div class="container">

<div id="accordion">
  <h3>Vitals</h3>
  <div>
    <p>
    <table border="1">
      <tbody>
        <tr>
          <td><b>Website</b></td>
          <td><a href="<?php echo e($website_brandfetchata->web_url); ?>" target="_blank" style="text-decoration: none;"> <?php echo e($website_brandfetchata->web_url); ?> </a></td>
        </tr>
        <tr>
          <tr>
          <td><b>Created Date</b></td>
          <td><?php echo e(date('m-d-Y H:i A',strtotime($website_brandfetchata->created_at))); ?></td>
        </tr>
        <tr>
          <td><b>Title</b></td>
          <td><?php echo e($website_brandfetchata->title); ?></td>
        </tr>
        <tr>
          <td><b>Summary</b></td>
          <td><?php echo e($website_brandfetchata->summary); ?></td>
        </tr>
        <tr>
          <td><b>Description</b></td>
          <td><?php echo e($website_brandfetchata->description); ?></td>
        </tr>
        <tr>
          <td><b>Keywords</b></td>
          <td><?php echo e($website_brandfetchata->keywords); ?></td>
        </tr>
        <tr>
          <td><b>Language</b></td>
          <td><?php echo e($website_brandfetchata->language); ?></td>
        </tr>
        <tr>
          <td><b>Pages Tested</b></td>
          <td><?php echo e($company->pages_tested); ?></td>
        </tr>
        <tr>
          <td><b>Has Analytics</b></td>
          <td><?php if($company->has_analytics==1){ echo "Yes"; } else { echo "No"; } ?></td>
        </tr>
        <tr>
          <td><b>Analytics Tool</b></td>
          <td><?php if($company->has_analytics==1){ echo $company->analytics_tool; } else { echo "-"; } ?></td>
        </tr>
        <tr>
          <td><b>Has Commerce</b></td>
          <td><?php if($company->has_ecommerce==1){ echo "Yes"; } else { echo "No"; } ?></td>
        </tr>
        <tr>
          <td><b>Ecommerce Name</b></td>
          <td><?php if($company->has_ecommerce==1){ echo  $company->ecommerce_name; } else { echo "-"; } ?></td>
        </tr>
        <tr>
          <td><b>Has Pixel</b></td>
          <td><?php if($company->has_pixel==1){ echo "Yes"; } else { echo "No"; } ?></td>
        </tr>
        <tr>
          <td><b>Address Details Provided</b></td>
          <td><?php echo e($company->address_details_provided); ?></td>
        </tr>
        <tr>
          <td><b>Has Result</b></td>
          <td><?php if($company->has_result==1){ echo "Yes"; } else { echo "No"; } ?></td>
        </tr>
        <tr>
          <td><b>Local Scan</b></td>
          <td><?php if($company->yext_api_success==1){ echo "Success"; } else { echo "Fail"; } ?></td>
        </tr>
        <tr>
          <td><b>Mobile Friendly</b></td>
          <td><?php if($company->is_mobile==1){ echo "Yes"; } else { echo "No"; } ?></td>
        </tr>
        <tr>
          <td><b>Has Mobile Site</b></td>
          <td><?php if($company->has_mobile_site==1){ echo "Yes"; } else { echo "No"; } ?></td>
        </tr>
        <tr>
          <td><b>Mobile Site</b></td>
          <td><?php if($company->mobile_site_url!=""){ echo $company->mobile_site_url; } else { echo "-"; } ?></td>
        </tr>
        <tr>
          <td><b>Tablet Friendly</b></td>
          <td><?php if($company->is_tablet==1){ echo "Yes"; } else { echo "No"; } ?></td>
        </tr>
        <tr>
          <td><b>Mobile Screenshot</b></td>
          <td>
          <div class="single">
            <a id="single_1" href="<?php echo e($company->mobile_screenshot_url); ?>" title="<?php echo e($company->cname); ?>">
              <img src="<?php echo e($company->mobile_screenshot_url); ?>" alt="" class="lazy" />
            </a>
          </div>

        </td>
        </tr>
        <tr>
          <td><b>Estimate Monthly Organic Traffic</b></td>
          <td><?php echo e($company->average_monthly_traffic); ?></td>
        </tr>
        <tr>
          <td><b>Google Adwords</b></td>
          <td><?php if($company->adwords_keywords!=""){ echo $company->adwords_keywords; } else { echo "-"; } ?></td>
        </tr>
        <tr>
          <td><b>Adwords Spend</b></td>
          <td><?php if($company->has_adwords_spend==1){ echo "Yes"; } else { echo "No"; } ?></td>
        </tr>
        <tr>
          <td><b>Has a Sitemap</b></td>
          <td><?php if($company->has_sitemap==1){ echo "Yes"; } else { echo "No"; } ?></td>
        </tr>
        <tr>
          <td><b>Sitemap has Issues</b></td>
          <td><?php if($company->sitemap_issues==1){ echo "Yes"; } else { echo "No"; } ?></td>
        </tr>
        <tr>
          <td><b>Days since Last Update</b></td>
          <td><?php if($company->stale_analysis==1){ echo "Yes"; } else { echo "No"; } ?></td>
        </tr>
        <tr>
          <td><b>Estimated Site Traffic</b></td>
          <td><?php echo e($company->website_traffic); ?></td>
        </tr>
      </tbody>
    </table>
    </p>
  </div>
  <h3>Images</h3>
  <div>


    <button id="open-btn" class="myBtn btn btn-primary">Media Library Widget</button>
    <button id="upload_widget" class="btn btn-primary">Upload Widget</button>
    <button id="upload_widget_code" class="btn btn-primary">Upload Widget Code</button>
    <button id="media_widget_code" class="btn btn-primary">Media Gallery Code</button>

    <p>
      <br>
    <code>
      <span id="uploadCode"></span>
    </code>
    <br>
    </p>
    <p>
      <span id="copyUploadCode" style="display: none;"><a href="javascript:void(0)" class="copy btn btn-primary" id="copyBtn" data-clipboard-target="#uploadCode" style="text-decoration: none;">Copy</a>&nbsp; <a href="javascript:void(0)" class="closeupload btn btn-danger" style="text-decoration: none;">Close</a></span>
    </p>


  </div>
  <h3>Domain</h3>
  <div>
    <p>
    <table border="1">
      <tbody>
        <tr>
          <td><b>Domain Parked ?</b></td>
          <td><?php if($company->is_parked==1){ echo "Yes"; } else { echo "No"; } ?></td>
        </tr>
        <tr>
          <td><b>Has an SSL ?</b></td>
          <td><?php if($company->has_ssl==1){ echo "Yes"; } else { echo "No"; } ?></td>
        </tr>
        <tr>
          <td><b>SSL Expired ?</b></td>
          <td><?php if($company->ssl_expired==1){ echo "Yes"; } else { echo "No"; } ?></td>
        </tr>
        <tr>
          <td><b>SSL Redirect</b></td>
          <td><?php if($company->ssl_redirect==1){ echo "Yes"; } else { echo "No"; } ?></td>
        </tr>
        <tr>
          <td><b>SSL Valid ?</b></td>
          <td><?php if($company->ssl_valid==1){ echo "Yes"; } else { echo "No"; } ?></td>
        </tr>
        <tr>
          <td><b>Age of Domain</b></td>
          <td><?php if($company->domain_age_days!=""){ echo $company->domain_age_days; } else { echo "-"; } ?></td>
        </tr>
        <tr>
          <td><b>Expiry Date</b></td>
          <td><?php echo e(date('m/d/Y', strtotime($company->expiry_date))); ?></td>
        </tr>
        <tr>
          <td><b>Registered Date</b></td>
          <td><?php echo e(date('m/d/Y', strtotime($company->registered_date))); ?></td>
        </tr>

      </tbody>
    </table>
    </p>
  </div>
  <h3>Discovered</h3>
  <div>
    <p>
    <table>
    <tbody>
        <tr>
          <td><b>Emails</b></td>
          <td><?php if($company->emails!=""){ echo $company->emails; } else { echo "-"; } ?></td>
        </tr>
        <tr>
          <td><b>Phones</b></td>
          <td><?php if($company->phones!=""){ echo $company->phones; } else { echo "-"; } ?></td>
        </tr>
    </tbody>
  </table>
</p>
  <br>
  <p>
  <h6>Logo Color Raw</h6>

  <table>
    <tbody>
        <tr>
          <td><b>Color Code</b></td>
          <td><?php echo e($website_brandfetchata->l_colour_code); ?> &nbsp; <span class="dot" style="background-color: <?php echo e($website_brandfetchata->l_colour_code); ?>;"></span></td>
        </tr>

        <tr>
          <td><b>Color Percentage</b></td>
          <td><?php echo e($website_brandfetchata->l_percentage*100); ?> %</span></td>
        </tr>
    </tbody>
  </table>
  </p>
  <br>

  <p>
  <h6>Website Font</h6>

  <table>
    <tbody>
        <tr>
          <td><b>Title tag</b></td>
          <td><?php echo e($website_brandfetchata->title_tag); ?></td>
        </tr>

        <tr>
          <td><b>Primary Font</b></td>
          <td><?php echo e($website_brandfetchata->primary_font); ?></td>
        </tr>
    </tbody>
  </table>
  </p>
  <br>

  <p>
    <h6>Website Color Category</h6>

    <table>
      <tbody>
          <tr>
            <td><b>Vibrant</b></td>
            <td><?php echo e($website_brandfetchata->vibrant); ?> &nbsp; <span class="dot" style="background-color: <?php echo e($website_brandfetchata->vibrant); ?>;"></span></td>
          </tr>
          <tr>
            <td><b>Dark</b></td>
            <td><?php echo e($website_brandfetchata->dark); ?> &nbsp; <span class="dot" style="background-color: <?php echo e($website_brandfetchata->dark); ?>;"></span></td>
          </tr>
          <tr>
            <td><b>Light</b></td>
            <td><?php echo e($website_brandfetchata->light); ?> &nbsp; <span class="dot" style="background-color: <?php echo e($website_brandfetchata->light); ?>;"></span></td>
          </tr>
      </tbody>
    </table>
  </p>
  <br>

  <p>
    <h6>Website Color Raw</h6>

    <table>
      <tbody>
          <tr>
            <td><b>Color Code</b></td>
            <td><?php echo e($website_brandfetchata->colour_code); ?> &nbsp; <span class="dot" style="background-color: <?php echo e($website_brandfetchata->colour_code); ?>;"></span></td>
          </tr>
          <tr>
            <td><b>Percentage</b></td>
            <td><?php echo e($website_brandfetchata->percentage*100); ?> %</td>
          </tr>
      </tbody>
    </table>
  </p>

  </div>
  <h3>Social</h3>
  <div>
    <p>
    <table>
      <tbody>
          <tr>
            <td><b>Has Instagram</b></td>
            <td><?php if($company->has_instagram!=""){ echo $company->has_instagram; } else { echo "-"; } ?></td>
          </tr>
          <tr>
            <td><b>Days since last Instagram Update</b></td>
            <td><?php echo e($company->days_since_update); ?></td>
          </tr>
          <tr>
            <td><b>Last Instatgram Update</b></td>
            <td><?php echo e(date('m/d/Y', strtotime($company->last_updated_date))); ?></td>
          </tr>
          <tr>
            <td><b>Twitter Info</b></td>
            <td><?php if($company->twitter_found==1){ echo "Yes"; } else { echo "No"; } ?></td>
          </tr>
          <tr>
            <td colspan="2"><?php if(!empty($website_brandfetchata->twitter)){ ?> <a href="<?php echo e($website_brandfetchata->twitter); ?>" target="_blank"><img src="<?php echo e(asset('socialmedia/twitter.png')); ?>"> </a> <?php } else { echo "No Twitter account found"; } ?></td>
          </tr>
          <tr>
            <td colspan="2"><?php if(!empty($website_brandfetchata->linkedin)){ ?> <a href="<?php echo e($website_brandfetchata->linkedin); ?>" target="_blank"><img src="<?php echo e(asset('socialmedia/linkedin.png')); ?>"> </a> <?php } else { echo "No LinkedIn account found"; } ?></b></td>
          </tr>
          <tr>
            <td colspan="2"><?php if(!empty($website_brandfetchata->github)){ ?> <a href="<?php echo e($website_brandfetchata->github); ?>" target="_blank"><img src="<?php echo e(asset('socialmedia/github-logo.png')); ?>"> </a> <?php } else { echo "No Github account found"; } ?></td>
          </tr>
          <tr>
            <td colspan="2"><?php if(!empty($website_brandfetchata->instagram)){ ?> <a href="<?php echo e($website_brandfetchata->instagram); ?>" target="_blank"><img src="<?php echo e(asset('socialmedia/instagram.png')); ?>"> </a> <?php } else { echo "No Instagram account found"; } ?></td>
          </tr>
          <tr>
            <td colspan="2"><?php if(!empty($website_brandfetchata->facebook)){ ?> <a href="<?php echo e($website_brandfetchata->facebook); ?>" target="_blank"><img src="<?php echo e(asset('socialmedia/facebook.png')); ?>"> </a> <?php } else { echo "No Facebook account found"; } ?></td>
          </tr>
          <tr>
            <td colspan="2"><?php if(!empty($website_brandfetchata->youtube)){ ?> <a href="<?php echo e($website_brandfetchata->youtube); ?>" target="_blank"><img src="<?php echo e(asset('socialmedia/youtube.png')); ?>"> </a> <?php } else { echo "No Youtube account found"; } ?></td>
          </tr>
          <tr>
            <td colspan="2"><?php if(!empty($website_brandfetchata->pinterest)){ ?> <a href="<?php echo e($website_brandfetchata->pinterest); ?>" target="_blank"><img src="<?php echo e(asset('socialmedia/pinterest.png')); ?>"> </a> <?php } else { echo "No Pinerest account found"; } ?></td>
          </tr>
          <tr>
            <td colspan="2"><?php if(!empty($website_brandfetchata->crunchbase)){ ?> <a href="<?php echo e($website_brandfetchata->crunchbase); ?>" target="_blank"><img src="<?php echo e(asset('socialmedia/crunchbase.png')); ?>"> </a> <?php } else { echo "No Crunchbase account found"; } ?></td>
          </tr>
      </tbody>
    </table>
    </p>
  </div>

  <!-- Cut Data..Paste here -->
  <h3>Hours</h3>
  <div>
    <p>
    <?php
      if(isset($hour_arr)&&!empty($hour_arr))
      {
        foreach($hour_arr as $key => $value)
        {
            echo $value; echo '<br>';
        }
      } else {
        echo "No listings !!";
      }


    ?>
    </p>
  </div>
<h3>Display Ad Reports</h3>
  <!-- <div>
    <?php if(count($sem_dispadreports)>0) { ?>
    <h4>Publisher Display Ads</h4>
    <table id="example" class="table table-striped table-bordered" style="width:100%;">
        <thead>
        <tr>
            <th>Sl.No</th>
            <th>Title</th>
            <th>Text</th>
            <th>First Seen</th>
            <th>Last Seen</th>
            <th>No. of time seen</th>
            <th>Visible URL</th>
        </tr>
        </thead>
        <tbody>

        <?php $i=1; foreach ($sem_dispadreports as $key => $value) { ?>
        <tr>
            <td><?php echo e($i); ?></td>
            <td><?php echo e($value->title); ?></td>
            <td><?php echo e($value->text); ?></td>
            <td><?php echo e($value->first_seen); ?></td>
            <td><?php echo e($value->last_seen); ?></td>
            <td><?php echo e($value->times_seen); ?></td>
            <td><?php echo e($value->visible_url); ?></td>
        </tr>

        <?php $i++; } ?>

        </tbody>
    </table>
    <?php } else { echo "Nothing Found !!"; } ?>
  </div> -->


  <div>
    <div class="tab">
      <button class="tablinks" onclick="openTab(event, 'publisher_text_ads')" id="defaultOpen">Publisher Display Ads</button>
      <button class="tablinks" onclick="openTab(event, 'publisher_advertisers')">Advertisers</button>
      <button class="tablinks" onclick="openTab(event, 'publisher_rank')">Publisher Rank</button>
    </div>

    <!-- Tab content -->
    <div id="publisher_text_ads" class="tabcontent">
      <!-- <h6>Publisher Display Ads</h6>  -->
      <br>
      <p>
        <?php if(count($sem_dispadreports)>0) { ?>
        <table id="example" class="table table-striped table-bordered" style="width:100%;">
            <thead>
            <tr>
                <th>Sl.No</th>
                <th>Title</th>
                <th>Text</th>
                <th>First Seen</th>
                <th>Last Seen</th>
                <th>No. of time seen</th>
                <th>Visible URL</th>
            </tr>
            </thead>
            <tbody>

            <?php $i=1; foreach ($sem_dispadreports as $key => $value) { ?>
            <tr>
                <td><?php echo e($i); ?></td>
                <td><?php echo e($value->title); ?></td>
                <td><?php echo e($value->text); ?></td>
                <td><?php echo e($value->first_seen); ?></td>
                <td><?php echo e($value->last_seen); ?></td>
                <td><?php echo e($value->times_seen); ?></td>
                <td><?php echo e($value->visible_url); ?></td>
            </tr>

            <?php $i++; } ?>

            </tbody>
        </table>
        <?php } else { echo "Nothing Found !!"; } ?>
      </p>
      </div>


    <div id="publisher_advertisers" class="tabcontent">
      <!-- <h3>Paris</h3> -->
      <br>
      <p>
        <?php if(count($sem_advertisers)>0) { ?>
        <table id="example" class="table table-striped table-bordered" style="width:100%;">
            <thead>
            <tr>
                <th>Sl.No</th>
                <th>Domain</th>
                <th>Ads Count</th>
                <th>First Seen</th>
                <th>Last Seen</th>
                <th>No. of time seen</th>
            </tr>
            </thead>
            <tbody>

            <?php $i=1; foreach ($sem_advertisers as $key => $value) { ?>
            <tr>
                <td><?php echo e($i); ?></td>
                <td><?php echo e($value->domain); ?></td>
                <td><?php echo e($value->ads_count); ?></td>
                <td><?php echo e($value->first_seen); ?></td>
                <td><?php echo e($value->last_seen); ?></td>
                <td><?php echo e($value->times_seen); ?></td>
            </tr>

            <?php $i++; } ?>

            </tbody>
        </table>
        <?php } else { echo "Nothing Found !!"; } ?>
      </p>
    </div>

    <div id="publisher_rank" class="tabcontent">
      <br>
      <p>
        <?php if(!empty($sem_publisher_rank)>0) { ?>
        <table id="example" class="table table-striped table-bordered" style="width:100%;">
            <thead>
            <tr>
                <th>Domain</th>
                <th>Ads Overall</th>
                <th>Text Ads</th>
                <th>Media Ads</th>
                <th>First Seen</th>
                <th>Last Seen</th>
                <th>Domain Overall</th>
                <th>No. of time seen</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><?php echo e($sem_publisher_rank ->domain); ?></td>
                <td><?php echo e($sem_publisher_rank ->ads_overall); ?></td>
                <td><?php echo e($sem_publisher_rank ->text_ads_overall); ?></td>
                <td><?php echo e($sem_publisher_rank ->media_ads_overall); ?></td>
                <td><?php echo e($sem_publisher_rank ->first_seen); ?></td>
                <td><?php echo e($sem_publisher_rank ->last_seen); ?></td>
                <td><?php echo e($sem_publisher_rank ->times_seen); ?></td>
                <td><?php echo e($sem_publisher_rank ->domain_overall); ?></td>
            </tr>
            </tbody>
        </table>
        <?php } else { echo "Nothing Found !!"; } ?>
      </p>
    </div>
  </div>

<h3>Marketing Overview</h3>
  <div>
    <p>
    <?php if(!empty($sem_marketoverview)) { ?>
    <table border="1">
      <tbody>
        <tr>
          <td><b>Database</b></td>
          <td><?php echo e($sem_marketoverview->database_rgn); ?></td>
        </tr>
        <tr>
          <td><b>Domain</b></td>
          <td><?php echo e($sem_marketoverview->domain); ?></td>
        </tr>
        <tr>
          <td><b>Rank</b></td>
          <td><?php echo e($sem_marketoverview->rank); ?></td>
        </tr>
        <tr>
          <td><b>Organic Keywords</b></td>
          <td><?php echo e($sem_marketoverview->organic_keywords); ?></td>
        </tr>
        <tr>
          <td><b>Organic Traffic</b></td>
          <td><?php echo e($sem_marketoverview->organic_traffic); ?></td>
        </tr>
        <tr>
          <td><b>Organic Cost</b></td>
          <td><?php echo e($sem_marketoverview->organic_cost); ?></td>
        </tr>
        <tr>
          <td><b>Adwords Keywords</b></td>
          <td><?php echo e($sem_marketoverview->adwords_keyword); ?></td>
        </tr>
        <tr>
          <td><b>Adwords Traffic</b></td>
          <td><?php echo e($sem_marketoverview->adwords_traffic); ?></td>
        </tr>
        <tr>
          <td><b>Adwords Cost</b></td>
          <td><?php echo e($sem_marketoverview->adwords_cost); ?></td>
        </tr>
        <tr>
          <td><b>PLA Keywords</b></td>
          <td><?php echo e($sem_marketoverview->pla_keywords); ?></td>
        </tr>
        <tr>
          <td><b>PLA Uniques</b></td>
          <td><?php echo e($sem_marketoverview->pla_uniques); ?></td>
        </tr>

      </tbody>
    </table>
  <?php } else { echo "No Data Found !!"; } ?>
    </p>
  </div>


<h3>Local Data</h3>
  <div>
    <p>

    <?php if(!empty($basic_scan_id)) {
      $list_accuracy = ($sc_det->name_sum + $sc_det->phone_sum + $sc_det->address_sum)/3;
      $similar_business = ($review_counts->no_of_review + $review_counts->review_rating)/2;


      if($sc_det->name_sum>50)
      {
        $name_sum_class = 'progress-bar progress-bar-danger progress-bar-dang';
      } else
      {
        $name_sum_class = 'progress-bar progress-bar-warning progress-bar-warn';
      }

      if($sc_det->phone_sum>50)
      {
        $phone_sum_class = 'progress-bar progress-bar-danger progress-bar-dang';
      } else
      {
        $phone_sum_class = 'progress-bar progress-bar-warning progress-bar-warn';
      }

      if($sc_det->address_sum>50)
      {
        $address_sum_class = 'progress-bar progress-bar-danger progress-bar-dang';
      } else
      {
        $address_sum_class = 'progress-bar progress-bar-warning progress-bar-warn';
      }


      if($review_counts->no_of_review>50)
      {
        $no_of_review_class = 'progress-bar progress-bar-danger progress-bar-dang';
      } else
      {
        $no_of_review_class = 'progress-bar progress-bar-warning progress-bar-warn';
      }


      if($review_counts->review_rating>50)
      {
        $review_rating_class = 'progress-bar progress-bar-danger progress-bar-dang';
      } else
      {
        $review_rating_class = 'progress-bar progress-bar-warning progress-bar-warn';
      }

      if($missing_schema_count>50)
      {
        $error_count_class = 'progress-bar progress-bar-danger progress-bar-dang';
      } else
      {
        $error_count_class = 'progress-bar progress-bar-warning progress-bar-warn';
      }



    ?>

      <div class="foot-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-md-4">
                    <div class="section-one-footer">
                        <h1 class="new-h1"><?php echo e(number_format((float)$list_accuracy, 0, '.', '')); ?> %</h1>
                        <p>LISTINGS INACCURACY</p>
                    </div>
                    <div class="progress-div">
                        <div class="section-progress-one">
                            <p>BUSINESS NAME</p>
                            <p>ADDRESS</p>
                            <p>PHONE NUMBER</p>
                        </div>
                        <div class="section-progress-two">
                            <div class="progress smart-progress">
                                <div class="<?php echo $name_sum_class; ?>" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo e($sc_det->name_sum); ?>%">
                                </div>
                            </div>
                            <div class="progress smart-progress">
                                <div class="<?php echo $address_sum_class; ?>" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo e($sc_det->address_sum); ?>%">
                                </div>
                            </div>
                            <div class="progress smart-progress">
                                <div class="<?php echo $phone_sum_class; ?>" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo e($sc_det->phone_sum); ?>%">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4">
                    <div class="section-one-footer">
                        <h1 class="new-h1"><?php echo e(number_format((float)$similar_business, 0, '.', '')); ?>%</h1>
                        <p>OF SIMILAR BUSINESSES IN YOUR REGION HAVE BETTER REVIEWS</p>
                    </div>
                    <div class="progress-div">
                        <div class="section-progress-one">
                            <p>REVIEW RATING</p>
                            <p># OF REVIEWS</p>
                        </div>
                        <div class="section-progress-two">
                            <div class="progress smart-progress">
                                <div class="<?php echo $review_rating_class; ?>" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo e($review_counts->review_rating); ?>%">
                                </div>
                            </div>
                            <div class="progress smart-progress">
                                <div class="<?php echo $no_of_review_class; ?>" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo e($review_counts->no_of_review); ?>%">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4">
                    <div class="section-one-footer">
                        <h1 class="new-h1"><?php echo e($missing_schema_count); ?>%</h1>
                        <p>ERROR SCANNING YOUR WEBSITE</p>
                    </div>
                    <div class="progress-div">
                        <div class="section-progress-one">
                            <p>SCHEMA</p>
                        </div>
                        <div class="section-progress-two">
                            <div class="progress smart-progress">
                                <div class="<?php echo $error_count_class; ?>" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo e($missing_schema_count); ?>%">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <br>
    <table id="example" class="table table-bordered" style="width:100%;">
        <thead>
        <tr>
            <th>Logo</th>
            <th>Name</th>
            <th>Errors</th>
            <th>See Errors</th>
        </tr>
        </thead>
        <tbody>

        <?php $error_flag = 0; $name_error_flag = 0; $phone_error_flag = 0; $address_error_flag = 0; $zip_error_flag = 0;
         foreach ($yext_res as $key => $value) {

          $cname = $company->cname;
          $phone = preg_replace('/[^A-Za-z0-9\-]/', '',str_replace(array(' ','-'), '', $company->phone));
          $caddress = $company->address1;

          $yext_phone_sub = substr($value->phone,0,2);

          if($yext_phone_sub=='+1')
          {
            $yext_phone = substr($value->phone,2);
          } else
          {
            $yext_phone = $value->phone;
          }

          $czip = $company->zip;

          if($cname!=$value->compname)
          {
            $name_error_flag = 1;
          } else
          {
            $name_error_flag = 0;
          }

          if($phone!=$yext_phone)
          {
            $phone_error_flag = 1;
          } else
          {
            $phone_error_flag = 0;
          }

          if($caddress!=$value->address)
          {
            $address_error_flag = 1;
          } else
          {
            $address_error_flag = 0;
          }

          if($czip!=$value->zip)
          {
            $zip_error_flag = 1;
          } else
          {
            $zip_error_flag = 0;
          }

          if($name_error_flag==0&&$phone_error_flag==0&&$address_error_flag==0&&$zip_error_flag==0)
          {
            $error_flag = 0;
          }
          else
          {
            $error_flag = 1;
          }

          $sc_res = \App\Http\Controllers\CompanyController::scandetailsdata($value->siteId,$value->scn_basic_id);

          if($value->url=='')
          {
            $siteUrl = 'javascript:void(0)';
          } else
          {
            $siteUrl = $value->url;
          }


        ?>

        <tr>
            <td align="center"><a href="<?php echo e($siteUrl); ?>" <?php if($value->url!='') { ?>target="_blank"<?php } ?>><img src="<?php echo e($sc_res->logo); ?>" <?php if($value->url!='') { ?> alt="<?php echo $value->siteId; ?>"<?php } else { ?>alt="No URL" <?php } ?>></a></td>
            <td><?php echo e($value->siteId); ?></td>
            <td><?php if($error_flag==1) { echo "Yes"; } else { echo "No"; } ?></td>
            <td><?php if($error_flag==1) { ?> <a href="#myModal" class="btn modalpop" data-toggle="modal" data-id="<?php echo $company->company_id; ?>_<?php echo $value->scn_basic_id; ?>_<?php echo $value->siteId; ?>_<?php echo $name_error_flag; ?>_<?php echo $phone_error_flag; ?>_<?php echo $address_error_flag; ?>_<?php echo $zip_error_flag; ?>" >See Errors</a> <?php } ?></td>

        <?php } ?>

        </tbody>
        </table>

<br>

<!-- <a href="<?php echo e(url('company/pdf/'.$company->company_id.'/'.$basic_scan_id->basic_id)); ?>" class="btn btn-primary pdfdownload" style="text-decoration: none;"><span id="pdfbtntext">Download PDF</span></a> &nbsp; --> <a href="<?php echo e(url('company/excel/'.$company->company_id.'/'.$basic_scan_id->basic_id)); ?>" class="btn btn-primary" style="text-decoration: none;">Download Excel</a> &nbsp;<!--  <a href="<?php echo e(url('company/sendmail/'.$company->company_id.'/'.$basic_scan_id->basic_id)); ?>" class="btn btn-primary" style="text-decoration: none;">Email Results</a> -->


<!-- <a href="#myModal_email" class="btn modalpopemail btn-primary" data-toggle="modal" style="text-decoration: none;" >Email Results</a> -->


  <?php } else { echo "No Data Found !!"; } ?>
    </p>
  </div>



<h3>Site Data</h3>
  <div>

    <?php if(!empty($sitedata)) {
    $arr_url = explode(",", $sitedata->page_url); ?>
    <p>Page URL(s)</p>
    <table>
      <thead>
        <th style="text-align: center;">Sl.No</th>
        <th style="text-align: center;">Url</th>
      </thead>
      <tbody>
        <?php $i=1; foreach($arr_url as $k => $v) { ?>
          <tr>
          <td style="text-align: center;"><?php echo e($i); ?></td>
          <td><a href="<?php echo e($v); ?>" target="_blank" style="text-decoration: none;"><?php echo e($v); ?></a></td>
        </tr>
        <?php $i++; } ?>

      </tbody>
    </table>
    <br>
    <?php
    $filepath =  asset('storage/'.$sitedata->content_path);
    //$filepath = storage_path('public/'.$sitedata->content_path); ?>
    <a href="<?php echo e(url($filepath)); ?>" class="btn btn-primary" download style="text-decoration: none;">Download Content File</a>

  <?php } else { echo "No Data Found !!"; } ?>

  </div>


</div>
<br>

</div>

</div>



<div id="myModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Errors</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <p><span id="name_check"></span>&nbsp; <?php echo e($company->cname); ?></p>
                <p><span id="address_check"></span>&nbsp; <?php echo e($company->address1); ?></p>
                <p><span id="phone_check"></i></span>&nbsp; <?php echo e($company->phone); ?></p>
                <p><span id="zip_check"></i></span>&nbsp; <?php echo e($company->zip); ?></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<div id="myModal_email" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Email Results</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
              <div id="form_area">
                <p><input type="text" name="to_email_name" id="to_email_name" value="" class="form-control" placeholder="Recipent Name"></p>
                <p><input type="text" name="to_email_id" id="to_email_id" value="" class="form-control" placeholder="Recipent Email"></p>
                <input type="hidden" name="data_params" id="data_params" value="<?php echo $company->company_id; ?>">
              </div>

                <span id="email_send_error" style="color: red;"></span>
                <span id="email_send_success" style="color: green;"></span>
            </div>
            <div class="modal-footer">
              <button type="button" id="email_send" class="btn btn-primary" >Send</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script> -->
<!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script> -->
<script type="text/javascript">
// function ValidateEmail()
// {
//   var mail = document.getElementById('email').value;
//  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
//   {
//     return true;
//   }
//     alert("You have entered an invalid email address!")
//     return false;
// }

$(document).ready(function() {

    var validUrl = false;
    var web_url = document.getElementById('company_url').value;
    var pattern = /^(http|https)?:\/\/[a-zA-Z0-9-\.]+\.[a-z]{2,4}/;


    validUrl = pattern.test(web_url)
    if(validUrl==false){
      //$("#company_url").val("");
      document.getElementById("urlError").style.display = !validUrl ? "block" : "none";
    } else
    {
      document.getElementById("urlError").style.display = !validUrl ? "block" : "none";
    }
});

function Validate() {
    var isValid = false;
    var regex = /^[0-9-+()'']*$/;
    isValid = regex.test(document.getElementById("phone").value);
    document.getElementById("spnError").style.display = !isValid ? "block" : "none";

    if(isValid==false)
    {
      $("#phone").val("");
    }
    return isValid;
}

function validUrl()
{
  var validUrl = false;
  var web_url = document.getElementById('company_url').value;
  var pattern = /^(http|https)?:\/\/[a-zA-Z0-9-\.]+\.[a-z]{2,4}/;


  validUrl = pattern.test(web_url)
  if(validUrl==false){
    $("#company_url").val("");
    document.getElementById("urlError").style.display = !validUrl ? "block" : "none";
  } else
  {
    document.getElementById("urlError").style.display = !validUrl ? "block" : "none";
  }
}

$(document).ready(function(){
    $(".modalpop").click(function(){
        id = $(this).attr("data-id");
        var res = id.split("_");
        var cid = res[0];
        var bs_id = res[1];
        var siteid = res[2];

        var name_error_flag = res[3];
        var address_error_flag = res[5];
        var phone_error_flag = res[4];
        var zip_error_flag = res[6];

        //console.log(name_error_flag);

        if(name_error_flag==1)
        {
          $("#name_check").html('<i class="fa fa-times" style="color: red;"></i>');
        } else
        {
          $("#name_check").html('<i class="fa fa-check"></i>');
        }

        if(address_error_flag==1)
        {
          $("#address_check").html('<i class="fa fa-times" style="color: red;"></i>');
        } else
        {
          $("#address_check").html('<i class="fa fa-check"></i>');
        }

        if(phone_error_flag==1)
        {
          $("#phone_check").html('<i class="fa fa-times" style="color: red;"></i>');
        } else
        {
          $("#phone_check").html('<i class="fa fa-check"></i>');
        }

        if(zip_error_flag==1)
        {
          $("#zip_check").html('<i class="fa fa-times" style="color: red;"></i>');
        } else
        {
          $("#zip_check").html('<i class="fa fa-check"></i>');
        }


        // $.ajaxSetup({
        //   headers: {
        //       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //   }
        // });

        // $.ajax({
        //       url: "<?php echo e(url('/company/errorchecking')); ?>",
        //       method: 'post',
        //       data: {
        //          cid:cid,bs_id:bs_id,siteid:siteid
        //       },
        //       success: function(result){


        // }});

    });

    // $(".pdfdownload").click(function(){
    //   $("#pdfbtntext").text("Downloading..");
    //   $(this).off("click").attr('href', "javascript: void(0);");
    // });


$("#email_send").click(function() {

  var params = $("#data_params").val();
  var res = params.split("_");
  var cid = res[0];
  //var bs_id = res[1];
  var mail = $("#to_email_id").val();
  var name = $("#to_email_name").val();

  if(mail!=""&&name!="")
  {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
    {
      $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
            });

      $.ajax({
            url: "<?php echo e(url('/company/sendmail')); ?>",
            method: 'post',
            data: {
               compId:cid,to_email:mail,name:name
            },
            success: function(result){
               $("#email_send_error").val("");
               if(result==1)
               {
                $("#form_area").hide();
                $("#email_send_success").text("Email sent successfully");
                $("#email_send").hide();
                $("#myModal_email").html("");
               }

      }});
      $("#email_send_error").text("");
    } else
    {
      $("#email_send_error").text("Please enter valid email id");
    }

} else
{
  if(mail==""&&name=="")
  {
    $("#email_send_error").text("Please enter recipient name and email id");
  } else if(mail=="")
  {
    $("#email_send_error").text("Please enter recipient email id");
  } else if(name=="")
  {
    $("#email_send_error").text("Please enter recipient name");
  }

}


});



});

function checkemailexist(cid)
{
  var mail = document.getElementById('email').value;
  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
  {

    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

       $.ajax({
          url: "<?php echo e(url('/company/editcheckemail')); ?>",
          method: 'post',
          data: {
             email: mail,cid:cid
          },
          success: function(result){

             if(result=='1'){

                $("#email_err").text('Email Id already exist');
                $("#email").val("");


             } else {
                $("#email_err").text('');
             }
          }
        });

  } else{
    //alert("You have entered an invalid email address!")
    $("#email_err").text('You have entered an invalid email address!');
    $("#email").val("");
  }
}




</script>

<?php echo $__env->make('layouts.companylist', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\smart1_enrichment\smartoneapp\resources\views/editcompany.blade.php ENDPATH**/ ?>