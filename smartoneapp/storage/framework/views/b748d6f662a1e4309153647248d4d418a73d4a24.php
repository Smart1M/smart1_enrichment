<?php $__env->startSection('content'); ?>


<p>
<?php if(session('status')): ?>
    <div class="alert alert-success">
        <?php echo e(session('status')); ?>

    </div>
<?php endif; ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
        
        <ul id="horizontal-list">
            <li>
                <a href="<?php echo e(url('customers/add')); ?>" >
                <button type="button" class="btn btn-primary" name="create" class="">Customer <i class="fa fa-plus"></i></button>
                </a> 
            </li>
        </ul> 

        <table id="example" class="table table-striped table-bordered" style="width:100%;">
        <thead>
        <tr>
            <th>Sl.No</th>
            <th>Name</th>
            <th>Company</th>
            <th>DOB</th>
            <th>Email</th>
            <th>Mobile</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>

        <?php $i=1; foreach ($customer as $key => $value) { ?>
        <tr>
            <td><?php echo e($i); ?> </td>
            <td><?php if($value->fname): ?><?php echo e($value->fname); ?><?php endif; ?></td>
            <td><?php echo e($value->cname); ?></td>
            <td><?php echo e($value->dob); ?></td>
            <td><?php echo e($value->email); ?></td>
            <td><?php echo e($value->mob); ?></td>
            <td><span id="status_<?php echo e($value->cust_id); ?>"><?php if($value->cstatus==0): ?> Active <?php else: ?> Inactive <?php endif; ?> </span></td>
            <td><a  href="<?php echo e(url('customers/edit/'.$value->cust_id)); ?>" title="Edit" style="text-decoration: none; color: inherit;"><i class="far fa-edit"></i></a>&nbsp; <a id="<?php echo e($value->cust_id); ?>" href="javascript:void(0)" title="Delete" style="text-decoration: none; color: inherit;" class="deletecustomer"><i class="far fa-trash-alt"></i></a></td>
        </tr>

        <?php $i++; } ?>

        </tbody>
        </table>
            
        </div>
    </div>
</div>
</p>



<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.companylist', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\smartoneapp\resources\views/customer.blade.php ENDPATH**/ ?>