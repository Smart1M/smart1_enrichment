<?php $__env->startSection('content'); ?>

<?php if($errors->any()): ?>
  <div class="alert alert-danger">
    <ul>
        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <li><?php echo e($error); ?></li>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ul>
  </div><br />
<?php endif; ?>
<div class="dash-main-body">
<div class="dash-main-form">
  <form action="<?php echo e(url('/permissions/savepermissions')); ?>" method="post">
     <?php echo csrf_field(); ?>
        <div class="col-lg-12 row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                    <label>
                          Roles
                    </label> 
                <input type="text" name="name" id="name" value="<?php echo e($roles->name); ?>" readonly="">
                <input type="hidden" name="roleid" id="roleid" value="<?php echo e($roles->id); ?>" readonly="">
              </div>

              <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                <br>
                    <label>
                          Select Permissions
                    </label> 
                    <?php $i=1; foreach ($permissions as $key => $value) { ?>

                      <input type="checkbox" name="asperms[]" value="<?php echo e($value->id); ?>" <?php if(!empty($assignPermissionsArr)){if(in_array($value->id, $assignPermissionsArr)){ ?> checked=""  <?php }} ?>>&nbsp;<?php echo e($value->name); ?> <br>

                    <?php $i++; } ?>
               
              </div>
        </div>
        
    <div class="col-lg-6 row">    
        <div class="col-lg-6">
              <input type="submit" value="Submit" style="float: left;">
        </div>
    </div>
  </form>
</div>
</div>

<?php $__env->stopSection(); ?>

<script type="text/javascript">

</script>

<?php echo $__env->make('layouts.list', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\smartoneapp\resources\views/assignpermissions.blade.php ENDPATH**/ ?>