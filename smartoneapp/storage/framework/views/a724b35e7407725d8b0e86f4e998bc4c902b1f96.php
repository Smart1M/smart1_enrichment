<?php $__env->startSection('content'); ?>

<?php if($errors->any()): ?>
  <div class="alert alert-danger">
    <ul>
        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <li><?php echo e($error); ?></li>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ul>
  </div><br />
<?php endif; ?>
<div class="dash-main-body">
<div class="dash-main-form">
  <form action="<?php echo e(url('company/save')); ?>" method="post" name="companyadd">
    <?php echo csrf_field(); ?>
      <div class="col-lg-12 row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Name <span style="color: red;">*</span>
                  </label>
              <input type="text" name="cname" id="cname"  autocomplete="cname">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Email <span style="color: red;">*</span>
                  </label>
              <input type="text" name="email" id="email" autocomplete="email" onchange="checkemailexist()">
              <span style="color: red; font-size: 14px;" id="email_err"></span>
            </div>
      </div>
       <div class="col-lg-12 row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Address1 <span style="color: red;">*</span>
                  </label>
                <input type="text" name="address1" id="address1"  autocomplete="address1">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Address2
                  </label>
                <input type="text" name="address2" id="address2" autocomplete="address2">
            </div>
      </div>
      <div class="col-lg-12 row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        City <span style="color: red;">*</span>
                  </label>
              <input type="text" name="city" id="city"  autocomplete="city">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        State <span style="color: red;">*</span>
                  </label>
                <!-- <input type="text" name="state" id="state" required autocomplete="state"> -->
                <select name="state" id="state"  style="width: 100%; height: 30px;">
                  <option value="">Select State</option>
                  <?php foreach($states as $key => $value) { ?>
                      <option value="<?php echo $value->state_id; ?>"><?php echo e($value->state_name); ?></option>
                  <?php } ?>
                </select>
            </div>
      </div>
      <div class="col-lg-12 row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Zip <span style="color: red;">*</span>
                  </label>
              <input type="text" name="zip" id="zip"  autocomplete="zip">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Country <span style="color: red;">*</span>
                  </label>
                <input type="text" name="country" id="country"  autocomplete="country">
            </div>
      </div>
      <div class="col-lg-12 row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Website URL <span style="color: red;">*</span>
                  </label>
              <input type="url" placeholder="https://example.com" pattern="https://.*" name="company_url" id="company_url"  autocomplete="company_url" onchange="validUrl()">
              <span id="urlError" style="color: Red; display: none; font-size: 14px;">Invalid URL</span>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Phone <span style="color: red;">*</span>
                  </label>
              <input type="text" name="phone" id="phone" autocomplete="phone" onchange="Validate()">
              <span id="spnError" style="color: Red; display: none; font-size: 14px;">Invalid phone number</span>
            </div>

      </div>


      <div class="col-lg-12 row">
      <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Media Partner <span style="color: red;">*</span>
                  </label>
              <select name="media_partner" id="media_partner"  style="width: 100%; height: 30px;">
                  <option value="">Select Media Partner</option>
                  <?php foreach($media_partner as $key => $value) { ?>
                      <option value="<?php echo $value->media_partner; ?>"><?php echo e($value->media_partner); ?></option>
                  <?php } ?>
                </select>
            </div>
          </div>

  <div class="col-lg-12 row">
      <div class="col-lg-12">
            <input type="submit" class="dash-main-form-btn" value="Submit">
            <a href="<?php echo e(url('/company')); ?>"><input type="button" class="dash-main-form-btn" value="Cancel" ></a>
      </div>
  </div>
</form>
</div>
</div>

<?php $__env->stopSection(); ?>

<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script type="text/javascript">
// $(document).ready(function() {

//     var regExp = /[a-z]/i;
//     $('#phone').on('keydown keyup', function(e) {
//       var value = String.fromCharCode(e.which) || e.key;


//       if (regExp.test(value)) {
//         e.preventDefault();
//         return false;
//       }
//     });
// });

function Validate() {
    var isValid = false;
    var regex = /^[0-9-+()' ']*$/;
    isValid = regex.test(document.getElementById("phone").value);
    document.getElementById("spnError").style.display = !isValid ? "block" : "none";

    if(isValid==false)
    {
      $("#phone").val("");
    }
    return isValid;
}

// function ValidateEmail()
// {
//   var mail = document.getElementById('email').value;
//  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
//   {
//     return true;
//   }
//     alert("You have entered an invalid email address!")
//     return false;
// }

function checkemailexist()
{
  var mail = document.getElementById('email').value;
  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
  {

    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

       $.ajax({
          url: "<?php echo e(url('/company/checkemail')); ?>",
          method: 'post',
          data: {
             email: mail
          },
          success: function(result){

             if(result=='1'){

                $("#email_err").text('Email Id already exist');
                $("#email").val("");


             } else {
                $("#email_err").text('');
             }
          }
        });

  } else{
    //alert("You have entered an invalid email address!")
    $("#email_err").text('You have entered an invalid email address!');
    $("#email").val("");
  }
}

function validUrl()
{
  var validUrl = false;
  var web_url = document.getElementById('company_url').value;
  var pattern = /^(http|https)?:\/\/[a-zA-Z0-9-\.]+\.[a-z]{2,4}/;


  validUrl = pattern.test(web_url)
  if(validUrl==false){
    $("#company_url").val("");
    document.getElementById("urlError").style.display = !validUrl ? "block" : "none";
  } else
  {
    document.getElementById("urlError").style.display = !validUrl ? "block" : "none";
  }
}
</script>

<?php echo $__env->make('layouts.companylist', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\smart1_enrichment\smartoneapp\resources\views/addcompany.blade.php ENDPATH**/ ?>