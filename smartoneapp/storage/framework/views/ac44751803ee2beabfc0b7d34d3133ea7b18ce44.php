<?php $__env->startSection('content'); ?>

<p>
    <?php if(session('status')): ?>
    <div class="alert alert-success">
        <?php echo e(session('status')); ?>

    </div>
    <?php endif; ?>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12" style="width:100%;">

                <ul id="horizontal-list">
                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('add company')): ?>
                    <li>
                        <a href="<?php echo e(url('company/add')); ?>">
                            <button type="button" class="btn btn-primary" name="create" class="">Company <i
                                    class="fa fa-plus"></i></button>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo e(url('googlemapdata')); ?>">
                            <button type="button" class="btn btn-primary" name="create" class="">Upload Sheet <i
                                    class="fa fa-upload"></i></button>
                        </a>
                    </li>
                    <?php endif; ?>
                    <!-- <li>
                <a href="<?php echo e(url('customers')); ?>" >
                <button type="button" class="btn btn-primary" name="create" class="">Customer <i class="fa fa-user-plus"></i></button>
                </a>
            </li> -->
                </ul>

                <table id="example" class="table table-striped table-bordered" style="width:100%;">
                    <thead>
                        <tr>
                            <th>Sl.No</th>
                            <th>Partner</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>Status</th>
                            <th>Actions</th>
                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('play game')): ?><th>Play Game</th><?php endif; ?>
                        </tr>
                    </thead>
                    <tbody>

                        <?php $i=1; foreach ($company as $key => $value) { $value->email = str_replace(',', ', ', $value->email);?>
                        <tr>
                            <td><?php echo e($i); ?></td>
                            <td><?php echo e($value->media_partner); ?></td>
                            <td><?php echo e($value->cname); ?></td>
                            <td><?php echo e($value->email); ?></td>
                            <td><?php echo e($value->phone); ?></td>
                            <td><span id="status_<?php echo e($value->company_id); ?>"><?php if($value->status==0): ?> Active <?php else: ?> Inactive
                                    <?php endif; ?> </span></td>
                            <td><?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit company')): ?><a href="<?php echo e(url('company/edit/'.$value->company_id)); ?>" title="Edit"
                                    style="text-decoration: none; color: inherit;"><i
                                        class="far fa-edit"></i></a><?php endif; ?> <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('delete company')): ?>&nbsp; <a
                                    id="<?php echo e($value->company_id); ?>" href="javascript:void(0)" title="Delete"
                                    style="text-decoration: none; color: inherit;" class="deletecompany"><i
                                        class="far fa-trash-alt"></i></a><?php endif; ?></td>
                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('play game')): ?><td><a class="btn btn-primary"
                                    href="<?php echo e(url('company/playgame/'.$value->company_id)); ?>"
                                    style="width: 150px; text-decoration: none;"><i class="fas fa-gamepad"></i> Play a
                                    game</a></td><?php endif; ?>
                        </tr>

                        <?php $i++; } ?>

                    </tbody>
                </table>

            </div>
        </div>
    </div>
</p>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.companylist', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\smart1_enrichment\smartoneapp\resources\views/company.blade.php ENDPATH**/ ?>