<?php $__env->startSection('content'); ?>

<?php if($errors->any()): ?>
  <div class="alert alert-danger">
    <ul>
        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <li><?php echo e($error); ?></li>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ul>
  </div><br />
<?php endif; ?>

<div class="loading">Loading&#8230;</div>

<div class="dash-main-body">
<div class="dash-main-form">
  <form>
     <?php echo csrf_field(); ?>
        <div class="col-lg-12 row">
              <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                    <label>
                          Enter URL <span style="color: red;">*</span>
                    </label> 
                <input type="text" name="web_url" id="web_url" required="">
              </div>
        </div>
        
    <div class="col-lg-6 row">    
        <div class="col-lg-6">
              <input type="button" value="Submit" class="saveUrl" id="saveUrl" style="float: left;">
        </div>
    </div>
  </form>
</div>
</div>

<?php $__env->stopSection(); ?>

<script type="text/javascript">

</script>

<?php echo $__env->make('layouts.brandfetch', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\smartoneapp\resources\views/brandfetch.blade.php ENDPATH**/ ?>