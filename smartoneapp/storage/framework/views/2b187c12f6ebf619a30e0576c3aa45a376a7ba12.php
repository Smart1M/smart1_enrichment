<?php $__env->startSection('content'); ?>


<p>
<?php if(session('status')): ?>
    <div class="alert alert-success">
        <?php echo e(session('status')); ?>

    </div>
<?php endif; ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
        
        <ul id="horizontal-list">
            <li>
                <a href="<?php echo e(url('permissions/add')); ?>" >
                <button type="button" class="btn btn-primary" name="create" class="">Permisions <i class="fa fa-plus"></i></button>
                </a> 
            </li>
            <li>
                <a href="<?php echo e(url('roles')); ?>">
                <button type="button" name="create" class="btn btn-primary">Roles <i class="fa fa-file"></i></button>
                </a> 
            </li>
        </ul>  
        

        <table id="example" class="table table-striped table-bordered" style="width:100%;">
        <thead>
        <tr>
            <th>Sl.No</th>
            <th>Permission</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>

        <?php $i=1; foreach ($permissions as $key => $value) { ?>
        <tr>
            <td><?php echo e($i); ?></td>
            <td><?php echo e($value->name); ?></td>
            <td><span id="status_<?php echo e($value->id); ?>"><?php if($value->pstatus==0): ?> Active <?php else: ?> Inactive <?php endif; ?> </span></td>
            <td><a  href="<?php echo e(url('permissions/edit/'.$value->id)); ?>" title="Edit" style="text-decoration: none; color: inherit;"><i class="far fa-edit"></i></a>&nbsp; <a id="<?php echo e($value->id); ?>"" href="javascript:void(0)" title="Delete" style="text-decoration: none; color: inherit;" class="deletepermissions"><i class="far fa-trash-alt"></i></a></td>
        </tr>

        <?php $i++; } ?>

        </tbody>
        </table>
            
        </div>
    </div>
</div>
</p>

<?php $__env->stopSection(); ?>

<script type="text/javascript">

</script>

<?php echo $__env->make('layouts.permission', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\smartoneapp\resources\views/permissions.blade.php ENDPATH**/ ?>