<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo e(config('app.name', 'Laravel')); ?></title>

    <!-- Scripts -->
    <!-- <script src="<?php echo e(asset('js/app.js')); ?>" defer></script> -->
    <link rel="stylesheet" href="<?php echo e(asset('css/css/style.css')); ?>">
    <script src="https://kit.fontawesome.com/ef363a0a6e.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Datatables --->
    <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/> -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link href="<?php echo e(asset('css/css/sweetalert2.min.css')); ?>" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="<?php echo e(asset('css/css/fancybox/jquery.fancybox.css?v=2.1.7')); ?>" rel="stylesheet">


    <!-- Fonts -->
    <!-- <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"> -->

    <!-- Styles -->
    <!-- <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet"> -->
    <style type="text/css">
      ul#horizontal-list {
        min-width: 696px;
        list-style: none;
        padding-top: 20px;
        }
        ul#horizontal-list li {
          display: inline;
        }

        /* Table CSS */

        table {
          border-collapse: collapse;
          width: 100%;
        }

        table, th, td {
          border: 1px solid black;
        }
        th {
          /*text-align: center;*/
          height: 50px;
        }
        td {
          /*text-align: center;*/
          height: 25px;
          width: 25%;
        }

        /* Absolute Center Spinner */
      .loading {
        position: fixed;
        z-index: 999;
        height: 2em;
        width: 2em;
        overflow: visible;
        margin: auto;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
      }

      /* Transparent Overlay */
      .loading:before {
        content: '';
        display: block;
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0,0,0,0.3);
      }

      /* :not(:required) hides these rules from IE9 and below */
      .loading:not(:required) {
        /* hide "loading..." text */
        font: 0/0 a;
        color: transparent;
        text-shadow: none;
        background-color: transparent;
        border: 0;
      }

      .loading:not(:required):after {
        content: '';
        display: block;
        font-size: 10px;
        width: 1em;
        height: 1em;
        margin-top: -0.5em;
        -webkit-animation: spinner 1500ms infinite linear;
        -moz-animation: spinner 1500ms infinite linear;
        -ms-animation: spinner 1500ms infinite linear;
        -o-animation: spinner 1500ms infinite linear;
        animation: spinner 1500ms infinite linear;
        border-radius: 0.5em;
        -webkit-box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.5) -1.5em 0 0 0, rgba(0, 0, 0, 0.5) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
        box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) -1.5em 0 0 0, rgba(0, 0, 0, 0.75) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
      }

      /* Animation */

      @-webkit-keyframes spinner {
        0% {
          -webkit-transform: rotate(0deg);
          -moz-transform: rotate(0deg);
          -ms-transform: rotate(0deg);
          -o-transform: rotate(0deg);
          transform: rotate(0deg);
        }
        100% {
          -webkit-transform: rotate(360deg);
          -moz-transform: rotate(360deg);
          -ms-transform: rotate(360deg);
          -o-transform: rotate(360deg);
          transform: rotate(360deg);
        }
      }
      @-moz-keyframes spinner {
        0% {
          -webkit-transform: rotate(0deg);
          -moz-transform: rotate(0deg);
          -ms-transform: rotate(0deg);
          -o-transform: rotate(0deg);
          transform: rotate(0deg);
        }
        100% {
          -webkit-transform: rotate(360deg);
          -moz-transform: rotate(360deg);
          -ms-transform: rotate(360deg);
          -o-transform: rotate(360deg);
          transform: rotate(360deg);
        }
      }
      @-o-keyframes spinner {
        0% {
          -webkit-transform: rotate(0deg);
          -moz-transform: rotate(0deg);
          -ms-transform: rotate(0deg);
          -o-transform: rotate(0deg);
          transform: rotate(0deg);
        }
        100% {
          -webkit-transform: rotate(360deg);
          -moz-transform: rotate(360deg);
          -ms-transform: rotate(360deg);
          -o-transform: rotate(360deg);
          transform: rotate(360deg);
        }
      }
      @keyframes  spinner {
        0% {
          -webkit-transform: rotate(0deg);
          -moz-transform: rotate(0deg);
          -ms-transform: rotate(0deg);
          -o-transform: rotate(0deg);
          transform: rotate(0deg);
        }
        100% {
          -webkit-transform: rotate(360deg);
          -moz-transform: rotate(360deg);
          -ms-transform: rotate(360deg);
          -o-transform: rotate(360deg);
          transform: rotate(360deg);
        }
      }


      div.gallery {
        margin: 5px;
        border: 1px solid #ccc;
        float: left;
        width: 150px;
        /*height: 200px;*/
      }

      div.gallery:hover {
        border: 1px solid #777;
      }

      div.gallery img {
        width: 100px;
        height: 100px;
        margin: 0 0 0 25px;
      }



      div.single {
        margin: auto;
        width: 150px;
        /*height: 200px;*/
      }

      div.single:hover {
        border: 1px solid #777;
      }

      div.single img {
        width: 100px;
        height: 100px;
        margin: 0 0 0 0;
      }

      div.desc {
        padding: 15px;
        text-align: center;
      }

      .dot {
    height: 15px;
    width: 15px;

    border-radius: 50%;
    display: inline-block;
  }
  form .error {
        color: #ff0000;
        font-size: 14px;
      }


    </style>
</head>
<body>
    <div class="main-wrapper">


            <?php echo $__env->make('includes.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <div class="col-lg-10 col-md-8 col-sm-12 col-12 dash-main">
                  <!-- <div class="dash-main-nav">
                        <span class="openbtn" " onclick="openNav()">&#9776;</span>
                        <div class="dash-main-nav-search">
                              <i class="fas fa-search"></i><input type="text" placeholder="Search...">
                        </div>
                        <div class="dash-main-nav-settings">
                              <a href="#"><i class="fas fa-cog"></i></a>
                        </div>
                  </div> -->

                    <?php echo $__env->yieldContent('content'); ?>

            </div>
      </div>
</body>
</html>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script> -->

<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?php echo e(asset('js/sweetalert2.all.min.js')); ?>"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="<?php echo e(asset('js/fancybox/jquery.mousewheel.pack.js')); ?>"></script>
<script src="<?php echo e(asset('js/fancybox/jquery.fancybox.pack.js?v=2.1.7')); ?>"></script>
<!-- <script type="text/javascript" src="<?php echo e(asset('js/lazyload/jquery.lazy.min.js')); ?>"></script> -->
<script src="https://media-library.cloudinary.com/global/all.js"></script>
<script src="https://widget.cloudinary.com/v2.0/global/all.js" type="text/javascript">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/es6-promise/4.1.1/es6-promise.auto.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/clipboard@1/dist/clipboard.min.js"></script>


<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/es6-promise/4.1.1/es6-promise.auto.min.js"></script> -->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
<!-- <script src="https://product-gallery.cloudinary.com/all.js" type="text/javascript">
</script> -->

<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable();

    $('form[name="companyadd"]').validate({
      rules: {
        cname: 'required',
        email: 'required',
        address1: 'required',
        city: 'required',
        state: 'required',
        zip: 'required',
        country: 'required',
        company_url: {
          required: true,
          url: true
        },
        phone: 'required',
        media_partner: 'required'
      },
      messages: {
        name: 'Please add a name',
        email: 'Please enter email id',
        address1: 'Please enter address',
        city: 'Please enter city',
        state: 'Please enter state',
        zip: 'Please enter zip',
        country: 'Please enter country',
        company_url: 'Please enter company website',
        phone: 'Please enter phone',
        media_partner: 'Please select Media Partner'
      },
      submitHandler: function(form) {
        form.submit();
      }
    });

    $('form[name="companyedit"]').validate({
      rules: {
        cname: 'required',
        email: 'required',
        address1: 'required',
        city: 'required',
        state: 'required',
        zip: 'required',
        country: 'required',
        company_url: {
          required: true,
          url: true
        },
        phone: 'required',
        media_partner: 'required'
      },
      messages: {
        name: 'Please add a name',
        email: 'Please enter email id',
        address1: 'Please enter address',
        city: 'Please enter city',
        state: 'Please enter state',
        zip: 'Please enter zip',
        country: 'Please enter country',
        company_url: 'Please enter a valid URL',
        phone: 'Please enter phone',
        media_partner: 'Please select Media Partner'
      },
      invalidHandler: function(event, validator) {
      // 'this' refers to the form
      var errors = validator.numberOfInvalids();
      if (errors) {
        var message = errors == 1
          ? 'You missed 1 field. It has been highlighted'
          : 'You missed ' + errors + ' fields. They have been highlighted';
        $("div.error span").html(message);
        $("#urlError").hide();
      }
    },
      submitHandler: function(form) {
        form.submit();
      }
    });

    var company_id = $("#company_id").val();
    var company_name = $("#cname").val();
    // var publicId = 'widget_upload_'+makeid(5);


    var myWidget = cloudinary.createUploadWidget({
    cloudName: 'smart1snap',
    uploadPreset: 'monteply',folder: 'Joshua/'+company_name+'_Joshua'},
     (error, result) => {
      if (!error && result && result.event === "success") {
        console.log('Done! Here is the image info: ', result.info);
      }
    }
    )

    document.getElementById("upload_widget").addEventListener("click", function(){
      myWidget.open();
    }, false);


});

function makeid(length) {
   var result           = '';
   var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
   var charactersLength = characters.length;
   for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return result;
}


$(document).ready(function(){

    var clipboard = new Clipboard('.copy');
    clipboard.on('success', function(e) {
        console.log('Copied text: ' + e.text);
        Swal.fire({title: "Copied", text: "Paste this snippet in your page.", type: "success"});
    });
    clipboard.on('error', function(e) {
        console.log(e);
    });
    function copyText() {

    }



  $('#upload_widget_code').on('click',function(){
    var cl_cid = $("#company_id").val();
    var cl_name = $("#company_name").val();
    var email = $("#email").val();
    var company_url = $("#company_url").val();
    var phone = $("#phone").val();
    var widgetflag = 1;

    $('#upload_widget_code').html("Generating Code");
    $('#upload_widget_code').prop("disabled", true);
    $("#uploadCode").text("");


      $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });

     $.ajax({
        url: "<?php echo e(url('/cloudinarywidget/save')); ?>",
        method: 'post',
        data: {
           cl_cid: cl_cid,cl_name: cl_name,email:email,company_url:company_url,phone:phone,widgetflag:widgetflag
        },
        success: function(data){


              var APP_URL = <?php echo json_encode(url('/')); ?>;
              $("#uploadCode").text("<iframe id='myFrame' src="+APP_URL+'/'+data+" max-width=100% width=100% height=650px frameborder='0'></iframe>");
              $("#copyUploadCode").css("display","block");

              $('#upload_widget_code').prop("disabled", false);
              $('#upload_widget_code').html("Upload Widget Code");
        },
        error : function(data)
        {

          Swal.fire(
            'Oops!',
            'Something went wrong!',
            'error'
          )
        }
      });
  });


  $('#media_widget_code').on('click',function(){
    var cl_cid = $("#company_id").val();
    var cl_name = $("#company_name").val();
    var email = $("#email").val();
    var company_url = $("#company_url").val();
    var phone = $("#phone").val();
    var widgetflag = 2;

    $('#media_widget_code').html("Generating Code");
    $('#media_widget_code').prop("disabled", true);
    $("#uploadCode").text("");


      $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });

     $.ajax({
        url: "<?php echo e(url('/cloudinarywidget/save')); ?>",
        method: 'post',
        data: {
           cl_cid: cl_cid,cl_name: cl_name,email:email,company_url:company_url,phone:phone,widgetflag:widgetflag
        },
        success: function(data){


              var APP_URL = <?php echo json_encode(url('/')); ?>;
              $("#uploadCode").text("<iframe id='myFrame' src="+APP_URL+'/'+data+" max-width=100% width=100% height=650px frameborder='0'></iframe>");
              $("#copyUploadCode").css("display","block");

              $('#media_widget_code').prop("disabled", false);
              $('#media_widget_code').html("Media Gallery Code");
        },
        error : function(data)
        {

          Swal.fire(
            'Oops!',
            'Something went wrong!',
            'error'
          )
        }
      });
  });




});

$(document).ready(function(){

    $('.closeupload').on('click',function()
    {
      $("#uploadCode").text("");
      $("#copyUploadCode").css("display","none");
    });

    $('#select_all').on('click',function(){
        if(this.checked){
            $('.checkbox').each(function(){
                this.checked = true;
            });
        }else{
             $('.checkbox').each(function(){
                this.checked = false;
            });
        }
    });

    $('.checkbox').on('click',function(){
        if($('.checkbox:checked').length == $('.checkbox').length){
            $('#select_all').prop('checked',true);
        }else{
            $('#select_all').prop('checked',false);
        }
    });

    if($('.checkbox:checked').length == $('.checkbox').length){
            $('#select_all').prop('checked',true);
        }else{
            $('#select_all').prop('checked',false);
        }
});



(function($) {
  $.fn.inputFilter = function(inputFilter) {
    return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      }
    });
  };
}(jQuery));

$("#country").inputFilter(function(value) {
  return /^[a-z]*$/i.test(value); });

$("#city").inputFilter(function(value) {
  return /^[a-z]*$/i.test(value); });


$("#single_1").fancybox({
    openEffect  : 'elastic',
    closeEffect : 'elastic',

    helpers : {
      title : {
        type : 'inside'
      }
    }
  });




$(document).ready(function() {
  $(".fancybox").fancybox({
    openEffect  : 'elastic',
    closeEffect : 'elastic'
  });


});

$(document).ready(function() {
    $(".loading").hide();
    //document.getElementById("defaultOpen").click();
});




$(document).ready(function(){

var table = $('#example').DataTable();
$('#example tbody').on( 'click', 'a.deletecompany', function () {


 Swal.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.value) {

    // table
    //   .row( $(this).parents('tr') )
    //   .remove()
    //   .draw();

      $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

       var cid = $(this).attr("id");
       $.ajax({
          url: "<?php echo e(url('/company/delete')); ?>",
          method: 'post',
          data: {
             companyid: cid
          },
          success: function(result){
             if(result==1){

              $("#status_"+cid).text('Inactive');

              var APP_URL = <?php echo json_encode(url('/')); ?>;
              Swal.fire({title: "Deleted", text: "Company data has been deleted.", type: "success"}).then(

                function(){
                     window.location.href = APP_URL+'/company';
                 }
              );
             }
          },
          error : function(result)
          {

            Swal.fire(
              'Oops!',
              'Something went wrong!',
              'error'
            )
          }
        });



    // Swal.fire(
    //   'Deleted!',
    //   'Your file has been deleted.',
    //   'success'
    // )

  }





})


});


/* API Calls */

$( "#proceedapi" ).click(function() {

     var checklist_weblogo = $('#checklist_weblogo').prop('checked') ? $('#checklist_weblogo').val() : '';
     var checklist_websitecolors =  $('#checklist_websitecolors').prop('checked') ? $('#checklist_websitecolors').val() : '';
     var checklist_colorsfromlogo = $('#checklist_colorsfromlogo').prop('checked') ? $('#checklist_colorsfromlogo').val() : '';
     var checklist_websitefont = $('#checklist_websitefont').prop('checked') ? $('#checklist_websitefont').val() : '';
     var checklist_websitescreenshot = $('#checklist_websitescreenshot').prop('checked') ? $('#checklist_websitescreenshot').val() : '';
     var checklist_companysocialmedia = $('#checklist_companysocialmedia').prop('checked') ? $('#checklist_companysocialmedia').val() : '';
     var checklist_websiteimages = $('#checklist_websiteimages').prop('checked') ? $('#checklist_websiteimages').val() : '';
     var checklist_websitemetadata = $('#checklist_websitemetadata').prop('checked') ? $('#checklist_websitemetadata').val() : '';
     var checklist_dispadvertisingreports = $('#checklist_dispadvertisingreports').prop('checked') ? $('#checklist_dispadvertisingreports').val() : '';
     var checklist_marketingoverview = $('#checklist_marketingoverview').prop('checked') ? $('#checklist_marketingoverview').val() : '';
     var checklist_advertisers = $('#checklist_advertisers').prop('checked') ? $('#checklist_advertisers').val() : '';
     var checklist_publisher_rank = $('#checklist_publisher_rank').prop('checked') ? $('#checklist_publisher_rank').val() : '';
     var checklist_localdata = $('#checklist_localdata').prop('checked') ? $('#checklist_localdata').val() : '';
     var checklist_sitecontent = $('#checklist_sitecontent').prop('checked') ? $('#checklist_sitecontent').val() : '';

     if ($('input:checkbox').filter(':checked').length < 1)
     {
      Swal.fire(
            'Oops!',
            'Please choose any of the option to proceed !',
            'error'
          )
      return false;
     }


     var company_id = $("#company_id").val();
     var web_url = $("#company_url").val();
     if(web_url!=""&&company_id!=""){


      var pattern = /^(http|https)?:\/\/[a-zA-Z0-9-\.]+\.[a-z]{2,4}/;

      if(pattern.test(web_url)){


          $("#proceedapi").attr("disabled", true);
          $(".loading").show();
          $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
          });



          $.ajax({
          url: "<?php echo e(url('/company/fetchapi')); ?>",
          method: 'post',
          data: {
          web_url: web_url,company_id:company_id,checklist_weblogo:checklist_weblogo,checklist_websitecolors:checklist_websitecolors,checklist_colorsfromlogo:checklist_colorsfromlogo,checklist_websitefont:checklist_websitefont,checklist_websitescreenshot:checklist_websitescreenshot,checklist_companysocialmedia:checklist_companysocialmedia,checklist_websiteimages:checklist_websiteimages,checklist_websitemetadata:checklist_websitemetadata,checklist_dispadvertisingreports:checklist_dispadvertisingreports,checklist_marketingoverview:checklist_marketingoverview,checklist_advertisers:checklist_advertisers,checklist_publisher_rank:checklist_publisher_rank,checklist_localdata:checklist_localdata,checklist_sitecontent:checklist_sitecontent
          },
          success: function(result){

          $(".loading").hide();
          //$("#web_url").val("");
          $("#proceedapi").attr("disabled", false);

          var APP_URL = <?php echo json_encode(url('/')); ?>;

          if(result==1){


          Swal.fire({title: "Good job", text: "Data has been saved!", type: "success"}).then(

          function(){
               window.location.href = APP_URL+'/company/edit/'+company_id;
           }
          );

          } else if(result.message){

            Swal.fire(result.message,'','warning').then(

            function(){
                 window.location.href = APP_URL+'/company/edit/'+company_id;
             }
            );

          } else {

          Swal.fire({title: "Oops", text: "Something went wrong!!", type: "error"}).then(

          function(){
               window.location.href = APP_URL+'/company/edit/'+company_id;
           }
          );



          }
          },
          error : function(result)
          {
            $(".loading").hide();
            $("#proceedapi").attr("disabled", false);

            Swal.fire({title: "Oops", text: "Something went wrong!", type: "error"}).then(

            function(){
                 window.location.href = APP_URL+'/company/edit/'+company_id;
             }
            );
          }

        });


      } else {
          Swal.fire(
            'Invalid URL',
            'Please Enter Valid URL!',
            'error'
          )
          return false;
      }



}
else {


      Swal.fire(
        'Field Missing!',
        'Both fields are compulsory!',
        'error'
      )

}
});


});


$( function() {
    $( "#accordion" ).accordion({
      collapsible: true, active: false, heightStyle: 'content'
    });
  });


//$( "#accordion" ).accordion();


function openTab(evt, tabName) {
  // Declare all variables
  var i, tabcontent, tablinks;

  // Get all elements with class="tabcontent" and hide them
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }

  // Get all elements with class="tablinks" and remove the class "active"
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }

  // Show the current tab, and add an "active" class to the button that opened the tab
  document.getElementById(tabName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();

/* Cloudinary media gallery widget */

if(company_id!=undefined)
    {
      $("#open-btn").click(function()
      {

        var cname = $("#cname").val();
        var unixtime = $("#unix_time").val();
        // var auth = sha256('cloud_name=smart1snap&timestamp='+unixtime+'&username=toddswickard@smart1marketing.comTjnx2UPBY6c4qWgxOEIpWTOhNJk');
        var auth = sha256('cloud_name=smart1snap&timestamp='+unixtime+'&username=toddswickard@smart1marketing.comTjnx2UPBY6c4qWgxOEIpWTOhNJk');

        //console.log(auth);
        window.ml = cloudinary.createMediaLibrary({
         cloud_name: 'smart1snap',
         api_key: '872488644391297',
         username: 'toddswickard@smart1marketing.com',
         //inline_container: '.cms-container',
         timestamp: unixtime,
         signature:auth,
         z_index: 99999,
         button_class: 'myBtn btn btn-primary',
         button_caption: 'Media Library Widget',
         insert_transformation: true,
        }, {
         insertHandler: function (data) {
           console.log("Hello World");
           data.assets.forEach(asset => { console.log("Inserted asset:",
              JSON.stringify(asset, null, 2)) })
           }
        },
        document.getElementById("open-btn")
        )
        window.ml.show({folder: {path: "Joshua/"+cname+"_Joshua"}});

      });

    }






function sha256(ascii) {
  function rightRotate(value, amount) {
    return (value>>>amount) | (value<<(32 - amount));
  };

  var mathPow = Math.pow;
  var maxWord = mathPow(2, 32);
  var lengthProperty = 'length'
  var i, j; // Used as a counter across the whole file
  var result = ''

  var words = [];
  var asciiBitLength = ascii[lengthProperty]*8;

  //* caching results is optional - remove/add slash from front of this line to toggle
  // Initial hash value: first 32 bits of the fractional parts of the square roots of the first 8 primes
  // (we actually calculate the first 64, but extra values are just ignored)
  var hash = sha256.h = sha256.h || [];
  // Round constants: first 32 bits of the fractional parts of the cube roots of the first 64 primes
  var k = sha256.k = sha256.k || [];
  var primeCounter = k[lengthProperty];
  /*/
  var hash = [], k = [];
  var primeCounter = 0;
  //*/

  var isComposite = {};
  for (var candidate = 2; primeCounter < 64; candidate++) {
    if (!isComposite[candidate]) {
      for (i = 0; i < 313; i += candidate) {
        isComposite[i] = candidate;
      }
      hash[primeCounter] = (mathPow(candidate, .5)*maxWord)|0;
      k[primeCounter++] = (mathPow(candidate, 1/3)*maxWord)|0;
    }
  }

  ascii += '\x80' // Append Ƈ' bit (plus zero padding)
  while (ascii[lengthProperty]%64 - 56) ascii += '\x00' // More zero padding
  for (i = 0; i < ascii[lengthProperty]; i++) {
    j = ascii.charCodeAt(i);
    if (j>>8) return; // ASCII check: only accept characters in range 0-255
    words[i>>2] |= j << ((3 - i)%4)*8;
  }
  words[words[lengthProperty]] = ((asciiBitLength/maxWord)|0);
  words[words[lengthProperty]] = (asciiBitLength)

  // process each chunk
  for (j = 0; j < words[lengthProperty];) {
    var w = words.slice(j, j += 16); // The message is expanded into 64 words as part of the iteration
    var oldHash = hash;
    // This is now the undefinedworking hash", often labelled as variables a...g
    // (we have to truncate as well, otherwise extra entries at the end accumulate
    hash = hash.slice(0, 8);

    for (i = 0; i < 64; i++) {
      var i2 = i + j;
      // Expand the message into 64 words
      // Used below if
      var w15 = w[i - 15], w2 = w[i - 2];

      // Iterate
      var a = hash[0], e = hash[4];
      var temp1 = hash[7]
        + (rightRotate(e, 6) ^ rightRotate(e, 11) ^ rightRotate(e, 25)) // S1
        + ((e&hash[5])^((~e)&hash[6])) // ch
        + k[i]
        // Expand the message schedule if needed
        + (w[i] = (i < 16) ? w[i] : (
            w[i - 16]
            + (rightRotate(w15, 7) ^ rightRotate(w15, 18) ^ (w15>>>3)) // s0
            + w[i - 7]
            + (rightRotate(w2, 17) ^ rightRotate(w2, 19) ^ (w2>>>10)) // s1
          )|0
        );
      // This is only used once, so *could* be moved below, but it only saves 4 bytes and makes things unreadble
      var temp2 = (rightRotate(a, 2) ^ rightRotate(a, 13) ^ rightRotate(a, 22)) // S0
        + ((a&hash[1])^(a&hash[2])^(hash[1]&hash[2])); // maj

      hash = [(temp1 + temp2)|0].concat(hash); // We don't bother trimming off the extra ones, they're harmless as long as we're truncating when we do the slice()
      hash[4] = (hash[4] + temp1)|0;
    }

    for (i = 0; i < 8; i++) {
      hash[i] = (hash[i] + oldHash[i])|0;
    }
  }

  for (i = 0; i < 8; i++) {
    for (j = 3; j + 1; j--) {
      var b = (hash[i]>>(j*8))&255;
      result += ((b < 16) ? 0 : '') + b.toString(16);
    }
  }
  return result;
}

</script>
<?php echo $__env->make('includes.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /**PATH C:\xampp\htdocs\smart1_enrichment\smartoneapp\resources\views/layouts/companylist.blade.php ENDPATH**/ ?>