<?php $__env->startSection('content'); ?>

<?php if($errors->any()): ?>
  <div class="alert alert-danger">
    <ul>
        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <li><?php echo e($error); ?></li>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ul>
  </div><br />
<?php endif; ?>

<div class="loading">Loading&#8230;</div>

<div class="dash-main-body">
<div class="dash-main-form">
  <form>
     <?php echo csrf_field(); ?>

     <?php // print_r($company); ?>
        <div class="col-lg-12 row">
              <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                    <label>
                          Select Company <span style="color: red;">*</span>
                    </label> 
                
                <select name="company_id" id="company_id" class="js-example-basic-single" style="width: 100%;" required="">
                  <option>---Select Company---</option>
                <?php $i=1; foreach ($company as $key => $cvalue) { ?>
  <option value="<?php echo e($cvalue->company_id); ?>"><?php echo e($cvalue->cname); ?></option>

   <?php $i++; } ?>
  
</select>
                
              </div>

               <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                    <label>
                          Enter URL <span style="color: red;">*</span>
                    </label> 
                <input type="text" name="web_url" id="web_url" required="">
                <span style="font-size: 14px; color: #898383;">Valid URLs : http://domain-name.com or http://www.domain-name.com</span>
              </div>
        </div>
        
    <div class="col-lg-6 row">    
        <div class="col-lg-6">
              <input type="button" value="Submit" class="saveUrl" id="saveUrl" style="float: left;">
        </div>
    </div>
  </form>
</div>
</div>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <table id="example" class="table table-striped table-bordered" style="width:100%;">
                <thead>
                <tr>
                    <th>Sl.No</th>
                    <th>Website Url</th>
                    <th>Pdf Download</th>
                    
                </tr>
                </thead>
                <tbody>
              
                <?php $i=1; foreach ($brandfetchinfo as $key => $value) { ?>
                <tr>
                    <td><?php echo e($i); ?></td>
                    <td><?php echo e($value->web_url); ?></td>
                    <td><a href="<?php echo e(url('brandfetch/export/'.$value->br_id)); ?>" class="" id="" title="Export" style="text-decoration: none; color: inherit;"><i class="fa fa-file-pdf-o"></i></a></td>
                    
                </tr>
              
                <?php $i++; } ?>
              
                </tbody>
                </table>

        </div>
    </div>
</div>


<?php $__env->stopSection(); ?>

<script type="text/javascript">

</script>

<?php echo $__env->make('layouts.brandfetch', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\joshua\smartoneapp\resources\views/brandfetch.blade.php ENDPATH**/ ?>