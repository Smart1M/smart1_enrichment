<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo e(config('app.name', 'Laravel')); ?></title>

    <!-- Scripts -->
    <!-- <script src="<?php echo e(asset('js/app.js')); ?>" defer></script> -->
    <link rel="stylesheet" href="<?php echo e(asset('css/css/style.css')); ?>">
    <script src="https://kit.fontawesome.com/ef363a0a6e.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

    <style type="text/css">
      form .error {
        color: #ff0000;
        font-size: 14px;
      }
      .emsg{
          color: red;
          font-size: 14px;
      }
      .hidden {
           visibility:hidden;
      }
      ul#horizontal-list {
        min-width: 696px;
        list-style: none;
        padding-top: 20px;
        }
        ul#horizontal-list li {
          display: inline;
        }
    </style>
</head>
<body>
    <div class="main-wrapper">
                        

            <?php echo $__env->make('includes.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <div class="col-lg-10 col-md-8 col-sm-12 col-12 dash-main">
                  <!-- <div class="dash-main-nav">
                        <span class="openbtn"  onclick="openNav()">&#9776;</span>
                        <div class="dash-main-nav-search">
                              <i class="fas fa-search"></i><input type="text" placeholder="Search...">
                        </div>
                        <div class="dash-main-nav-settings">
                              <a href="#"><i class="fas fa-cog"></i></a>
                        </div>
                  </div> -->
                  
                    <?php echo $__env->yieldContent('content'); ?>
                  
            </div>
      </div>
</body>
</html>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="<?php echo e(asset('js/sweetalert2.all.min.js')); ?>"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
<script src="https://media-library.cloudinary.com/global/all.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/es6-promise/4.1.1/es6-promise.auto.min.js"></script>
<script src="<?php echo e(asset('js/cloudwidget.js')); ?>"></script>
<script type="text/javascript">

  $(document).ready(function(){

     $('#example').DataTable();
  });

function checkwebsiteurl()
{
  var web_url = $("#company_url").val();
  

    var expression =  
/(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi; 
    var regex = new RegExp(expression); 
    if (web_url.match(regex)) 
    { 
        $("#url_err").text("");
    } else { 
        $("#url_err").text("Invalid URL");
        $("#company_url").val("");
    } 
  
}


function Validate() {
    var isValid = false;
    var regex = /^[0-9-+()' ']*$/;
    isValid = regex.test(document.getElementById("phone").value);
    document.getElementById("spnError").style.display = !isValid ? "block" : "none";
    
    if(isValid==false)
    {
      $("#phone").val("");
    } 
    return isValid;
}

function checkemailexist()
{
  var mail = document.getElementById('email').value;
  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
  {
    $("#email_err").text('');
  } else{
    //alert("You have entered an invalid email address!")
    $("#email_err").text('You have entered an invalid email address!');
    $("#email").val("");
  }
}

function showCode(cloudid)
{
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });


  $.ajax({
      url: "<?php echo e(url('/cloudinarywidget/fetch')); ?>",
      method: 'post',
      data: {
         cloud_id:cloudid
      },
      success: function(data){

            var APP_URL = <?php echo json_encode(url('/')); ?>;

            var codeHtml = "<iframe id='myFrame' src="+APP_URL+'/'+data+" max-width=100% width=100% height=650px frameborder='0'></iframe>";
            Swal.fire({title: "Copy this code", text: codeHtml});
      },
      error : function(data)
      {
        
        Swal.fire(
          'Oops!',
          'Something went wrong!',
          'error'
        )
      }
    });


  
}


$(document).ready(function(){


$("#cloudwidget").validate({
    
    rules: {
      cl_name: "required",
      phone: "required",
      email: "required",
      company_url: "required"
    },
    
    messages: {
      cl_name: "Please enter your name",
      phone: "Please enter your phone",
      email: "Please enter a valid email address",
      company_url: "Please enter website URL"
    }
  });


$('.cloudwid_btn').on('click', function() {
  var valid = $("#cloudwidget").valid();

  

  if(valid==true)
  {
    $("#buttonarea").hide();
    $("#listbutton").hide();
    $("#progresscloud").css('display','block');
    var cl_name = $("#cl_name").val();
    var email = $("#email").val();
    var company_url = $("#company_url").val();
    var phone = $("#phone").val();
    var widgetflag = 1;

    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

       $.ajax({
          url: "<?php echo e(url('/cloudinarywidget/save')); ?>",
          method: 'post',
          data: {
             cl_name: cl_name,email:email,company_url:company_url,phone:phone,widgetflag:widgetflag
          },
          success: function(data){
             
                Swal.fire({title: "Good job", text: "Cloud widget created", type: "success"});

                var APP_URL = <?php echo json_encode(url('/')); ?>;

                $("#cl_name").val('');
                $("#email").val('');
                $("#company_url").val('');
                $("#phone").val('');

                $("#buttonarea").show();
                $("#listbutton").show();
                $("#progresscloud").text("");

                $("#linktoggle").css('display','block');
                $("#textpart").css('display','block');
                //$("#copyButton").css('display','block');
                $("#widgetCode").attr("href", APP_URL+'/'+data);
                var codeHtml = "<iframe id='myFrame' src="+APP_URL+'/'+data+" max-width=100% width=100% height=650px frameborder='0'></iframe>";
                $("#embedcode").text(codeHtml);
          },
          error : function(data)
          {
            
            Swal.fire(
              'Oops!',
              'Something went wrong!',
              'error'
            )
          }
        });
  }
});



});

</script>
<?php echo $__env->make('includes.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\smart1_enrichment\smartoneapp\resources\views/layouts/cloudwidget.blade.php ENDPATH**/ ?>