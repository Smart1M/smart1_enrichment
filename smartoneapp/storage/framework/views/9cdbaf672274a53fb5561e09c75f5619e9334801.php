<?php $__env->startSection('content'); ?>


<p>
<?php if(session('status')): ?>
    <div class="alert alert-success">
        <?php echo e(session('status')); ?>

    </div>
<?php endif; ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
        
        <ul id="horizontal-list">
            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('add media')): ?>
            <li>
                <a href="<?php echo e(url('mediapartner/add')); ?>" >
                <button type="button" class="btn btn-primary" name="create" class="">Media Partner <i class="fa fa-plus"></i></button>
                </a> 
            </li>
            <?php endif; ?>
            <!-- <li>
                <a href="<?php echo e(url('customers')); ?>" >
                <button type="button" class="btn btn-primary" name="create" class="">Customer <i class="fa fa-user-plus"></i></button>
                </a> 
            </li> -->
        </ul> 

        <table id="example" class="table table-striped table-bordered" style="width:100%;">
        <thead>
        <tr>
            <th>Sl.No</th>
            <th>Media Partner</th>
            <th>Rate</th>
            <th>Internal Rep</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>

        <?php $i=1; foreach ($mediapartner as $key => $value) { ?>
        <tr>
            <td><?php echo e($i); ?></td>
            <td><?php echo e($value->media_partner); ?></td>
            <td><?php echo e($value->rate); ?></td>
            <td><?php echo e($value->internal_rep); ?></td>
            <td><?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit media')): ?><a  href="<?php echo e(url('mediapartner/edit/'.$value->media_id)); ?>" title="Edit" style="text-decoration: none; color: inherit;"><i class="far fa-edit"></i></a><?php endif; ?> <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('delete media')): ?>&nbsp; <a id="<?php echo e($value->media_id); ?>" href="javascript:void(0)" title="Delete" style="text-decoration: none; color: inherit;" class="deletemedia"><i class="far fa-trash-alt"></i></a><?php endif; ?></td>
        </tr>

        <?php $i++; } ?>

        </tbody>
        </table>
            
        </div>
    </div>
</div>
</p>



<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.mediapartnerlist', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\smart1_enrichment\smartoneapp\resources\views/mediapartner.blade.php ENDPATH**/ ?>