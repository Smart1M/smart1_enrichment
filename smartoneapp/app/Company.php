<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Company extends Authenticatable
{
    use Notifiable;
    protected $table='company';
    protected $primaryKey = 'company_id';
    protected $fillable = [
        'cname','email','phone','address1','address2','city','state','zip','country','updated_at','created_at','Client_number','Development_link'];

    protected $hidden=[
        'Password',
    ];
}
