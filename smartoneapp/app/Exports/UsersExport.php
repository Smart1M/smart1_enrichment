<?php

namespace App\Exports;

use App\User;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UsersExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

	protected $compid,$basicid;

	function __construct($compid,$basicid) {
	    $this->compid = $compid;
	    $this->basicid = $basicid;
	}

    public function collection()
    {
        $yext_res = DB::table('create_scan_basic')
                        ->leftjoin('scan_result','create_scan_basic.basic_id','=','scan_result.scn_basic_id')
                        ->select('scan_result.siteId')
                        ->where('create_scan_basic.company_id','=',$this->compid)
                        ->get();

        $scn_arr = json_decode(json_encode($yext_res), true);

        $comp_res = DB::table('company')
        				->select('cname','phone','address1','zip')
        				->where('company_id','=',$this->compid)
        				->first();  

        $scan_res = DB::table('create_scan_basic')
                        ->leftjoin('scan_result','create_scan_basic.basic_id','=','scan_result.scn_basic_id')
                        ->select('scan_result.siteId','scan_result.scn_basic_id','scan_result.name AS compname','scan_result.address','scan_result.phone','scan_result.zip','scan_result.url')
                        ->where('create_scan_basic.company_id','=',$this->compid)
                        ->get(); 

        foreach($scan_res as $k => $value)
        {
	    	  $cname = $comp_res->cname;
	          $phone = preg_replace('/[^A-Za-z0-9\-]/', '',str_replace(array(' ','-'), '', $comp_res->phone));
	          $caddress = $comp_res->address1;

	          $yext_phone_sub = substr($value->phone,0,2);

	          if($yext_phone_sub=='+1')
	          {
	            $yext_phone = substr($value->phone,2);
	          } else 
	          {
	            $yext_phone = $value->phone;
	          }

	          $czip = $comp_res->zip;

	          if($cname!=$value->compname)
	          {
	            $name_error_flag = 1;
	            $name_stat = "Name : ".$cname;
	          } else
	          {
	            $name_error_flag = 0;
	            $name_stat = '';
	          }

	          if($phone!=$yext_phone)
	          {
	            $phone_error_flag = 1;
	            $phone_stat = ", Phone : ".$phone;
	          } else
	          {
	            $phone_error_flag = 0;
	            $phone_stat = '';
	          }

	          if($caddress!=$value->address)
	          {
	            $address_error_flag = 1;
	            $address_stat = ", Address : ".$caddress;
	          } else
	          {
	            $address_error_flag = 0;
	            $address_stat = '';
	          }

	          if($czip!=$value->zip)
	          {
	            $zip_error_flag = 1;
	            $zip_stat = ", Zip : ".$czip;
	          } else
	          {
	            $zip_error_flag = 0;
	            $zip_stat = '';
	          }

	          if($name_error_flag==0&&$phone_error_flag==0&&$address_error_flag==0&&$zip_error_flag==0)
	          {
	            $error_flag = 0;
	            $error_stat = 'No';
	          }
	          else
	          {
	            $error_flag = 1;
	            $error_stat = 'Yes';
	          }

	          $scn_arr[$k]['is_error'] = $error_stat;

	          if($error_flag == 1)
	          {
	          	$scn_arr[$k]['errors'] = ltrim($name_stat.''.$phone_stat.''.$address_stat.''.$zip_stat,',');
	          } else
	          {
	          	$scn_arr[$k]['errors'] = '';
	          }
	          
        }

                        
        return collect($scn_arr);
    }

    public function headings(): array
    {
        return [
            'Site Id',
            'Error',
            'Error Fileds',
        ];
    }


}
