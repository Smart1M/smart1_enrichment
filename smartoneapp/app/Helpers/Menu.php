<?php
namespace App\Helpers;
 
use Illuminate\Support\Facades\DB;
 
class Menu {
    /**
     * @param int $user_id User-id
     * 
     * @return string
     */
    public static function get_parentmenu() {
        $menu = DB::table('menu_basic')
        		->leftjoin('permissions','permissions.id','=','menu_basic.menu_perm_name')
        		->where('menu_basic.active_status','=',0)
        		->where('menu_basic.menu_type','=',1)
        		->select('menu_basic.menu_id','menu_basic.menu_name','menu_basic.menu_icon','menu_basic.menu_perm_name','menu_basic.menu_slug','menu_basic.is_sub','permissions.name')
        		->orderBy('menu_basic.menu_order')
        		->get();
         
        return $menu;
    }

    public static function get_submenu($id) {
        $menu = DB::table('menu_basic')
        		->where('active_status','=',0)
        		->where('menu_type','=',2)
        		->where('parent_id','=',$id)
        		->select('menu_id','menu_name','menu_perm_name','menu_slug','is_sub')
        		->orderBy('menu_order')
        		->get();
         
        return $menu;
    }

}