<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use PDF;
use Log;

class SilktideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function reports($id)
    {

        $silktide_primary = DB::table('silktide_prospect')
                                ->select('st_id','report_id','url','created_at')
                                ->where('company_id',$id)
                                ->get();
        return view('silktidereports',compact('silktide_primary'));
    }
    public function index()
    {
        $company = DB::table('company')
                    ->select('company_id','cname')
                    ->where('status','=',0)
                    ->get();

        $silktide_primary = DB::table('silktide_prospect')
                            ->select(DB::raw('COUNT(company_id) as records'),'company_id','url')
                            ->groupBy('company_id')
                            ->groupBy('url')
                            ->get();
        return view('silktide',compact('silktide_primary','company'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datetime = date('Y-m-d H:i:s');
        set_time_limit(300);
        /* ------ Silktide Prospect API ----- */
        $user = auth()->user();
        $user_id = $user->id;

        $webUrl = $request->web_url;
        $company_id = $request->company_id;
        $data = json_encode(array('url' => $webUrl ));
        $url = 'https://api.prospect.silktide.com/api/v1/report';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
          'api-key: 398a2e9cb7fc76f58678cc5bb41bfb5bd8f839c4',
          'Content-Type: application/json',
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);


        $result = curl_exec($curl);
        $result = json_decode($result,true);
        if(!$result || $result['status']=='error') {
            if($result['issue']=='redirect'){
                $r['error'] = "The web address you entered redirects to another website. If you'd like to test this website instead, change your url to <a href=".$result['url'].">".$result['url']."</a>";
                Log::error("The web address you entered redirects to another website. If you'd like to test this website instead, change your url to ".$result['url']);
                return $r;
            }
            $r['error'] = $result['error'];
            return $r;
        }
        curl_close($curl);

        $reportId = $result['reportId'];

        $detail_url = "https://api.prospect.silktide.com/api/v1/report/".$reportId;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $detail_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'api-key: 398a2e9cb7fc76f58678cc5bb41bfb5bd8f839c4',
          'Content-Type: application/json',
        ));

        $fetch_data = curl_exec($ch);
        $fetch_data = json_decode($fetch_data,true);
        //echo '<pre>'; print_r($fetch_data);
        //echo implode(',',$fetch_data['report']['paid_search']['adwords_keywords']);

        /* --- Save to 'silktide_prospect' table -- */
        // $exists = DB::table('silktide_prospect')->where('url',$webUrl)->first();
        // if(empty($exists)){
            $st_Id = DB::table('silktide_prospect')
                ->insertGetId([
                    'report_id' => $reportId,
                    'account_id' => $fetch_data['report']['account_id'],
                    'company_id' => $company_id,
                    'url' => $webUrl,
                    'user_id' => $user_id,
                    'created_at' => $datetime
                ]);
        // }else{
        //     $st_Id = $exists->st_id;
        // }

        /* --- Save to 'st_sitecontent' table --- */

        DB::table('st_sitecontent')
            ->updateOrInsert(['st_id' => $st_Id],
                ['average_words_per_page' => (isset($fetch_data['report']['amount_of_content']['average_words_per_page'])) ? $fetch_data['report']['amount_of_content']['average_words_per_page'] : 0,
                'pages_found' => (isset($fetch_data['report']['amount_of_content']['pages_found'])) ? $fetch_data['report']['amount_of_content']['pages_found'] : 0,
                'total_word_count' => (isset($fetch_data['report']['amount_of_content']['total_word_count'])) ? $fetch_data['report']['amount_of_content']['total_word_count'] : 0,
                'cms_solution' => (isset($fetch_data['report']['content_management_system']['cms_solution'])&&!empty($fetch_data['report']['content_management_system']['cms_solution'])) ? implode(',',$fetch_data['report']['content_management_system']['cms_solution']) : '',
                'has_cms' => (isset($fetch_data['report']['content_management_system']['has_cms'])) ? $fetch_data['report']['content_management_system']['has_cms'] : 0,
                'pages_discovered_count' => (isset($fetch_data['report']['page_count']['pages_discovered_count'])) ? $fetch_data['report']['page_count']['pages_discovered_count'] : 0,
                'created_at' => $datetime
            ]);

        /* --- Save to 'st_vitals' table --- */

        DB::table('st_vitals')
        ->updateOrInsert(['st_id' => $st_Id],
            ['pages_tested' => (isset($fetch_data['report']['amount_of_content']['pages_tested'])) ? $fetch_data['report']['amount_of_content']['pages_tested'] : 0,
            'analytics_tool' => (isset($fetch_data['report']['analytics']['analytics_tool'])) ? $fetch_data['report']['analytics']['analytics_tool'] : '',
            'has_analytics' => (isset($fetch_data['report']['analytics']['has_analytics'])) ? $fetch_data['report']['analytics']['has_analytics'] : 0,
            'domain' => (isset($fetch_data['report']['domain'])) ? $fetch_data['report']['domain'] : '',
            'ecommerce_name' => (isset($fetch_data['report']['ecommerce']['ecommerce_name'])) ? $fetch_data['report']['ecommerce']['ecommerce_name'] : '',
            'has_ecommerce' => (isset($fetch_data['report']['ecommerce']['has_ecommerce'])) ? $fetch_data['report']['ecommerce']['has_ecommerce'] : 0,
            'has_pixel' => (isset($fetch_data['report']['facebook_retargeting']['has_pixel'])) ? $fetch_data['report']['facebook_retargeting']['has_pixel'] : 0,
            'address_details_provided' => (isset($fetch_data['report']['local_presence']['address_details_provided'])) ? $fetch_data['report']['local_presence']['address_details_provided'] : 0,
            'has_result' => (isset($fetch_data['report']['local_presence']['has_result'])) ? $fetch_data['report']['local_presence']['has_result'] : 0,
            'yext_api_success' => (isset($fetch_data['report']['local_presence']['yext_api_success'])) ? $fetch_data['report']['local_presence']['yext_api_success'] : 0,
            'has_mobile_site' => (isset($fetch_data['report']['mobile']['has_mobile_site'])) ? $fetch_data['report']['mobile']['has_mobile_site'] : 0,
            'is_mobile' => (isset($fetch_data['report']['mobile']['is_mobile'])) ? $fetch_data['report']['mobile']['is_mobile'] : 0,
            'is_tablet' => (isset($fetch_data['report']['mobile']['is_tablet'])) ? $fetch_data['report']['mobile']['is_tablet'] : 0,
            'mobile_screenshot_url' => (isset($fetch_data['report']['mobile']['mobile_screenshot_url'])) ? $fetch_data['report']['mobile']['mobile_screenshot_url'] : '',
            'mobile_site_url' => (isset($fetch_data['report']['mobile']['mobile_site_url'])) ? $fetch_data['report']['mobile']['mobile_site_url'] : '',
            'average_monthly_traffic' => (isset($fetch_data['report']['organic_search']['average_monthly_traffic'])) ? $fetch_data['report']['organic_search']['average_monthly_traffic'] : 0,
            'adwords_keywords' => (isset($fetch_data['report']['paid_search']['adwords_keywords'])&&!empty($fetch_data['report']['paid_search']['adwords_keywords'])) ? implode(',', $fetch_data['report']['paid_search']['adwords_keywords']) : '',
            'has_adwords_spend' => (isset($fetch_data['report']['paid_search']['has_adwords_spend'])) ? $fetch_data['report']['paid_search']['has_adwords_spend'] : 0,
            'has_sitemap' => (isset($fetch_data['report']['sitemap']['has_sitemap'])) ? $fetch_data['report']['sitemap']['has_sitemap'] : 0,
            'sitemap_issues' => (isset($fetch_data['report']['sitemap']['sitemap_issues'])) ? $fetch_data['report']['sitemap']['sitemap_issues'] : 0,
            'stale_analysis' => (isset($fetch_data['report']['stale_analysis'])) ? $fetch_data['report']['stale_analysis'] : 0,
            'created_at' => $datetime

        ]);


        /* --- Save to 'st_discovered' table --- */

        DB::table('st_discovered')
        ->updateOrInsert(['st_id' => $st_Id],
            ['email' => (isset($fetch_data['report']['contact_details']['email'])&&!empty($fetch_data['report']['contact_details']['email'])) ? implode(',',$fetch_data['report']['contact_details']['email']) : '',
            'emails' => (isset($fetch_data['report']['contact_details']['emails'])&&!empty($fetch_data['report']['contact_details']['emails'])) ? implode(',',$fetch_data['report']['contact_details']['emails']) : '',
            'phone' => (isset($fetch_data['report']['contact_details']['phone'])&&!empty($fetch_data['report']['contact_details']['phone'])) ? implode(',',$fetch_data['report']['contact_details']['phone']) : '',
            'phones' => (isset($fetch_data['report']['contact_details']['phones'])&&!empty($fetch_data['report']['contact_details']['phones'])) ? implode(',',$fetch_data['report']['contact_details']['phones']) : '',
            'created_at' => $datetime
        ]);

        /* --- Save to 'st_domain_info' table --- */

        DB::table('st_domain_info')
        ->updateOrInsert(['st_id' => $st_Id],
            ['domain_age_days' => (isset($fetch_data['report']['domain_age']['domain_age_days'])) ? $fetch_data['report']['domain_age']['domain_age_days'] : '',
            'expiry_date' => (isset($fetch_data['report']['domain_age']['expiry_date'])) ? $fetch_data['report']['domain_age']['expiry_date'] : '',
            'registered_date' => (isset($fetch_data['report']['domain_age']['registered_date'])) ? $fetch_data['report']['domain_age']['registered_date'] : '',
            'created_at' => $datetime
        ]);


        /* --- Save to 'st_domain' table --- */
            // print_r($fetch_data['report']);
        DB::table('st_domain')
        ->updateOrInsert(['st_id' => $st_Id],
            ['is_parked' => (isset($fetch_data['report']['parked_domain_detection']['is_parked'])) ? $fetch_data['report']['parked_domain_detection']['is_parked'] : '0',
            'has_ssl' => (isset($fetch_data['report']['ssl']['has_ssl'])) ? $fetch_data['report']['ssl']['has_ssl'] : '0',
            'ssl_expired' => (isset($fetch_data['report']['ssl']['ssl_expired'])) ? $fetch_data['report']['ssl']['ssl_expired'] : '0',
            'ssl_redirect' => (isset($fetch_data['report']['ssl']['ssl_redirect'])) ? $fetch_data['report']['ssl']['ssl_redirect'] : '0',
            'ssl_valid' => (isset($fetch_data['report']['ssl']['ssl_valid'])) ? $fetch_data['report']['ssl']['ssl_valid'] : '0',
            'created_at' => $datetime
        ]);

        /* --- Save to 'st_seo' table --- */
        // echo '<pre>'; print_r($fetch_data['report']);

        DB::table('st_seo')
        ->updateOrInsert(['st_id' => $st_Id],
            ['good_headings' => (isset($fetch_data['report']['headings']['good_headings'])) ? $fetch_data['report']['headings']['good_headings'] : 0,
            'has_content_for_every_heading' => (isset($fetch_data['report']['headings']['has_content_for_every_heading'])) ? $fetch_data['report']['headings']['has_content_for_every_heading'] : 0,
            'has_hierarchical_headings' => (isset($fetch_data['report']['headings']['has_hierarchical_headings'])) ? $fetch_data['report']['headings']['has_hierarchical_headings'] : 0,
            'has_single_h1_on_each_page' => (isset($fetch_data['report']['headings']['has_single_h1_on_each_page'])) ? $fetch_data['report']['headings']['has_single_h1_on_each_page'] : 0,
            'analysis_country' => (isset($fetch_data['report']['meta']['analysis_country'])) ? $fetch_data['report']['meta']['analysis_country'] : '',
            'detected_address' => (isset($fetch_data['report']['meta']['detected_address'])) ? $fetch_data['report']['meta']['detected_address'] : '',
            'detected_name' => (isset($fetch_data['report']['meta']['detected_name'])) ? $fetch_data['report']['meta']['detected_name'] : '',
            'detected_phone' => (isset($fetch_data['report']['meta']['detected_phone'])) ? $fetch_data['report']['meta']['detected_phone'] : '',
            'num_keywords_ranked_for' => (isset($fetch_data['report']['organic_search']['num_keywords_ranked_for'])) ? $fetch_data['report']['organic_search']['num_keywords_ranked_for'] : 0,
            'top_keywords_ranked_for' => (isset($fetch_data['report']['organic_search']['top_keywords_ranked_for'])) ? json_encode($fetch_data['report']['organic_search']['top_keywords_ranked_for']) : '',
            'duplicated_items' => (isset($fetch_data['report']['page_titles_and_descriptions']['duplicated_items'])) ? $fetch_data['report']['page_titles_and_descriptions']['duplicated_items'] : 0,
            'homepage_title_tag' => (isset($fetch_data['report']['page_titles_and_descriptions']['homepage_title_tag'])) ? $fetch_data['report']['page_titles_and_descriptions']['homepage_title_tag'] : '',
            'missing_items' => (isset($fetch_data['report']['page_titles_and_descriptions']['missing_items'])) ? $fetch_data['report']['page_titles_and_descriptions']['missing_items'] : '',
            'pages_duplicated_description_count' => (isset($fetch_data['report']['page_titles_and_descriptions']['pages_duplicated_description_count'])) ? $fetch_data['report']['page_titles_and_descriptions']['pages_duplicated_description_count'] : 0,
            'pages_duplicated_title_count' => (isset($fetch_data['report']['page_titles_and_descriptions']['pages_duplicated_title_count'])) ? $fetch_data['report']['page_titles_and_descriptions']['pages_duplicated_title_count'] : 0,
            'pages_missing_description_count' => (isset($fetch_data['report']['page_titles_and_descriptions']['pages_missing_description_count'])) ? $fetch_data['report']['page_titles_and_descriptions']['pages_missing_description_count'] : 0,
            'pages_missing_title_count' => (isset($fetch_data['report']['page_titles_and_descriptions']['pages_missing_title_count'])) ? $fetch_data['report']['page_titles_and_descriptions']['pages_missing_title_count'] : 0,
            'percent_duplicated_descriptions' => (isset($fetch_data['report']['page_titles_and_descriptions']['percent_duplicated_descriptions'])) ? $fetch_data['report']['page_titles_and_descriptions']['percent_duplicated_descriptions'] : 0,
            'percent_duplicated_titles' => (isset($fetch_data['report']['page_titles_and_descriptions']['percent_duplicated_titles'])) ? $fetch_data['report']['page_titles_and_descriptions']['percent_duplicated_titles'] : 0,
            'percent_missing_descriptions' => (isset($fetch_data['report']['page_titles_and_descriptions']['percent_missing_descriptions'])) ? $fetch_data['report']['page_titles_and_descriptions']['percent_missing_descriptions'] : 0,
            'percent_missing_titles' => (isset($fetch_data['report']['page_titles_and_descriptions']['percent_missing_titles'])) ? $fetch_data['report']['page_titles_and_descriptions']['percent_missing_titles'] : 0,
            'created_at' => $datetime
        ]);

        /* --- Save to 'st_images' table --- */

        DB::table('st_images')
        ->updateOrInsert(['st_id' => $st_Id],
            ['image_count' => (isset($fetch_data['report']['images']['image_count'])) ? $fetch_data['report']['images']['image_count'] : 0,
            'non_web_friendly_count' => (isset($fetch_data['report']['images']['non_web_friendly_count'])) ? $fetch_data['report']['images']['non_web_friendly_count'] : 0,
            'percent_images_sized' => (isset($fetch_data['report']['images']['percent_images_sized'])) ? $fetch_data['report']['images']['percent_images_sized'] : 0,
            'stretched_image_count' => (isset($fetch_data['report']['images']['stretched_image_count'])) ? $fetch_data['report']['images']['stretched_image_count'] : 0,
            'created_at' => $datetime
        ]);

        /* --- Save to 'st_social' table --- */

        DB::table('st_social')
        ->updateOrInsert(['st_id' => $st_Id],
            ['has_instagram' => (isset($fetch_data['report']['instagram_account']['has_instagram'])) ? $fetch_data['report']['instagram_account']['has_instagram'] : 0,
            'days_since_update' => (isset($fetch_data['report']['last_updated']['days_since_update'])) ? $fetch_data['report']['last_updated']['days_since_update'] : 0,
            'last_updated_date' => (isset($fetch_data['report']['last_updated']['last_updated_date'])) ? $fetch_data['report']['last_updated']['last_updated_date'] : '',
            'twitter_found' => (isset($fetch_data['report']['twitter']['found'])) ? $fetch_data['report']['twitter']['found'] : 0,
            'created_at' => $datetime
        ]);

        /* --- Save to 'st_video' table --- */

        DB::table('st_video')
        ->updateOrInsert(['st_id' => $st_Id],
            ['vendor_vendor' => (isset($fetch_data['report']['vendor']['vendor'])) ? $fetch_data['report']['vendor']['vendor']: '',
            'has_video' => (isset($fetch_data['report']['video']['has_video'])) ? $fetch_data['report']['video']['has_video'] : 0,
            'video_vendor' => (isset($fetch_data['report']['video']['vendor'])) ? $fetch_data['report']['video']['vendor'] : '',
            'website_traffic' => (isset($fetch_data['report']['website_traffic']['website_traffic'])) ? $fetch_data['report']['website_traffic']['website_traffic'] : 0,
            'created_at' => $datetime
        ]);

        return '1';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function exporttopdf($id)
    {
        $curr_time = md5(time());
        $st_id = $id;
        $data = DB::table('silktide_prospect')
                 ->leftjoin('st_sitecontent','silktide_prospect.st_id','=','st_sitecontent.st_id')
                 ->leftjoin('st_vitals','silktide_prospect.st_id','=','st_vitals.st_id')
                 ->leftjoin('st_discovered','silktide_prospect.st_id','=','st_discovered.st_id')
                 ->leftjoin('st_domain','silktide_prospect.st_id','=','st_domain.st_id')
                 ->leftjoin('st_domain_info','silktide_prospect.st_id','=','st_domain_info.st_id')
                 ->leftjoin('st_seo','silktide_prospect.st_id','=','st_seo.st_id')
                 ->leftjoin('st_images','silktide_prospect.st_id','=','st_images.st_id')
                 ->leftjoin('st_social','silktide_prospect.st_id','=','st_social.st_id')
                 ->leftjoin('st_video','silktide_prospect.st_id','=','st_video.st_id')
                 ->select('silktide_prospect.url','silktide_prospect.created_at','st_sitecontent.average_words_per_page','st_sitecontent.pages_found','st_sitecontent.total_word_count','st_sitecontent.cms_solution','st_sitecontent.has_cms','st_vitals.pages_tested','st_vitals.analytics_tool','st_vitals.has_analytics','st_vitals.domain','st_vitals.ecommerce_name','st_vitals.has_ecommerce','st_vitals.has_pixel','st_vitals.address_details_provided','st_vitals.has_result','st_vitals.yext_api_success','st_vitals.has_mobile_site','st_vitals.is_mobile','st_vitals.is_tablet','st_vitals.mobile_screenshot_url','st_vitals.mobile_site_url','st_vitals.average_monthly_traffic','st_vitals.adwords_keywords','st_vitals.has_adwords_spend','st_vitals.has_sitemap','st_vitals.sitemap_issues','st_vitals.stale_analysis','st_discovered.emails','st_discovered.phones','st_domain_info.domain_age_days','st_domain_info.expiry_date','st_domain_info.registered_date','st_seo.good_headings','st_seo.has_content_for_every_heading','st_seo.has_hierarchical_headings','st_seo.has_single_h1_on_each_page','st_seo.analysis_country','st_seo.detected_address','st_seo.detected_name','st_seo.detected_phone','st_seo.num_keywords_ranked_for','st_seo.top_keywords_ranked_for','st_seo.duplicated_items','st_seo.homepage_title_tag','st_seo.missing_items','st_seo.pages_duplicated_description_count','st_seo.pages_duplicated_title_count','st_seo.pages_missing_description_count','st_seo.pages_missing_title_count','st_seo.percent_duplicated_descriptions','st_seo.percent_duplicated_titles','st_seo.percent_missing_descriptions','st_seo.percent_missing_titles','st_images.image_count','st_images.non_web_friendly_count','st_images.percent_images_sized','st_images.stretched_image_count','st_social.has_instagram','st_social.days_since_update','st_social.last_updated_date','st_social.twitter_found','st_video.vendor_vendor','st_video.has_video','st_video.video_vendor','st_video.website_traffic','st_domain.is_parked','st_domain.has_ssl','st_domain.ssl_expired','st_domain.ssl_redirect','st_domain.ssl_valid')
                 ->where('silktide_prospect.st_id','=',$st_id)
                 ->first();

        $data = json_decode(json_encode($data), true);

        //$data = $data->toArray();

        // echo '<pre>'; print_r($data); die();

        $pdf = PDF::loadView('silktidepdf', $data);

        return $pdf->download('Silktide_'.$curr_time.'.pdf');
        die();
    }
}
