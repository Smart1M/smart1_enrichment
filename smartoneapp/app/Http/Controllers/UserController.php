<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
Use DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = DB::table('users')
                 ->leftjoin('roles','users.role_id','=','roles.id')
                 ->select('users.id','users.name','users.email','users.mobile','users.ustatus','roles.name AS rolename','roles.id AS roleid')
                 ->where('users.user_type','=',0)
                 ->where('users.ustatus','=',0)
                 ->get();
        return view('users',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = DB::table('roles')
                    ->where('rstatus',0)
                    ->get();
        return view('addusers',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = DB::table('roles')
                    ->where('rstatus',0)
                    ->get();

        $users = DB::table('users')
                    ->where('id',$id)
                    ->first();
        
        return view('editusers',compact('users','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required', 'string', 'max:255',
            'role_id' => 'required',
            'email' => 'required', 'string', 'email', 'max:255', 'unique:users',
            'mobile' => 'required',
            //'password' => 'required|confirmed|min:8',
        ]);

        if(isset($request->password))
        {
            $newpassword = $request->password;
            $confirm_password = $request->password_confirmation;
            if($confirm_password!=$newpassword)
            {
                return redirect('users/edit/'.$id)->with('error_msg', 'Password Doesnot match');
            } else 
            {

                DB::table('users')
                    ->where('id', $id)
                    ->update([
                        'name' => $validatedData['name'],
                        'role_id' => $validatedData['role_id'],
                        'email' => $validatedData['email'],
                        'mobile' => $validatedData['mobile'],
                        'username' => $validatedData['email'],
                        'password' => Hash::make($request->password),
                        'rawpassword' => $request->password
                    ]);

                return redirect('users')->with('status', 'User data Updated!');
            }
        } else 
        {
            DB::table('users')
                    ->where('id', $id)
                    ->update([
                        'name' => $validatedData['name'],
                        'role_id' => $validatedData['role_id'],
                        'email' => $validatedData['email'],
                        'mobile' => $validatedData['mobile'],
                        'username' => $validatedData['email']
                    ]);

                return redirect('users')->with('status', 'User data Updated!');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->userid;
        DB::table('users')
            ->where('id', $id)
            ->update(['ustatus' => 1]);
         return "1";  
    }

    public function changepassword()
    {
        return view('changepassword');
    }

    public function updatepassword(Request $request)
    {
        $user = auth()->user();
        $user_id = $user->id;

        $current_password = $request->currentpassword;
        $newpassword = $request->newpassword;
        $confirm_password = $request->password_confirmation;

        $users = DB::table('users')
                    ->where('id','=',$user_id)
                    ->first();
        $old_password = $users->rawpassword;

        if($current_password!=$old_password)
        {
            return redirect('changepassword')->with('error_msg', 'Old password Doesnot match');
        } else 
        {
            DB::table('users')
            ->where('id', $user_id)
            ->update([
                'password' => Hash::make($newpassword),
                'rawpassword' => $newpassword
            ]);

            return redirect('changepassword')->with('password_success', 'Password changed.Please login to continue');
        }

        //return view('changepassword');
    }

    public function checkemail(Request $request)
    {
        $email = $request->email;
        $mailcount = DB::table('users')
                    ->where('email','=',$email)
                    ->where('ustatus','=',0)
                    ->count();

        return $mailcount;
        die();
    }

    public function editcheckemail(Request $request)
    {
        $email = $request->email;
        $userid = $request->userid;
        $mailcount = DB::table('users')
                    ->where('email','=',$email)
                    ->where('ustatus','=',0)
                    ->where('id','!=',$userid)
                    ->count();

        return $mailcount;
        die();
    }
}
