<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use PDF;
use Log;
use Cloudder;
use Exception;
use Storage;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;

class ApiController extends Controller
{

    public function index(Request $request)
    {
        if($request['api_token']==env('API_KEY'))
        {
            $companies = DB::table('company')
                    ->select('company_id','cname as company_name')
                    ->where('status','=',0)
                    ->get();
            $response['status'] = 200;
            $response['message'] = 'success';
            $response['data'] = $companies;
            return response()->json($response,200);

        }else{
            $response['status'] = 401;
            $response['message'] = 'Authentication failed';
            return response()->json($response,401);
        }

    }

    public function companyReport(Request $request)
    {
        if($request['api_token']==env('API_KEY'))
        {
            if (!$request->has('date_id')) {
                if (!$request['company_id']) {
                    $response['status'] = 404;
                    $response['message'] = 'Missing data field : company_id';
                    return response()->json($response, 404);
                }

                $exists = DB::table('company_reports')
                            ->where('company_id', $request['company_id'])
                            ->select('id as date_id', 'company_reports.created_at as created')
                            ->get();
                if ($exists) {
                    $response['status'] = 200;
                    $response['message'] = 'Select a date and pass date_id';
                    $response['data'] = $exists;
                    return response()->json($response, 200);
                } else {
                    $response['status'] = 404;
                    $response['message'] = 'Company not found';
                    return response()->json($response, 401);
                }
            }else{
                $id = $request['date_id'];
                $curr_time = DB::table('company_reports')
                                ->where('id',$id)
                                ->value('created_at');
                $curr_time = date('m-d-Y', strtotime($curr_time));
                $report = DB::table('company_reports')
                                ->where('id',$id)
                                ->first();
                $br_id = $report->br_id;
                $st_id = $report->st_id;
                $id = $report->company_id;
                $company_name = DB::table('company')
                                ->where('company_id',$id)
                                ->value('cname');

                $company = DB::table('company')
                                ->leftjoin('company_reports','company_reports.company_id','company.company_id')
                                ->select('company.cname as company_name', 'company.address1', 'company.address2', 'company.city','company.state_name','company.zip','company.country','company.email','company.phone','company.company_url','company.media_partner','company_reports.created_at')
                                ->where('company.company_id', $id)
                                ->first();

                $uploadWidget = DB::table('cloudwidget')
                                    ->select('widget_url as uploadWidget')
                                    ->where('cl_cid', '=', $id)
                                    ->where('widget_type', 1)
                                    ->first();
                if($uploadWidget){
                    $uploadWidget->uploadWidget = url('/').'/'.$uploadWidget->uploadWidget;
                    $uploadWidget->uploadWidget = "<iframe id='myFrame' src=$uploadWidget->uploadWidget max-width=100% width=100% height=650px frameborder='0'></iframe>";
                }

                $mediaLibrary = DB::table('cloudwidget')
                                    ->select('widget_url as mediaLibrary')
                                    ->where('cl_cid', '=', $id)
                                    ->where('widget_type', 2)
                                    ->first();
                if($mediaLibrary){
                    $mediaLibrary->mediaLibrary = url('/').'/'.$mediaLibrary->mediaLibrary;
                    $mediaLibrary->mediaLibrary = "<iframe id='myFrame' src=$mediaLibrary->mediaLibrary max-width=100% width=100% height=650px frameborder='0'></iframe>";
                }

                $design    = DB::table('brandfetch_primary')
                                ->leftjoin('br_logo_colour_raw', 'brandfetch_primary.br_id', '=', 'br_logo_colour_raw.br_id')
                                ->leftjoin('br_website_font', 'brandfetch_primary.br_id', '=', 'br_website_font.br_id')
                                ->leftjoin('br_web_colour_category', 'brandfetch_primary.br_id', '=', 'br_web_colour_category.br_id')
                                ->select('br_logo_colour_raw.l_colour_code as logoColor', 'br_website_font.primary_font as siteFont', 'br_web_colour_category.dark as primaryColor', 'br_web_colour_category.light as secondaryColor')
                                ->where('brandfetch_primary.br_id', $br_id)
                                ->first();

                $pageUrls   = DB::table('company_sitedata')
                                ->select('page_count', 'page_url')
                                ->where('company_id', '=', $id)
                                ->first();
                $hours      = DB::table('company_hours')
                                    ->select('weekday_text as hours')
                                    ->where('company_id', '=', $id)
                                    ->first();

                $social = DB::table('brandfetch_primary')
                                ->leftjoin('br_company_social_media', 'brandfetch_primary.br_id', '=', 'br_company_social_media.br_id')
                                ->select('br_company_social_media.twitter', 'br_company_social_media.linkedin', 'br_company_social_media.github', 'br_company_social_media.facebook', 'br_company_social_media.youtube', 'br_company_social_media.pinterest', 'br_company_social_media.instagram')
                                ->where('brandfetch_primary.br_id', $br_id)
                                ->first();

                $metadata = DB::table('brandfetch_primary')
                                ->leftjoin('br_website_meta_data', 'brandfetch_primary.br_id', '=', 'br_website_meta_data.br_id')
                                ->select('br_website_meta_data.title', 'br_website_meta_data.summary', 'br_website_meta_data.description', 'br_website_meta_data.keywords')
                                ->where('brandfetch_primary.br_id', $br_id)
                                ->first();

                $images = DB::table('brandfetch_primary')
                                ->leftjoin('br_website_images', 'brandfetch_primary.br_id', '=', 'br_website_images.br_id')
                                ->where('brandfetch_primary.br_id', $br_id)
                                ->pluck('br_website_images.image_url')
                                ->all();
                if(isset($images)){
                    $links['images'] = $images;
                }

                $company        = (isset($company)) ? json_decode(json_encode($company), true) : [];
                // $uploadWidget   = (isset($uploadWidget)) ? json_decode(json_encode($uploadWidget), true) : [];
                // $mediaLibrary   = (isset($mediaLibrary)) ? json_decode(json_encode($mediaLibrary), true) : [];
                $design         = (isset($design)) ? json_decode(json_encode($design), true) : [];
                $pageUrls       = (isset($pageUrls)) ? json_decode(json_encode($pageUrls), true) : [];
                $hours          = (isset($hours)) ? json_decode(json_encode($hours), true) : [];
                $social         = (isset($social)) ? json_decode(json_encode($social), true) : [];
                $metadata       = (isset($metadata)) ? json_decode(json_encode($metadata), true) : [];
                $links          = (isset($links)) ? json_decode(json_encode($links), true) : [];
                $data           = array_merge($company,$design,$pageUrls,$hours,$social,$metadata,$links);

                $response['status'] = 200;
                $response['message'] = 'success';
                $response['data'] = $data;
                return response()->json($response,200);
            }
        }else{
            $response['status'] = 401;
            $response['message'] = 'Authentication failed';
            return response()->json($response,401);
        }

    }
    public function brandFetchReport(Request $request)
    {
        if ($request['api_token']==env('API_KEY')) {
            if (!$request->has('date_id')) {

                if (!$request['company_id']) {
                    $response['status'] = 404;
                    $response['message'] = 'Missing data field : company_id';
                    return response()->json($response, 404);
                }

                $company_exists = DB::table('company')
                            ->where('company.company_id', $request['company_id'])
                            ->first();
                if (!$company_exists) {
                    $response['status'] = 404;
                    $response['message'] = 'Company not found';
                    return response()->json($response, 404);
                }
                $exists = DB::table('brandfetch_primary')
                            ->join('company', 'company.company_id', 'brandfetch_primary.company_id')
                            ->where('company.company_id', $request['company_id'])
                            ->select('br_id as date_id', 'brandfetch_primary.created_at as created')
                            ->get();
                if ($exists) {
                    $response['status'] = 200;
                    $response['message'] = 'Select a date and pass date_id';
                    $response['data'] = $exists;
                    return response()->json($response, 200);
                } else {
                    $response['status'] = 404;
                    $response['message'] = 'Data not found';
                    return response()->json($response, 401);
                }
            }else{
                $curr_time = md5(time());
                $br_id = $request->date_id;
                $data = DB::table('brandfetch_primary')
                         ->leftjoin('br_company_social_media','brandfetch_primary.br_id','=','br_company_social_media.br_id')
                         ->leftjoin('br_logo','brandfetch_primary.br_id','=','br_logo.br_id')
                         ->leftjoin('br_logo_background','brandfetch_primary.br_id','=','br_logo_background.br_id')
                         ->leftjoin('br_logo_colour_category','brandfetch_primary.br_id','=','br_logo_colour_category.br_id')
                         ->leftjoin('br_logo_colour_raw','brandfetch_primary.br_id','=','br_logo_colour_raw.br_id')
                         ->leftjoin('br_website_font','brandfetch_primary.br_id','=','br_website_font.br_id')
                         ->leftjoin('br_image_path','brandfetch_primary.br_id','=','br_image_path.br_id')
                         ->leftjoin('br_website_meta_data','brandfetch_primary.br_id','=','br_website_meta_data.br_id')
                         ->leftjoin('br_website_screenshot','brandfetch_primary.br_id','=','br_website_screenshot.br_id')
                         ->leftjoin('br_web_colour_category','brandfetch_primary.br_id','=','br_web_colour_category.br_id')
                         ->leftjoin('br_web_colour_raw','brandfetch_primary.br_id','=','br_web_colour_raw.br_id')
                         ->select('brandfetch_primary.web_url','brandfetch_primary.created_at','br_company_social_media.twitter','br_company_social_media.linkedin','br_company_social_media.github','br_company_social_media.instagram','br_company_social_media.facebook','br_company_social_media.youtube','br_company_social_media.pinterest','br_company_social_media.crunchbase','br_logo.web_logo','br_logo_background.bg_color','br_logo_colour_category.vibrant','br_logo_colour_category.dark','br_logo_colour_category.light','br_logo_colour_raw.l_colour_code','br_image_path.file_path','br_logo_colour_raw.l_percentage','br_website_font.title_tag','br_website_font.primary_font','br_website_meta_data.title','br_website_meta_data.summary','br_website_meta_data.description','br_website_meta_data.keywords','br_website_meta_data.language','br_website_screenshot.scn_url','br_web_colour_category.vibrant','br_web_colour_category.dark','br_web_colour_category.light','br_web_colour_raw.colour_code','br_web_colour_raw.percentage')
                         ->where('brandfetch_primary.br_id','=',$br_id)
                         ->first();

                $data = json_decode(json_encode($data), true);

                $response['status'] = 200;
                $response['message'] = 'success';
                $response['data'] = $data;
                return response()->json($response,200);
            }
        }else{
            $response['status'] = 401;
            $response['message'] = 'Authentication failed';
            return response()->json($response,401);
        }

    }
    public function silktideReport(Request $request)
    {
        if($request['api_token']==env('API_KEY'))
        {
            if (!$request->has('date_id')) {
                if (!$request['company_id']) {
                    $response['status'] = 404;
                    $response['message'] = 'Missing data field : company_id';
                    return response()->json($response, 404);
                }

                $company_exists = DB::table('company')
                            ->where('company.company_id', $request['company_id'])
                            ->first();
                if (!$company_exists) {
                    $response['status'] = 404;
                    $response['message'] = 'Company not found';
                    return response()->json($response, 404);
                }
                $exists = DB::table('silktide_prospect')
                            ->join('company', 'company.company_id', 'silktide_prospect.company_id')
                            ->where('company.company_id', $request['company_id'])
                            ->select('st_id as date_id', 'silktide_prospect.created_at as created')
                            ->get();
                if ($exists) {
                    $data = json_decode(json_encode($exists), true);

                    $response['status'] = 200;
                    $response['message'] = 'Select a date and pass date_id';
                    $response['data'] = $data;
                    return response()->json($response, 200);
                }else{
                    $response['status'] = 404;
                    $response['message'] = 'Data not found';
                    return response()->json($response,401);
                }
            } else {
                $curr_time = md5(time());
                $st_id = $request->date_id;
                $data = DB::table('silktide_prospect')
                        ->leftjoin('st_sitecontent','silktide_prospect.st_id','=','st_sitecontent.st_id')
                        ->leftjoin('st_vitals','silktide_prospect.st_id','=','st_vitals.st_id')
                        ->leftjoin('st_discovered','silktide_prospect.st_id','=','st_discovered.st_id')
                        ->leftjoin('st_domain','silktide_prospect.st_id','=','st_domain.st_id')
                        ->leftjoin('st_domain_info','silktide_prospect.st_id','=','st_domain_info.st_id')
                        ->leftjoin('st_seo','silktide_prospect.st_id','=','st_seo.st_id')
                        ->leftjoin('st_images','silktide_prospect.st_id','=','st_images.st_id')
                        ->leftjoin('st_social','silktide_prospect.st_id','=','st_social.st_id')
                        ->leftjoin('st_video','silktide_prospect.st_id','=','st_video.st_id')
                        ->select('silktide_prospect.url','silktide_prospect.created_at','st_sitecontent.average_words_per_page','st_sitecontent.pages_found','st_sitecontent.total_word_count','st_sitecontent.cms_solution','st_sitecontent.has_cms','st_vitals.pages_tested','st_vitals.analytics_tool','st_vitals.has_analytics','st_vitals.domain','st_vitals.ecommerce_name','st_vitals.has_ecommerce','st_vitals.has_pixel','st_vitals.address_details_provided','st_vitals.has_result','st_vitals.yext_api_success','st_vitals.has_mobile_site','st_vitals.is_mobile','st_vitals.is_tablet','st_vitals.mobile_screenshot_url','st_vitals.mobile_site_url','st_vitals.average_monthly_traffic','st_vitals.adwords_keywords','st_vitals.has_adwords_spend','st_vitals.has_sitemap','st_vitals.sitemap_issues','st_vitals.stale_analysis','st_discovered.emails','st_discovered.phones','st_domain_info.domain_age_days','st_domain_info.expiry_date','st_domain_info.registered_date','st_seo.good_headings','st_seo.has_content_for_every_heading','st_seo.has_hierarchical_headings','st_seo.has_single_h1_on_each_page','st_seo.analysis_country','st_seo.detected_address','st_seo.detected_name','st_seo.detected_phone','st_seo.num_keywords_ranked_for','st_seo.top_keywords_ranked_for','st_seo.duplicated_items','st_seo.homepage_title_tag','st_seo.missing_items','st_seo.pages_duplicated_description_count','st_seo.pages_duplicated_title_count','st_seo.pages_missing_description_count','st_seo.pages_missing_title_count','st_seo.percent_duplicated_descriptions','st_seo.percent_duplicated_titles','st_seo.percent_missing_descriptions','st_seo.percent_missing_titles','st_images.image_count','st_images.non_web_friendly_count','st_images.percent_images_sized','st_images.stretched_image_count','st_social.has_instagram','st_social.days_since_update','st_social.last_updated_date','st_social.twitter_found','st_video.vendor_vendor','st_video.has_video','st_video.video_vendor','st_video.website_traffic','st_domain.is_parked','st_domain.has_ssl','st_domain.ssl_expired','st_domain.ssl_redirect','st_domain.ssl_valid')
                        ->where('silktide_prospect.st_id','=',$st_id)
                        ->first();

                $data = json_decode(json_encode($data), true);

                $response['status'] = 200;
                $response['message'] = 'success';
                $response['data'] = $data;
                return response()->json($response,200);
            }
        }else{
            $response['status'] = 401;
            $response['message'] = 'Authentication failed';
            return response()->json($response,401);
        }

    }
    public function storeCompany(Request $request)
    {
        try{

            $postdata=$request->all();
            $datetime = date('Y-m-d H:i:s');
            $rules=array(
                'id'=>'required',
                'cname' => 'required',
                'phone' => 'required',
                'email' => 'required|regex:/(.+)@(.+)\.(.+)/i',
                'company_url'=>'required'
            );
            $messages=array(
                    'id.required'   =>'Knack Record ID required',
                    'cname.required' => 'cname required',
                    'email.required' => 'Email required',
                    'phone.required' => 'Phone is required',
                    'company_url.required'=>'Company URL required'
                );
            $validator=Validator::make(array(
                'id'=>isset($postdata['id'])?$postdata['field_2']:'',
                'cname'=>isset($postdata['field_2'])?$postdata['field_2']:'',
                'email'=>isset($postdata['field_4_raw']['email'])?$postdata['field_4_raw']['email']:'',
                'phone'=>isset($postdata['field_5'])?$postdata['field_5']:'',
                'company_url'=>isset($postdata['field_6_raw']['url'])?$postdata['field_6_raw']['url']:''),
                $rules,
                $messages);
            if($validator->fails())
            {
                $messages=$validator->messages();
                $errors['status']=417;
                $errors['message']=$messages->first();
                return response()->json($errors,417);
            }
            DB::table('company')->insert([
                "cname"=>$postdata['field_2'],
                "email"=>isset($postdata['field_4_raw']['email'])?$postdata['field_4_raw']['email']:'',
                "address1"=>isset($postdata['field_3'])?$postdata['field_3']:"",
                "address2"=>isset($postdata['field_4'])?$postdata['field_4']:"",
                "phone"=>$postdata['field_5'],
                "state"=>isset($postdata['field_3_raw']['state'])?$postdata['field_3_raw']['state']:"",
                "city"=>isset($postdata['field_3_raw']['city'])?$postdata['field_3_raw']['city']:"",
                "zip"=>isset($postdata['field_3_raw']['zip'])?$postdata['field_3_raw']['zip']:"",
                "media_partner"=>isset($postdata['field_7'])?$postdata['field_7']:"",
                "company_url"=>isset($postdata['field_6_raw']['url'])?$postdata['field_6_raw']['url']:'',
                "created_at"=>$datetime,
                "knackID"=>$postdata['id'],
            ]);
            $company=DB::table('company')->where('company.knackID',$postdata['id'])->first();
            if($company->knackID){
                $request->merge([
                    'company_id'=>$company->company_id,
                    'web_url'=>$company->company_url,
                    'knackID'=>$company->knackID,
                    'checklist_advertisers'=>12,
                    'checklist_companysocialmedia'=>8,
                    'checklist_dispadvertisingreports'=>11,
                    'checklist_localdata'=>15,
                    'checklist_marketingoverview'=>14,
                    'checklist_publisher_rank'=>13,
                    'checklist_sitecontent'=>16,
                    'checklist_websitecolors'=>3,
                    'checklist_websitefont'=>6,
                    'checklist_websiteimages'=>9,
                    'checklist_weblogo'=>1,
                    'checklist_colorsfromlogo'=>4,
                    'checklist_websitemetadata'=>10,
                    'checklist_websitescreenshot'=>7
                    ]);
                app('App\Http\Controllers\CompanyController')->fetchapires($request);
                app('App\Http\Controllers\CompanyReportController')->store($request);
                //cloudwidget
                $request->merge([
                    'cl_name'=>$company->cname,
                    'cl_email'=>$company->email,
                    'cl_cid'=>$company->company_id,
                    'cl_phone'=>$company->phone,
                    'company_url'=>$company->company_url,
                    ]);
                //uploadwidget
                $request->merge(['widgetflag'=>1]);
                app('App\Http\Controllers\CloudinaryWidgetController')->store($request);
                //media gallery
                $request->merge(['widgetflag'=>2]);
                app('App\Http\Controllers\CloudinaryWidgetController')->store($request);
                //customer gallery
                $request->merge(['widgetflag'=>3]);
                app('App\Http\Controllers\CloudinaryWidgetController')->store($request);
                //marketing asstes upload widget
                $request->merge(['widgetflag'=>4]);
                app('App\Http\Controllers\CloudinaryWidgetController')->store($request);
                //marketing assets gallery
                $request->merge(['widgetflag'=>5]);
                app('App\Http\Controllers\CloudinaryWidgetController')->store($request);
                //customer view upload widget
                $request->merge(['widgetflag'=>6]);
                app('App\Http\Controllers\CloudinaryWidgetController')->store($request);
                //customer view gallery
                $request->merge(['widgetflag'=>7]);
                app('App\Http\Controllers\CloudinaryWidgetController')->store($request);

                //update knack record
                $design    = DB::table('brandfetch_primary')
                ->leftjoin('br_logo_colour_raw', 'brandfetch_primary.br_id', '=', 'br_logo_colour_raw.br_id')
                ->leftjoin('br_website_font', 'brandfetch_primary.br_id', '=', 'br_website_font.br_id')
                ->leftjoin('br_web_colour_category', 'brandfetch_primary.br_id', '=', 'br_web_colour_category.br_id')
                ->select('br_logo_colour_raw.l_colour_code as logoColor', 'br_website_font.primary_font as siteFont', 'br_web_colour_category.dark as primaryColor', 'br_web_colour_category.light as secondaryColor')
                ->where('company_id', $company->company_id)
                ->first();
                $pageUrls = DB::table('company_sitedata')
                        ->select('page_count', 'page_url')
                        ->where('company_id', '=',$company->company_id)
                        ->first();
                $hours= DB::table('company_hours')
                    ->select('weekday_text as hours')
                    ->where('company_id', '=',$company->company_id)
                    ->first();
                $social = DB::table('brandfetch_primary')
                ->leftjoin('br_company_social_media', 'brandfetch_primary.br_id', '=', 'br_company_social_media.br_id')
                ->select('br_company_social_media.twitter as twitter', 'br_company_social_media.linkedin as linkedin', 'br_company_social_media.github as github', 'br_company_social_media.facebook as facebook', 'br_company_social_media.youtube as youtube', 'br_company_social_media.pinterest as pinterest', 'br_company_social_media.instagram as instagram')
                ->where('company_id',$company->company_id)
                ->first();
                $exists = DB::table('brandfetch_primary')->where('web_url',$company->company_url)->first();
                $st_seo=DB::table('st_seo')
                    ->select('good_headings','homepage_title_tag','has_content_for_every_heading','has_single_h1_on_each_page','detected_address')
                    ->where('st_id',$exists->br_id)
                    ->first();
                $metadata = DB::table('brandfetch_primary')
                ->leftjoin('br_website_meta_data', 'brandfetch_primary.br_id', '=', 'br_website_meta_data.br_id')
                ->select('br_website_meta_data.title as title', 'br_website_meta_data.summary as summary', 'br_website_meta_data.description as description', 'br_website_meta_data.keywords as keywords')
                ->where('company_id',$company->company_id)
                ->first();
                $images = DB::table('brandfetch_primary')
                        ->leftjoin('br_website_images', 'brandfetch_primary.br_id', '=', 'br_website_images.br_id')
                        ->where('company_id',$company->company_id)
                        ->pluck('br_website_images.image_url')
                        ->all();
                if(isset($images)){
                    $links['images'] = $images;
                }
                $exists = DB::table('silktide_prospect')
                ->join('company','company.company_id','silktide_prospect.company_id')
                ->where('company.company_id', $company->company_id)
                ->first();
                $data = DB::table('silktide_prospect')
                        ->leftjoin('st_sitecontent','silktide_prospect.st_id','=','st_sitecontent.st_id')
                        ->leftjoin('st_vitals','silktide_prospect.st_id','=','st_vitals.st_id')
                        ->leftjoin('st_discovered','silktide_prospect.st_id','=','st_discovered.st_id')
                        ->leftjoin('st_domain','silktide_prospect.st_id','=','st_domain.st_id')
                        ->leftjoin('st_domain_info','silktide_prospect.st_id','=','st_domain_info.st_id')
                        ->leftjoin('st_seo','silktide_prospect.st_id','=','st_seo.st_id')
                        ->leftjoin('st_images','silktide_prospect.st_id','=','st_images.st_id')
                        ->leftjoin('st_social','silktide_prospect.st_id','=','st_social.st_id')
                        ->leftjoin('st_video','silktide_prospect.st_id','=','st_video.st_id')
                        ->select('silktide_prospect.url','silktide_prospect.created_at','st_sitecontent.average_words_per_page as average_words','st_sitecontent.pages_found as pages_found','st_sitecontent.total_word_count as total_word_count','st_sitecontent.cms_solution as cms_solution','st_sitecontent.has_cms as has_cms','st_vitals.pages_tested as pages_tested','st_vitals.analytics_tool as analytics_tool','st_vitals.has_analytics as has_analytics','st_vitals.domain as domain','st_vitals.ecommerce_name as ecommerce_name','st_vitals.has_ecommerce as has_ecommerce','st_vitals.has_pixel as has_pixel','st_vitals.address_details_provided as address_details_provided','st_vitals.has_result as has_result','st_vitals.yext_api_success as yext_api_success','st_vitals.has_mobile_site as has_mobile_site','st_vitals.is_mobile as is_mobile','st_vitals.is_tablet as is_tablet','st_vitals.mobile_screenshot_url as mobile_screenshot_url','st_vitals.mobile_site_url as mobile_site_url','st_vitals.average_monthly_traffic as average_monthly_traffic','st_vitals.adwords_keywords as adwords_keywords','st_vitals.has_adwords_spend as has_adwords_spend','st_vitals.has_sitemap as has_sitemap','st_vitals.sitemap_issues as sitemap_issues','st_vitals.stale_analysis as stale_analysis','st_discovered.emails as emails','st_discovered.phones as phones','st_domain_info.domain_age_days as domain_age_days','st_domain_info.expiry_date as expiry_date','st_domain_info.registered_date as registered_date','st_seo.good_headings as good_headings','st_seo.has_content_for_every_heading as has_content_for_every_heading','st_seo.has_hierarchical_headings as has_hierarchical_headings','st_seo.has_single_h1_on_each_page as has_single_h1_on_each_page','st_seo.analysis_country as analysis_country','st_seo.detected_address as detected_address','st_seo.detected_name as detected_name','st_seo.detected_phone','st_seo.num_keywords_ranked_for','st_seo.top_keywords_ranked_for','st_seo.duplicated_items','st_seo.homepage_title_tag','st_seo.missing_items','st_seo.pages_duplicated_description_count','st_seo.pages_duplicated_title_count','st_seo.pages_missing_description_count','st_seo.pages_missing_title_count','st_seo.percent_duplicated_descriptions','st_seo.percent_duplicated_titles','st_seo.percent_missing_descriptions','st_seo.percent_missing_titles','st_images.image_count','st_images.non_web_friendly_count','st_images.percent_images_sized','st_images.stretched_image_count','st_social.has_instagram','st_social.days_since_update','st_social.last_updated_date','st_social.twitter_found','st_video.vendor_vendor','st_video.has_video','st_video.video_vendor','st_video.website_traffic','st_domain.is_parked','st_domain.has_ssl','st_domain.ssl_expired','st_domain.ssl_redirect','st_domain.ssl_valid')
                        ->where('silktide_prospect.st_id','=',$exists->st_id)
                        ->first();

                $widget=DB::table('cloudwidget')
                            ->select('widget_url')
                            ->where('cl_cid','=',$company->company_id)
                            ->where('widget_type','=',1)
                            ->first();
                $App_url='https://data.smart1.solutions/';
                $upload_widget=isset($widget)?$App_url.$widget->widget_url:'';
                $widget=DB::table('cloudwidget')
                    ->select('widget_url')
                    ->where('cl_cid','=',$company->company_id)
                    ->where('widget_type','=',3)
                    ->first();
                $customer_gallery=isset($widget)?$App_url.$widget->widget_url:'';
                $widget=DB::table('cloudwidget')
                    ->select('widget_url')
                    ->where('cl_cid','=',$company->company_id)
                    ->where('widget_type','=',4)
                    ->first();
                $maUpload=isset($widget)?$App_url.$widget->widget_url:'';
                $widget=$widget=DB::table('cloudwidget')
                ->select('widget_url')
                ->where('cl_cid','=',$company->company_id)
                ->where('widget_type','=',5)
                ->first();
                $maGallery=isset($widget)?$App_url.$widget->widget_url:'';
                $widget=DB::table('cloudwidget')
                    ->select('widget_url')
                    ->where('cl_cid','=',$company->company_id)
                    ->where('widget_type','=',6)
                    ->first();
                $CVUploadWidget=isset($widget)?$App_url.$widget->widget_url:'';
                $widget=DB::table('cloudwidget')
                    ->select('widget_url')
                    ->where('cl_cid','=',$company->company_id)
                    ->where('widget_type','=',7)
                    ->first();
                $CVGallery=isset($widget)?$App_url.$widget->widget_url:'';
                $data=json_encode(array(
                    'field_1'=>$company->company_id,
                    'field_2'=>$company->cname,
                    'field_3'=>$company->address1,
                    'field_4'=>$company->email,
                    'field_5'=>$company->phone,
                    'field_6'=>$company->company_url,
                    'field_7'=>$company->media_partner,
                    'field_8'=>$company->created_at,
                    'field_9'=>isset($design->logoColor)?$design->logoColor:'null',
                    'field_10'=>isset($design->siteFont)?$design->siteFont:'null',
                    'field_11'=>isset($design->primaryColor)?$design->primaryColor:'null',
                    'field_12'=>isset($design->secondaryColor)?$design->secondaryColor:'null',
                    'field_13'=>isset($pageUrls->page_count)?$pageUrls->page_count:0,
                    'field_14'=>isset($pageUrls->page_url)?$pageUrls->page_url:'null',
                    'field_15'=>isset($hours->hours)?$hours->hours:'null',
                    'field_16'=>isset($social->twitter)?$social->twitter:'null',
                    'field_17'=>isset($social->linkedin)?$social->linkedin:'null',
                    'field_18'=>isset($social->github)?$social->github:'null',
                    'field_19'=>isset($social->facebook)?$social->facebook:'null',
                    'field_20'=>isset($social->youtube)?$social->youtube:'null',
                    'field_21'=>isset($social->pinterest)?$social->pinterest:'null',
                    'field_22'=>isset($social->instagram)?$social->instagram:'null',
                    'field_23'=>isset($metadata->title)?$metadata->title:'null',
                    'field_24'=>isset($metadata->summary)?$metadata->summary:'null',
                    'field_25'=>isset($metadata->description)?$metadata->description:'null',
                    'field_26'=>isset($metadata->keywords)?$metadata->keywords:'null',
                    'field_27'=>isset($links)?$links['images']:'null',
                    'field_28'=>$links,
                    'field_29'=>$company->created_at,
                    'field_30'=>isset($data->average_words)?$data->average_words:'null',
                    'field_31'=>isset($data->pages_found)?$data->pages_found:'0',
                    'field_32'=>isset($data->total_word_count)?$data->total_word_count:'0',
                    'field_33'=>isset($data->cms_solution)?$data->cms_solution:'null',
                    'field_34'=>isset($data->has_cms)?$data->has_cms:'No',
                    'field_35'=>isset($data->pages_tested)?$data->pages_tested:'null',
                    'field_36'=>isset($data->analytics_tool)?$data->analytics_tool:'null',
                    'field_37'=>isset($data->has_analytics)?$data->has_analytics:'No',
                    'field_38'=>isset($data->domain)?$data->domain:'null',
                    'field_39'=>isset($data->ecommerce_name)?$data->ecommerce_name:'null',
                    'field_40'=>isset($data->has_ecommerce)?$data->has_ecommerce:'No',
                    'field_41'=>isset($data->has_pixel)?$data->has_pixel:'No',
                    'field_42'=>isset($data->address_details_provided)?$data->address_details_provided:'null',
                    'field_43'=>isset($data->has_result)?$data->has_result:'No',
                    'field_44'=>isset($data->yext_api_success)?$data->yext_api_success:'null',
                    'field_45'=>isset($data->has_mobile_site)?$data->has_mobile_site:'No',
                    'field_46'=>isset($data->is_mobile)?$data->is_mobile:'No',
                    'field_47'=>isset($data->is_tablet)?$data->is_tablet:'No',
                    'field_48'=>isset($data->mobile_screenshot_url)?$data->mobile_screenshot_url:'null',
                    'field_49'=>isset($data->mobile_site_url)?$data->mobile_site_url:'null',
                    'field_50'=>isset($data->average_monthly_traffic)?$data->average_monthly_traffic:'null',
                    'field_51'=>isset($data->adwords_keywords)?$data->adwords_keywords:'null',
                    'field_52'=>isset($data->has_adwords_spend)?$data->has_adwords_spend:'No',
                    'field_53'=>isset($data->has_sitemap)?$data->has_sitemap:'No',
                    'field_54'=>isset($data->sitemap_issues)?$data->sitemap_issues:'null',
                    'field_55'=>isset($data->stale_analysis)?$data->stale_analysis:'null',
                    'field_56'=>isset($data->emails)?$data->emails:'null',
                    'field_57'=>isset($data->phones)?$data->phones:'null',
                    'field_58'=>isset($data->domain_age_days)?$data->domain_age_days:'null',
                    'field_59'=>isset($data->expiry_date)?$data->expiry_date:'null',
                    'field_60'=>isset($data->registered_date)?$data->registered_date:'null',
                    'field_61'=>isset($data->good_headings)?$data->good_headings:'null',
                    'field_62'=>isset($data->has_content_for_every_heading)?$data->has_content_for_every_heading:'no',
                    'field_63'=>isset($data->has_hierarchical_headings)?$data->has_hierarchical_headings:'No',
                    'field_64'=>isset($data->has_single_h1_on_each_page)?$data->has_single_h1_on_each_page:'No',
                    'field_65'=>isset($data->analysis_country)?$data->analysis_country:'null',
                    'field_66'=>isset($data->detected_address)?$data->detected_address:'null',
                    'field_67'=>isset($data->detected_name)?$data->detected_name:'null',
                    'field_68'=>isset($data->detected_phone)?$data->detected_phone:'null',
                    'field_69'=>isset($data->num_keywords_ranked_for)?$data->num_keywords_ranked_for:'null',
                    'field_70'=>isset($data->top_keywords_ranked_for)?$data->top_keywords_ranked_for:'null',
                    'field_71'=>isset($data->duplicated_items)?$data->duplicated_items:'null',
                    'field_72'=>isset($data->homepage_title_tag)?$data->homepage_title_tag:'null',
                    'field_73'=>isset($data->missing_items)?$data->missing_items:'null',
                    'field_74'=>isset($data->pages_duplicated_description_count)?$data->pages_duplicated_description_count:'0',
                    'field_75'=>isset($data->pages_duplicated_title_count)?$data->pages_duplicated_title_count:'0',
                    'field_76'=>isset($data->pages_missing_description_count)?$data->pages_missing_description_count:'0',
                    'field_77'=>isset($data->pages_missing_title_count)?$data->pages_missing_title_count:'0',
                    'field_78'=>isset($data->percent_duplicated_descriptions)?$data->percent_duplicated_descriptions:'null',
                    'field_79'=>isset($data->percent_duplicated_titles)?$data->percent_duplicated_titles:'null',
                    'field_80'=>isset($data->percent_missing_descriptions)?$data->percent_missing_descriptions:'null',
                    'field_81'=>isset($data->percent_missing_titles)?$data->percent_missing_titles:'null',
                    'field_82'=>isset($data->image_count)?$data->image_count:'0',
                    'field_83'=>isset($data->non_web_friendly_count)?$data->non_web_friendly_count:'0',
                    'field_84'=>isset($data->percent_images_sized)?$data->percent_images_sized:'null',
                    'field_85'=>isset($data->stretched_image_count)?$data->stretched_image_count:'0',
                    'field_86'=>isset($data->has_instagram)?$data->has_instagram:'No',
                    'field_87'=>isset($data->days_since_update)?$data->days_since_update:'0',
                    'field_88'=>isset($data->last_updated_date)?$data->last_updated_date:'null',
                    'field_89'=>isset($data->twitter_found)?$data->twitter_found:'null',
                    'field_90'=>isset($data->vendor_vendor)?$data->vendor_vendor:'null',
                    'field_91'=>isset($data->has_video)?$data->has_video:'null',
                    'field_92'=>isset($data->video_vendor)?$data->video_vendor:'null',
                    'field_93'=>isset($data->website_traffic)?$data->website_traffic:'null',
                    'field_94'=>isset($data->is_parked)?$data->is_parked:'No',
                    'field_95'=>isset($data->has_ssl)?$data->has_ssl:'No',
                    'field_96'=>isset($data->ssl_expired)?$data->ssl_expired:'null',
                    'field_97'=>isset($data->ssl_redirect)?$data->ssl_redirect:'',
                    'field_98'=>isset($data->ssl_valid)?$data->ssl_valid:'null',
                    'field_337'=>$company->Development_link,
                    'field_338'=>$company->Client_number,
                    'field_339'=>$company->password2,
                    'field_340'=>'https://data.smart1.solutions/'.str_replace(' ','_',$company->cname).'_'.$company->zip,
                    'field_341'=>'NA',
                    'field_342'=>isset($upload_widget)?$upload_widget:'',
                    'field_343'=>isset($customer_gallery)?$customer_gallery:'',
                    'field_344'=>'NA',
                    'field_345'=>isset($maUpload)?$maUpload:'',
                    'field_346'=>isset($maGallery)?$maGallery:'',
                    'field_347'=>'NA',
                    'field_348'=>isset($CVUploadWidget)?$CVUploadWidget:'',
                    'field_349'=>isset($CVGallery)?$CVGallery:'',
                ));
                $curl = curl_init();

                curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.knack.com/v1/objects/object_1/records/".$company->knackID,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "PUT",
                CURLOPT_POSTFIELDS =>$data,
                CURLOPT_HTTPHEADER => array(
                    "X-Knack-Application-Id: 5f7e2af82c55b5001636420b",
                    "X-Knack-REST-API-Key: d2541fdd-43c2-4d4d-9350-c1b787bfe87c",
                    "Content-Type: application/json",
                    "Cookie: connect.sid=s%3A-599MYE7MII1h8OXi_XdmlzFcZ3oGfdp.v%2BU8UBVXsphpnw5W6ymm6%2Ft0RyKKY%2FXgL8FkXrNvDdY"
                ),
                ));
                $response = curl_exec($curl);
                curl_close($curl);
                $resp['status']=200;
                $resp['message']='Success';
                return response()->json($resp,200);
            }

        }
        catch(QueryException $e){
            $errorMessage=explode(' ',$e->getMessage());
            $errorNumber=$errorMessage[4];
            switch($errorNumber){
                case '1048':
                    $response['status']=1048;
                    $response['message']=$errorMessage[6].' '.$errorMessage[7].' '.$errorMessage[8].' '.$errorMessage[9];
                    return response()->json($response,417);
                break;
                case '1062':
                    $response['status']=1062;
                    $response['message']= 'ID already exists';
                    return response()->json($response,409);
                break;
                case '1054':
                    $response['status']=1054;
                    $response['message']= $errorMessage[5].' '.$errorMessage[6].' '.$errorMessage[7];
                    return response()->json($response,417);
                break;
                default:
                    $response['status']=1065;
                    $response['message']= 'Failed';
                    return response()->json($response,417);
            }
        }
        catch(Exception $ex){
            $response['message']='Failed';
            $response['status']=405;
            return response()->json($response,405);
        }
    }
}
