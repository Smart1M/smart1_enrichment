<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menu = DB::table('menu_basic')
                ->leftjoin('permissions','menu_basic.menu_perm_name','=','permissions.id')
                ->select('menu_basic.menu_id','menu_basic.menu_name','menu_basic.menu_type','menu_basic.menu_perm_name','menu_basic.menu_slug','menu_basic.menu_icon','menu_basic.is_sub','menu_basic.parent_id','menu_basic.menu_order','menu_basic.active_status','permissions.id','permissions.name')
                ->where('menu_basic.menu_type','=',1)
                ->orderBy('menu_basic.menu_order')
                ->get();

        return view('menu',compact('menu'));
    }

    public function fetchsubmenu(Request $request)
    {
        $menu_id = $request->menuid;
        $submenu = DB::table('menu_basic')
                    ->select('menu_id','menu_name','menu_slug','menu_order','active_status')
                    ->where('parent_id','=',$menu_id)
                    ->orderBy('menu_order')
                    ->get();

        //echo '<pre>'; print_r($submenu); die();  
        return json_encode($submenu);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parent_menu = DB::table('menu_basic')
                    ->where([
                                ['active_status', '=', '0'],
                                ['menu_type', '=', '1'],
                            ])
                    ->select('menu_id','menu_name')
                    ->get();

        $permissions = DB::table('permissions')
                    ->where('pstatus', '=', '0')
                    ->select('id','name')
                    ->get();

        return view('addmenu',compact('parent_menu','permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth()->user();
        $user_id = $user->id;

        $current_date = date('Y-m-d H:i:s');
        $is_sub = 0;
        $menu_name = $request->menu_name;
        $menu_type = $request->menu_type;
        $menu_permission = $request->menu_permission;
        $menu_slug = $request->menu_slug;
        $menu_order = $request->menu_order;
        $parent_menu = $request->parent_menu;
        $menu_icon = $request->menu_icon;

        // if($menu_type == 2)
        // {
        //     $is_sub = 1;
        // }
        $menu_id = DB::table('menu_basic')
            ->insertGetId(['menu_name' => $request->menu_name,'menu_type'=>$request->menu_type,'menu_perm_name'=>$request->menu_permission,'menu_slug'=> $request->menu_slug,'menu_icon'=>$menu_icon,'is_sub'=>$is_sub,'parent_id'=>$parent_menu,'menu_order'=>$request->menu_order,'created_date'=>$current_date,'added_by'=>$user_id]);

        if($parent_menu!=0)
        {
            $is_sub = 1;
            DB::table('menu_basic')
                ->where('menu_id', $parent_menu)
                ->update(['is_sub' => 1]);
        }

        return "1";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $parent_menu = DB::table('menu_basic')
                    ->where([
                                ['active_status', '=', '0'],
                                ['menu_type', '=', '1'],
                            ])
                    ->select('menu_id','menu_name')
                    ->get();

        $permissions = DB::table('permissions')
                    ->where('pstatus', '=', '0')
                    ->select('id','name')
                    ->get();

        $menu_det = DB::table('menu_basic')
                        ->where('menu_id',$id)
                        ->first();

        return view('editmenu',compact('menu_det','parent_menu','permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = auth()->user();
        $user_id = $user->id;

        $menu_id = $request->menu_id;

        $current_date = date('Y-m-d H:i:s');
        $is_sub = 0;
        $menu_name = $request->menu_name;
        $menu_type = $request->menu_type;
        $menu_permission = $request->menu_permission;
        $menu_slug = $request->menu_slug;
        $menu_order = $request->menu_order;
        $parent_menu = $request->parent_menu;

        $pre = DB::table('menu_basic')
                            ->where('menu_id', $menu_id)
                            ->select('parent_id')
                            ->first();

        $pre_parent_id = $pre->parent_id;
           

        DB::table('menu_basic')
            ->where('menu_id', $menu_id)
            ->update(['menu_name' => $request->menu_name,'menu_type'=>$request->menu_type,'menu_perm_name'=>$request->menu_permission,'menu_slug'=> $request->menu_slug,'is_sub'=>$is_sub,'parent_id'=>$parent_menu,'menu_order'=>$request->menu_order,'updated_date'=>$current_date,'added_by'=>$user_id]);

        /* Check whether any sub menu exist for previous parent id, if no update the coloum to 0 */

        $sub_exist_count = DB::table('menu_basic')
                                ->where('parent_id','=',$pre_parent_id)
                                ->count();   

        if($sub_exist_count==0)
        {
            DB::table('menu_basic')
                ->where('menu_id','=',$pre_parent_id)
                ->update(['is_sub' => 0]);
        } 

        /* Check whether the current parent id `is_sub` coloum in 1, if not update it to 1 */

        $check_curr_menu_substatus = DB::table('menu_basic')
                                        ->where('menu_id','=',$parent_menu)
                                        ->where('is_sub','=',1)
                                        ->count();

        if($check_curr_menu_substatus==0)
        {
            DB::table('menu_basic')
                ->where('menu_id','=',$parent_menu)
                ->update(['is_sub' => 1]);
        }                               

        return "1";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->menuid;
        $menu_det = DB::table('menu_basic')
                        ->where('menu_id',$id)
                        ->first();

        $is_sub = $menu_det->is_sub;

        $current_date = date('Y-m-d H:i:s');
        DB::table('menu_basic')
            ->where('menu_id', $id)
            ->update(['active_status' => 1,'updated_date'=>$current_date]);
        
        if($is_sub==1)
        {
            DB::table('menu_basic')
                ->where('parent_id', $id)
                ->update(['active_status' => 1,'updated_date'=>$current_date]);
        } 

        return "1";
    }
}
