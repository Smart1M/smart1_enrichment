<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CompanyLoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';
    protected $guard = 'company';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function username()
    {
        return 'cname';
    }
    protected function credentials(Request $request)
    {  
       return ['cname' => $request->{$this->username()}, 'password' => $request['password'],'zip'=>$request->zip];
    }
    public function showLoginForm(Request $request)
    {
        $clink=$request->route('cname');
        $clink_arr=explode('_',$clink);
        $zip=array_pop($clink_arr);
        $cname='';
        foreach($clink_arr as $s)
        {
            $cname=$cname.' '.$s;
        }
        return view('company.auth.login',compact('cname','zip'));
    }
    /**
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'password' => 'required',
        ]);
    }
    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        if(Auth::guard('company')->attempt($this->credentials($request)))
        {   
            return redirect(route('company.customergallery'));
        }
        $clink=$request->route('cname');
        $clink_arr=explode('_',$clink);
        $zip=array_pop($clink_arr);
        $cname='';
        foreach($clink_arr as $s)
        {
            $cname=$cname.' '.$s;
        }
        $logerror="*Invalid URL or password!";
        return redirect()->back()->with('logerror',$logerror);
    }
 /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        if (Auth::guard('company')->check())
        {
            Auth::guard('company')->logout();
            $cname=str_replace(' ','_',$request->cname);
            $zip=$request->zip;
            $redirectPath='/'.$cname.'_'.$zip;
            return redirect($redirectPath);
        }
    }


}