<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Log;
use PDF;
use Cloudder;
use Storage;

class BrandfetchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function reports($id)
    {

        $brandfetchinfo = DB::table('brandfetch_primary')
                            ->where('company_id',$id)
                            ->get();

        return view('brandfetchreports',compact('brandfetchinfo'));
    }
    public function index()
    {

        $company = DB::table('company')
                    ->select('company_id', 'cname')
                    ->where('status', '==', 0)
                    ->get();

        $brandfetchinfo = DB::table('brandfetch_primary')
                            ->select('company_id','web_url',DB::raw('COUNT(company_id) as records'))
                            ->groupBy('company_id')
                            ->groupBy('web_url')
                            ->get();

        return view('brandfetch',compact('brandfetchinfo','company'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(Request $request){

        $datetime = date('Y-m-d H:i:s');
        ini_set('max_execution_time', 300);
        set_time_limit(300);
        /*--- API to fetch Website logo ---*/

        $user       = auth()->user();
        $user_id    = $user->id;

        $web_url    = $request->web_url;
        $company_id = $request->company_id;

        $comp_name  = DB::table('company')
                    ->select('cname')
                    ->where('company_id', '=', $company_id)
                    ->first();


        $data           =  json_encode(array("domain" => $web_url));
        $logo_api_url   = 'https://api.brandfetch.io/v1/logo';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_URL, $logo_api_url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
          'x-api-key: oouXaXg0zf77frXoUmsWp9ncqgdHwyaT6BlOaB3a',
          'Content-Type: application/json',
        ));
        curl_setopt($curl, CURLOPT_TIMEOUT, 400);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        $result = curl_exec($curl);
        curl_close($curl);
        $res_array = json_decode($result);
        $res_array = get_object_vars($res_array);
        if($res_array['statusCode'] != 200) {
            $r['error'] = $res_array['response'];
            return $r;
        }else{
            /*--- Save to 'brandfetch_primary' table ---*/

            $br_Id = DB::table('brandfetch_primary')
                 ->insertGetId([
                     'web_url' => $web_url,
                     'created_at' => $datetime,
                     'company_id' => $company_id,
                     'user_id' => $user_id
                 ]);



            $web_logo = $res_array['response']->logo->image;
            ;
            // Getting page header data
            $array_weblogo = @get_headers($web_logo);
            $string_logo = $array_weblogo[0];
            if (strpos($string_logo, "200")) {
                $comp_name->cname = preg_replace('~[?&#\%<>]~', '-', $comp_name->cname);
                $upload = Cloudder::upload($web_logo, 'Joshua/'.$comp_name->cname."_Joshua/logo_br_".uniqid());
                if ($upload) {
                    $brlogo_picId = Cloudder::getPublicId();
                    $tag = str_replace(' ', '_', $comp_name->cname);
                    Cloudder::addTag($tag, $brlogo_picId);
                } else {
                    $brlogo_picId = "Not uploaded";
                }
            } else {
                $brlogo_picId = "Broken URL";
            }

            $web_log_message = "";
            $message = "";

            $message = "Saved primary data to bradfetch_primary. ID: ".$br_Id;
            Log::info($message);

            /*--- Save to 'br_logo' table ---*/
            if (!empty($res_array)) {
                DB::table('br_logo')
                    ->updateOrInsert(
                        ['br_id' => $br_Id],
                        ['web_logo' => $web_logo,
                        'br_cat_id' => 1,
                        'pic_id'=>$brlogo_picId,
                        'created_at' => $datetime
                    ]
                    );

                $web_log_message = "Api to fetch website logo successfull. Saved to 'br_logo'";
                Log::info($message);
            }
        }
        // }else {
        //     $web_logo = "";
        //     $web_log_message = "Api to fetch website logo. No data retireved";
        //     Log::error($web_log_message);
        // }

        /*--- API to fetch Website colour ---*/
        $web_colour_message = "";
        $web_colour_api_url = 'https://api.brandfetch.io/v1/color';

        $ch_web_colour = curl_init();
        curl_setopt($ch_web_colour, CURLOPT_POST, 1);
        curl_setopt($ch_web_colour, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch_web_colour, CURLOPT_URL, $web_colour_api_url);
        curl_setopt($ch_web_colour, CURLOPT_HTTPHEADER, array(
          'x-api-key: oouXaXg0zf77frXoUmsWp9ncqgdHwyaT6BlOaB3a',
          'Content-Type: application/json',
        ));
        curl_setopt($ch_web_colour, CURLOPT_TIMEOUT, 400);
        curl_setopt($ch_web_colour, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch_web_colour, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);


        $result_web_colour = curl_exec($ch_web_colour);
        curl_close($ch_web_colour);

        $res_web_colour_array = json_decode($result_web_colour);
        $res_web_colour_array = get_object_vars($res_web_colour_array);
        if($res_web_colour_array['statusCode']==200){

            $web_colour_category = (isset($res_web_colour_array['response']->filtered)) ? $res_web_colour_array['response']->filtered : '';
            $web_colour_raw = (isset($res_web_colour_array['response']->raw)) ? $res_web_colour_array['response']->raw : '';
            foreach($web_colour_category as $key => $value){
                $web_cc[] = $value;
            }

            DB::table('br_web_colour_category')
                 ->updateOrInsert(['br_id' => $br_Id],
                    ['vibrant' => $web_cc[2],
                    'dark' => $web_cc[0],
                    'light' => $web_cc[1],
                    'br_cat_id' => 1,
                    'created_at' => $datetime
                ]);

            if(!empty($web_colour_raw))
            {
                foreach($web_colour_raw as $k => $v){
                    DB::table('br_web_colour_raw')
                    ->updateOrInsert(['br_id' => $br_Id,'colour_code' => $v->color],
                        ['percentage' => $v->percentage,
                        'br_cat_id' => 1,
                        'created_at' => $datetime
                    ]);
                }
            }

            $web_colour_message = "Website Color api successfull.  Data inserted to 'br_web_colour_category' and 'br_web_colour_raw'";
            Log::info($web_colour_message);

        } else {
            $web_colour_message = "Website Color api. No data fetched";
            Log::error($web_colour_message);
        }

        /*--- API to fetch Logo Colour ---*/
        if($web_logo){
            $logo_colour_message = "";

            $logo_colour_api_url = 'https://api.brandfetch.io/v1/color-api/get-colors-from-logo?';
            $logo_url = $web_logo;
            $logo_data =  json_encode(array("image" => $logo_url));

            $ch_logo_colour = curl_init();
            curl_setopt($ch_logo_colour, CURLOPT_POST, 1);
            curl_setopt($ch_logo_colour, CURLOPT_POSTFIELDS, $logo_data);
            curl_setopt($ch_logo_colour, CURLOPT_URL, $logo_colour_api_url);
            curl_setopt($ch_logo_colour, CURLOPT_HTTPHEADER, array(
            'x-api-key: oouXaXg0zf77frXoUmsWp9ncqgdHwyaT6BlOaB3a',
            'Content-Type: application/json',
            ));
            curl_setopt($ch_logo_colour, CURLOPT_TIMEOUT, 400);
            curl_setopt($ch_logo_colour, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch_logo_colour, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

            $result_logo_colour = curl_exec($ch_logo_colour);
            curl_close($ch_logo_colour);

            $res_logo_colour_array = json_decode($result_logo_colour);
            $res_logo_colour_array = get_object_vars($res_logo_colour_array);
            if(isset($res_logo_colour_array['statusCode']))
            {
                $logo_colour_category = $res_logo_colour_array['response']->filtered;
                $logo_colour_raw = $res_logo_colour_array['response']->raw;
                $logo_colour_bg = $res_logo_colour_array['response']->background;

                foreach($logo_colour_category as $key => $value){
                    $logo_cc[] = $value;
                }
                $logo_bg_colour = "";

                if(is_object($logo_colour_bg))
                {
                    foreach($logo_colour_bg as $k => $v){
                        $logo_bg[] = $v;
                    }

                    $logo_bg_colour = $logo_bg[0];
                } else {
                    $logo_bg_colour = $logo_colour_bg;
                }


                DB::table('br_logo_colour_category')
                    ->updateOrInsert(['br_id' => $br_Id],
                    ['vibrant' => $logo_cc[0],
                        'dark' => $logo_cc[1],
                        'light' => $logo_cc[2],
                        'br_cat_id' => 1,
                        'created_at' => $datetime
                    ]);

                foreach($logo_colour_raw as $k => $v){
                    DB::table('br_logo_colour_raw')
                        ->updateOrInsert(['br_id' => $br_Id,'l_colour_code' => $v->color],
                        ['l_percentage' => $v->percentage,
                        'br_cat_id' => 1,
                        'created_at' => $datetime
                    ]);
                }

                DB::table('br_logo_background')
                    ->updateOrInsert(['br_id' => $br_Id],
                        ['bg_color' => $logo_bg_colour,
                        'br_cat_id' => 1,
                        'created_at' => $datetime
                    ]);

                $logo_colour_message = "Logo Color api successfull.  Data inserted to 'br_logo_colour_category','br_logo_colour_raw' and 'br_logo_background'";
                Log::info($logo_colour_message);
            } else {
                $logo_colour_message = "Logo Color api. No data fetched";
                Log::error($logo_colour_message);
            }
        }

        /* --- API to fetch Wesite Fonts --- */

        $web_font_api_url = 'https://api.brandfetch.io/v1/font';
        $font_data =  json_encode(array("domain" => $web_url,"fresh" => false));

        $ch_web_font = curl_init();
        curl_setopt($ch_web_font, CURLOPT_POST, 1);
        curl_setopt($ch_web_font, CURLOPT_POSTFIELDS, $font_data);
        curl_setopt($ch_web_font, CURLOPT_URL, $web_font_api_url);
        curl_setopt($ch_web_font, CURLOPT_HTTPHEADER, array(
          'x-api-key: oouXaXg0zf77frXoUmsWp9ncqgdHwyaT6BlOaB3a',
          'Content-Type: application/json',
        ));
        curl_setopt($ch_web_font, CURLOPT_TIMEOUT, 400);
        curl_setopt($ch_web_font, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch_web_font, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        $result_web_font = curl_exec($ch_web_font);
        curl_close($ch_web_font);

        $res_web_font_array = json_decode($result_web_font);
        $res_web_font_array = get_object_vars($res_web_font_array);

        if(isset($res_web_font_array)&&!empty($res_web_font_array)){
            if ($res_web_font_array['statusCode']==200) {
                    $font_array = json_decode(json_encode($res_web_font_array['response']), True);
                        DB::table('br_website_font')
                            ->updateOrInsert(['br_id' => $br_Id],
                                ['title_tag' => $font_array["title"]["font"],
                                'primary_font' =>$font_array["paragraph"]["font"],
                                'br_cat_id' => 2,
                                'created_at' => $datetime,
                            ]);
            }

        }

        /*--- API to fetch Wesite Images ---*/

        $web_images_api_url = 'https://api.brandfetch.io/v1/image';
        $web_img_data =  json_encode(array("domain" => $web_url,"fresh" => false));

        $ch_web_images = curl_init();
        curl_setopt($ch_web_images, CURLOPT_POST, 1);
        curl_setopt($ch_web_images, CURLOPT_POSTFIELDS, $web_img_data);
        curl_setopt($ch_web_images, CURLOPT_URL, $web_images_api_url);
        curl_setopt($ch_web_images, CURLOPT_HTTPHEADER, array(
          'x-api-key: oouXaXg0zf77frXoUmsWp9ncqgdHwyaT6BlOaB3a',
          'Content-Type: application/json',
        ));
        curl_setopt($ch_web_images, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch_web_images, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch_web_images, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        $result_web_images = curl_exec($ch_web_images);
        curl_close($ch_web_images);

        $res_web_images_array = json_decode($result_web_images);
        $res_web_images_array = get_object_vars($res_web_images_array);

        if($res_web_images_array['statusCode']==200)
        {

            $websiteimages="";
            $i=1;
            foreach($res_web_images_array['response'] as $key => $value){

                $web_img_array = json_decode(json_encode($value), True);
                /* Uploading the website images to cloudinary in `$comp_name->cname` folder */
                $array_webimages = @get_headers($web_img_array['image']);
                $string_webimages = $array_webimages[0];

                if(strpos($string_webimages, "200"))
                {
                    $comp_name->cname = preg_replace('~[?&#\%<>]~','-',$comp_name->cname);
                    $upload_webimg = Cloudder::upload($web_img_array['image'],'Joshua/'.$comp_name->cname."_Joshua/web_img_br_".uniqid());

                    if($upload_webimg)
                    {
                        $brwebimg_picId = Cloudder::getPublicId();
                        $tag = str_replace(' ', '_', $comp_name->cname);
                        Cloudder::addTag($tag, $brwebimg_picId);
                    } else
                    {
                        $brwebimg_picId = "Not uploaded";
                    }
                } else
                {
                    $brwebimg_picId = "Broken URL";
                }

                if($i>1){
                    $websiteimages=$websiteimages.",".$web_img_array['image'];
                }
                else{
                    $websiteimages=$web_img_array['image'];
                }


                DB::table('br_website_images')
                    ->updateOrInsert(['br_id' => $br_Id,'image_url' => $web_img_array['image']],
                        ['br_cat_id' => 1,
                        'webimg_pic_id' => $brwebimg_picId,
                        'created_at' => $datetime
                    ]);

                $i++;
            }

            $filepath = $comp_name->cname.'_'.time().'_'."_Joshua.txt";

            Storage::put($filepath, $websiteimages);

           // $file_access_path=$_SERVER['SERVER_NAME']."/joshua/smartoneapp/storage/app/".$filepath;

                     DB::table('br_image_path')
                            ->insert([
                                'br_id' => $br_Id,
                                'file_path' => $filepath
                            ]);
        }

        /*--- API to fetch Wesite Screenshot ---*/

        $web_scn_api_url = 'https://api.brandfetch.io/v1/screenshot';
        $web_scn_data =  json_encode(array("url" => $web_url,"fresh" => false));

        $ch_web_scn_images = curl_init();
        curl_setopt($ch_web_scn_images, CURLOPT_POST, 1);
        curl_setopt($ch_web_scn_images, CURLOPT_POSTFIELDS, $web_scn_data);
        curl_setopt($ch_web_scn_images, CURLOPT_URL, $web_scn_api_url);
        curl_setopt($ch_web_scn_images, CURLOPT_HTTPHEADER, array(
          'x-api-key: oouXaXg0zf77frXoUmsWp9ncqgdHwyaT6BlOaB3a',
          'Content-Type: application/json',
        ));
        curl_setopt($ch_web_scn_images, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch_web_scn_images, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch_web_scn_images, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);


        $result_web_scn_images = curl_exec($ch_web_scn_images);
        curl_close($ch_web_scn_images);

        $res_web_scn_images_array = json_decode($result_web_scn_images);
        $res_web_scn_images_array = get_object_vars($res_web_scn_images_array);
        if(isset($res_web_scn_images_array['statusCode']) && $res_web_scn_images_array['statusCode'] != '200'){
            Log::error($res_web_scn_images_array['response']." for ".$comp_name->cname);
            $r['error'] = $res_web_scn_images_array['response']." for ".$comp_name->cname;
        }
        if($res_web_scn_images_array['statusCode']==200)
        {
            /* Uploading the website screenshot to cloudinary in `$comp_name->cname` folder */
            $array_webscnimages = @get_headers($res_web_scn_images_array['response']->image);
            $string_webscnimages = $array_webscnimages[0];

            if(strpos($string_webscnimages, "200"))
            {
                $comp_name->cname = preg_replace('~[?&#\%<>]~','-',$comp_name->cname);
                $upload_websc = Cloudder::upload($res_web_scn_images_array['response']->image,'Joshua/'.$comp_name->cname."_Joshua/web_scn_br_".uniqid());

                if($upload_websc)
                {
                    $brwebscn_picId = Cloudder::getPublicId();
                    $tag = str_replace(' ', '_', $comp_name->cname);
                    Cloudder::addTag($tag, $brwebscn_picId);
                } else
                {
                    $brwebscn_picId = "Not uploaded";
                }
            } else
            {
                $brwebscn_picId = "Broken URL";
            }
                DB::table('br_website_screenshot')
                    ->updateOrInsert(['br_id' => $br_Id],
                        ['scn_url' => $res_web_scn_images_array['response']->image,
                        'br_cat_id' => 1,
                        'webscn_pic_id' => $brwebscn_picId,
                        'created_at' => $datetime
                    ]);

        }

        /*--- API to fetch Wesite Metadata ---*/

        $web_meta_api_url = 'https://api.brandfetch.io/v1/company';
        $web_meta_data =  json_encode(array("domain" => $web_url,"fresh" => true));

        $ch_web_meta = curl_init();
        curl_setopt($ch_web_meta, CURLOPT_POST, 1);
        curl_setopt($ch_web_meta, CURLOPT_POSTFIELDS, $web_meta_data);
        curl_setopt($ch_web_meta, CURLOPT_URL, $web_meta_api_url);
        curl_setopt($ch_web_meta, CURLOPT_HTTPHEADER, array(
          'x-api-key: oouXaXg0zf77frXoUmsWp9ncqgdHwyaT6BlOaB3a',
          'Content-Type: application/json',
        ));
        curl_setopt($ch_web_meta, CURLOPT_TIMEOUT, 280);
        curl_setopt($ch_web_meta, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch_web_meta, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch_web_meta, CURLOPT_CONNECTTIMEOUT, 0);

        $result_web_meta = curl_exec($ch_web_meta);
        curl_close($ch_web_meta);

        $res_web_meta_array = json_decode($result_web_meta);
        $res_web_meta_array = get_object_vars($res_web_meta_array);

        //if($res_web_meta_array['statusCode'] != "404")

        if($res_web_meta_array['statusCode']==200)
        {
          //  print_r($res_web_meta_array);
            DB::table('br_website_meta_data')
            ->updateOrInsert(['br_id' => $br_Id],
                ['title' => $res_web_meta_array['response']->name,
                // 'summary' => $res_web_meta_array['response']->summary,
                'description' => $res_web_meta_array['response']->description,
                'keywords' => $res_web_meta_array['response']->keywords,
                'language' => $res_web_meta_array['response']->language,
                'br_cat_id' => 3,
                'created_at' => $datetime
            ]);
            if(isset($res_web_meta_array['response']->twitter->url)) { $twitter = $res_web_meta_array['response']->twitter->url; } else { $twitter = ""; }
            if(isset($res_web_meta_array['response']->linkedin->url)) { $linkedin = $res_web_meta_array['response']->linkedin->url; } else { $linkedin = ""; }
            if(isset($res_web_meta_array['response']->github->url)) { $github = $res_web_meta_array['response']->github->url; } else { $github = ""; }
            if(isset($res_web_meta_array['response']->instagram->url)) { $instagram = $res_web_meta_array['response']->instagram->url; } else { $instagram = ""; }
            if(isset($res_web_meta_array['response']->facebook->url)) { $facebook = $res_web_meta_array['response']->facebook->url; } else { $facebook = ""; }
            if(isset($res_web_meta_array['response']->youtube->url)) { $youtube = $res_web_meta_array['response']->youtube->url; } else { $youtube = ""; }
            if(isset($res_web_meta_array['response']->pinterest->url)) { $pinterest = $res_web_meta_array['response']->pinterest->url; } else { $pinterest = ""; }
            if(isset($res_web_meta_array['response']->crunchbase->url)) { $crunchbase = $res_web_meta_array['response']->crunchbase->url; } else { $crunchbase = ""; }

            DB::table('br_company_social_media')
                ->updateOrInsert(['br_id' => $br_Id],
                        ['twitter' => $twitter,
                        'linkedin' => $linkedin,
                        'github' => $github,
                        'instagram' => $instagram,
                        'facebook' => $facebook,
                        'youtube' => $youtube,
                        'pinterest' => $pinterest,
                        'crunchbase' => $crunchbase,
                        'br_cat_id' => 4,
                        'created_at' => $datetime
                    ]);
        }

        return '1';
    }



    public function store_bkp(Request $request)
    {
        $datetime = date('Y-m-d H:i:s');

        /*--- API to fetch Website logo ---*/

        $web_url = $request->web_url;

        $data =  json_encode(array("domain" => $web_url));
        $logo_api_url = 'https://api.brandfetch.io/v1/logo';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_URL, $logo_api_url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
          'x-api-key: oouXaXg0zf77frXoUmsWp9ncqgdHwyaT6BlOaB3a',
          'Content-Type: application/json',
        ));
        curl_setopt($curl, CURLOPT_TIMEOUT, 400);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);


        $result = curl_exec($curl);

        if(!$result){die("Connection Failure");}
        curl_close($curl);
        $res_array = json_decode($result);
        $res_array = get_object_vars($res_array);
        $web_logo = $res_array['response']->logo->image;;


        /* --- Save to 'brandfetch_primary' table --- */

        $br_Id = DB::table('brandfetch_primary')
            ->insertGetId([
                'web_url' => $web_url,
                'created_at' => $datetime
            ]);

        $web_log_message = "";
        $message = "";

        $message = "Saved primary data to bradfetch_primary. ID: ".$br_Id;
        Log::info($message);

        /* --- Save to 'br_logo' table --- */

        if(!empty($res_array))
        {
            DB::table('br_logo')
            ->insert([
                'br_id' => $br_Id,
                'web_logo' => $web_logo,
                'br_cat_id' => 1,
                'created_at' => $datetime
            ]);

        $web_log_message = "Api to fetch website logo successfull. Saved to 'br_logo'";
        Log::info($message);
        } else {
        $web_log_message = "Api to fetch website logo. No data retireved";
        Log::error($web_log_message);
        }


        /*--- API to fetch Website colour ---*/

        $web_colour_message = "";
        $web_colour_api_url = 'https://api.brandfetch.io/v1/color';

        $ch_web_colour = curl_init();
        curl_setopt($ch_web_colour, CURLOPT_POST, 1);
        curl_setopt($ch_web_colour, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch_web_colour, CURLOPT_URL, $web_colour_api_url);
        curl_setopt($ch_web_colour, CURLOPT_HTTPHEADER, array(
          'x-api-key: oouXaXg0zf77frXoUmsWp9ncqgdHwyaT6BlOaB3a',
          'Content-Type: application/json',
        ));
        curl_setopt($ch_web_colour, CURLOPT_TIMEOUT, 400);
        curl_setopt($ch_web_colour, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch_web_colour, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);


        $result_web_colour = curl_exec($ch_web_colour);

        if(!$result_web_colour){die("Connection Failure");}
        curl_close($ch_web_colour);
        $res_web_colour_array = json_decode($result_web_colour);
        $res_web_colour_array = get_object_vars($res_web_colour_array);

        if($res_web_colour_array['statusCode']==200){

        $web_colour_category = $res_web_colour_array['response']->filtered;
        $web_colour_raw = $res_web_colour_array['response']->raw;

        foreach($web_colour_category as $key => $value){
            $web_cc[] = $value;
        }

        DB::table('br_web_colour_category')
            ->insert([
                'br_id' => $br_Id,
                'vibrant' => $web_cc[2],
                'dark' => $web_cc[0],
                'light' => $web_cc[1],
                'br_cat_id' => 1,
                'created_at' => $datetime
            ]);

        foreach($web_colour_raw as $k => $v){
            DB::table('br_web_colour_raw')
            ->insert([
                'br_id' => $br_Id,
                'colour_code' => $v->color,
                'percentage' => $v->percentage,
                'br_cat_id' => 1,
                'created_at' => $datetime
            ]);
        }

            $web_colour_message = "Website Color api successfull.  Data inserted to 'br_web_colour_category' and 'br_web_colour_raw'";
            Log::info($web_colour_message);

        } else {
            $web_colour_message = "Website Color api. No data fetched";
            Log::error($web_colour_message);
        }


        /* --- API to fetch Logo Colour --- */

        $logo_colour_message = "";

        $logo_colour_api_url = 'https://api.brandfetch.io/v1/color-api/get-colors-from-logo?';
        $logo_url = $web_logo;
        $logo_data =  json_encode(array("image" => $logo_url));

        $ch_logo_colour = curl_init();
        curl_setopt($ch_logo_colour, CURLOPT_POST, 1);
        curl_setopt($ch_logo_colour, CURLOPT_POSTFIELDS, $logo_data);
        curl_setopt($ch_logo_colour, CURLOPT_URL, $logo_colour_api_url);
        curl_setopt($ch_logo_colour, CURLOPT_HTTPHEADER, array(
          'x-api-key: oouXaXg0zf77frXoUmsWp9ncqgdHwyaT6BlOaB3a',
          'Content-Type: application/json',
        ));
        curl_setopt($ch_logo_colour, CURLOPT_TIMEOUT, 400);
        curl_setopt($ch_logo_colour, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch_logo_colour, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);


        $result_logo_colour = curl_exec($ch_logo_colour);

        if(!$result_logo_colour){die("Connection Failure");}
        curl_close($ch_logo_colour);
        $res_logo_colour_array = json_decode($result_logo_colour);
        $res_logo_colour_array = get_object_vars($res_logo_colour_array);

        if(!empty($res_logo_colour_array))
        {
            $logo_colour_category = $res_logo_colour_array['response']->filtered;
            $logo_colour_raw = $res_logo_colour_array['response']->raw;
            $logo_colour_bg = $res_logo_colour_array['response']->background;

            foreach($logo_colour_category as $key => $value){
                $logo_cc[] = $value;
            }
            $logo_bg_colour = "";

            if(is_object($logo_colour_bg))
            {
                foreach($logo_colour_bg as $k => $v){
                    $logo_bg[] = $v;
                }

                $logo_bg_colour = $logo_bg[0];
            } else {
                $logo_bg_colour = $logo_colour_bg;
            }


            DB::table('br_logo_colour_category')
                ->insert([
                    'br_id' => $br_Id,
                    'vibrant' => $logo_cc[0],
                    'dark' => $logo_cc[1],
                    'light' => $logo_cc[2],
                    'br_cat_id' => 1,
                    'created_at' => $datetime
                ]);

            foreach($logo_colour_raw as $k => $v){
                DB::table('br_logo_colour_raw')
                ->insert([
                    'br_id' => $br_Id,
                    'l_colour_code' => $v->color,
                    'l_percentage' => $v->percentage,
                    'br_cat_id' => 1,
                    'created_at' => $datetime
                ]);
            }

            DB::table('br_logo_background')
                ->insert([
                    'br_id' => $br_Id,
                    'bg_color' => $logo_bg_colour,
                    'br_cat_id' => 1,
                    'created_at' => $datetime
                ]);

            $logo_colour_message = "Logo Color api successfull.  Data inserted to 'br_logo_colour_category','br_logo_colour_raw' and 'br_logo_background'";
            Log::info($logo_colour_message);
        } else {
            $logo_colour_message = "Logo Color api. No data fetched";
            Log::error($logo_colour_message);
        }


        // /* --- API to fetch Wesite Fonts --- */

        $web_font_api_url = 'https://api.brandfetch.io/v1/font';
        $font_data =  json_encode(array("domain" => $web_url,"fresh" => false));

        $ch_web_font = curl_init();
        curl_setopt($ch_web_font, CURLOPT_POST, 1);
        curl_setopt($ch_web_font, CURLOPT_POSTFIELDS, $font_data);
        curl_setopt($ch_web_font, CURLOPT_URL, $web_font_api_url);
        curl_setopt($ch_web_font, CURLOPT_HTTPHEADER, array(
          'x-api-key: oouXaXg0zf77frXoUmsWp9ncqgdHwyaT6BlOaB3a',
          'Content-Type: application/json',
        ));
        curl_setopt($ch_web_font, CURLOPT_TIMEOUT, 400);
        curl_setopt($ch_web_font, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch_web_font, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);


        $result_web_font = curl_exec($ch_web_font);

        if(!$result_web_font){die("Connection Failure");}
        curl_close($ch_web_font);
        $res_web_font_array = json_decode($result_web_font);
        $res_web_font_array = get_object_vars($res_web_font_array);

        if(isset($res_web_font_array)&&!empty($res_web_font_array)){

            foreach($res_web_font_array['response'] as $key => $value){

                $font_array = json_decode(json_encode($value), True);

                DB::table('br_website_font')
                ->insert([
                    'br_id' => $br_Id,
                    'title_tag' => $font_array['tag'],
                    'primary_font' => $font_array['font'],
                    'br_cat_id' => 2,
                    'created_at' => $datetime
                ]);
            }

        }


        /* --- API to fetch Wesite Images --- */

        $web_images_api_url = 'https://api.brandfetch.io/v1/image';
        $web_img_data =  json_encode(array("domain" => $web_url,"fresh" => false));

        $ch_web_images = curl_init();
        curl_setopt($ch_web_images, CURLOPT_POST, 1);
        curl_setopt($ch_web_images, CURLOPT_POSTFIELDS, $web_img_data);
        curl_setopt($ch_web_images, CURLOPT_URL, $web_images_api_url);
        curl_setopt($ch_web_images, CURLOPT_HTTPHEADER, array(
          'x-api-key: oouXaXg0zf77frXoUmsWp9ncqgdHwyaT6BlOaB3a',
          'Content-Type: application/json',
        ));
        curl_setopt($ch_web_images, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch_web_images, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch_web_images, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);


        $result_web_images = curl_exec($ch_web_images);

        if(!$result_web_images){die("Connection Failure");}
        curl_close($ch_web_images);

        $res_web_images_array = json_decode($result_web_images);
        $res_web_images_array = get_object_vars($res_web_images_array);

        if(isset($res_web_images_array)&&!empty($res_web_images_array)){

            foreach($res_web_images_array['response'] as $key => $value){

                $web_img_array = json_decode(json_encode($value), True);

                DB::table('br_website_images')
                ->insert([
                    'br_id' => $br_Id,
                    'image_url' => $web_img_array['image'],
                    'br_cat_id' => 1,
                    'created_at' => $datetime
                ]);
            }
        }


        /* --- API to fetch Wesite Screenshot --- */

        $web_scn_api_url = 'https://api.brandfetch.io/v1/screenshot';
        $web_scn_data =  json_encode(array("url" => $web_url,"fresh" => false));

        $ch_web_scn_images = curl_init();
        curl_setopt($ch_web_scn_images, CURLOPT_POST, 1);
        curl_setopt($ch_web_scn_images, CURLOPT_POSTFIELDS, $web_scn_data);
        curl_setopt($ch_web_scn_images, CURLOPT_URL, $web_scn_api_url);
        curl_setopt($ch_web_scn_images, CURLOPT_HTTPHEADER, array(
          'x-api-key: oouXaXg0zf77frXoUmsWp9ncqgdHwyaT6BlOaB3a',
          'Content-Type: application/json',
        ));
        curl_setopt($ch_web_scn_images, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch_web_scn_images, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch_web_scn_images, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);


        $result_web_scn_images = curl_exec($ch_web_scn_images);

        if(!$result_web_scn_images){die("Connection Failure");}
        curl_close($ch_web_scn_images);
        $res_web_scn_images_array = json_decode($result_web_scn_images);
        $res_web_scn_images_array = get_object_vars($res_web_scn_images_array);

        if(isset($res_web_scn_images_array)&&!empty($res_web_scn_images_array))
        {
            DB::table('br_website_screenshot')
            ->insert([
                'br_id' => $br_Id,
                'scn_url' => $res_web_scn_images_array['response']->image,
                'br_cat_id' => 1,
                'created_at' => $datetime
            ]);
        }




        /* --- API to fetch Wesite Metadata --- */

        $web_meta_api_url = 'https://api.brandfetch.io/v1/company';
        $web_meta_data =  json_encode(array("domain" => $web_url,"fresh" => true,"renderJS" => true));

        $ch_web_meta = curl_init();
        curl_setopt($ch_web_meta, CURLOPT_POST, 1);
        curl_setopt($ch_web_meta, CURLOPT_POSTFIELDS, $web_meta_data);
        curl_setopt($ch_web_meta, CURLOPT_URL, $web_meta_api_url);
        curl_setopt($ch_web_meta, CURLOPT_HTTPHEADER, array(
          'x-api-key: oouXaXg0zf77frXoUmsWp9ncqgdHwyaT6BlOaB3a',
          'Content-Type: application/json',
        ));
        curl_setopt($ch_web_meta, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch_web_meta, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch_web_meta, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);


        $result_web_meta = curl_exec($ch_web_meta);

        if(!$result_web_meta){die("Connection Failure");}
        curl_close($ch_web_meta);
        $res_web_meta_array = json_decode($result_web_meta);
        $res_web_meta_array = get_object_vars($res_web_meta_array);

        if(isset($res_web_meta_array)&&!empty($res_web_meta_array))
        {
            DB::table('br_website_meta_data')
            ->insert([
                'br_id' => $br_Id,
                'title' => $res_web_meta_array['response']->title,
                'summary' => $res_web_meta_array['response']->summary,
                'description' => $res_web_meta_array['response']->description,
                'keywords' => $res_web_meta_array['response']->keywords,
                'language' => $res_web_meta_array['response']->language,
                'br_cat_id' => 3,
                'created_at' => $datetime
            ]);
        }




        /* --- API to fetch Company Social Media --- */

        $company_social_api_url = 'https://api.brandfetch.io/v1/company-api/get-company-social-media?';
        $company_data =  json_encode(array("domain" => $web_url,"fresh" => true,"renderJS" => true));

        $ch_company = curl_init();
        curl_setopt($ch_company, CURLOPT_POST, 1);
        curl_setopt($ch_company, CURLOPT_POSTFIELDS, $company_data);
        curl_setopt($ch_company, CURLOPT_URL, $company_social_api_url);
        curl_setopt($ch_company, CURLOPT_HTTPHEADER, array(
          'x-api-key: oouXaXg0zf77frXoUmsWp9ncqgdHwyaT6BlOaB3a',
          'Content-Type: application/json',
        ));
        curl_setopt($ch_company, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch_company, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch_company, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);


        $result_company = curl_exec($ch_company);

        if(!$result_company){die("Connection Failure");}
        curl_close($ch_company);
        $res_company_array = json_decode($result_company);
        $res_company_array = get_object_vars($res_company_array);

        if(isset($res_company_array)&&!empty($res_company_array))
        {
            if(isset($res_company_array['response']->twitter)) { $twitter = $res_company_array['response']->twitter; } else { $twitter = ""; }
        if(isset($res_company_array['response']->linkedin)) { $linkedin = $res_company_array['response']->linkedin; } else {
            $linkedin = ""; }
        if(isset($res_company_array['response']->github)) { $github = $res_company_array['response']->github; } else { $github = ""; }
        if(isset($res_company_array['response']->instagram)) { $instagram = $res_company_array['response']->instagram; } else { $instagram = ""; }
        if(isset($res_company_array['response']->facebook)) { $facebook = $res_company_array['response']->facebook; } else { $facebook = ""; }
        if(isset($res_company_array['response']->youtube)) { $youtube = $res_company_array['response']->youtube; } else { $youtube = ""; }
        if(isset($res_company_array['response']->pinterest)) { $pinterest = $res_company_array['response']->pinterest; } else { $pinterest = ""; }
        if(isset($res_company_array['response']->crunchbase)) { $crunchbase = $res_company_array['response']->crunchbase; } else { $crunchbase = ""; }


        DB::table('br_company_social_media')
            ->insert([
                'br_id' => $br_Id,
                'twitter' => $twitter,
                'linkedin' => $linkedin,
                'github' => $github,
                'instagram' => $instagram,
                'facebook' => $facebook,
                'youtube' => $youtube,
                'pinterest' => $pinterest,
                'crunchbase' => $crunchbase,
                'br_cat_id' => 4,
                'created_at' => $datetime
            ]);
        }




        return '1';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     public function exporttopdf($id)
    {
        $curr_time = md5(time());
        $br_id = $id;
        $data = DB::table('brandfetch_primary')
                 ->leftjoin('br_company_social_media','brandfetch_primary.br_id','=','br_company_social_media.br_id')
                 ->leftjoin('br_logo','brandfetch_primary.br_id','=','br_logo.br_id')
                 ->leftjoin('br_logo_background','brandfetch_primary.br_id','=','br_logo_background.br_id')
                 ->leftjoin('br_logo_colour_category','brandfetch_primary.br_id','=','br_logo_colour_category.br_id')
                 ->leftjoin('br_logo_colour_raw','brandfetch_primary.br_id','=','br_logo_colour_raw.br_id')
                 ->leftjoin('br_website_font','brandfetch_primary.br_id','=','br_website_font.br_id')
                 ->leftjoin('br_image_path','brandfetch_primary.br_id','=','br_image_path.br_id')
                 ->leftjoin('br_website_meta_data','brandfetch_primary.br_id','=','br_website_meta_data.br_id')
                 ->leftjoin('br_website_screenshot','brandfetch_primary.br_id','=','br_website_screenshot.br_id')
                 ->leftjoin('br_web_colour_category','brandfetch_primary.br_id','=','br_web_colour_category.br_id')
                 ->leftjoin('br_web_colour_raw','brandfetch_primary.br_id','=','br_web_colour_raw.br_id')
                 ->select('brandfetch_primary.web_url','brandfetch_primary.created_at','br_company_social_media.twitter','br_company_social_media.linkedin','br_company_social_media.github','br_company_social_media.instagram','br_company_social_media.facebook','br_company_social_media.youtube','br_company_social_media.pinterest','br_company_social_media.crunchbase','br_logo.web_logo','br_logo_background.bg_color','br_logo_colour_category.vibrant','br_logo_colour_category.dark','br_logo_colour_category.light','br_logo_colour_raw.l_colour_code','br_image_path.file_path','br_logo_colour_raw.l_percentage','br_website_font.title_tag','br_website_font.primary_font','br_website_meta_data.title','br_website_meta_data.summary','br_website_meta_data.description','br_website_meta_data.keywords','br_website_meta_data.language','br_website_screenshot.scn_url','br_web_colour_category.vibrant','br_web_colour_category.dark','br_web_colour_category.light','br_web_colour_raw.colour_code','br_web_colour_raw.percentage')
                 ->where('brandfetch_primary.br_id','=',$br_id)
                 ->first();

                $data = json_decode(json_encode($data), true);


        $pdf = PDF::loadView('brandfetchpdf', $data);

        return $pdf->download('Brandfetch_'.$curr_time.'.pdf');
        die();
    }

}
