<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use DB;
use Cloudder;

class CompanyGalleryController extends Controller
{
  
    public function customergallery()
    {   
        if(Auth::guard('company')->check())
        {
        $company=Auth::guard('company')->user();

        //create tag for cloudinary
        $cloudImagetag = str_replace(' ', '_', $company->cname);
        return view('company.customergallery',compact('company','cloudImagetag'));
        }
        return abort(404);
    }
    public function seeMarketingAssets()
    {   
        if(Auth::guard('company')->check())
        {
        $company=Auth::guard('company')->user();

        //create tag for cloudinary
        $cloudImagetag = str_replace(' ', '_', $company->cname);
        return view('company.marketingAssets',compact('company','cloudImagetag'));
        }
        return abort(404);
    }
    public function customerview()
    {   
        if(Auth::guard('company')->check())
        {
        $company=Auth::guard('company')->user();

        //create tag for cloudinary
        $cloudImagetag = str_replace(' ', '_', $company->cname);
        return view('company.customerview',compact('company','cloudImagetag'));
        }
        return abort(404);
    }
}
