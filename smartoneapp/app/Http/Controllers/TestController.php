<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use DB;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datetime = date('Y-m-d H:i:s');
        
        // /* ------ Silktide Prospect API ----- */

        // $data = json_encode(array('url' => 'prospect.silktide.com' ));
        // $url = 'https://api.prospect.silktide.com/api/v1/report';

        // $curl = curl_init();
        // curl_setopt($curl, CURLOPT_POST, 1);
        // curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        // curl_setopt($curl, CURLOPT_URL, $url);
        // curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        //   'api-key: 398a2e9cb7fc76f58678cc5bb41bfb5bd8f839c4',
        //   'Content-Type: application/json',
        // ));
        // curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        // curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        
        // $result = curl_exec($curl);
        
        // if(!$result){die("Connection Failure");}
        // curl_close($curl);
        // $result = json_decode($result,true);


        // $reportId = $result['reportId'];

        // $detail_url = "https://api.prospect.silktide.com/api/v1/report/".$reportId;

        // $ch = curl_init();    
        // curl_setopt($ch, CURLOPT_URL, $detail_url);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,0);
        // curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        //   'api-key: 398a2e9cb7fc76f58678cc5bb41bfb5bd8f839c4',
        //   'Content-Type: application/json',
        // ));
        
        // $fetch_data = curl_exec($ch);
        // $fetch_data = json_decode($fetch_data,true);
        // echo '<pre>'; print_r($fetch_data);
        
        // /* --- Save to 'silktide_prospect' table -- */

        // $st_Id = DB::table('silktide_prospect')
        //     ->insertGetId([
        //         'report_id' => $reportId,
        //         'account_id' => $fetch_data['report']['account_id'],
        //         'url' => $url,
        //         'created_at' => $datetime
        //     ]);

        // /* --- Save to 'st_sitecontent' table --- */

        // DB::table('st_sitecontent')
        // ->insert([
        //     'st_id' => $st_Id,
        //     'average_words_per_page' => (isset($fetch_data['report']['amount_of_content']['average_words_per_page'])) ? $fetch_data['report']['amount_of_content']['average_words_per_page'] : 0,
        //     'pages_found' => (isset($fetch_data['report']['amount_of_content']['pages_found'])) ? $fetch_data['report']['amount_of_content']['pages_found'] : 0,
        //     'total_word_count' => (isset($fetch_data['report']['amount_of_content']['total_word_count'])) ? $fetch_data['report']['amount_of_content']['total_word_count'] : 0,
        //     'cms_solution' => (isset($fetch_data['report']['content_management_system']['cms_solution'])&&!empty($fetch_data['report']['content_management_system']['cms_solution'])) ? implode(',',$fetch_data['report']['content_management_system']['cms_solution']) : '',
        //     'has_cms' => (isset($fetch_data['report']['content_management_system']['has_cms'])) ? $fetch_data['report']['content_management_system']['has_cms'] : 0,
        //     'pages_discovered_count' => (isset($fetch_data['report']['page_count']['pages_discovered_count'])) ? $fetch_data['report']['page_count']['pages_discovered_count'] : 0,
        //     'created_at' => $datetime
        // ]);

        // /* --- Save to 'st_vitals' table --- */

        // DB::table('st_vitals')
        // ->insert([
        //     'st_id' => $st_Id,
        //     'pages_tested' => (isset($fetch_data['report']['amount_of_content']['pages_tested'])) ? $fetch_data['report']['amount_of_content']['pages_tested'] : 0,
        //     'analytics_tool' => (isset($fetch_data['report']['analytics']['analytics_tool'])) ? $fetch_data['report']['analytics']['analytics_tool'] : '',
        //     'has_analytics' => (isset($fetch_data['report']['analytics']['has_analytics'])) ? $fetch_data['report']['analytics']['has_analytics'] : 0,
        //     'domain' => (isset($fetch_data['report']['domain'])) ? $fetch_data['report']['domain'] : '',
        //     'ecommerce_name' => (isset($fetch_data['report']['ecommerce']['ecommerce_name'])) ? $fetch_data['report']['ecommerce']['ecommerce_name'] : '',
        //     'has_ecommerce' => (isset($fetch_data['report']['ecommerce']['has_ecommerce'])) ? $fetch_data['report']['ecommerce']['has_ecommerce'] : 0,
        //     'has_pixel' => (isset($fetch_data['report']['facebook_retargeting']['has_pixel'])) ? $fetch_data['report']['facebook_retargeting']['has_pixel'] : 0,
        //     'address_details_provided' => (isset($fetch_data['report']['local_presence']['address_details_provided'])) ? $fetch_data['report']['local_presence']['address_details_provided'] : 0,
        //     'has_result' => (isset($fetch_data['report']['local_presence']['has_result'])) ? $fetch_data['report']['local_presence']['has_result'] : 0,
        //     'yext_api_success' => (isset($fetch_data['report']['local_presence']['yext_api_success'])) ? $fetch_data['report']['local_presence']['yext_api_success'] : 0,
        //     'has_mobile_site' => (isset($fetch_data['report']['mobile']['has_mobile_site'])) ? $fetch_data['report']['mobile']['has_mobile_site'] : 0,
        //     'is_mobile' => (isset($fetch_data['report']['mobile']['is_mobile'])) ? $fetch_data['report']['mobile']['is_mobile'] : 0,
        //     'is_tablet' => (isset($fetch_data['report']['mobile']['is_tablet'])) ? $fetch_data['report']['mobile']['is_tablet'] : 0,
        //     'mobile_screenshot_url' => (isset($fetch_data['report']['mobile']['mobile_screenshot_url'])) ? $fetch_data['report']['mobile']['mobile_screenshot_url'] : '',
        //     'mobile_site_url' => (isset($fetch_data['report']['mobile']['mobile_site_url'])) ? $fetch_data['report']['mobile']['mobile_site_url'] : '',
        //     'average_monthly_traffic' => (isset($fetch_data['report']['organic_search']['average_monthly_traffic'])) ? $fetch_data['report']['organic_search']['average_monthly_traffic'] : 0,
        //     'adwords_keywords' => (isset($fetch_data['report']['paid_search']['adwords_keywords'])&&!empty($fetch_data['report']['paid_search']['adwords_keywords'])) ? implode(',', $fetch_data['report']['paid_search']['adwords_keywords']) : '',
        //     'has_adwords_spend' => (isset($fetch_data['report']['paid_search']['has_adwords_spend'])) ? $fetch_data['report']['paid_search']['has_adwords_spend'] : 0,
        //     'has_sitemap' => (isset($fetch_data['report']['sitemap']['has_sitemap'])) ? $fetch_data['report']['sitemap']['has_sitemap'] : 0,
        //     'sitemap_issues' => (isset($fetch_data['report']['sitemap']['sitemap_issues'])) ? $fetch_data['report']['sitemap']['sitemap_issues'] : 0,
        //     'stale_analysis' => (isset($fetch_data['report']['stale_analysis'])) ? $fetch_data['report']['stale_analysis'] : 0,
        //     'created_at' => $datetime

        // ]);


        // /* --- Save to 'st_discovered' table --- */

        // DB::table('st_discovered')
        // ->insert([
        //     'st_id' => $st_Id,
        //     'email' => (isset($fetch_data['report']['contact_details']['email'])&&!empty($fetch_data['report']['contact_details']['email'])) ? implode(',',$fetch_data['report']['contact_details']['email']) : '',
        //     'emails' => (isset($fetch_data['report']['contact_details']['emails'])&&!empty($fetch_data['report']['contact_details']['emails'])) ? implode(',',$fetch_data['report']['contact_details']['emails']) : '',
        //     'phone' => (isset($fetch_data['report']['contact_details']['phone'])&&!empty($fetch_data['report']['contact_details']['phone'])) ? implode(',',$fetch_data['report']['contact_details']['phone']) : '',
        //     'phones' => (isset($fetch_data['report']['contact_details']['phones'])&&!empty($fetch_data['report']['contact_details']['phones'])) ? implode(',',$fetch_data['report']['contact_details']['phones']) : '',
        //     'created_at' => $datetime
        // ]);

        // /* --- Save to 'st_domain_info' table --- */

        // DB::table('st_domain_info')
        // ->insert([
        //     'st_id' => $st_Id,
        //     'domain_age_days' => (isset($fetch_data['report']['domain_age']['domain_age_days'])) ? $fetch_data['report']['domain_age']['domain_age_days'] : '',
        //     'expiry_date' => (isset($fetch_data['report']['domain_age']['expiry_date'])) ? $fetch_data['report']['domain_age']['expiry_date'] : '',
        //     'registered_date' => (isset($fetch_data['report']['domain_age']['registered_date'])) ? $fetch_data['report']['domain_age']['registered_date'] : '',
        //     'created_at' => $datetime
        // ]);


        // /* --- Save to 'st_domain' table --- */

        // DB::table('st_domain')
        // ->insert([
        //     'st_id' => $st_Id,
        //     'is_parked' => (isset($fetch_data['report']['parked_domain_detection']['is_parked'])) ? $fetch_data['report']['parked_domain_detection']['is_parked'] : '',
        //     'has_ssl' => (isset($fetch_data['report']['ssl']['has_ssl'])) ? $fetch_data['report']['ssl']['has_ssl'] : '',
        //     'ssl_expired' => (isset($fetch_data['report']['ssl']['ssl_expired'])) ? $fetch_data['report']['ssl']['ssl_expired'] : '',
        //     'ssl_redirect' => (isset($fetch_data['report']['ssl']['ssl_redirect'])) ? $fetch_data['report']['ssl']['ssl_redirect'] : '',
        //     'ssl_valid' => (isset($fetch_data['report']['ssl']['ssl_valid'])) ? $fetch_data['report']['ssl']['ssl_valid'] : '',
        //     'created_at' => $datetime
        // ]);

        // /* --- Save to 'st_seo' table --- */

        // DB::table('st_seo')
        // ->insert([
        //     'st_id' => $st_Id,
        //     'good_headings' => (isset($fetch_data['report']['headings']['good_headings'])) ? $fetch_data['report']['headings']['good_headings'] : 0,
        //     'has_content_for_every_heading' => (isset($fetch_data['report']['headings']['has_content_for_every_heading'])) ? $fetch_data['report']['headings']['has_content_for_every_heading'] : 0,
        //     'has_hierarchical_headings' => (isset($fetch_data['report']['headings']['has_hierarchical_headings'])) ? $fetch_data['report']['headings']['has_hierarchical_headings'] : 0,
        //     'has_single_h1_on_each_page' => (isset($fetch_data['report']['headings']['has_single_h1_on_each_page'])) ? $fetch_data['report']['headings']['has_single_h1_on_each_page'] : 0,
        //     'analysis_country' => (isset($fetch_data['report']['meta']['analysis_country'])) ? $fetch_data['report']['meta']['analysis_country'] : '',
        //     'detected_address' => (isset($fetch_data['report']['meta']['detected_address'])) ? $fetch_data['report']['meta']['detected_address'] : '',
        //     'detected_name' => (isset($fetch_data['report']['meta']['detected_name'])) ? $fetch_data['report']['meta']['detected_name'] : '',
        //     'detected_phone' => (isset($fetch_data['report']['meta']['detected_phone'])) ? $fetch_data['report']['meta']['detected_phone'] : '',
        //     'num_keywords_ranked_for' => (isset($fetch_data['report']['organic_search']['num_keywords_ranked_for'])) ? $fetch_data['report']['organic_search']['num_keywords_ranked_for'] : 0,
        //     'top_keywords_ranked_for' => (isset($fetch_data['report']['organic_search']['top_keywords_ranked_for'])) ? $fetch_data['report']['organic_search']['top_keywords_ranked_for'] : '',
        //     'duplicated_items' => (isset($fetch_data['report']['page_titles_and_descriptions']['duplicated_items'])) ? $fetch_data['report']['page_titles_and_descriptions']['duplicated_items'] : 0,
        //     'homepage_title_tag' => (isset($fetch_data['report']['page_titles_and_descriptions']['homepage_title_tag'])) ? $fetch_data['report']['page_titles_and_descriptions']['homepage_title_tag'] : '',
        //     'missing_items' => (isset($fetch_data['report']['page_titles_and_descriptions']['missing_items'])) ? $fetch_data['report']['page_titles_and_descriptions']['missing_items'] : '',
        //     'pages_duplicated_description_count' => (isset($fetch_data['report']['page_titles_and_descriptions']['pages_duplicated_description_count'])) ? $fetch_data['report']['page_titles_and_descriptions']['pages_duplicated_description_count'] : 0,
        //     'pages_duplicated_title_count' => (isset($fetch_data['report']['page_titles_and_descriptions']['pages_duplicated_title_count'])) ? $fetch_data['report']['page_titles_and_descriptions']['pages_duplicated_title_count'] : 0,
        //     'pages_missing_description_count' => (isset($fetch_data['report']['page_titles_and_descriptions']['pages_missing_description_count'])) ? $fetch_data['report']['page_titles_and_descriptions']['pages_missing_description_count'] : 0,
        //     'pages_missing_title_count' => (isset($fetch_data['report']['page_titles_and_descriptions']['pages_missing_title_count'])) ? $fetch_data['report']['page_titles_and_descriptions']['pages_missing_title_count'] : 0,
        //     'percent_duplicated_descriptions' => (isset($fetch_data['report']['page_titles_and_descriptions']['percent_duplicated_descriptions'])) ? $fetch_data['report']['page_titles_and_descriptions']['percent_duplicated_descriptions'] : 0,
        //     'percent_duplicated_titles' => (isset($fetch_data['report']['page_titles_and_descriptions']['percent_duplicated_titles'])) ? $fetch_data['report']['page_titles_and_descriptions']['percent_duplicated_titles'] : 0,
        //     'percent_missing_descriptions' => (isset($fetch_data['report']['page_titles_and_descriptions']['percent_missing_descriptions'])) ? $fetch_data['report']['page_titles_and_descriptions']['percent_missing_descriptions'] : 0,
        //     'percent_missing_titles' => (isset($fetch_data['report']['page_titles_and_descriptions']['percent_missing_titles'])) ? $fetch_data['report']['page_titles_and_descriptions']['percent_missing_titles'] : 0,
        //     'created_at' => $datetime
        // ]);

        // /* --- Save to 'st_images' table --- */

        // DB::table('st_images')
        // ->insert([
        //     'st_id' => $st_Id,
        //     'image_count' => (isset($fetch_data['report']['images']['image_count'])) ? $fetch_data['report']['images']['image_count'] : 0,
        //     'non_web_friendly_count' => (isset($fetch_data['report']['images']['non_web_friendly_count'])) ? $fetch_data['report']['images']['non_web_friendly_count'] : 0,
        //     'percent_images_sized' => (isset($fetch_data['report']['images']['percent_images_sized'])) ? $fetch_data['report']['images']['percent_images_sized'] : 0,
        //     'stretched_image_count' => (isset($fetch_data['report']['images']['stretched_image_count'])) ? $fetch_data['report']['images']['stretched_image_count'] : 0,    
        //     'created_at' => $datetime
        // ]);

        // /* --- Save to 'st_social' table --- */

        // DB::table('st_social')
        // ->insert([
        //     'st_id' => $st_Id,
        //     'has_instagram' => (isset($fetch_data['report']['instagram_account']['has_instagram'])) ? $fetch_data['report']['instagram_account']['has_instagram'] : 0,
        //     'days_since_update' => (isset($fetch_data['report']['last_updated']['days_since_update'])) ? $fetch_data['report']['last_updated']['days_since_update'] : 0,
        //     'last_updated_date' => (isset($fetch_data['report']['last_updated']['last_updated_date'])) ? $fetch_data['report']['last_updated']['last_updated_date'] : '',
        //     'twitter_found' => (isset($fetch_data['report']['twitter']['found'])) ? $fetch_data['report']['twitter']['found'] : 0,    
        //     'created_at' => $datetime
        // ]);

        // /* --- Save to 'st_video' table --- */

        // DB::table('st_video')
        // ->insert([
        //     'st_id' => $st_Id,
        //     'vendor_vendor' => (isset($fetch_data['report']['vendor']['vendor'])) ? $fetch_data['report']['vendor']['vendor'] : '',
        //     'has_video' => (isset($fetch_data['report']['video']['has_video'])) ? $fetch_data['report']['video']['has_video'] : 0,
        //     'video_vendor' => (isset($fetch_data['report']['video']['vendor'])) ? $fetch_data['report']['video']['vendor'] : '',
        //     'website_traffic' => (isset($fetch_data['report']['website_traffic']['website_traffic'])) ? $fetch_data['report']['website_traffic']['website_traffic'] : 0,    
        //     'created_at' => $datetime
        // ]);

        
        /* ------ Brandfetch API ---- */

        /*--- API to fetch Website logo ---*/

        $web_url = 'http://www.cyspansystems.com/';

        $data =  json_encode(array("domain" => $web_url));
        $logo_api_url = 'https://api.brandfetch.io/v1/logo-api/get-website-logo?';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_URL, $logo_api_url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
          'x-api-key: QyDNEwxRhf6cpYl2Wx6Hi5KSCYNS67PF4FIWqnBM',
          'Content-Type: application/json',
        ));
        curl_setopt($curl, CURLOPT_TIMEOUT, 400);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        
        $result = curl_exec($curl);
        
        if(!$result){die("Connection Failure");}
        curl_close($curl);
        $res_array = json_decode($result);
        $res_array = get_object_vars($res_array);
        $web_logo = $res_array['response']->image;
        

        /* --- Save to 'brandfetch_primary' table --- */

        $br_Id = DB::table('brandfetch_primary')
            ->insertGetId([
                'web_url' => $web_url,
                'created_at' => $datetime
            ]);


        /* --- Save to 'br_logo' table --- */

        DB::table('br_logo')
            ->insert([
                'br_id' => $br_Id,
                'web_logo' => $web_logo,
                'br_cat_id' => 1,
                'created_at' => $datetime
            ]);


        /*--- API to fetch Website colour ---*/

        $web_colour_api_url = 'https://api.brandfetch.io/v1/color-api/get-website-colors?';

        $ch_web_colour = curl_init();
        curl_setopt($ch_web_colour, CURLOPT_POST, 1);
        curl_setopt($ch_web_colour, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch_web_colour, CURLOPT_URL, $web_colour_api_url);
        curl_setopt($ch_web_colour, CURLOPT_HTTPHEADER, array(
          'x-api-key: QyDNEwxRhf6cpYl2Wx6Hi5KSCYNS67PF4FIWqnBM',
          'Content-Type: application/json',
        ));
        curl_setopt($ch_web_colour, CURLOPT_TIMEOUT, 400);
        curl_setopt($ch_web_colour, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch_web_colour, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        
        $result_web_colour = curl_exec($ch_web_colour);
        
        if(!$result_web_colour){die("Connection Failure");}
        curl_close($ch_web_colour);
        $res_web_colour_array = json_decode($result_web_colour);
        $res_web_colour_array = get_object_vars($res_web_colour_array);
        
        $web_colour_category = $res_web_colour_array['response']->filtered;
        $web_colour_raw = $res_web_colour_array['response']->raw;

        foreach($web_colour_category as $key => $value){
            $web_cc[] = $value;
        }
        
        DB::table('br_web_colour_category')
            ->insert([
                'br_id' => $br_Id,
                'vibrant' => $web_cc[2],
                'dark' => $web_cc[0],
                'light' => $web_cc[1],
                'br_cat_id' => 1,
                'created_at' => $datetime
            ]);

        foreach($web_colour_raw as $k => $v){
            DB::table('br_web_colour_raw')
            ->insert([
                'br_id' => $br_Id,
                'colour_code' => $v->color,
                'percentage' => $v->percentage,
                'br_cat_id' => 1,
                'created_at' => $datetime
            ]);
        }

        /* --- API to fetch Logo Colour --- */
        
        $logo_colour_api_url = 'https://api.brandfetch.io/v1/color-api/get-colors-from-logo?';
        $logo_url = $web_logo;
        $logo_data =  json_encode(array("image" => $logo_url));

        $ch_logo_colour = curl_init();
        curl_setopt($ch_logo_colour, CURLOPT_POST, 1);
        curl_setopt($ch_logo_colour, CURLOPT_POSTFIELDS, $logo_data);
        curl_setopt($ch_logo_colour, CURLOPT_URL, $logo_colour_api_url);
        curl_setopt($ch_logo_colour, CURLOPT_HTTPHEADER, array(
          'x-api-key: QyDNEwxRhf6cpYl2Wx6Hi5KSCYNS67PF4FIWqnBM',
          'Content-Type: application/json',
        ));
        curl_setopt($ch_logo_colour, CURLOPT_TIMEOUT, 400);
        curl_setopt($ch_logo_colour, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch_logo_colour, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        
        $result_logo_colour = curl_exec($ch_logo_colour);
        
        if(!$result_logo_colour){die("Connection Failure");}
        curl_close($ch_logo_colour);
        $res_logo_colour_array = json_decode($result_logo_colour);
        $res_logo_colour_array = get_object_vars($res_logo_colour_array);

        $logo_colour_category = $res_logo_colour_array['response']->filtered;
        $logo_colour_raw = $res_logo_colour_array['response']->raw;
        $logo_colour_bg = $res_logo_colour_array['response']->background;

        foreach($logo_colour_category as $key => $value){
            $logo_cc[] = $value;
        }
        
        DB::table('br_logo_colour_category')
            ->insert([
                'br_id' => $br_Id,
                'vibrant' => $logo_cc[0],
                'dark' => $logo_cc[1],
                'light' => $logo_cc[2],
                'br_cat_id' => 1,
                'created_at' => $datetime
            ]);

        foreach($logo_colour_raw as $k => $v){
            DB::table('br_logo_colour_raw')
            ->insert([
                'br_id' => $br_Id,
                'l_colour_code' => $v->color,
                'l_percentage' => $v->percentage,
                'br_cat_id' => 1,
                'created_at' => $datetime
            ]);
        }

        DB::table('br_logo_background')
            ->insert([
                'br_id' => $br_Id,
                'bg_color' => $logo_colour_bg,
                'br_cat_id' => 1,
                'created_at' => $datetime
            ]);


        /* --- API to fetch Wesite Fonts --- */

        $web_font_api_url = 'https://api.brandfetch.io/v1/font-api/get-website-fonts?';
        $font_data =  json_encode(array("domain" => $web_url,"fresh" => true));

        $ch_web_font = curl_init();
        curl_setopt($ch_web_font, CURLOPT_POST, 1);
        curl_setopt($ch_web_font, CURLOPT_POSTFIELDS, $font_data);
        curl_setopt($ch_web_font, CURLOPT_URL, $web_font_api_url);
        curl_setopt($ch_web_font, CURLOPT_HTTPHEADER, array(
          'x-api-key: QyDNEwxRhf6cpYl2Wx6Hi5KSCYNS67PF4FIWqnBM',
          'Content-Type: application/json',
        ));
        curl_setopt($ch_web_font, CURLOPT_TIMEOUT, 400);
        curl_setopt($ch_web_font, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch_web_font, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        
        $result_web_font = curl_exec($ch_web_font);
        
        if(!$result_web_font){die("Connection Failure");}
        curl_close($ch_web_font);
        $res_web_font_array = json_decode($result_web_font);
        $res_web_font_array = get_object_vars($res_web_font_array);
        
        foreach($res_web_font_array['response'] as $key => $value){
            
            $font_array = json_decode(json_encode($value), True);
            
            DB::table('br_website_font')
            ->insert([
                'br_id' => $br_Id,
                'title_tag' => $font_array['tag'],
                'primary_font' => $font_array['font'],
                'br_cat_id' => 2,
                'created_at' => $datetime
            ]);
        }


        /* --- API to fetch Wesite Images --- */

        $web_images_api_url = 'https://api.brandfetch.io/v1/image-api/get-website-images?';
        $web_img_data =  json_encode(array("domain" => $web_url,"fresh" => true));

        $ch_web_images = curl_init();
        curl_setopt($ch_web_images, CURLOPT_POST, 1);
        curl_setopt($ch_web_images, CURLOPT_POSTFIELDS, $web_img_data);
        curl_setopt($ch_web_images, CURLOPT_URL, $web_images_api_url);
        curl_setopt($ch_web_images, CURLOPT_HTTPHEADER, array(
          'x-api-key: QyDNEwxRhf6cpYl2Wx6Hi5KSCYNS67PF4FIWqnBM',
          'Content-Type: application/json',
        ));
        curl_setopt($ch_web_images, CURLOPT_TIMEOUT, 400);
        curl_setopt($ch_web_images, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch_web_images, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        
        $result_web_images = curl_exec($ch_web_images);
        
        if(!$result_web_images){die("Connection Failure");}
        curl_close($ch_web_images);
        $res_web_images_array = json_decode($result_web_images);
        $res_web_images_array = get_object_vars($res_web_images_array);
        echo '<pre>'; print_r($res_web_images_array);

        foreach($res_web_images_array['response'] as $key => $value){
            
            $web_img_array = json_decode(json_encode($value), True);
            
            DB::table('br_website_images')
            ->insert([
                'br_id' => $br_Id,
                'image_url' => $web_img_array['image'],
                'br_cat_id' => 1,
                'created_at' => $datetime
            ]);
        }


        /* --- API to fetch Wesite Screenshot --- */

        $web_scn_api_url = 'https://api.brandfetch.io/v1/image-api/get-website-screenshot?';
        $web_scn_data =  json_encode(array("url" => $web_url,"fresh" => true));

        $ch_web_scn_images = curl_init();
        curl_setopt($ch_web_scn_images, CURLOPT_POST, 1);
        curl_setopt($ch_web_scn_images, CURLOPT_POSTFIELDS, $web_scn_data);
        curl_setopt($ch_web_scn_images, CURLOPT_URL, $web_scn_api_url);
        curl_setopt($ch_web_scn_images, CURLOPT_HTTPHEADER, array(
          'x-api-key: QyDNEwxRhf6cpYl2Wx6Hi5KSCYNS67PF4FIWqnBM',
          'Content-Type: application/json',
        ));
        curl_setopt($ch_web_scn_images, CURLOPT_TIMEOUT, 400);
        curl_setopt($ch_web_scn_images, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch_web_scn_images, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        
        $result_web_scn_images = curl_exec($ch_web_scn_images);
        
        if(!$result_web_scn_images){die("Connection Failure");}
        curl_close($ch_web_scn_images);
        $res_web_scn_images_array = json_decode($result_web_scn_images);
        $res_web_scn_images_array = get_object_vars($res_web_scn_images_array);

        DB::table('br_website_screenshot')
            ->insert([
                'br_id' => $br_Id,
                'scn_url' => $res_web_scn_images_array['response']->image,
                'br_cat_id' => 1,
                'created_at' => $datetime
            ]);


        /* --- API to fetch Wesite Metadata --- */

        $web_meta_api_url = 'https://api.brandfetch.io/v1/company-api/get-website-metadata?';
        $web_meta_data =  json_encode(array("domain" => $web_url,"fresh" => true,"renderJS" => true));

        $ch_web_meta = curl_init();
        curl_setopt($ch_web_meta, CURLOPT_POST, 1);
        curl_setopt($ch_web_meta, CURLOPT_POSTFIELDS, $web_meta_data);
        curl_setopt($ch_web_meta, CURLOPT_URL, $web_meta_api_url);
        curl_setopt($ch_web_meta, CURLOPT_HTTPHEADER, array(
          'x-api-key: QyDNEwxRhf6cpYl2Wx6Hi5KSCYNS67PF4FIWqnBM',
          'Content-Type: application/json',
        ));
        curl_setopt($ch_web_meta, CURLOPT_TIMEOUT, 400);
        curl_setopt($ch_web_meta, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch_web_meta, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        
        $result_web_meta = curl_exec($ch_web_meta);
        
        if(!$result_web_meta){die("Connection Failure");}
        curl_close($ch_web_meta);
        $res_web_meta_array = json_decode($result_web_meta);
        $res_web_meta_array = get_object_vars($res_web_meta_array);

        DB::table('br_website_meta_data')
            ->insert([
                'br_id' => $br_Id,
                'title' => $res_web_meta_array['response']->title,
                'summary' => $res_web_meta_array['response']->summary,
                'description' => $res_web_meta_array['response']->description,
                'keywords' => $res_web_meta_array['response']->keywords,
                'language' => $res_web_meta_array['response']->language,
                'br_cat_id' => 3,
                'created_at' => $datetime
            ]);


        /* --- API to fetch Company Social Media --- */

        $company_social_api_url = 'https://api.brandfetch.io/v1/company-api/get-company-social-media?';
        $company_data =  json_encode(array("domain" => $web_url,"fresh" => true,"renderJS" => true));

        $ch_company = curl_init();
        curl_setopt($ch_company, CURLOPT_POST, 1);
        curl_setopt($ch_company, CURLOPT_POSTFIELDS, $company_data);
        curl_setopt($ch_company, CURLOPT_URL, $company_social_api_url);
        curl_setopt($ch_company, CURLOPT_HTTPHEADER, array(
          'x-api-key: QyDNEwxRhf6cpYl2Wx6Hi5KSCYNS67PF4FIWqnBM',
          'Content-Type: application/json',
        ));
        curl_setopt($ch_company, CURLOPT_TIMEOUT, 400);
        curl_setopt($ch_company, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch_company, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        
        $result_company = curl_exec($ch_company);
        
        if(!$result_company){die("Connection Failure");}
        curl_close($ch_company);
        $res_company_array = json_decode($result_company);
        $res_company_array = get_object_vars($res_company_array);

        DB::table('br_company_social_media')
            ->insert([
                'br_id' => $br_Id,
                'twitter' => $res_company_array['response']->twitter,
                'linkedin' => $res_company_array['response']->linkedin,
                'github' => $res_company_array['response']->github,
                'instagram' => $res_company_array['response']->instagram,
                'facebook' => $res_company_array['response']->facebook,
                'youtube' => $res_company_array['response']->youtube,
                'pinterest' => $res_company_array['response']->pinterest,
                'crunchbase' => $res_company_array['response']->crunchbase,
                'br_cat_id' => 4,
                'created_at' => $datetime
            ]);

        die();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
