<?php

namespace App\Http\Controllers;

use App\Company;
use App\Exports\UsersExport;
use Carbon\Carbon;
use Cloudder;
use DB;
use Goutte;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Log;
use App\LogsTable;
use Maatwebsite\Excel\Facades\Excel;
use Mail;
use PDF;
use Storage;
use Illuminate\Support\Facades\Crypt;
// use Excel;
// use Maatwebsite\Excel\Concerns\FromCollection;
// use Maatwebsite\Excel\Concerns\Exportable;
// use Maatwebsite\Excel\Concerns\WithHeadings;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company = DB::table('company')
            ->leftjoin('mediapartners', 'company.media_partner', '=', 'mediapartners.media_partner')
            ->where('company.status', '=', 0)
            ->get();
        $scan=DB::table('company')
        ->join('company_reports','company.company_id','=','company_reports.company_id')
        ->select('company_reports.company_id','company_reports.created_at')
        ->get();
       foreach($scan as $row)
       {
            $row->created_at=date('m-d-y',strtotime($row->created_at));
       }
        return view('company', compact('company','scan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $states = DB::table('states_db')
            ->get();

        $media_partner = DB::table('mediapartners')
            ->get();

        return view('addcompany', compact('states', 'media_partner'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $msg = "";
        $current_date = date('Y-m-d H:i:s');

        $validatedData = $request->validate([
            'cname' => 'required|max:255',
            'email' => 'required|email',
            'company_url' => 'required',
            'phone' => 'required',
            'address1' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zip' => 'required',
            'country' => 'required',
            'media_partner' => 'required',
        ]);
        $validatedData['created_at'] = $current_date;
        // \App\Company::create($validatedData);
        $password=Hash::make($request->password);
        $password2=$request->passsword;
        if(strlen($request->password)==0){
            $password=Hash::make($request->Client_number);
            $password2=$request->Client_number;
        }
        $company_id=DB::table('company')
            ->insertGetId(['cname' => $request->cname, 'email' => $request->email, 'company_url' => $request->company_url, 'phone' => $request->phone, 'address1' => $request->address1, 'address2' => $request->address2, 'city' => $request->city, 'state_name' => $request->state, 'zip' => $request->zip, 'country' => $request->country, 'media_partner' => $request->media_partner, 'created_at' => $current_date,'Client_number'=>$request->Client_number,'password'=>$password,'Development_link'=>$request->Development_link,'password2'=>$password2]);
        //post to knack
        if($request->knackUpdate){
            $data=json_encode(array(
                'field_1'=>$company_id,
                'field_2'=>$request->cname,
                'field_3'=>$request->address1,
                'field_4'=>$request->email,
                'field_5'=>$request->phone,
                'field_6'=>$request->company_url,
                'field_7'=>$request->media_partner,
                'field_8'=>$current_date,
                'field_337'=>$request->Development_link,
                'field_338'=>$request->Client_number,
                'field_339'=>$password2,
            ));
            $curl = curl_init();

            curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.knack.com/v1/objects/object_1/records",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS =>$data,
            CURLOPT_HTTPHEADER => array(
                "X-Knack-Application-Id: 5f7e2af82c55b5001636420b",
                "X-Knack-REST-API-Key: d2541fdd-43c2-4d4d-9350-c1b787bfe87c",
                "Content-Type: application/json",
                "Cookie: connect.sid=s%3A-599MYE7MII1h8OXi_XdmlzFcZ3oGfdp.v%2BU8UBVXsphpnw5W6ymm6%2Ft0RyKKY%2FXgL8FkXrNvDdY"
            ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);
            if(isset(json_decode($response)->id)){
                DB::table('company')
                ->where('company_id', $company_id)
                ->update(['knackID' =>json_decode($response)->id]);
            }
        }
        return redirect('company')->with('status', 'Company Saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $cid = $request->company_id;
        $cdet = DB::table('company')
            ->select('company_url')
            ->where('company_id', '=', $cid)
            ->first();

        return $cdet->company_url;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $company = DB::table('company')
            ->leftjoin('silktide_prospect', 'silktide_prospect.company_id', '=', 'company.company_id')
            ->leftjoin('st_vitals', 'silktide_prospect.st_id', '=', 'st_vitals.st_id')
            ->leftjoin('st_video', 'silktide_prospect.st_id', '=', 'st_video.st_id')
            ->leftjoin('st_domain', 'silktide_prospect.st_id', '=', 'st_domain.st_id')
            ->leftjoin('st_domain_info', 'silktide_prospect.st_id', '=', 'st_domain_info.st_id')
            ->leftjoin('st_discovered', 'silktide_prospect.st_id', '=', 'st_discovered.st_id')
            ->leftjoin('st_social', 'silktide_prospect.st_id', '=', 'st_social.st_id')
            ->select('st_vitals.pages_tested', 'st_vitals.analytics_tool', 'st_vitals.has_analytics', 'st_vitals.domain', 'st_vitals.ecommerce_name', 'st_vitals.has_ecommerce', 'st_vitals.has_pixel', 'st_vitals.address_details_provided', 'st_vitals.has_result', 'st_vitals.yext_api_success', 'st_vitals.has_mobile_site', 'st_vitals.is_mobile', 'st_vitals.is_tablet', 'st_vitals.mobile_screenshot_url', 'st_vitals.mobile_site_url', 'st_vitals.average_monthly_traffic', 'st_vitals.adwords_keywords', 'st_vitals.has_adwords_spend', 'st_vitals.has_sitemap', 'st_vitals.sitemap_issues', 'st_vitals.stale_analysis', 'st_video.website_traffic', 'company.company_id','company.password2','company.Client_number','company.Development_link', 'company.cname', 'company.email', 'company.company_url', 'company.phone', 'company.address1', 'company.address2', 'company.city', 'company.state_name', 'company.zip', 'company.country', 'company.media_partner', 'company.status', 'company.updated_at', 'company.created_at', 'st_domain.is_parked', 'st_domain.has_ssl', 'st_domain.ssl_expired', 'st_domain.ssl_redirect', 'st_domain.ssl_valid', 'st_domain_info.domain_age_days', 'st_domain_info.expiry_date', 'st_domain_info.registered_date', 'st_discovered.emails', 'st_discovered.phones', 'st_social.has_instagram', 'st_social.days_since_update', 'st_social.last_updated_date', 'st_social.twitter_found')
            ->where('company.company_id', $id)
            ->first();

        $website_images = DB::table('company')
            ->leftjoin('brandfetch_primary', 'brandfetch_primary.company_id', '=', 'company.company_id')
            ->leftjoin('br_website_images', 'brandfetch_primary.br_id', '=', 'br_website_images.br_id')
            ->select('br_website_images.wim_id', 'br_website_images.image_url', 'br_website_images.webimg_pic_id')
            ->where('company.company_id', $id)
            ->get();

        // echo '<pre>'; print_r($website_images); die();
        $website_brandfetchata = DB::table('company')
            ->leftjoin('brandfetch_primary', 'brandfetch_primary.company_id', '=', 'company.company_id')
            ->leftjoin('br_logo', 'brandfetch_primary.br_id', '=', 'br_logo.br_id')
            ->leftjoin('br_company_social_media', 'brandfetch_primary.br_id', '=', 'br_company_social_media.br_id')
            ->leftjoin('br_website_meta_data', 'brandfetch_primary.br_id', '=', 'br_website_meta_data.br_id')
            ->leftjoin('br_logo_colour_raw', 'brandfetch_primary.br_id', '=', 'br_logo_colour_raw.br_id')
            ->leftjoin('br_website_font', 'brandfetch_primary.br_id', '=', 'br_website_font.br_id')
            ->leftjoin('br_web_colour_category', 'brandfetch_primary.br_id', '=', 'br_web_colour_category.br_id')
            ->leftjoin('br_web_colour_raw', 'brandfetch_primary.br_id', '=', 'br_web_colour_raw.br_id')
            ->select('br_logo.web_logo', 'br_company_social_media.twitter', 'br_company_social_media.linkedin', 'br_company_social_media.github', 'br_company_social_media.facebook', 'br_company_social_media.youtube', 'br_company_social_media.pinterest', 'br_company_social_media.crunchbase', 'brandfetch_primary.web_url', 'brandfetch_primary.created_at', 'br_website_meta_data.title', 'br_website_meta_data.summary', 'br_website_meta_data.description', 'br_website_meta_data.keywords', 'br_website_meta_data.language', 'br_logo_colour_raw.l_colour_code', 'br_logo_colour_raw.l_percentage', 'br_website_font.title_tag', 'br_website_font.primary_font', 'br_web_colour_category.vibrant', 'br_web_colour_category.dark', 'br_web_colour_category.light', 'br_web_colour_raw.colour_code', 'br_web_colour_raw.percentage')
            ->where('company.company_id', $id)
            ->first();

        //echo '<pre>'; print_r($website_brandfetchata); die();

        $states = DB::table('states_db')
            ->get();

        $media_partner = DB::table('mediapartners')
            ->get();

        $sem_dispadreports = DB::table('sem_displayadreport')
            ->select('report_id', 'company_id', 'title', 'text', 'first_seen', 'last_seen', 'times_seen', 'visible_url')
            ->where('company_id', '=', $id)
            ->get();

        $sem_marketoverview = DB::table('sem_domain_overview')
            ->select('overview_id', 'database_rgn', 'domain', 'rank', 'organic_keywords', 'organic_traffic', 'organic_cost', 'adwords_keyword', 'adwords_traffic', 'adwords_cost', 'pla_keywords', 'pla_uniques')
            ->where('company_id', '=', $id)
            ->first();

        $sem_advertisers = DB::table('sem_advertisers')
            ->select('advertiser_id', 'domain', 'ads_count', 'first_seen', 'last_seen', 'times_seen')
            ->where('company_id', '=', $id)
            ->get();

        $sem_publisher_rank = DB::table('sem_publisher_rank')
            ->select('domain', 'ads_overall', 'text_ads_overall', 'media_ads_overall', 'first_seen', 'last_seen', 'times_seen', 'domain_overall')
            ->where('company_id', '=', $id)
            ->first();

        $basic_scan_id = DB::table('create_scan_basic')
            ->select('basic_id')
            ->where('company_id', '=', $id)
            ->first();

        $yext_res = DB::table('create_scan_basic')
            ->join('scan_result', 'create_scan_basic.basic_id', '=', 'scan_result.scn_basic_id')
            ->select('scan_result.siteId', 'scan_result.scn_basic_id', 'scan_result.name AS compname', 'scan_result.address', 'scan_result.phone', 'scan_result.zip', 'scan_result.url')
            ->where('create_scan_basic.company_id', '=', $id)
            ->get();


        $sitedata = DB::table('company_sitedata')
            ->select('site_id', 'page_count', 'page_url', 'content_path', 'created_date')
            ->where('company_id', '=', $id)
            ->first();

        if (!empty($basic_scan_id)) {
            $bs_id = $basic_scan_id->basic_id;

            $sc_det = DB::table('scan_result')
                ->select(DB::raw('SUM(match_name_score) as name_sum'), DB::raw('SUM(match_phone_score) as phone_sum'), DB::raw('SUM(match_address_score) as address_sum'))
                ->where('scn_basic_id', '=', $bs_id)
                ->first();

            $review_counts = DB::table('scan_result')
                ->where('review_count_percentile', '!=', 100)
                ->where('review_score_percentile', '!=', 100)
                ->select(DB::raw('SUM(review_count_percentile) as no_of_review'), DB::raw('SUM(review_score_percentile) as review_rating'))
                ->where('scn_basic_id', '=', $bs_id)
                ->first();

            $missing_schema_count = DB::table('scan_result')
                ->select('review_count')
                ->where('review_count', '=', 0)
                ->where('scn_basic_id', '=', $bs_id)
                ->count();
        } else {
            $sc_det = $review_counts = $missing_schema_count = '';

        }

        $company_name = str_replace(' ', '%20', $company->cname);
        $hour_arr = array();

        /* Place Search API starts */

        $url = 'https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=' . $company_name . '&inputtype=textquery&key=AIzaSyAVzWibWlEc43MAKZk2N1PG6suW50uTnI4';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'api-key: 398a2e9cb7fc76f58678cc5bb41bfb5bd8f839c4',
            'Content-Type: application/json',
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        $result = curl_exec($curl);

        if (!$result) {
            //die("Connection Failure");
            $hour_arr = '';
        } else {
            curl_close($curl);
            $result = json_decode($result, true);

            if (isset($result['candidates']) && !empty($result['candidates'])) {
                $place_id = $result['candidates'][0]['place_id'];

                /* Place Search API ends */

                /* Place Details API starts */

                $hour_url = 'https://maps.googleapis.com/maps/api/place/details/json?place_id=' . $place_id . '&fields=opening_hours/weekday_text&key=AIzaSyAVzWibWlEc43MAKZk2N1PG6suW50uTnI4';

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $hour_url);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'api-key: 398a2e9cb7fc76f58678cc5bb41bfb5bd8f839c4',
                    'Content-Type: application/json',
                ));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

                $res = curl_exec($ch);

                if (!$res) {die("Connection Failure");}
                curl_close($ch);
                $res = json_decode($res, true);
                // echo '<pre>'; print_r($res); die();

                if (isset($res['result']) && !empty($res['result'])) {
                    $hour_arr = $res['result']['opening_hours']['weekday_text'];
                }

                /* Place Details API ends */
            }
        }
        $cloudImagetag=str_replace(' ', '_', $company->cname);

        return view('editcompany', compact('company', 'website_images', 'hour_arr', 'states', 'website_brandfetchata', 'sem_dispadreports', 'sem_marketoverview', 'sem_advertisers', 'sem_publisher_rank', 'basic_scan_id', 'yext_res', 'sc_det', 'review_counts', 'missing_schema_count', 'media_partner', 'sitedata','cloudImagetag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $current_date = date('Y-m-d H:i:s');
        $validatedData = $request->validate([
            'cname' => 'required|max:255',
            'email' => 'required|email',
            'company_url' => 'required',
            'phone' => 'required',
            'address1' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zip' => 'required',
            'country' => 'required',
            'media_partner' => 'required',
        ]);
        $validatedData['created_at'] = $current_date;

        $password=Hash::make($request->password);
        $password2=$request->password;
        if(strlen($request->password)==0){
            $password=Hash::make($request->Client_number);
            $password2=$request->Client_number;
        }
        DB::table('company')
            ->where('company_id', $id)
            ->update(['cname' => $request->cname, 'email' => $request->email, 'company_url' => $request->company_url, 'phone' => $request->phone, 'address1' => $request->address1, 'address2' => $request->address2, 'city' => $request->city, 'state_name' => $request->state, 'zip' => $request->zip, 'country' => $request->country, 'media_partner' => $request->media_partner, 'updated_at' => $current_date,'Client_number'=>$request->Client_number,'password'=>$password,'Development_link'=>$request->Development_link,'password2'=>$password2]);

        //send update to knack
        $company= DB::table('company')->where('company_id', $id)->first();
        if($company->knackID){
            $data=json_encode(array(
                'field_1'=>$id,
                'field_2'=>$request->cname,
                'field_3'=>$request->address1,
                'field_4'=>$request->email,
                'field_5'=>$request->phone,
                'field_6'=>$request->company_url,
                'field_7'=>$request->media_partner,
                'field_337'=>$request->Development_link,
                'field_338'=>$request->Client_number,
                'field_339'=>$password2
            ));

            $curl = curl_init();

            curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.knack.com/v1/objects/object_1/records/".$company->knackID,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "PUT",
            CURLOPT_POSTFIELDS =>$data,
            CURLOPT_HTTPHEADER => array(
                "X-Knack-Application-Id: 5f7e2af82c55b5001636420b",
                "X-Knack-REST-API-Key: d2541fdd-43c2-4d4d-9350-c1b787bfe87c",
                "Content-Type: application/json",
                "Cookie: connect.sid=s%3A-599MYE7MII1h8OXi_XdmlzFcZ3oGfdp.v%2BU8UBVXsphpnw5W6ymm6%2Ft0RyKKY%2FXgL8FkXrNvDdY"
            ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);
        }
        return redirect('company')->with('status', 'Company details Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $current_date = date('Y-m-d H:i:s');
        $id = $request->companyid;
        DB::table('company')
            ->where('company_id', $id)
            ->update(['status' => 1, 'updated_at' => $current_date]);
        return "1";
    }

    public function playgame($id)
    {
        $company = DB::table('company')
            ->select('company_id', 'cname', 'company_url')
            ->where('company_id', $id)
            ->first();

        return view('playgame', compact('company'));
    }

    public function fetchapires(Request $request)
    {
        $datetime = date('Y-m-d H:i:s');
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        /* ------ Silktide Prospect API ----- */
        if(!$request->knackID){
            $user           = auth()->user();
            $user_id        = $user->id;
        }
        else{
            $user_id=1;
        }

        // Start logging of playgame //
        $countError=0;
        $logs = new LogsTable();
        $logs->userId = auth()->user()->id;
        $logs->companyId = $request->company_id;
        $logs->content = 'Scanned';
        $logs->level   = 'info';
        $logs->save();
        $logs->uniqueId = $logs->id;
        $logs->save();
        //logged//

        $webUrl         = $request->web_url;
        $company_id     = $request->company_id;
        $company_name   = DB::table('company')
                            ->where('company_id', '=', $company_id)
                            ->value('cname');

        $company        = DB::table('silktide_prospect')
                            ->select('company_id', 'st_id', 'url')
                            ->where('company_id', $company_id)
                            ->first();
        //if ($silktideCount == 0) {
            $data   = json_encode(array('url' => $webUrl));
            $url    = 'https://api.prospect.silktide.com/api/v1/report';

            $curl   = curl_init();
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'api-key: 398a2e9cb7fc76f58678cc5bb41bfb5bd8f839c4',
                'Content-Type: application/json',
            ));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

            $result = curl_exec($curl);
            $result = json_decode($result, true);
            // print_r($result);
            if (!$result || (isset($result['status']) && $result['status']=='error')) {
                if($result['issue']=='redirect'){
                    $r['error'] = "The web address you entered redirects to another website. If you'd like to test this website instead, change your url to <a href=".$result['url'].">".$result['url']."</a>";
                    Log::error($company_name.": The web address you entered redirects to another website. If you'd like to test this website instead, change your url to ".$result['url']);
                    // Start logging of playgame //
                    $countError++;
                    $log = new LogsTable();
                    $log->userId = auth()->user()->id;
                    $log->companyId = $request->company_id;
                    $log->apiName = 'Silktide Report';
                    $log->api = 'Silktide';
                    $log->content = $company_name.": The web address you entered redirects to another website. If you'd like to test this website instead, change your url to ".$result['url'];
                    $log->level   = 'error';
                    $log->save();
                    $log->uniqueId = $logs->id;
                    $log->save();
                    //logged//
                }
                $r['api'] = 'API Silktide';
                $r['error'] = $result['error'];
                Log::error($company_name.": ".$r['api'].": ".$r['error']);
                // Start logging of playgame //
                $countError++;
                $log = new LogsTable();
                $log->userId = auth()->user()->id;
                $log->companyId = $request->company_id;
                $log->apiName = 'Silktide Report';
                $log->api = 'Silktide';
                $log->content = $result['error'];
                $log->level   = 'error';
                $log->save();
                $log->uniqueId = $logs->id;
                $log->save();
                //logged//
            }else{
                curl_close($curl);

                $reportId = $result['reportId'];

                $detail_url = "https://api.prospect.silktide.com/api/v1/report/" . $reportId;

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $detail_url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'api-key: 398a2e9cb7fc76f58678cc5bb41bfb5bd8f839c4',
                    'Content-Type: application/json',
                ));

                $fetch_data = curl_exec($ch);
                $fetch_data = json_decode($fetch_data, true);
                if (!$fetch_data || $fetch_data['status']=='error') {
                    $r['api'] = 'API Silktide';
                    $r['error'] = $fetch_data['message'];
                    Log::error($company_name.": ".$r['api'].": ".$r['error']);
                    // Start logging of playgame //
                    $countError++;
                    $log = new LogsTable();
                    $log->userId = auth()->user()->id;
                    $log->companyId = $request->company_id;
                    $log->apiName = 'Silktide Report';
                    $log->api = 'Silktide';
                    $log->content =$r['error'];
                    $log->level   = 'error';
                    $log->save();
                    $log->uniqueId = $logs->id;
                    $log->save();
                    //logged//
                }else{

                /* --- Save to 'silktide_prospect' table -- */
                    // if (empty($company)) {
                    $st_Id = DB::table('silktide_prospect')
                        ->insertGetId([
                            'report_id' => $reportId,
                            'account_id' => $fetch_data['report']['account_id'],
                            'company_id' => $company_id,
                            'url' => $webUrl,
                            'user_id' => $user_id,
                            'created_at' => $datetime,
                        ]);
                    // }else{
                    //     $st_Id = $company->st_id;
                    // }

                    /* --- Save to 'st_sitecontent' table --- */
                    DB::table('st_sitecontent')
                    ->updateOrInsert(
                        ['st_id' => $st_Id],
                        ['average_words_per_page' => (isset($fetch_data['report']['amount_of_content']['average_words_per_page'])) ? $fetch_data['report']['amount_of_content']['average_words_per_page'] : 0,
                        'pages_found' => (isset($fetch_data['report']['amount_of_content']['pages_found'])) ? $fetch_data['report']['amount_of_content']['pages_found'] : 0,
                        'total_word_count' => (isset($fetch_data['report']['amount_of_content']['total_word_count'])) ? $fetch_data['report']['amount_of_content']['total_word_count'] : 0,
                        'cms_solution' => (isset($fetch_data['report']['content_management_system']['cms_solution']) && !empty($fetch_data['report']['content_management_system']['cms_solution'])) ? implode(',', $fetch_data['report']['content_management_system']['cms_solution']) : '',
                        'has_cms' => (isset($fetch_data['report']['content_management_system']['has_cms'])) ? $fetch_data['report']['content_management_system']['has_cms'] : 0,
                        'pages_discovered_count' => (isset($fetch_data['report']['page_count']['pages_discovered_count'])) ? $fetch_data['report']['page_count']['pages_discovered_count'] : 0,
                        'created_at' => $datetime,]
                    );

                    /* --- Save to 'st_vitals' table --- */
                    DB::table('st_vitals')
                    ->updateOrInsert(
                        ['st_id' => $st_Id],
                        ['pages_tested' => (isset($fetch_data['report']['amount_of_content']['pages_tested'])) ? $fetch_data['report']['amount_of_content']['pages_tested'] : 0,
                        'analytics_tool' => (isset($fetch_data['report']['analytics']['analytics_tool'])) ? $fetch_data['report']['analytics']['analytics_tool'] : '',
                        'has_analytics' => (isset($fetch_data['report']['analytics']['has_analytics'])) ? $fetch_data['report']['analytics']['has_analytics'] : 0,
                        'domain' => (isset($fetch_data['report']['domain'])) ? $fetch_data['report']['domain'] : '',
                        'ecommerce_name' => (isset($fetch_data['report']['ecommerce']['ecommerce_name'])) ? $fetch_data['report']['ecommerce']['ecommerce_name'] : '',
                        'has_ecommerce' => (isset($fetch_data['report']['ecommerce']['has_ecommerce'])) ? $fetch_data['report']['ecommerce']['has_ecommerce'] : 0,
                        'has_pixel' => (isset($fetch_data['report']['facebook_retargeting']['has_pixel'])) ? $fetch_data['report']['facebook_retargeting']['has_pixel'] : 0,
                        'address_details_provided' => (isset($fetch_data['report']['local_presence']['address_details_provided'])) ? $fetch_data['report']['local_presence']['address_details_provided'] : 0,
                        'has_result' => (isset($fetch_data['report']['local_presence']['has_result'])) ? $fetch_data['report']['local_presence']['has_result'] : 0,
                        'yext_api_success' => (isset($fetch_data['report']['local_presence']['yext_api_success'])) ? $fetch_data['report']['local_presence']['yext_api_success'] : 0,
                        'has_mobile_site' => (isset($fetch_data['report']['mobile']['has_mobile_site'])) ? $fetch_data['report']['mobile']['has_mobile_site'] : 0,
                        'is_mobile' => (isset($fetch_data['report']['mobile']['is_mobile'])) ? $fetch_data['report']['mobile']['is_mobile'] : 0,
                        'is_tablet' => (isset($fetch_data['report']['mobile']['is_tablet'])) ? $fetch_data['report']['mobile']['is_tablet'] : 0,
                        'mobile_screenshot_url' => (isset($fetch_data['report']['mobile']['mobile_screenshot_url'])) ? $fetch_data['report']['mobile']['mobile_screenshot_url'] : '',
                        'mobile_site_url' => (isset($fetch_data['report']['mobile']['mobile_site_url'])) ? $fetch_data['report']['mobile']['mobile_site_url'] : '',
                        'average_monthly_traffic' => (isset($fetch_data['report']['organic_search']['average_monthly_traffic'])) ? $fetch_data['report']['organic_search']['average_monthly_traffic'] : 0,
                        'adwords_keywords' => (isset($fetch_data['report']['paid_search']['adwords_keywords']) && !empty($fetch_data['report']['paid_search']['adwords_keywords'])) ? implode(',', $fetch_data['report']['paid_search']['adwords_keywords']) : '',
                        'has_adwords_spend' => (isset($fetch_data['report']['paid_search']['has_adwords_spend'])) ? $fetch_data['report']['paid_search']['has_adwords_spend'] : 0,
                        'has_sitemap' => (isset($fetch_data['report']['sitemap']['has_sitemap'])) ? $fetch_data['report']['sitemap']['has_sitemap'] : 0,
                        'sitemap_issues' => (isset($fetch_data['report']['sitemap']['sitemap_issues'])) ? $fetch_data['report']['sitemap']['sitemap_issues'] : 0,
                        'stale_analysis' => (isset($fetch_data['report']['stale_analysis'])) ? $fetch_data['report']['stale_analysis'] : 0,
                        'created_at' => $datetime,]
                    );

                    /* --- Save to 'st_discovered' table --- */
                    DB::table('st_discovered')
                    ->updateOrInsert(
                        ['st_id' => $st_Id],
                        ['email' => (isset($fetch_data['report']['contact_details']['email']) && !empty($fetch_data['report']['contact_details']['email'])) ? implode(',', $fetch_data['report']['contact_details']['email']) : '',
                        'emails' => (isset($fetch_data['report']['contact_details']['emails']) && !empty($fetch_data['report']['contact_details']['emails'])) ? implode(',', $fetch_data['report']['contact_details']['emails']) : '',
                        'phone' => (isset($fetch_data['report']['contact_details']['phone']) && !empty($fetch_data['report']['contact_details']['phone'])) ? implode(',', $fetch_data['report']['contact_details']['phone']) : '',
                        'phones' => (isset($fetch_data['report']['contact_details']['phones']) && !empty($fetch_data['report']['contact_details']['phones'])) ? implode(',', $fetch_data['report']['contact_details']['phones']) : '',
                        'created_at' => $datetime,]
                    );

                    /* --- Save to 'st_domain_info' table --- */
                    DB::table('st_domain_info')
                    ->updateOrInsert(
                        ['st_id' => $st_Id],
                        ['domain_age_days' => (isset($fetch_data['report']['domain_age']['domain_age_days'])) ? $fetch_data['report']['domain_age']['domain_age_days'] : '',
                        'expiry_date' => (isset($fetch_data['report']['domain_age']['expiry_date'])) ? $fetch_data['report']['domain_age']['expiry_date'] : '',
                        'registered_date' => (isset($fetch_data['report']['domain_age']['registered_date'])) ? $fetch_data['report']['domain_age']['registered_date'] : '',
                        'created_at' => $datetime,]
                    );

                    /* --- Save to 'st_domain' table --- */
                    DB::table('st_domain')
                    ->updateOrInsert(
                        ['st_id' => $st_Id],
                        ['is_parked' => (isset($fetch_data['report']['parked_domain_detection']['is_parked'])) ? $fetch_data['report']['parked_domain_detection']['is_parked'] : 0,
                        'has_ssl' => (isset($fetch_data['report']['ssl']['has_ssl'])) ? $fetch_data['report']['ssl']['has_ssl'] : '',
                        'ssl_expired' => (isset($fetch_data['report']['ssl']['ssl_expired'])) ? $fetch_data['report']['ssl']['ssl_expired'] : '',
                        'ssl_redirect' => (isset($fetch_data['report']['ssl']['ssl_redirect'])) ? $fetch_data['report']['ssl']['ssl_redirect'] : '',
                        'ssl_valid' => (isset($fetch_data['report']['ssl']['ssl_valid'])) ? $fetch_data['report']['ssl']['ssl_valid'] : '',
                        'created_at' => $datetime,]
                    );

                    /* --- Save to 'st_seo' table --- */
                    DB::table('st_seo')
                    ->updateOrInsert(
                        ['st_id' => $st_Id],
                        ['good_headings' => (isset($fetch_data['report']['headings']['good_headings'])) ? $fetch_data['report']['headings']['good_headings'] : 0,
                        'has_content_for_every_heading' => (isset($fetch_data['report']['headings']['has_content_for_every_heading'])) ? $fetch_data['report']['headings']['has_content_for_every_heading'] : 0,
                        'has_hierarchical_headings' => (isset($fetch_data['report']['headings']['has_hierarchical_headings'])) ? $fetch_data['report']['headings']['has_hierarchical_headings'] : 0,
                        'has_single_h1_on_each_page' => (isset($fetch_data['report']['headings']['has_single_h1_on_each_page'])) ? $fetch_data['report']['headings']['has_single_h1_on_each_page'] : 0,
                        'analysis_country' => (isset($fetch_data['report']['meta']['analysis_country'])) ? $fetch_data['report']['meta']['analysis_country'] : '',
                        'detected_address' => (isset($fetch_data['report']['meta']['detected_address'])) ? $fetch_data['report']['meta']['detected_address'] : '',
                        'detected_name' => (isset($fetch_data['report']['meta']['detected_name'])) ? $fetch_data['report']['meta']['detected_name'] : '',
                        'detected_phone' => (isset($fetch_data['report']['meta']['detected_phone'])) ? $fetch_data['report']['meta']['detected_phone'] : '',
                        'num_keywords_ranked_for' => (isset($fetch_data['report']['organic_search']['num_keywords_ranked_for'])) ? $fetch_data['report']['organic_search']['num_keywords_ranked_for'] : 0,
                        'top_keywords_ranked_for' => (isset($fetch_data['report']['organic_search']['top_keywords_ranked_for'])) ? json_encode($fetch_data['report']['organic_search']['top_keywords_ranked_for']) : '',
                        'duplicated_items' => (isset($fetch_data['report']['page_titles_and_descriptions']['duplicated_items'])) ? $fetch_data['report']['page_titles_and_descriptions']['duplicated_items'] : 0,
                        'homepage_title_tag' => (isset($fetch_data['report']['page_titles_and_descriptions']['homepage_title_tag'])) ? $fetch_data['report']['page_titles_and_descriptions']['homepage_title_tag'] : '',
                        'missing_items' => (isset($fetch_data['report']['page_titles_and_descriptions']['missing_items'])) ? $fetch_data['report']['page_titles_and_descriptions']['missing_items'] : '',
                        'pages_duplicated_description_count' => (isset($fetch_data['report']['page_titles_and_descriptions']['pages_duplicated_description_count'])) ? $fetch_data['report']['page_titles_and_descriptions']['pages_duplicated_description_count'] : 0,
                        'pages_duplicated_title_count' => (isset($fetch_data['report']['page_titles_and_descriptions']['pages_duplicated_title_count'])) ? $fetch_data['report']['page_titles_and_descriptions']['pages_duplicated_title_count'] : 0,
                        'pages_missing_description_count' => (isset($fetch_data['report']['page_titles_and_descriptions']['pages_missing_description_count'])) ? $fetch_data['report']['page_titles_and_descriptions']['pages_missing_description_count'] : 0,
                        'pages_missing_title_count' => (isset($fetch_data['report']['page_titles_and_descriptions']['pages_missing_title_count'])) ? $fetch_data['report']['page_titles_and_descriptions']['pages_missing_title_count'] : 0,
                        'percent_duplicated_descriptions' => (isset($fetch_data['report']['page_titles_and_descriptions']['percent_duplicated_descriptions'])) ? $fetch_data['report']['page_titles_and_descriptions']['percent_duplicated_descriptions'] : 0,
                        'percent_duplicated_titles' => (isset($fetch_data['report']['page_titles_and_descriptions']['percent_duplicated_titles'])) ? $fetch_data['report']['page_titles_and_descriptions']['percent_duplicated_titles'] : 0,
                        'percent_missing_descriptions' => (isset($fetch_data['report']['page_titles_and_descriptions']['percent_missing_descriptions'])) ? $fetch_data['report']['page_titles_and_descriptions']['percent_missing_descriptions'] : 0,
                        'percent_missing_titles' => (isset($fetch_data['report']['page_titles_and_descriptions']['percent_missing_titles'])) ? $fetch_data['report']['page_titles_and_descriptions']['percent_missing_titles'] : 0,
                        'created_at' => $datetime,]
                    );

                    /* --- Save to 'st_images' table --- */
                    DB::table('st_images')
                    ->updateOrInsert(
                        ['st_id' => $st_Id],
                        ['image_count' => (isset($fetch_data['report']['images']['image_count'])) ? $fetch_data['report']['images']['image_count'] : 0,
                        'non_web_friendly_count' => (isset($fetch_data['report']['images']['non_web_friendly_count'])) ? $fetch_data['report']['images']['non_web_friendly_count'] : 0,
                        'percent_images_sized' => (isset($fetch_data['report']['images']['percent_images_sized'])) ? $fetch_data['report']['images']['percent_images_sized'] : 0,
                        'stretched_image_count' => (isset($fetch_data['report']['images']['stretched_image_count'])) ? $fetch_data['report']['images']['stretched_image_count'] : 0,
                        'created_at' => $datetime,]
                    );

                    /* --- Save to 'st_social' table --- */
                    DB::table('st_social')
                    ->updateOrInsert(
                        ['st_id' => $st_Id],
                        ['has_instagram' => (isset($fetch_data['report']['instagram_account']['has_instagram'])) ? $fetch_data['report']['instagram_account']['has_instagram'] : 0,
                        'days_since_update' => (isset($fetch_data['report']['last_updated']['days_since_update'])) ? $fetch_data['report']['last_updated']['days_since_update'] : 0,
                        'last_updated_date' => (isset($fetch_data['report']['last_updated']['last_updated_date'])) ? $fetch_data['report']['last_updated']['last_updated_date'] : '',
                        'twitter_found' => (isset($fetch_data['report']['twitter']['found'])) ? $fetch_data['report']['twitter']['found'] : 0,
                        'created_at' => $datetime,]
                    );

                    /* --- Save to 'st_video' table --- */
                    DB::table('st_video')
                    ->updateOrInsert(
                        ['st_id' => $st_Id],
                        ['vendor_vendor' => (isset($fetch_data['report']['vendor']['vendor'])) ? $fetch_data['report']['vendor']['vendor'] : '',
                        'has_video' => (isset($fetch_data['report']['video']['has_video'])) ? $fetch_data['report']['video']['has_video'] : 0,
                        'video_vendor' => (isset($fetch_data['report']['video']['vendor'])) ? $fetch_data['report']['video']['vendor'] : '',
                        'website_traffic' => (isset($fetch_data['report']['website_traffic']['website_traffic'])) ? $fetch_data['report']['website_traffic']['website_traffic'] : 0,
                        'created_at' => $datetime,]
                    );
                }
        }

        /* Brandfetch Api * /
        /*--- Save to 'brandfetch_primary' table ---*/
        $brcheckExists = DB::table('brandfetch_primary')
            ->select('br_id', 'company_id')
            ->where('company_id', '=', $company_id)
            ->first();

        // if (!empty($brcheckExists)) {
        //     $br_Id = $brcheckExists->br_id;
        // } else {
            $br_Id = DB::table('brandfetch_primary')
                ->insertGetId([
                    'web_url' => $webUrl,
                    'created_at' => $datetime,
                    'company_id' => $company_id,
                    'user_id' => $user_id,
                ]);
                $web_log_message = "";
                $message = "";
                $message = "Saved primary data to bradfetch_primary. ID: " . $br_Id;
                Log::info($message);
        // }

        $checklist_weblogo = $request->checklist_weblogo;
        //$checklist_socialmedialogo = $request->checklist_socialmedialogo;
        $checklist_websitecolors = $request->checklist_websitecolors;
        $checklist_colorsfromlogo = $request->checklist_colorsfromlogo;
        //$checklist_alternativecolors = $request->checklist_alternativecolors;
        $checklist_websitefont = $request->checklist_websitefont;
        $checklist_websitescreenshot = $request->checklist_websitescreenshot;
        $checklist_companysocialmedia = $request->checklist_companysocialmedia;
        $checklist_websiteimages = $request->checklist_websiteimages;
        $checklist_websitemetadata = $request->checklist_websitemetadata;
        $checklist_dispadvertisingreports = $request->checklist_dispadvertisingreports;
        $checklist_marketingoverview = $request->checklist_marketingoverview;
        $checklist_advertisers = $request->checklist_advertisers;
        $checklist_publisher_rank = $request->checklist_publisher_rank;
        $checklist_localdata = $request->checklist_localdata;
        $checklist_sitecontent = $request->checklist_sitecontent;

        /* API to fetch SEM Rush Analytics - Display Advertising Reports - Advertisers starts */
        if ($checklist_advertisers != '') {
            // if ($sem_advertiserCount == 0) {
                $comp_det = DB::table('company')
                    ->select('cname', 'company_url')
                    ->where('company_id', '=', $company_id)
                    ->first();

                if (!empty($comp_det->company_url)) {
                    $sem_advertiserurl = 'https://api.semrush.com/analytics/da/v2/?action=report&key=e1f9daf365615157eaa1906ac391a0dd&domain=' . $comp_det->company_url . '&type=publisher_advertisers';

                    $curl = curl_init();
                    curl_setopt($curl, CURLOPT_URL, $sem_advertiserurl);
                    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                    ));
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

                    $result = curl_exec($curl);
                    curl_close($curl);
                    // $result = json_encode($result, true);
                    if ($result == "ERROR 50 :: NOTHING FOUND" || isset($result->errorMessage)){
                        if(isset($result->errorMessage)){
                            $r['api'] = 'Advertisers';
                            $r['error'] = $result->errorMessage;
                            Log::error($company_name.": ".$r['api'].": ".$r['error']);
                             // Start logging of playgame //
                            $countError++;
                            $log = new LogsTable();
                            $log->userId = auth()->user()->id;
                            $log->companyId = $request->company_id;
                            $log->apiName = 'API to Display Advertising Reports';
                            $log->api = 'Semrush';
                            $log->content =$r['error'];
                            $log->level   = 'error';
                            $log->save();
                            $log->uniqueId = $logs->id;
                            $log->save();
                            //logged//
                        }
                        $r['api'] = 'Advertisers';
                        $r['error'] = $result;
                        Log::error($company_name.": ".$r['api'].": ".$r['error']);
                         // Start logging of playgame //
                        $countError++;
                        $log = new LogsTable();
                        $log->userId = auth()->user()->id;
                        $log->companyId = $request->company_id;
                        $log->apiName = 'API to Display Advertising Reports';
                        $log->api = 'Semrush';
                        $log->content =$r['error'];
                        $log->level   = 'error';
                        $log->save();
                        $log->uniqueId = $logs->id;
                        $log->save();
                        //logged//
                    }else{
                        $result = json_encode($result, true);
                        $response_ar = explode('\n', $result);
                        foreach (array_slice($response_ar, 1) as $key => $value) {
                            $data_ar = explode(';', $value);

                            if ($value != end($data_ar)) {
                                DB::table('sem_advertisers')
                                ->updateOrInsert(['company_id' => $company_id,'domain' => (isset($data_ar[0])) ? $data_ar[0] : ''],
                                        ['ads_count' => (isset($data_ar[1])) ? $data_ar[1] : '',
                                        'first_seen' => (isset($data_ar[2])) ? $data_ar[2] : '',
                                        'last_seen' => (isset($data_ar[3])) ? $data_ar[3] : '',
                                        'times_seen' => (isset($data_ar[4])) ? $data_ar[4] : '',
                                        'created_at' => $datetime,
                                    ]);
                            }

                        }
                    }
                } else {
                    $message = $comp_det->cname . " No Web URL";
                    Log::error($result);
                }
            // }
        }
        /* API to fetch SEM Rush Analytics - Display Advertising Reports - Advertisers ends */

        /*--- API to fetch Company Social Media starts ---*/
        if ($checklist_companysocialmedia != '') {
            // if ($br_sociallogoCount == 0) {
                $company_social_api_url = 'https://api.brandfetch.io/v1/company';
                $company_data = json_encode(array("domain" => $webUrl, "fresh" => true));

                $ch_company = curl_init();
                curl_setopt($ch_company, CURLOPT_POST, 1);
                curl_setopt($ch_company, CURLOPT_POSTFIELDS, $company_data);
                curl_setopt($ch_company, CURLOPT_URL, $company_social_api_url);
                curl_setopt($ch_company, CURLOPT_HTTPHEADER, array(
                    'x-api-key: oouXaXg0zf77frXoUmsWp9ncqgdHwyaT6BlOaB3a',
                    'Content-Type: application/json',
                ));
                curl_setopt($ch_company, CURLOPT_TIMEOUT, 280);
                curl_setopt($ch_company, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch_company, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                curl_setopt($ch_company, CURLOPT_CONNECTTIMEOUT, 0);

                $result_company = curl_exec($ch_company);
                $res_company_array = json_decode($result_company);
                if (!$res_company_array || (isset($res_company_array->statusCode) && $res_company_array->statusCode!='200') || (isset($res_company_array->errorMessage))) {
                    if(isset($res_company_array->statusCode)){
                        $r['api'] = 'Company Social Media';
                        $r['error'] = $res_company_array->response;
                        Log::error($company_name.": ".$r['api'].": ".$r['error']);
                         // Start logging of playgame //
                        $countError++;
                        $log = new LogsTable();
                        $log->userId = auth()->user()->id;
                        $log->companyId = $request->company_id;
                        $log->apiName = 'API to fetch Company Social Media';
                        $log->api = 'Brandfetch';
                        $log->content =$r['error'];
                        $log->level   = 'error';
                        $log->save();
                        $log->uniqueId = $logs->id;
                        $log->save();
                        //logged//
                    }
                    if(isset($res_company_array->errorMessage)){
                        $r['api'] = 'Company Social Media';
                        $r['error'] = $res_company_array->errorMessage;
                        Log::error($company_name.": ".$r['api'].": ".$r['error']);
                         // Start logging of playgame //
                        $countError++;
                        $log = new LogsTable();
                        $log->userId = auth()->user()->id;
                        $log->companyId = $request->company_id;
                        $log->apiName = 'API to fetch Company Social Media';
                        $log->api = 'Brandfetch';
                        $log->content =$r['error'];
                        $log->level   = 'error';
                        $log->save();
                        $log->uniqueId = $logs->id;
                        $log->save();
                        //logged//
                    }
                }
                curl_close($ch_company);
                $res_company_array = get_object_vars($res_company_array);

                if (isset($res_company_array) && !empty($res_company_array)) {
                    if(isset($res_company_array['response']->twitter->url)) { $twitter = $res_company_array['response']->twitter->url; } else { $twitter = ""; }
                    if(isset($res_company_array['response']->linkedin->url)) { $linkedin = $res_company_array['response']->linkedin->url; } else { $linkedin = ""; }
                    if(isset($res_company_array['response']->github->url)) { $github = $res_company_array['response']->github->url; } else { $github = ""; }
                    if(isset($res_company_array['response']->instagram->url)) { $instagram = $res_company_array['response']->instagram->url; } else { $instagram = ""; }
                    if(isset($res_company_array['response']->facebook->url)) { $facebook = $res_company_array['response']->facebook->url; } else { $facebook = ""; }
                    if(isset($res_company_array['response']->youtube->url)) { $youtube = $res_company_array['response']->youtube->url; } else { $youtube = ""; }
                    if(isset($res_company_array['response']->pinterest->url)) { $pinterest = $res_company_array['response']->pinterest->url; } else { $pinterest = ""; }
                    if(isset($res_company_array['response']->crunchbase->url)) { $crunchbase = $res_company_array['response']->crunchbase->url; } else { $crunchbase = ""; }

                    DB::table('br_company_social_media')
                        ->updateOrInsert(['br_id' => $br_Id],
                                ['twitter' => $twitter,
                                'linkedin' => $linkedin,
                                'github' => $github,
                                'instagram' => $instagram,
                                'facebook' => $facebook,
                                'youtube' => $youtube,
                                'pinterest' => $pinterest,
                                'crunchbase' => $crunchbase,
                                'br_cat_id' => 4,
                                'created_at' => $datetime,
                            ]);
                }
            // }
        }
        /*--- API to fetch Company Social Media ends ---*/

        /* API to fetch SEM Rush Analytics - Display Advertising Reports - Publisher Display Ads starts */
        if ($checklist_dispadvertisingreports != '') {
            // if ($sem_dispadvertisingreportsCount == 0) {
                $comp_det = DB::table('company')
                    ->select('cname', 'company_url')
                    ->where('company_id', '=', $company_id)
                    ->first();

                if (!empty($comp_det->company_url)) {
                    $sem_displayadurl = 'https://api.semrush.com/analytics/da/v2/?action=report&key=e1f9daf365615157eaa1906ac391a0dd&domain=' . $comp_det->company_url . '&type=publisher_text_ads';

                    $curl = curl_init();
                    curl_setopt($curl, CURLOPT_URL, $sem_displayadurl);
                    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                    ));
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

                    $result = curl_exec($curl);
                    // $result = json_encode($result, true);
                    curl_close($curl);
                    if ($result == "ERROR 50 :: NOTHING FOUND" || isset($result->errorMessage)){
                        if(isset($result->errorMessage)){
                            $r['api'] = 'Display Ad Reports';
                            $r['error'] = $result->errorMessage;
                            Log::error($company_name.": ".$r['api'].": ".$r['error']);
                            // Start logging of playgame //
                            $countError++;
                            $log = new LogsTable();
                            $log->userId = auth()->user()->id;
                            $log->companyId = $request->company_id;
                            $log->apiName = ' API - Publisher Display Ads';
                            $log->api = 'Semrush';
                            $log->content =$r['error'];
                            $log->level   = 'error';
                            $log->save();
                            $log->uniqueId = $logs->id;
                            $log->save();
                            //logged//
                        }
                        $r['api'] = 'Display Ad Reports';
                        $r['error'] = $result;
                        Log::error($company_name.": ".$r['api'].": ".$r['error']);
                        // Start logging of playgame //
                        $countError++;
                        $log = new LogsTable();
                        $log->userId = auth()->user()->id;
                        $log->companyId = $request->company_id;
                        $log->apiName = ' API  - Publisher Display Ads';
                        $log->api = 'Semrush';
                        $log->content =$r['error'];
                        $log->level   = 'error';
                        $log->save();
                        $log->uniqueId = $logs->id;
                        $log->save();
                        //logged//
                    }else{
                        $result = json_encode($result, true);
                        $response_ar = explode('\n', $result);
                        foreach (array_slice($response_ar, 1) as $key => $value) {
                            $data_ar = explode(';', $value);

                            if ($value != end($data_ar)) {
                                DB::table('sem_displayadreport')
                                    ->updateOrInsert(['company_id' => $company_id,'title' => (isset($data_ar[0])) ? $data_ar[0] : ''],
                                        ['text' => (isset($data_ar[1])) ? $data_ar[1] : '',
                                        'first_seen' => (isset($data_ar[2])) ? $data_ar[2] : '',
                                        'last_seen' => (isset($data_ar[3])) ? $data_ar[3] : '',
                                        'times_seen' => (isset($data_ar[4])) ? $data_ar[4] : '',
                                        'visible_url' => (isset($data_ar[5])) ? $data_ar[5] : '',
                                        'created_at' => $datetime,
                                    ]);
                            }
                        }
                    }

                } else {
                    $message = $comp_det->cname . " No Web URL";
                    Log::error($result);
                    $r['api'] = 'Display Ad Reports';
                    $r['error'] = $comp_det->cname . " No Web URL";
                    // Start logging of playgame //
                        $countError++;
                        $log = new LogsTable();
                        $log->userId = auth()->user()->id;
                        $log->companyId = $request->company_id;
                        $log->apiName = ' API - Publisher Display Ads';
                        $log->api = 'Semrush';
                        $log->content =$r['error'];
                        $log->level   = 'error';
                        $log->save();
                        $log->uniqueId = $logs->id;
                        $log->save();
                        //logged//
                }
            // }
        }
        /* API to fetch SEM Rush Analytics - Display Advertising Reports - Publisher Display Ads ends */

        /* YEXT API Starts */
        if ($checklist_localdata != '') {
            $yextBasicExists = DB::table('create_scan_basic')
                ->select('basic_id')
                ->where('company_id', '=', $company_id)
                ->first();
            if (!empty($yextBasicExists)) {
                DB::table('create_scan_basic')->where('basic_id', '=', $yextBasicExists->basic_id)->delete();
                DB::table('scan_result')->where('scn_basic_id', '=', $yextBasicExists->basic_id)->delete();
                DB::table('scan_website_result')->where('basic_id', '=', $yextBasicExists->basic_id)->delete();
            }

            $accounId = '2013495';
            $vparam = date('Ymd');

            $companydet = DB::table('company')
                ->select('company_id', 'cname', 'company_url', 'phone', 'address1')
                ->where('company_id', $company_id)
                ->first();

            $cname = $companydet->cname;
            $address = $companydet->address1;
            $cphone = $companydet->phone;

            /* Create Scan */

            $createScan_api_url = 'https://api.yext.com/v2/accounts/' . $accounId . '/scan?v=' . $vparam;
            $scan_data = json_encode(array("name" => $cname, "address" => $address, "phone" => $cphone, "url" => $webUrl));

            $ch_basic_scan = curl_init();
            curl_setopt($ch_basic_scan, CURLOPT_POST, 1);
            curl_setopt($ch_basic_scan, CURLOPT_POSTFIELDS, $scan_data);
            curl_setopt($ch_basic_scan, CURLOPT_URL, $createScan_api_url);
            curl_setopt($ch_basic_scan, CURLOPT_HTTPHEADER, array(
                'api-key: 754875a5dbaa9d1a3a86e9793eb6dee1',
                'Content-Type: application/json',
            ));
            curl_setopt($ch_basic_scan, CURLOPT_TIMEOUT, 400);
            curl_setopt($ch_basic_scan, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch_basic_scan, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

            $result_basic_scan = curl_exec($ch_basic_scan);
            $res_array = json_decode($result_basic_scan);

            if (!empty($res_array->meta->errors)) {
                $basicScan_log_message = "Error in Create Scan API. Date : " . $datetime;
                Log::error($basicScan_log_message);

                DB::table('scan_errors')
                    ->updateOrInsert(['company_id' => $company_id],
                        ['company_url' => $webUrl,
                        'code' => $res_array->meta->errors[0]->code,
                        'error_type' => $res_array->meta->errors[0]->type,
                        'message' => $res_array->meta->errors[0]->message,
                        // 'uuid' => $res_array->meta->uuid,
                        'created_at' => $datetime,
                    ]);
                $r['message'] = $res_array->meta->errors[0]->message;
                // Start logging of playgame //
                $countError++;
                $log = new LogsTable();
                $log->userId = auth()->user()->id;
                $log->companyId = $request->company_id;
                $log->apiName = 'YEXT API';
                $log->api = 'YEXT';
                $log->content =$r['message'];
                $log->level   = 'error';
                $log->save();
                $log->uniqueId = $logs->id;
                $log->save();
                //logged//
            } else {
                $uuid = $res_array->meta->uuid;
                $jobId = $res_array->response->jobId;
                $websiteScanJobId = $res_array->response->websiteScanJobId;

                DB::table('create_scan_basic')
                    ->updateOrInsert(['company_id' => $company_id],
                        ['company_url' => $webUrl,
                        'jobId' => $jobId,
                        'websiteScanJobId' => $websiteScanJobId,
                        'uuid' => $uuid,
                        'created_at' => $datetime,
                    ]);
                $basicScan = DB::table('create_scan_basic')->where('company_id',$company_id)->first();
                $basicScan_Id = $basicScan->basic_id;
                $basicScan_log_message = "Saved basic data to create_scan_basic. ID: " . $basicScan_Id . ". Date : " . $datetime;
                Log::info($basicScan_log_message);

                $i = 0;
                $cname = preg_replace('~[?&#\%<>]~','-',$cname);
                // Cloudder::delete('Joshua/'. $cname ."_Joshua/Yext/");
                foreach ($res_array->response->sites as $key => $value) {
                    $siteIds_arr[] = $value->siteId;
                    DB::table('create_scan_details')
                        ->updateOrInsert(['scn_id' => $basicScan_Id, 'siteId' => $value->siteId],
                            ['name' => $value->name,
                            'homepage' => $value->homepage,
                            'logo' => $value->logo->url,
                            'height' => $value->logo->height,
                            'width' => $value->logo->width,
                            'created_at' => $datetime,
                        ]);

                    $upload = Cloudder::upload($value->logo->url, 'Joshua/Yext/'. $value->siteId . "logo");
                }

                /* Generte Result */
                $siteIds = implode(',', $siteIds_arr);
                $scan_getresult_url = 'https://api.yext.com/v2/accounts/' . $accounId . '/scan/' . $jobId . '/' . $siteIds . '?v='. $vparam;

                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $scan_getresult_url);
                curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                    'api-key: 754875a5dbaa9d1a3a86e9793eb6dee1',
                    'Content-Type: application/json',
                ));
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

                $result = curl_exec($curl);
                $result_arr = json_decode($result);
                foreach ($result_arr->response as $k => $v) {
                    $resultScan_Id = DB::table('scan_result')
                        ->updateOrInsert(['scn_basic_id' => $basicScan_Id, 'siteId' => $v->siteId],
                            ['scn_res_status' => isset($v->status) ? $v->status : '',
                            'externalid' => isset($v->externalId) ? $v->externalId : '',
                            'url' => isset($v->url) ? $v->url : '',
                            'name' => isset($v->name) ? $v->name : '',
                            'address' => isset($v->address) ? $v->address : '',
                            'sublocality' => isset($v->sublocality) ? $v->sublocality : '',
                            'city' => isset($v->city) ? $v->city : '',
                            'state' => isset($v->state) ? $v->state : '',
                            'zip' => isset($v->zip) ? $v->zip : '',
                            'country' => isset($v->country) ? $v->country : '',
                            'phone' => isset($v->phone) ? $v->phone : '',
                            'match_name' => isset($v->match_name) ? $v->match_name : '',
                            'match_address' => isset($v->match_address) ? $v->match_address : '',
                            'match_phone' => isset($v->match_phone) ? $v->match_phone : '',
                            'match_name_score' => isset($v->match_name_score) ? $v->match_name_score : '',
                            'match_phone_score' => isset($v->match_phone_score) ? $v->match_phone_score : '',
                            'match_address_score' => isset($v->match_address_score) ? $v->match_address_score : '',
                            'review_count' => isset($v->review_count) ? $v->review_count : '',
                            'review_rating' => isset($v->review_rating) ? $v->review_rating : '',
                            'business_website' => isset($v->business_website) ? $v->business_website : '',
                            'has_powerlistings' => isset($v->has_powerlistings) ? $v->has_powerlistings : '',
                            'review_score_percentile' => isset($v->review_score_percentile) ? $v->review_score_percentile : '',
                            'review_count_percentile' => isset($v->review_count_percentile) ? $v->review_count_percentile : '',
                            'primary_category' => isset($v->primary_category) ? $v->primary_category : '',
                            'has_hours' => isset($v->has_hours) ? $v->has_hours : '',
                            'created_at' => $datetime,
                        ]);
                }
                $resultScan_log_message = "Saved result scan data data to scan_result. `scn_basic_id`: " . $basicScan_Id . ". Date : " . $datetime;
                Log::info($resultScan_log_message);
            }
        }
        /* YEXT API ends */

        /* API to fetch SEM Rush Analytics - Domain Overview starts */
        if ($checklist_marketingoverview != '') {
            // if ($sem_marketingoverviewCount == 0) {
                $comp_det = DB::table('company')
                    ->select('cname', 'company_url')
                    ->where('company_id', '=', $company_id)
                    ->first();

                if (!empty($comp_det->company_url)) {
                    $sem_marketoverviewurl = 'https://api.semrush.com/?key=e1f9daf365615157eaa1906ac391a0dd&type=domain_ranks&export_columns=Db,Dn,Rk,Or,Ot,Oc,Ad,At,Ac,Sh,Sv&domain=' . $comp_det->company_url . '&database=us';

                    $curl = curl_init();
                    curl_setopt($curl, CURLOPT_URL, $sem_marketoverviewurl);
                    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                    ));
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

                    $result = curl_exec($curl);
                    curl_close($curl);
                    // print_r("<pre>".print_r($result,true)."</pre>");
                    // $result = json_encode($result, true);
                    if ($result == "ERROR 50 :: NOTHING FOUND" || isset($result->errorMessage)){
                        if(isset($result->errorMessage)){
                            $r['api'] = 'Marketing Overview';
                            $r['error'] = $result->errorMessage;
                            Log::error($company_name.": ".$r['api'].": ".$r['error']);
                            // Start logging of playgame //
                            $countError++;
                            $log = new LogsTable();
                            $log->userId = auth()->user()->id;
                            $log->companyId = $request->company_id;
                            $log->apiName = 'API to fetch Domain Overview ';
                            $log->api = 'Semrush';
                            $log->content =$r['error'];
                            $log->level   = 'error';
                            $log->save();
                            $log->uniqueId = $logs->id;
                            $log->save();
                            //logged//
                        }
                        $r['api'] = 'Marketing Overview';
                        $r['error'] = $result;
                        Log::error($company_name.": ".$r['api'].": ".$r['error']);
                        // Start logging of playgame //
                        $countError++;
                        $log = new LogsTable();
                        $log->userId = auth()->user()->id;
                        $log->companyId = $request->company_id;
                        $log->apiName = 'API to fetch  Domain Overview ';
                        $log->api = 'Semrush';
                        $log->content =$r['error'];
                        $log->level   = 'error';
                        $log->save();
                        $log->uniqueId = $logs->id;
                        $log->save();
                        //logged//
                    }
                    if ($result != "ERROR 50 :: NOTHING FOUND") {
                        $response_ar = explode('\n', $result);
                        $i = 0;
                        foreach (array_slice($response_ar, 1) as $key => $value) {
                            if ($i == 0) {
                                $data_ar = explode(';', $value);

                                if (!empty($data_ar)) {
                                    DB::table('sem_domain_overview')
                                        ->updateOrInsert(['company_id' => $company_id,'domain' => (isset($data_ar[1])) ? $data_ar[1] : ''],
                                            ['database_rgn' => (isset($data_ar[0])) ? $data_ar[0] : '',
                                            'rank' => (isset($data_ar[2])) ? $data_ar[2] : '',
                                            'organic_keywords' => (isset($data_ar[3])) ? $data_ar[3] : '',
                                            'organic_traffic' => (isset($data_ar[4])) ? $data_ar[4] : '',
                                            'organic_cost' => (isset($data_ar[5])) ? $data_ar[5] : '',
                                            'adwords_keyword' => (isset($data_ar[6])) ? $data_ar[6] : '',
                                            'adwords_traffic' => (isset($data_ar[7])) ? $data_ar[7] : '',
                                            'adwords_cost' => (isset($data_ar[8])) ? $data_ar[8] : '',
                                            'pla_keywords' => (isset($data_ar[9])) ? $data_ar[9] : '',
                                            'pla_uniques' => (isset($data_ar[10])) ? $data_ar[10] : '',
                                            'created_at' => $datetime,
                                        ]);
                                }
                            }
                            $i++;
                        }
                    } else {
                        $message = $comp_det->cname . ", URL : " . $comp_det->company_url . " API Result : " . $result;
                        Log::error($result);
                        $r['api'] = 'Marketing Overview';
                        $r['error'] = $result;
                        // Start logging of playgame //
                        $countError++;
                        $log = new LogsTable();
                        $log->userId = auth()->user()->id;
                        $log->companyId = $request->company_id;
                        $log->apiName = 'API to fetch  Domain Overview ';
                        $log->api = 'Semrush';
                        $log->content =$r['error'];
                        $log->level   = 'error';
                        $log->save();
                        $log->uniqueId = $logs->id;
                        $log->save();
                        //logged//
                    }
                } else {
                    $message = $comp_det->cname . " No Web URL";
                    Log::error($result);
                }
            // }
        }
        /* API to fetch SEM Rush Analytics - Domain Overview ends */

        /* API to fetch SEM Rush Analytics - Display Advertising Reports - Publisher Rank starts */
        if ($checklist_publisher_rank != '') {
            // if ($sem_pubrankCount == 0) {
                $comp_det = DB::table('company')
                    ->select('cname', 'company_url')
                    ->where('company_id', '=', $company_id)
                    ->first();

                if (!empty($comp_det->company_url)) {
                    $sem_pubrankurl = 'https://api.semrush.com/analytics/da/v2/?action=report&key=e1f9daf365615157eaa1906ac391a0dd&domain=' . $comp_det->company_url . '&type=publisher_rank';
                    $curl = curl_init();
                    curl_setopt($curl, CURLOPT_URL, $sem_pubrankurl);
                    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                    ));
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                    $result = curl_exec($curl);
                    curl_close($curl);
                    $result = json_encode($result, true);
                    if ($result == "ERROR 50 :: NOTHING FOUND" || isset($result->errorMessage)){
                        if(isset($result->errorMessage)){
                            $r['api'] = 'Publisher Rank';
                            $r['error'] = $result->errorMessage;
                            Log::error($company_name.": ".$r['api'].": ".$r['error']);
                            // Start logging of playgame //
                            $countError++;
                            $log = new LogsTable();
                            $log->userId = auth()->user()->id;
                            $log->companyId = $request->company_id;
                            $log->apiName = 'API to fetch  Publisher Rank';
                            $log->api = 'Semrush';
                            $log->content =$r['error'];
                            $log->level   = 'error';
                            $log->save();
                            $log->uniqueId = $logs->id;
                            $log->save();
                            //logged//
                        }
                        $r['api'] = 'Publisher Rank';
                        $r['error'] = $result;
                        Log::error($company_name.": ".$r['api'].": ".$r['error']);
                        // Start logging of playgame //
                        $countError++;
                        $log = new LogsTable();
                        $log->userId = auth()->user()->id;
                        $log->companyId = $request->company_id;
                        $log->apiName = 'API to fetch  Publisher Rank';
                        $log->api = 'Semrush';
                        $log->content =$r['error'];
                        $log->level   = 'error';
                        $log->save();
                        $log->uniqueId = $logs->id;
                        $log->save();
                        //logged//
                    }
                    if ($result != "ERROR 50 :: NOTHING FOUND") {
                        $response_ar = explode('\n', $result);
                        foreach (array_slice($response_ar, 1) as $key => $value) {
                            $data_ar = explode(';', $value);

                            if ($value != end($data_ar)) {
                                DB::table('sem_publisher_rank')
                                     ->updateOrInsert(['company_id' => $company_id,'domain' => (isset($data_ar[0])) ? $data_ar[0] : ''],
                                       [ 'ads_overall' => (isset($data_ar[1])) ? $data_ar[1] : '',
                                        'text_ads_overall' => (isset($data_ar[2])) ? $data_ar[2] : '',
                                        'media_ads_overall' => (isset($data_ar[3])) ? $data_ar[3] : '',
                                        'first_seen' => (isset($data_ar[4])) ? $data_ar[4] : '',
                                        'last_seen' => (isset($data_ar[5])) ? $data_ar[5] : '',
                                        'times_seen' => (isset($data_ar[6])) ? $data_ar[6] : '',
                                        'created_at' => $datetime,
                                    ]);
                            }
                        }
                    } else {
                        $message = $comp_det->cname . ", URL : " . $comp_det->company_url . " API Result : " . $result;
                        Log::error($result);
                        $r['api'] = 'Publisher Rank';
                        $r['error'] = $result;
                        // Start logging of playgame //
                        $countError++;
                        $log = new LogsTable();
                        $log->userId = auth()->user()->id;
                        $log->companyId = $request->company_id;
                        $log->apiName = 'API to fetch  Publisher Rank';
                        $log->api = 'Semrush';
                        $log->content =$r['error'];
                        $log->level   = 'error';
                        $log->save();
                        $log->uniqueId = $logs->id;
                        $log->save();
                        //logged//
                    }
                } else {
                    $message = $comp_det->cname . " No Web URL";
                    Log::error($result);
                }
            // }
        }
        /* API to fetch SEM Rush Analytics - Display Advertising Reports - Publisher Rank ends */

        /* Scrap website content starts */
        if ($checklist_sitecontent != '') {
            $comp_det = DB::table('company')
                ->select('cname', 'company_url')
                ->where('company_id', '=', $company_id)
                ->first();
            // $file_headers = @get_headers($comp_det->company_url);
            // print_r($file_headers);
            // print_r(strpos($file_headers[0], '404 Not Found'));
            //
            $ch = curl_init($comp_det->company_url);

            curl_setopt($ch, CURLOPT_NOBODY, true);
            curl_exec($ch);
            $retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if($retcode!='200') {
                $r['api'] = 'Site Content';
                $r['error'] = $comp_det->company_url.' is returning error code : '.$retcode;
                Log::error($company_name.": ".$r['api'].": ".$r['error']);
                // Start logging of playgame //
                $countError++;
                $log = new LogsTable();
                $log->userId = auth()->user()->id;
                $log->companyId = $request->company_id;
                $log->apiName = 'Scrap website content';
                $log->api = 'Site Content';
                $log->content =$r['error'];
                $log->level   = 'error';
                $log->save();
                $log->uniqueId = $logs->id;
                $log->save();
                //logged//
            }
            curl_close($ch);

            $ul = $comp_det->company_url;
            // $urlContent = file_get_contents($ul);
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $ul);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);

            $urlContent = curl_exec($ch);
            curl_close($ch);

                // return $data;
            $mn_domain = $this->get_domain($ul);

            Storage::disk('public')->makeDirectory($mn_domain);

            $dom = new \DOMDocument();
            @$dom->loadHTML($urlContent);
            $xpath = new \DOMXPath($dom);
            $hrefs = $xpath->evaluate("/html/body//a");

            $arr_url[] = "";
            for ($i = 0; $i < $hrefs->length; $i++) {
                $href = $hrefs->item($i);
                $url = $href->getAttribute('href');
                $url = filter_var($url, FILTER_SANITIZE_URL);

                $arr_url[] = $url;

                $path = parse_url($url, PHP_URL_PATH); // get path from url
                $extension = pathinfo($path, PATHINFO_EXTENSION); // get ext from path

                // validate url
                if (!filter_var($url, FILTER_VALIDATE_URL) === false) {
                    if (strpos($url, $mn_domain) !== false) {

                        $crawler = Goutte::request('GET', $url);

                        // $status_code = Goutte::getResponse()->getStatus();
                        // if($status_code==200){
                        //     echo '200 OK<br>';
                        // }
                        $crawler->filter('body')->each(function ($node) {

                            $company_id = $_POST['company_id'];
                            $comp_det = DB::table('company')
                                ->select('cname', 'company_url')
                                ->where('company_id', '=', $company_id)
                                ->first();
                            $ul = $comp_det->company_url;
                            $mn_domain = $this->get_domain($ul);
                            $cont = $node->text();

                            Storage::disk('public')->append($mn_domain . '/contents_page.txt', $cont);

                        });

                    }
                }
            }

            $filepath = storage_path('public/' . $mn_domain . '/contents_page.txt');

            $arr_url = array_filter(array_unique($arr_url));
            $page_count = count($arr_url);
            $url_str = implode(",", $arr_url);

            // Insert data
            $site_id = DB::table('company_sitedata')
                ->insertGetId(['company_id' => $company_id, 'page_count' => $page_count, 'page_url' => $url_str, 'content_path' => $mn_domain . '/contents_page.txt', 'created_date' => $datetime]);
            //Cloudder::upload($filepath,'Joshua/'.$comp_det->cname."_Joshua/website_content_scrap_".uniqid());
        }
        /* Scrap website content ends */

        /*--- API to fetch Website colour starts ---*/
        if ($checklist_websitecolors != '') {
            // if ($br_webcolorCount == 0) {
                $web_colour_message = "";
                $data = json_encode(array("domain" => $webUrl));
                $web_colour_api_url = 'https://api.brandfetch.io/v1/color';

                $ch_web_colour = curl_init();
                curl_setopt($ch_web_colour, CURLOPT_POST, 1);
                curl_setopt($ch_web_colour, CURLOPT_POSTFIELDS, $data);
                curl_setopt($ch_web_colour, CURLOPT_URL, $web_colour_api_url);
                curl_setopt($ch_web_colour, CURLOPT_HTTPHEADER, array(
                    'x-api-key: oouXaXg0zf77frXoUmsWp9ncqgdHwyaT6BlOaB3a',
                    'Content-Type: application/json',
                ));
                curl_setopt($ch_web_colour, CURLOPT_TIMEOUT, 400);
                curl_setopt($ch_web_colour, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch_web_colour, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

                $result_web_colour = curl_exec($ch_web_colour);
                $result_web_colour = json_decode($result_web_colour);

                if (!$result_web_colour || isset($result_web_colour->errorMessage)) {
                    $r['api'] = 'Website Colors';
                    $r['error'] = $result_web_colour->errorMessage;
                    Log::error($company_name.": ".$r['api'].": ".$r['error']);
                    // Start logging of playgame //
                    $countError++;
                    $log = new LogsTable();
                    $log->userId = auth()->user()->id;
                    $log->companyId = $request->company_id;
                    $log->apiName = 'API to fetch Website color';
                    $log->api = 'Brandfetch';
                    $log->content =$r['error'];
                    $log->level   = 'error';
                    $log->save();
                    $log->uniqueId = $logs->id;
                    $log->save();
                    //logged//
                }
                curl_close($ch_web_colour);
                $res_web_colour_array = get_object_vars($result_web_colour);

                if ($res_web_colour_array['statusCode']==200) {

                    $web_colour_category = (isset($res_web_colour_array['response']->filtered)) ? $res_web_colour_array['response']->filtered : '';
                    $web_colour_raw = (isset($res_web_colour_array['response']->raw)) ? $res_web_colour_array['response']->raw : '';
                    foreach ($web_colour_category as $key => $value) {
                        $web_cc[] = $value;
                    }
                    DB::table('br_web_colour_category')
                        ->updateOrInsert(['br_id' => $br_Id],
                                ['vibrant' => $web_cc[2],
                                'dark' => $web_cc[0],
                                'light' => $web_cc[1],
                                'br_cat_id' => 1,
                                'created_at' => $datetime,
                            ]);

                    if (!empty($web_colour_raw)) {
                        foreach ($web_colour_raw as $k => $v) {
                            DB::table('br_web_colour_raw')
                                ->updateOrInsert(['br_id' => $br_Id,'colour_code' => $v->color],
                                    ['percentage' => $v->percentage,
                                    'br_cat_id' => 1,
                                    'created_at' => $datetime,
                                ]);
                        }
                    }
                    $web_colour_message = "Website Color api successfull.  Data inserted to 'br_web_colour_category' and 'br_web_colour_raw'";
                    Log::info($web_colour_message);
                } else {
                    $web_colour_message = "Website Color api. No data fetched";
                    Log::error($web_colour_message);
                    // Start logging of playgame //
                    $countError++;
                    $log = new LogsTable();
                    $log->userId = auth()->user()->id;
                    $log->companyId = $request->company_id;
                    $log->apiName = 'API to fetch Website color';
                    $log->api = 'Brandfetch';
                    $log->content =$web_colour_message;
                    $log->level   = 'error';
                    $log->save();
                    $log->uniqueId = $logs->id;
                    $log->save();
                    //logged//
                }
            // }
        }
        /*--- API to fetch Website colour ends ---*/

        /* API to fetch website fonts starts */
        if ($checklist_websitefont != '') {
            // if ($br_webfontCount == 0) {
                $web_font_api_url = 'https://api.brandfetch.io/v1/font';
                $font_data = json_encode(array("domain" => $webUrl, "fresh" => false));

                $ch_web_font = curl_init();
                curl_setopt($ch_web_font, CURLOPT_POST, 1);
                curl_setopt($ch_web_font, CURLOPT_POSTFIELDS, $font_data);
                curl_setopt($ch_web_font, CURLOPT_URL, $web_font_api_url);
                curl_setopt($ch_web_font, CURLOPT_HTTPHEADER, array(
                    'x-api-key: oouXaXg0zf77frXoUmsWp9ncqgdHwyaT6BlOaB3a',
                    'Content-Type: application/json',
                ));
                curl_setopt($ch_web_font, CURLOPT_TIMEOUT, 400);
                curl_setopt($ch_web_font, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch_web_font, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

                $result_web_font = curl_exec($ch_web_font);
                $res_web_font_array = json_decode($result_web_font);
                if (!$res_web_font_array || $res_web_font_array->statusCode!='200') {
                    $r['api'] = 'Website Fonts';
                    $r['error'] = $res_web_font_array->response;
                    Log::error($company_name.": ".$r['api'].": ".$r['error']);
                    // Start logging of playgame //
                    $countError++;
                    $log = new LogsTable();
                    $log->userId = auth()->user()->id;
                    $log->companyId = $request->company_id;
                    $log->apiName = 'API to fetch website fonts';
                    $log->api = 'Brandfetch';
                    $log->content =$r['error'];
                    $log->level   = 'error';
                    $log->save();
                    $log->uniqueId = $logs->id;
                    $log->save();
                    //logged//
                }
                curl_close($ch_web_font);
                $res_web_font_array = get_object_vars($res_web_font_array);

                if (isset($res_web_font_array) && !empty($res_web_font_array)) {
                    if ($res_web_font_array['statusCode'] = 200) {
                        $font_array = json_decode(json_encode($res_web_font_array['response']), True);
                        DB::table('br_website_font')
                            ->updateOrInsert(['br_id' => $br_Id],
                                ['title_tag' => isset($font_array["title"]["font"])?$font_array["title"]["font"]:'',
                                'primary_font' =>isset($font_array["paragraph"]["font"])?$font_array["paragraph"]["font"]:'',
                                'br_cat_id' => 2,
                                'created_at' => $datetime,
                            ]);
                    } else {
                        $web_font_message = "Brandfetch Website Font API ; URL : " . $webUrl . ", Response : " . $res_web_font_array['response'] . ", StatusCode : " . $res_web_font_array['statusCode'];
                        Log::error($web_font_message);
                        $r['api'] = 'Website Font API';
                        $r['error'] = $web_font_message;
                        // Start logging of playgame //
                        $countError++;
                        $log = new LogsTable();
                        $log->userId = auth()->user()->id;
                        $log->companyId = $request->company_id;
                        $log->apiName = 'API to fetch website fonts';
                        $log->api = 'Brandfetch';
                        $log->content =$r['error'];
                        $log->level   = 'error';
                        $log->save();
                        $log->uniqueId = $logs->id;
                        $log->save();
                        //logged//
                    }
                }
            // }
        }
        /* API to fetch website fonts ends */

        /* API to fetch website images starts */
        if ($checklist_websiteimages != '') {
            // if ($br_webimagesCount == 0) {
                $web_images_api_url = 'https://api.brandfetch.io/v1/image';
                $web_img_data = json_encode(array("domain" => $webUrl, "fresh" => false));

                $ch_web_images = curl_init();
                curl_setopt($ch_web_images, CURLOPT_POST, 1);
                curl_setopt($ch_web_images, CURLOPT_POSTFIELDS, $web_img_data);
                curl_setopt($ch_web_images, CURLOPT_URL, $web_images_api_url);
                curl_setopt($ch_web_images, CURLOPT_HTTPHEADER, array(
                    'x-api-key: oouXaXg0zf77frXoUmsWp9ncqgdHwyaT6BlOaB3a',
                    'Content-Type: application/json',
                ));
                curl_setopt($ch_web_images, CURLOPT_TIMEOUT, 0);
                curl_setopt($ch_web_images, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch_web_images, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

                $result_web_images = curl_exec($ch_web_images);
                $res_web_images_array = json_decode($result_web_images);
                if (!$res_web_images_array || isset($res_web_images_array->errorMessage)){
                    $r['api'] = 'Website Images';
                    $r['error'] = $res_web_images_array->errorMessage;
                    Log::error($company_name.": ".$r['api'].": ".$r['error']);
                    // Start logging of playgame //
                    $countError++;
                    $log = new LogsTable();
                    $log->userId = auth()->user()->id;
                    $log->companyId = $request->company_id;
                    $log->apiName = 'API to fetch website images';
                    $log->api = 'Brandfetch';
                    $log->content =$r['error'];
                    $log->level   = 'error';
                    $log->save();
                    $log->uniqueId = $logs->id;
                    $log->save();
                    //logged//
                }
                curl_close($ch_web_images);

                $res_web_images_array = get_object_vars($res_web_images_array);
                if ($res_web_images_array['statusCode'] != "404") {
                    $websiteimages = "";
                    $i = 1;
                    $comp_name = DB::table('company')
                        ->select('cname')
                        ->where('company_id', '=', $company_id)
                        ->first();
                    foreach ($res_web_images_array['response'] as $key => $value) {
                        $web_img_array = json_decode(json_encode($value), true);
                        /* Uploading the website images to cloudinary in `$comp_name->cname` folder */
                        $array_webimages = @get_headers($web_img_array['image']);
                        $string_webimages = $array_webimages[0];
                        if (strpos($string_webimages, "200")) {
                            $comp_name->cname = preg_replace('~[?&#\%<>]~','-',$comp_name->cname);
                            $upload_webimg = Cloudder::upload($web_img_array['image'], 'Joshua/' . $comp_name->cname . "_Joshua/web_img_br_" . uniqid());
                            if ($upload_webimg) {
                                $brwebimg_picId = Cloudder::getPublicId();
                                $tag = str_replace(' ', '_', $comp_name->cname);
                                Cloudder::addTag($tag, $brwebimg_picId);
                            } else {
                                $brwebimg_picId = "Not uploaded";
                            }
                        } else {
                            $brwebimg_picId = "Broken URL";
                        }
                        if ($i > 1) {
                            $websiteimages = $websiteimages . "," . $web_img_array['image'];
                        } else {
                            $websiteimages = $web_img_array['image'];
                        }
                        DB::table('br_website_images')
                            ->updateOrInsert(['br_id' => $br_Id,'image_url' => $web_img_array['image']],
                                ['br_cat_id' => 1,
                                'webimg_pic_id' => $brwebimg_picId,
                                'created_at' => $datetime,
                            ]);
                        $i++;
                    }
                    $filepath = $comp_name->cname . '_' . time() . '_' . "_Joshua.txt";
                    Storage::put($filepath, $websiteimages);
                    DB::table('br_image_path')
                        ->insert([
                            'br_id' => $br_Id,
                            'file_path' => $filepath,
                        ]);
                }
            // }
        }
        /* API to fetch website images ends */

        /*--- API to fetch Website logo starts ---*/
        if ($checklist_weblogo != '') {
            // if ($br_weblogoCount == 0) {
                $comp_name = DB::table('company')
                    ->select('cname')
                    ->where('company_id', '=', $company_id)
                    ->first();

                $data = json_encode(array("domain" => $webUrl));
                $logo_api_url = 'https://api.brandfetch.io/v1/logo';

                $curl = curl_init();
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                curl_setopt($curl, CURLOPT_URL, $logo_api_url);
                curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                    'x-api-key: oouXaXg0zf77frXoUmsWp9ncqgdHwyaT6BlOaB3a',
                    'Content-Type: application/json',
                ));
                curl_setopt($curl, CURLOPT_TIMEOUT, 400);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

                $result = curl_exec($curl);
                $res_array = json_decode($result);
                if (!$res_array || $res_array->statusCode!='200') {
                    $r['api'] = 'Websites Logo';
                    $r['error'] = $res_array->response;
                    Log::error($company_name.": ".$r['api'].": ".$r['error']);
                     // Start logging of playgame //
                    $countError++;
                    $log = new LogsTable();
                     $log->userId = auth()->user()->id;
                     $log->companyId = $request->company_id;
                     $log->apiName = 'API to fetch Website logo';
                     $log->api = 'Brandfetch';
                     $log->content =$r['error'];
                     $log->level   = 'error';
                     $log->save();
                     $log->uniqueId = $logs->id;
                     $log->save();
                     //logged//
                }
                curl_close($curl);

                $res_array = get_object_vars($res_array);
                if ($res_array['statusCode'] == 200) {
                    $web_logo = $res_array['response']->logo->image;;
                } else {
                    $web_logo = "";
                }

                // Getting page header data
                if ($web_logo) {
                    $array_weblogo = @get_headers($web_logo);
                    $string_logo = $array_weblogo[0];
                    if (strpos($string_logo, "200")) {
                        $comp_name->cname = preg_replace('~[?&#\%<>]~', '-', $comp_name->cname);
                        $upload = Cloudder::upload($web_logo, 'Joshua/' . $comp_name->cname . "_Joshua/logo_br_" . uniqid());
                        if ($upload) {
                            $brlogo_picId = Cloudder::getPublicId();
                            $tag = str_replace(' ', '_', $comp_name->cname);
                            Cloudder::addTag($tag, $brlogo_picId);
                        } else {
                            $brlogo_picId = "Not uploaded";
                        }
                    }
                }else {
                    $brlogo_picId = "Broken URL";
                }

                /*--- Save to 'br_logo' table ---*/
                if (!empty($res_array)) {
                    DB::table('br_logo')
                        ->updateOrInsert(['br_id' => $br_Id],
                            ['web_logo' => $web_logo,
                            'br_cat_id' => 1,
                            'pic_id' => $brlogo_picId,
                            'created_at' => $datetime,
                        ]);
                    $web_log_message = "Api to fetch website logo successfull. Saved to table 'br_logo'";
                    Log::info($web_log_message);
                } else {
                    $web_log_message = "Api to fetch website logo. No data retireved";
                    Log::error($web_log_message);
                    $r['api'] = 'Website logo';
                    $r['error'] = 'Api to fetch website logo. No data retireved';
                    // Start logging of playgame //
                    $countError++;
                    $log = new LogsTable();
                    $log->userId = auth()->user()->id;
                    $log->companyId = $request->company_id;
                    $log->apiName = 'API to fetch Website logo';
                    $log->api = 'Brandfetch';
                    $log->content =$r['error'];
                    $log->level   = 'error';
                    $log->save();
                    $log->uniqueId = $logs->id;
                    $log->save();
                    //logged//
                }
            // }
        }
        /*--- API to fetch Website logo ends ---*/

         /* API to fetch website logo colors starts */
         if ($checklist_colorsfromlogo != '') {
            // if ($br_logocolorCount == 0) {
                $logo_colour_message = "";
                $br_fetchweblogo = DB::table('br_logo')
                    ->select('web_logo')
                    ->where('br_id', '=', $br_Id)
                    ->first();
                if (!empty($br_fetchweblogo->web_logo)) {
                    $logo_colour_api_url = 'https://api.brandfetch.io/v1/color-api/get-colors-from-logo?';
                    $logo_url = $br_fetchweblogo->web_logo;
                    $logo_data = json_encode(array("image" => $logo_url));

                    $ch_logo_colour = curl_init();
                    curl_setopt($ch_logo_colour, CURLOPT_POST, 1);
                    curl_setopt($ch_logo_colour, CURLOPT_POSTFIELDS, $logo_data);
                    curl_setopt($ch_logo_colour, CURLOPT_URL, $logo_colour_api_url);
                    curl_setopt($ch_logo_colour, CURLOPT_HTTPHEADER, array(
                        'x-api-key: oouXaXg0zf77frXoUmsWp9ncqgdHwyaT6BlOaB3a',
                        'Content-Type: application/json',
                    ));
                    curl_setopt($ch_logo_colour, CURLOPT_TIMEOUT, 400);
                    curl_setopt($ch_logo_colour, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch_logo_colour, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

                    $result_logo_colour = curl_exec($ch_logo_colour);
                    $res_logo_colour_array = json_decode($result_logo_colour);
                    if (!$result_logo_colour || (isset($res_logo_colour_array->errorMessage))) {
                        $r['api'] = 'Colors from Logo';
                        $r['error'] = $res_logo_colour_array->errorMessage;
                        Log::error($company_name.": ".$r['api'].": ".$r['error']);
                        // Start logging of playgame //
                        $countError++;
                        $log = new LogsTable();
                        $log->userId = auth()->user()->id;
                        $log->companyId = $request->company_id;
                        $log->apiName = 'API to fetch Website logo colors';
                        $log->api = 'Brandfetch';
                        $log->content =$r['error'];
                        $log->level   = 'error';
                        $log->save();
                        $log->uniqueId = $logs->id;
                        $log->save();
                        //logged//
                    }
                    curl_close($ch_logo_colour);
                    $res_logo_colour_array = get_object_vars($res_logo_colour_array);

                    if (isset($res_logo_colour_array['response']->background)) {
                        $logo_colour_category = $res_logo_colour_array['response']->filtered;
                        $logo_colour_raw = $res_logo_colour_array['response']->raw;
                        $logo_colour_bg = $res_logo_colour_array['response']->background;
                        foreach ($logo_colour_category as $key => $value) {
                            $logo_cc[] = $value;
                        }
                        $logo_bg_colour = "";
                        if (is_object($logo_colour_bg)) {
                            foreach ($logo_colour_bg as $k => $v) {
                                $logo_bg[] = $v;
                            }

                            $logo_bg_colour = $logo_bg[0];
                        } else {
                            $logo_bg_colour = $logo_colour_bg;
                        }
                        DB::table('br_logo_colour_category')
                            ->updateOrInsert(
                                ['br_id' => $br_Id],
                                ['vibrant' => $logo_cc[0],
                                'dark' => $logo_cc[1],
                                'light' => $logo_cc[2],
                                'br_cat_id' => 1,
                                'created_at' => $datetime,
                            ]
                            );
                        foreach ($logo_colour_raw as $k => $v) {
                            DB::table('br_logo_colour_raw')
                                ->updateOrInsert(
                                    ['br_id' => $br_Id,'l_colour_code' => $v->color],
                                    ['l_percentage' => $v->percentage,
                                    'br_cat_id' => 1,
                                    'created_at' => $datetime,
                                ]
                                );
                        }
                        DB::table('br_logo_background')
                            ->updateOrInsert(
                                ['br_id' => $br_Id],
                                ['bg_color' => $logo_bg_colour,
                                'br_cat_id' => 1,
                                'created_at' => $datetime,
                            ]
                            );

                        $logo_colour_message = "Logo Color api successfull.  Data inserted to 'br_logo_colour_category','br_logo_colour_raw' and 'br_logo_background'";
                        Log::info($logo_colour_message);
                    } else {
                        $logo_colour_message = "Logo Color api. No data fetched";
                        Log::error($logo_colour_message);
                        $r['api'] = 'Colors from Logo';
                        $r['error'] = $logo_colour_message;
                         // Start logging of playgame //
                        $countError++;
                        $log = new LogsTable();
                        $log->userId = auth()->user()->id;
                        $log->companyId = $request->company_id;
                        $log->apiName = 'API to fetch Website logo colors';
                        $log->api = 'Brandfetch';
                        $log->content =$r['error'];
                        $log->level   = 'error';
                        $log->save();
                        $log->uniqueId = $logs->id;
                        $log->save();
                        //logged//
                    }
                } else {
                    $logo_colour_message = $company_name.": No Logo found to fetch Color api. No data fetched";
                    Log::error($logo_colour_message);
                    $r['api'] = 'Colors from Logo';
                    $r['error'] = $logo_colour_message;
                     // Start logging of playgame //
                        $countError++;
                        $log = new LogsTable();
                        $log->userId = auth()->user()->id;
                        $log->companyId = $request->company_id;
                        $log->apiName = 'API to fetch Website logo colors';
                        $log->api = 'Brandfetch';
                        $log->content =$r['error'];
                        $log->level   = 'error';
                        $log->save();
                        $log->uniqueId = $logs->id;
                        $log->save();
                        //logged//
                }
            // }
        }
        /* API to fetch logo colors ends */
        /* API to fetch website metadata starts */
        if ($checklist_websitemetadata != '') {
            // if ($br_webmetadataCount == 0) {
                $web_meta_api_url = 'https://api.brandfetch.io/v1/company';
                $web_meta_data = json_encode(array("domain" => $webUrl, "fresh" => true, "renderJS" => true));

                $ch_web_meta = curl_init();
                curl_setopt($ch_web_meta, CURLOPT_POST, 1);
                curl_setopt($ch_web_meta, CURLOPT_POSTFIELDS, $web_meta_data);
                curl_setopt($ch_web_meta, CURLOPT_URL, $web_meta_api_url);
                curl_setopt($ch_web_meta, CURLOPT_HTTPHEADER, array(
                    'x-api-key: oouXaXg0zf77frXoUmsWp9ncqgdHwyaT6BlOaB3a',
                    'Content-Type: application/json',
                ));
                curl_setopt($ch_web_meta, CURLOPT_TIMEOUT, 280);
                curl_setopt($ch_web_meta, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch_web_meta, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                curl_setopt($ch_web_meta, CURLOPT_CONNECTTIMEOUT, 0);

                $result_web_meta = curl_exec($ch_web_meta);
                $res_web_meta_array = json_decode($result_web_meta);
                if (!$res_web_meta_array || isset($res_web_meta_array->errorMessage)){
                    $r['api'] = 'Website Metadata';
                    $r['error'] = $res_web_meta_array->errorMessage;
                    Log::error($company_name.": ".$r['api'].": ".$r['error']);
                     // Start logging of playgame //
                    $countError++;
                    $log = new LogsTable();
                     $log->userId = auth()->user()->id;
                     $log->companyId = $request->company_id;
                     $log->apiName = 'API to fetch Website metadata';
                     $log->api = 'Brandfetch';
                     $log->content =$r['error'];
                     $log->level   = 'error';
                     $log->save();
                     $log->uniqueId = $logs->id;
                     $log->save();
                     //logged//
                }
                curl_close($ch_web_meta);
                $res_web_meta_array = get_object_vars($res_web_meta_array);
                if (isset($res_web_meta_array['response'])) {
                    DB::table('br_website_meta_data')
                        ->updateOrInsert(['br_id' => $br_Id],
                            ['title' => isset($res_web_meta_array["response"]->name)?$res_web_meta_array["response"]->name:'',
                            // 'summary' => $res_web_meta_array['response']->summary,
                            'description' => isset($res_web_meta_array["response"]->description)?$res_web_meta_array["response"]->description:'',
                            'keywords' => isset($res_web_meta_array['response']->keywords)?$res_web_meta_array['response']->keywords:'',
                            'language' => isset($res_web_meta_array['response']->language)?$res_web_meta_array['response']->language:'',
                            'br_cat_id' => 3,
                            'created_at' => $datetime,
                        ]);
                }
            // }
        }
        /* API to fetch website metadata ends */

        /* API to fetch website screenshot starts */
        if ($checklist_websitescreenshot != '') {
            // if ($br_webscreenshotCount == 0) {
                $comp_name = DB::table('company')
                    ->select('cname')
                    ->where('company_id', '=', $company_id)
                    ->first();

                $web_scn_api_url = 'https://api.brandfetch.io/v1/screenshot';
                $web_scn_data = json_encode(array("url" => $webUrl, "fresh" => false));

                $ch_web_scn_images = curl_init();
                curl_setopt($ch_web_scn_images, CURLOPT_POST, 1);
                curl_setopt($ch_web_scn_images, CURLOPT_POSTFIELDS, $web_scn_data);
                curl_setopt($ch_web_scn_images, CURLOPT_URL, $web_scn_api_url);
                curl_setopt($ch_web_scn_images, CURLOPT_HTTPHEADER, array(
                    'x-api-key: oouXaXg0zf77frXoUmsWp9ncqgdHwyaT6BlOaB3a',
                    'Content-Type: application/json',
                ));
                curl_setopt($ch_web_scn_images, CURLOPT_TIMEOUT, 0);
                curl_setopt($ch_web_scn_images, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch_web_scn_images, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

                $result_web_scn_images = curl_exec($ch_web_scn_images);
                $res_web_scn_images_array = json_decode($result_web_scn_images);

                if (!$result_web_scn_images || isset($res_web_scn_images_array->message)){
                    $r['api'] = 'Website Screenshot';
                    $r['error'] = $res_web_scn_images_array->message;
                    Log::error($company_name.": ".$r['api'].": ".$r['error']);
                    // Start logging of playgame //
                    $countError++;
                    $log = new LogsTable();
                    $log->userId = auth()->user()->id;
                    $log->companyId = $request->company_id;
                    $log->apiName = 'API to fetch Website screenshot';
                    $log->api = 'Brandfetch';
                    $log->content =$r['error'];
                    $log->level   = 'error';
                    $log->save();
                    $log->uniqueId = $logs->id;
                    $log->save();
                    //logged//
                }
                curl_close($ch_web_scn_images);
                $res_web_scn_images_array = get_object_vars($res_web_scn_images_array);

                if (isset($res_web_scn_images_array) && !empty($res_web_scn_images_array) && ($res_web_scn_images_array['statusCode']==200)) {
                    /* Uploading the website screenshot to cloudinary in `$comp_name->cname` folder */
                    $array_webscnimages = @get_headers($res_web_scn_images_array['response']->image);
                    $string_webscnimages = $array_webscnimages[0];
                    if (strpos($string_webscnimages, "200")) {
                        $comp_name->cname = preg_replace('~[?&#\%<>]~','-',$comp_name->cname);
                        $upload_websc = Cloudder::upload($res_web_scn_images_array['response']->image, 'Joshua/' . $comp_name->cname . "_Joshua/web_scn_br_" . uniqid());
                        if ($upload_websc) {
                            $brwebscn_picId = Cloudder::getPublicId();
                            $tag = str_replace(' ', '_', $comp_name->cname);
                            Cloudder::addTag($tag, $brwebscn_picId);
                        } else {
                            $brwebscn_picId = "Not uploaded";
                        }
                    } else {
                        $brwebscn_picId = "Broken URL";
                    }
                    DB::table('br_website_screenshot')
                        ->updateOrInsert(['br_id' => $br_Id],
                            ['scn_url' => $res_web_scn_images_array['response']->image,
                            'br_cat_id' => 1,
                            'webscn_pic_id' => $brwebscn_picId,
                            'created_at' => $datetime,
                        ]);
                }
            // }
        }
        /* API to fetch website screenshot ends */

        //update knack record after scan
        if(!($request->knackID)){
            $company=DB::table('company')
                    ->where('company_id','=',$company_id)->first();
            $knackID=DB::table('company')
                    ->where('company_id','=',$company_id)
                    ->value('knackID');
            $design    = DB::table('brandfetch_primary')
            ->leftjoin('br_logo_colour_raw', 'brandfetch_primary.br_id', '=', 'br_logo_colour_raw.br_id')
            ->leftjoin('br_website_font', 'brandfetch_primary.br_id', '=', 'br_website_font.br_id')
            ->leftjoin('br_web_colour_category', 'brandfetch_primary.br_id', '=', 'br_web_colour_category.br_id')
            ->select('br_logo_colour_raw.l_colour_code as logoColor', 'br_website_font.primary_font as siteFont', 'br_web_colour_category.dark as primaryColor', 'br_web_colour_category.light as secondaryColor')
            ->where('company_id', $company->company_id)
            ->first();
            $pageUrls = DB::table('company_sitedata')
                    ->select('page_count', 'page_url')
                    ->where('company_id', '=',$company->company_id)
                    ->first();
            $hours= DB::table('company_hours')
                ->select('weekday_text as hours')
                ->where('company_id', '=',$company->company_id)
                ->first();
            $social = DB::table('brandfetch_primary')
            ->leftjoin('br_company_social_media', 'brandfetch_primary.br_id', '=', 'br_company_social_media.br_id')
            ->select('br_company_social_media.twitter as twitter', 'br_company_social_media.linkedin as linkedin', 'br_company_social_media.github as github', 'br_company_social_media.facebook as facebook', 'br_company_social_media.youtube as youtube', 'br_company_social_media.pinterest as pinterest', 'br_company_social_media.instagram as instagram')
            ->where('company_id',$company->company_id)
            ->first();
            $exists = DB::table('brandfetch_primary')->where('web_url',$webUrl)->first();
            $st_seo=DB::table('st_seo')
                ->select('good_headings','homepage_title_tag','has_content_for_every_heading','has_single_h1_on_each_page','detected_address')
                ->where('st_id',$exists->br_id)
                ->first();
            $metadata = DB::table('brandfetch_primary')
            ->leftjoin('br_website_meta_data', 'brandfetch_primary.br_id', '=', 'br_website_meta_data.br_id')
            ->select('br_website_meta_data.title as title', 'br_website_meta_data.summary as summary', 'br_website_meta_data.description as description', 'br_website_meta_data.keywords as keywords')
            ->where('company_id',$company->company_id)
            ->first();
            $images = DB::table('brandfetch_primary')
                    ->leftjoin('br_website_images', 'brandfetch_primary.br_id', '=', 'br_website_images.br_id')
                    ->where('company_id',$company->company_id)
                    ->pluck('br_website_images.image_url')
                    ->all();
            if(isset($images)){
                $links['images'] = $images;
            }
            $exists = DB::table('silktide_prospect')
            ->join('company','company.company_id','silktide_prospect.company_id')
            ->where('company.company_id', $company->company_id)
            ->first();
            $data = DB::table('silktide_prospect')
                    ->leftjoin('st_sitecontent','silktide_prospect.st_id','=','st_sitecontent.st_id')
                    ->leftjoin('st_vitals','silktide_prospect.st_id','=','st_vitals.st_id')
                    ->leftjoin('st_discovered','silktide_prospect.st_id','=','st_discovered.st_id')
                    ->leftjoin('st_domain','silktide_prospect.st_id','=','st_domain.st_id')
                    ->leftjoin('st_domain_info','silktide_prospect.st_id','=','st_domain_info.st_id')
                    ->leftjoin('st_seo','silktide_prospect.st_id','=','st_seo.st_id')
                    ->leftjoin('st_images','silktide_prospect.st_id','=','st_images.st_id')
                    ->leftjoin('st_social','silktide_prospect.st_id','=','st_social.st_id')
                    ->leftjoin('st_video','silktide_prospect.st_id','=','st_video.st_id')
                    ->select('silktide_prospect.url','silktide_prospect.created_at','st_sitecontent.average_words_per_page as average_words','st_sitecontent.pages_found as pages_found','st_sitecontent.total_word_count as total_word_count','st_sitecontent.cms_solution as cms_solution','st_sitecontent.has_cms as has_cms','st_vitals.pages_tested as pages_tested','st_vitals.analytics_tool as analytics_tool','st_vitals.has_analytics as has_analytics','st_vitals.domain as domain','st_vitals.ecommerce_name as ecommerce_name','st_vitals.has_ecommerce as has_ecommerce','st_vitals.has_pixel as has_pixel','st_vitals.address_details_provided as address_details_provided','st_vitals.has_result as has_result','st_vitals.yext_api_success as yext_api_success','st_vitals.has_mobile_site as has_mobile_site','st_vitals.is_mobile as is_mobile','st_vitals.is_tablet as is_tablet','st_vitals.mobile_screenshot_url as mobile_screenshot_url','st_vitals.mobile_site_url as mobile_site_url','st_vitals.average_monthly_traffic as average_monthly_traffic','st_vitals.adwords_keywords as adwords_keywords','st_vitals.has_adwords_spend as has_adwords_spend','st_vitals.has_sitemap as has_sitemap','st_vitals.sitemap_issues as sitemap_issues','st_vitals.stale_analysis as stale_analysis','st_discovered.emails as emails','st_discovered.phones as phones','st_domain_info.domain_age_days as domain_age_days','st_domain_info.expiry_date as expiry_date','st_domain_info.registered_date as registered_date','st_seo.good_headings as good_headings','st_seo.has_content_for_every_heading as has_content_for_every_heading','st_seo.has_hierarchical_headings as has_hierarchical_headings','st_seo.has_single_h1_on_each_page as has_single_h1_on_each_page','st_seo.analysis_country as analysis_country','st_seo.detected_address as detected_address','st_seo.detected_name as detected_name','st_seo.detected_phone','st_seo.num_keywords_ranked_for','st_seo.top_keywords_ranked_for','st_seo.duplicated_items','st_seo.homepage_title_tag','st_seo.missing_items','st_seo.pages_duplicated_description_count','st_seo.pages_duplicated_title_count','st_seo.pages_missing_description_count','st_seo.pages_missing_title_count','st_seo.percent_duplicated_descriptions','st_seo.percent_duplicated_titles','st_seo.percent_missing_descriptions','st_seo.percent_missing_titles','st_images.image_count','st_images.non_web_friendly_count','st_images.percent_images_sized','st_images.stretched_image_count','st_social.has_instagram','st_social.days_since_update','st_social.last_updated_date','st_social.twitter_found','st_video.vendor_vendor','st_video.has_video','st_video.video_vendor','st_video.website_traffic','st_domain.is_parked','st_domain.has_ssl','st_domain.ssl_expired','st_domain.ssl_redirect','st_domain.ssl_valid')
                    ->where('silktide_prospect.st_id','=',$exists->st_id)
                    ->first();
            //cloudinary widgets

            $App_url='https://data.smart1.solutions/';
            $request->merge([
                'cl_name'=>$company->cname,
                'cl_email'=>$company->email,
                'cl_cid'=>$company->company_id,
                'cl_phone'=>$company->phone,
                'company_url'=>$company->company_url,
                ]);
            //upload widget
            $widget=DB::table('cloudwidget')
                            ->select('widget_url')
                            ->where('cl_cid','=',$company->company_id)
                            ->where('widget_type','=',1)
                            ->first();
            if(empty($widget)){
                $request->merge(['widgetflag'=>1]);
                app('App\Http\Controllers\CloudinaryWidgetController')->store($request);
            }
            $upload_widget=isset($widget)?$App_url.$widget->widget_url:'';

            //customer_gallery
            $widget=DB::table('cloudwidget')
                    ->select('widget_url')
                    ->where('cl_cid','=',$company->company_id)
                    ->where('widget_type','=',3)
                    ->first();
            if(empty($widget)){
                $request->merge(['widgetflag'=>3]);
                app('App\Http\Controllers\CloudinaryWidgetController')->store($request);
            }
            $customer_gallery=isset($widget)?$App_url.$widget->widget_url:'';

           //marketing asstes upload widget
            $widget=DB::table('cloudwidget')
                    ->select('widget_url')
                    ->where('cl_cid','=',$company->company_id)
                    ->where('widget_type','=',4)
                    ->first();
            if(empty($widget)){
                $request->merge(['widgetflag'=>4]);
                app('App\Http\Controllers\CloudinaryWidgetController')->store($request);
            }
            $maUpload=isset($widget)?$App_url.$widget->widget_url:'';

            //marketing assets gallery
            $widget=$widget=DB::table('cloudwidget')
                ->select('widget_url')
                ->where('cl_cid','=',$company->company_id)
                ->where('widget_type','=',5)
                ->first();
            if(empty($widget)){
                $request->merge(['widgetflag'=>5]);
                app('App\Http\Controllers\CloudinaryWidgetController')->store($request);
            }
            $maGallery=isset($widget)?$App_url.$widget->widget_url:'';

            //customer view upload widget
            $widget=DB::table('cloudwidget')
                    ->select('widget_url')
                    ->where('cl_cid','=',$company->company_id)
                    ->where('widget_type','=',6)
                    ->first();
            if(empty($widget)){
                $request->merge(['widgetflag'=>6]);
                app('App\Http\Controllers\CloudinaryWidgetController')->store($request);
            }
            $CVUploadWidget=isset($widget)?$App_url.$widget->widget_url:'';

            //customer view gallery
            $widget=DB::table('cloudwidget')
                    ->select('widget_url')
                    ->where('cl_cid','=',$company->company_id)
                    ->where('widget_type','=',7)
                    ->first();
            if(empty($widget)){
                $request->merge(['widgetflag'=>7]);
                app('App\Http\Controllers\CloudinaryWidgetController')->store($request);
            }
            $CVGallery=isset($widget)?$App_url.$widget->widget_url:'';
            $data=json_encode(array(
                'field_9'=>isset($design->logoColor)?$design->logoColor:'null',
                'field_10'=>isset($design->siteFont)?$design->siteFont:'null',
                'field_11'=>isset($design->primaryColor)?$design->primaryColor:'null',
                'field_12'=>isset($design->secondaryColor)?$design->secondaryColor:'null',
                'field_13'=>isset($pageUrls->page_count)?$pageUrls->page_count:0,
                'field_14'=>isset($pageUrls->page_url)?$pageUrls->page_url:'null',
                'field_15'=>isset($hours->hours)?$hours->hours:'null',
                'field_16'=>isset($social->twitter)?$social->twitter:'null',
                'field_17'=>isset($social->linkedin)?$social->linkedin:'null',
                'field_18'=>isset($social->github)?$social->github:'null',
                'field_19'=>isset($social->facebook)?$social->facebook:'null',
                'field_20'=>isset($social->youtube)?$social->youtube:'null',
                'field_21'=>isset($social->pinterest)?$social->pinterest:'null',
                'field_22'=>isset($social->instagram)?$social->instagram:'null',
                'field_23'=>isset($metadata->title)?$metadata->title:'null',
                'field_24'=>isset($metadata->summary)?$metadata->summary:'null',
                'field_25'=>isset($metadata->description)?$metadata->description:'null',
                'field_26'=>isset($metadata->keywords)?$metadata->keywords:'null',
                'field_27'=>isset($links)?$links['images']:'null',
                'field_28'=>$links,
                'field_30'=>isset($data->average_words)?$data->average_words:'null',
                'field_31'=>isset($data->pages_found)?$data->pages_found:'0',
                'field_32'=>isset($data->total_word_count)?$data->total_word_count:'0',
                'field_33'=>isset($data->cms_solution)?$data->cms_solution:'null',
                'field_34'=>isset($data->has_cms)?$data->has_cms:'No',
                'field_35'=>isset($data->pages_tested)?$data->pages_tested:'null',
                'field_36'=>isset($data->analytics_tool)?$data->analytics_tool:'null',
                'field_37'=>isset($data->has_analytics)?$data->has_analytics:'No',
                'field_38'=>isset($data->domain)?$data->domain:'null',
                'field_39'=>isset($data->ecommerce_name)?$data->ecommerce_name:'null',
                'field_40'=>isset($data->has_ecommerce)?$data->has_ecommerce:'No',
                'field_41'=>isset($data->has_pixel)?$data->has_pixel:'No',
                'field_42'=>isset($data->address_details_provided)?$data->address_details_provided:'null',
                'field_43'=>isset($data->has_result)?$data->has_result:'No',
                'field_44'=>isset($data->yext_api_success)?$data->yext_api_success:'null',
                'field_45'=>isset($data->has_mobile_site)?$data->has_mobile_site:'No',
                'field_46'=>isset($data->is_mobile)?$data->is_mobile:'No',
                'field_47'=>isset($data->is_tablet)?$data->is_tablet:'No',
                'field_48'=>isset($data->mobile_screenshot_url)?$data->mobile_screenshot_url:'null',
                'field_49'=>isset($data->mobile_site_url)?$data->mobile_site_url:'null',
                'field_50'=>isset($data->average_monthly_traffic)?$data->average_monthly_traffic:'null',
                'field_51'=>isset($data->adwords_keywords)?$data->adwords_keywords:'null',
                'field_52'=>isset($data->has_adwords_spend)?$data->has_adwords_spend:'No',
                'field_53'=>isset($data->has_sitemap)?$data->has_sitemap:'No',
                'field_54'=>isset($data->sitemap_issues)?$data->sitemap_issues:'null',
                'field_55'=>isset($data->stale_analysis)?$data->stale_analysis:'null',
                'field_56'=>isset($data->emails)?$data->emails:'null',
                'field_57'=>isset($data->phones)?$data->phones:'null',
                'field_58'=>isset($data->domain_age_days)?$data->domain_age_days:'null',
                'field_59'=>isset($data->expiry_date)?$data->expiry_date:'null',
                'field_60'=>isset($data->registered_date)?$data->registered_date:'null',
                'field_61'=>isset($data->good_headings)?$data->good_headings:'null',
                'field_62'=>isset($data->has_content_for_every_heading)?$data->has_content_for_every_heading:'no',
                'field_63'=>isset($data->has_hierarchical_headings)?$data->has_hierarchical_headings:'No',
                'field_64'=>isset($data->has_single_h1_on_each_page)?$data->has_single_h1_on_each_page:'No',
                'field_65'=>isset($data->analysis_country)?$data->analysis_country:'null',
                'field_66'=>isset($data->detected_address)?$data->detected_address:'null',
                'field_67'=>isset($data->detected_name)?$data->detected_name:'null',
                'field_68'=>isset($data->detected_phone)?$data->detected_phone:'null',
                'field_69'=>isset($data->num_keywords_ranked_for)?$data->num_keywords_ranked_for:'null',
                'field_70'=>isset($data->top_keywords_ranked_for)?$data->top_keywords_ranked_for:'null',
                'field_71'=>isset($data->duplicated_items)?$data->duplicated_items:'null',
                'field_72'=>isset($data->homepage_title_tag)?$data->homepage_title_tag:'null',
                'field_73'=>isset($data->missing_items)?$data->missing_items:'null',
                'field_74'=>isset($data->pages_duplicated_description_count)?$data->pages_duplicated_description_count:'0',
                'field_75'=>isset($data->pages_duplicated_title_count)?$data->pages_duplicated_title_count:'0',
                'field_76'=>isset($data->pages_missing_description_count)?$data->pages_missing_description_count:'0',
                'field_77'=>isset($data->pages_missing_title_count)?$data->pages_missing_title_count:'0',
                'field_78'=>isset($data->percent_duplicated_descriptions)?$data->percent_duplicated_descriptions:'null',
                'field_79'=>isset($data->percent_duplicated_titles)?$data->percent_duplicated_titles:'null',
                'field_80'=>isset($data->percent_missing_descriptions)?$data->percent_missing_descriptions:'null',
                'field_81'=>isset($data->percent_missing_titles)?$data->percent_missing_titles:'null',
                'field_82'=>isset($data->image_count)?$data->image_count:'0',
                'field_83'=>isset($data->non_web_friendly_count)?$data->non_web_friendly_count:'0',
                'field_84'=>isset($data->percent_images_sized)?$data->percent_images_sized:'null',
                'field_85'=>isset($data->stretched_image_count)?$data->stretched_image_count:'0',
                'field_86'=>isset($data->has_instagram)?$data->has_instagram:'No',
                'field_87'=>isset($data->days_since_update)?$data->days_since_update:'0',
                'field_88'=>isset($data->last_updated_date)?$data->last_updated_date:'null',
                'field_89'=>isset($data->twitter_found)?$data->twitter_found:'null',
                'field_90'=>isset($data->vendor_vendor)?$data->vendor_vendor:'null',
                'field_91'=>isset($data->has_video)?$data->has_video:'null',
                'field_92'=>isset($data->video_vendor)?$data->video_vendor:'null',
                'field_93'=>isset($data->website_traffic)?$data->website_traffic:'null',
                'field_94'=>isset($data->is_parked)?$data->is_parked:'No',
                'field_95'=>isset($data->has_ssl)?$data->has_ssl:'No',
                'field_96'=>isset($data->ssl_expired)?$data->ssl_expired:'null',
                'field_97'=>isset($data->ssl_redirect)?$data->ssl_redirect:'',
                'field_98'=>isset($data->ssl_valid)?$data->ssl_valid:'null',
                'field_340'=>'https://data.smart1.solutions/'.str_replace(' ','_',$company->cname).'_'.$company->zip,
                'field_341'=>'NA',
                'field_342'=>isset($upload_widget)?$upload_widget:'',
                'field_343'=>isset($customer_gallery)?$customer_gallery:'',
                'field_344'=>'NA',
                'field_345'=>isset($maUpload)?$maUpload:'',
                'field_346'=>isset($maGallery)?$maGallery:'',
                'field_347'=>'NA',
                'field_348'=>isset($CVUploadWidget)?$CVUploadWidget:'',
                'field_349'=>isset($CVGallery)?$CVGallery:'',
            ));
            $curl = curl_init();

            curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.knack.com/v1/objects/object_1/records/".$knackID,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "PUT",
            CURLOPT_POSTFIELDS =>$data,
            CURLOPT_HTTPHEADER => array(
                "X-Knack-Application-Id: 5f7e2af82c55b5001636420b",
                "X-Knack-REST-API-Key: d2541fdd-43c2-4d4d-9350-c1b787bfe87c",
                "Content-Type: application/json",
                "Cookie: connect.sid=s%3A-599MYE7MII1h8OXi_XdmlzFcZ3oGfdp.v%2BU8UBVXsphpnw5W6ymm6%2Ft0RyKKY%2FXgL8FkXrNvDdY"
            ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);
        }
        if($countError>1){
            $return['errorId'] = $logs->id;
            $return['errors'] = $countError;
            return $return;
        }else{
            return '1';
        }
    }
    public function log($id=Null)
    {
        if($id){
            $logs = LogsTable::where('uniqueId', $id)
                        ->get();

        }else{
            $logs = LogsTable::leftJoin('company','company.company_id','=','logs_tables.companyId')
                            ->select('company.cname','logs_tables.*')
                            ->orderBy('logs_tables.created_at', 'DESC')
                            ->get();

        }
        return view('logs.log', compact('logs','id'));
    }
    public function get_domain($url)
    {
        if (!filter_var($url, FILTER_VALIDATE_URL) === false) {
            $pieces = parse_url($url);
            $domain = isset($pieces['host']) ? $pieces['host'] : $pieces['path'];
            if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
                return $regs['domain'];
            }
            return false;
        }

    }

/* Fetch Cloudinary Images to show Media Gallery Integration */

    public function fetchCloudinarymedia(Request $request)
    {
        $company_id = $request->companyid;

        $coudImg = DB::table('brandfetch_primary')
            ->leftjoin('company', 'company.company_id', '=', 'brandfetch_primary.company_id')
            ->select('company.cname')
            ->where('brandfetch_primary.company_id', '=', $company_id)
            ->first();
        $cloudImagetag = str_replace(' ', '_', $coudImg->cname);
        echo $cloudImagetag;
        die();
    }

    public static function scandetailsdata($siteId, $basicid)
    {
        $res = DB::table('create_scan_details')
            ->select('logo')
            ->where('scn_id', '=', $basicid)
            ->where('siteId', '=', $siteId)
            ->first();
        return $res;
    }

/* Downlad as Excel */

    public function excel($compid, $basicid)
    {
        $compdet = DB::table('company')
            ->where('company_id', '=', $compid)
            ->first();

        return Excel::download(new UsersExport($compid, $basicid), $compdet->cname . '_Scan-data_' . date('YmdHis') . '.xlsx');

    }

/* Send email */

    public function sendmail(Request $request)
    {
        $compId = $request->compId;
        $basicId = $request->basicId;

        $compdet = DB::table('company')
            ->where('company_id', '=', $compId)
            ->first();

        // $sc_det = DB::table('scan_result')
        //                 ->select(DB::raw('SUM(match_name_score) as name_sum'),DB::raw('SUM(match_phone_score) as phone_sum'),DB::raw('SUM(match_address_score) as address_sum'))
        //                 ->where('scn_basic_id','=',$basicId)
        //                 ->first();

        // $review_counts = DB::table('scan_result')
        //                     ->where('review_count_percentile','!=',100)
        //                     ->where('review_score_percentile','!=',100)
        //                     ->select(DB::raw('SUM(review_count_percentile) as no_of_review'),DB::raw('SUM(review_score_percentile) as review_rating'))
        //                     ->where('scn_basic_id','=',$basicId)
        //                     ->first();

        // $yext_res = DB::table('create_scan_basic')
        //                     ->leftjoin('scan_result','create_scan_basic.basic_id','=','scan_result.scn_basic_id')
        //                     ->select('scan_result.siteId','scan_result.scn_basic_id','scan_result.name AS compname','scan_result.address','scan_result.phone','scan_result.zip','scan_result.url')
        //                     ->where('create_scan_basic.company_id','=',$compId)
        //                     ->get();

        // $missing_schema_count = DB::table('scan_result')
        //                             ->select('review_count')
        //                             ->where('review_count','=',0)
        //                             ->where('scn_basic_id','=',$basicId)
        //                             ->count();

        $to_name = $request->name;
        $to_email = $request->to_email;
        $mail_subject = "Autopopulates with Smart 1 Local Listings Scan for " . $compdet->cname;

        $data = array('name' => $request->name, "body" => "Smart 1 Listings shall have no liability for the availability or contents of the scan results, which may vary based on information we receive from our partners. This
              scanning tool is supplied on an 'as is' and 'as available' basis. To the fullest extent under applicable law, Smart 1 Listings makes no, and disclaims all, warranties,
              guarantees and representations, whether express, implied, oral or otherwise.", );

        Mail::send('emails.mail', $data, function ($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name);
            $message->subject('Autopopulates with Smart 1 Local Listings Scan');
            $message->from('noreply@smart1leads.com', 'Smart 1');
        });

        return "1";

        //return view('emails.mail');

    }

/* Download as PDF */

    public function scanpdf($compId, $basicId)
    {
        $compdet = DB::table('company')
            ->where('company_id', '=', $compId)
            ->first();

        $sc_det = DB::table('scan_result')
            ->select(DB::raw('SUM(match_name_score) as name_sum'), DB::raw('SUM(match_phone_score) as phone_sum'), DB::raw('SUM(match_address_score) as address_sum'))
            ->where('scn_basic_id', '=', $basicId)
            ->first();

        $review_counts = DB::table('scan_result')
            ->where('review_count_percentile', '!=', 100)
            ->where('review_score_percentile', '!=', 100)
            ->select(DB::raw('SUM(review_count_percentile) as no_of_review'), DB::raw('SUM(review_score_percentile) as review_rating'))
            ->where('scn_basic_id', '=', $basicId)
            ->first();

        $yext_res = DB::table('create_scan_basic')
            ->leftjoin('scan_result', 'create_scan_basic.basic_id', '=', 'scan_result.scn_basic_id')
            ->select('scan_result.siteId', 'scan_result.scn_basic_id', 'scan_result.name AS compname', 'scan_result.address', 'scan_result.phone', 'scan_result.zip', 'scan_result.url')
            ->where('create_scan_basic.company_id', '=', $compId)
            ->get();

        $missing_schema_count = DB::table('scan_result')
            ->select('review_count')
            ->where('review_count', '=', 0)
            ->where('scn_basic_id', '=', $basicId)
            ->count();

        $pdf = PDF::loadView('scanpdf', compact('sc_det', 'review_counts', 'missing_schema_count', 'yext_res', 'compdet'));

        return $pdf->download($compdet->cname . '_Scan-data_' . date('YmdHis') . '.pdf');
    }

    public function checkemail(Request $request)
    {
        $email = $request->email;
        $mailcount = DB::table('company')
            ->where('email', '=', $email)
            ->where('status', '=', 0)
            ->count();

        return $mailcount;
        die();
    }

    public function editcheckemail(Request $request)
    {
        $email = $request->email;
        $cid = $request->cid;
        $mailcount = DB::table('company')
            ->where('email', '=', $email)
            ->where('status', '=', 0)
            ->where('company_id', '!=', $cid)
            ->count();

        return $mailcount;
        die();
    }

}
