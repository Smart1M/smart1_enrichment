<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use PDF;
use Log;
use Cloudder;
use DOMDocument;
use Storage;
use Goutte\Client;
use InvalidArgumentException;
use Symfony\Component\DomCrawler\Crawler;
class CompanyReportController extends Controller
{

    public function reports($id)
    {
        $reports = DB::table('company_reports')
                    ->where('company_id',$id)
                    ->select('id','company_id','url','created_at')
                    ->get();
        return view('companyreports',compact('reports'));
    }
    public function index()
    {
        $companies = DB::table('company')
                    ->select('company_id','cname')
                    ->where('status','=',0)
                    ->get();

        $reports = DB::table('company_reports')
                    ->select('url','company_id',DB::raw('COUNT(company_id) as records'))
                    ->groupBy('url')
                    ->groupBy('company_id')
                    ->get();
        return view('companyreport',compact('reports','companies'));
    }

    public function store(Request $request)
    {
        $datetime = date('Y-m-d H:i:s');
        set_time_limit(300);
        if(!$request->knackID){
            $user           = auth()->user();
            $user_id        = $user->id;
        }
        else{
                $user_id=1;
        }
        $webUrl = $request->web_url;
        $web_url    = $request->web_url;
        $company_id = $request->company_id;

        $comp_name  = DB::table('company')
                        ->select('cname')
                        ->where('company_id', '=', $company_id)
                        ->first();

        $hour_arr = array();

        /* Place Search API starts */
        $company_name = str_replace(' ', '%20', $comp_name->cname);
        $url = 'https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=' . $company_name . '&inputtype=textquery&key=AIzaSyAVzWibWlEc43MAKZk2N1PG6suW50uTnI4';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'api-key: 398a2e9cb7fc76f58678cc5bb41bfb5bd8f839c4',
            'Content-Type: application/json',
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        $result = curl_exec($curl);
        curl_close($curl);

        if (!$result) {
            $hour_arr = '';
        } else {
            $result = json_decode($result, true);

            if (isset($result['candidates']) && !empty($result['candidates'])) {
                $place_id = $result['candidates'][0]['place_id'];

                /* Place Search API ends */

                /* Place Details API starts */

                $hour_url = 'https://maps.googleapis.com/maps/api/place/details/json?place_id=' . $place_id . '&fields=opening_hours/weekday_text&key=AIzaSyAVzWibWlEc43MAKZk2N1PG6suW50uTnI4';

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $hour_url);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'api-key: 398a2e9cb7fc76f58678cc5bb41bfb5bd8f839c4',
                    'Content-Type: application/json',
                ));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

                $res = curl_exec($ch);
                curl_close($ch);

                if (!$res) {
                }else{
                    $res = json_decode($res, true);
                    if (isset($res['result']) && !empty($res['result'])) {
                        $hour_arr = $res['result']['opening_hours']['weekday_text'];
                        DB::table('company_hours')
                            ->updateOrInsert(['company_id' => $company_id],
                                ['weekday_text' => json_encode($hour_arr),
                            ]);
                    }
                }
                /* Place Details API ends */
            }
        }

        $data           =  json_encode(array("domain" => $web_url));
        $logo_api_url   = 'https://api.brandfetch.io/v1/logo';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_URL, $logo_api_url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
          'x-api-key: oouXaXg0zf77frXoUmsWp9ncqgdHwyaT6BlOaB3a',
          'Content-Type: application/json',
        ));
        curl_setopt($curl, CURLOPT_TIMEOUT, 400);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        $result = curl_exec($curl);
        curl_close($curl);
        $res_array = json_decode($result);
        $res_array = get_object_vars($res_array);

        /*--- Save to 'brandfetch_primary' table ---*/
        // $exists = DB::table('brandfetch_primary')->where('web_url',$web_url)->first();
        // if(empty($exists)){
             $br_Id = DB::table('brandfetch_primary')
                 ->insertGetId([
                     'web_url' => $web_url,
                     'created_at' => $datetime,
                     'company_id' => $company_id,
                     'user_id' => $user_id
                 ]);
        // }else{
        //     $br_Id = $exists->br_id;
        // }

        if($res_array['statusCode'] == 200)
        {
            $web_logo = $res_array['response']->logo->image;;
            // Getting page header data
            $array_weblogo = @get_headers($web_logo);
            $string_logo = $array_weblogo[0];
            if(strpos($string_logo, "200"))
            {
                $comp_name->cname = preg_replace('~[?&#\%<>]~','-',$comp_name->cname);
                $upload = Cloudder::upload($web_logo,'Joshua/'.$comp_name->cname."_Joshua/logo_br_".uniqid());
                if($upload){
                    $brlogo_picId = Cloudder::getPublicId();
                    $tag = str_replace(' ', '_', $comp_name->cname);
                    Cloudder::addTag($tag, $brlogo_picId);
                } else{
                    $brlogo_picId = "Not uploaded";
                }
            } else{
                $brlogo_picId = "Broken URL";
            }

            $web_log_message = "";
            $message = "";

            $message = "Saved primary data to bradfetch_primary. ID: ".$br_Id;
            Log::info($message);

            /*--- Save to 'br_logo' table ---*/
            if(!empty($res_array)){
                DB::table('br_logo')
                    ->updateOrInsert(['br_id' => $br_Id],
                        ['web_logo' => $web_logo,
                        'br_cat_id' => 1,
                        'pic_id'=>$brlogo_picId,
                        'created_at' => $datetime
                    ]);

                $web_log_message = "Api to fetch website logo successfull. Saved to 'br_logo'";
                Log::info($message);
            }
        }else {
            $web_logo = "";
            $web_log_message = "Api to fetch website logo. No data retireved";
            Log::error($web_log_message);
        }

        /*--- API to fetch Website colour ---*/
        $web_colour_message = "";
        $web_colour_api_url = 'https://api.brandfetch.io/v1/color';

        $ch_web_colour = curl_init();
        curl_setopt($ch_web_colour, CURLOPT_POST, 1);
        curl_setopt($ch_web_colour, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch_web_colour, CURLOPT_URL, $web_colour_api_url);
        curl_setopt($ch_web_colour, CURLOPT_HTTPHEADER, array(
          'x-api-key: oouXaXg0zf77frXoUmsWp9ncqgdHwyaT6BlOaB3a',
          'Content-Type: application/json',
        ));
        curl_setopt($ch_web_colour, CURLOPT_TIMEOUT, 400);
        curl_setopt($ch_web_colour, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch_web_colour, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);


        $result_web_colour = curl_exec($ch_web_colour);
        curl_close($ch_web_colour);

        $res_web_colour_array = json_decode($result_web_colour);
        $res_web_colour_array = get_object_vars($res_web_colour_array);
        if($res_web_colour_array['statusCode']==200 && !isset($res_web_colour_array['errorMessage']) && $res_web_colour_array['statusCode'] == 200){

            $web_colour_category = (isset($res_web_colour_array['response']->filtered)) ? $res_web_colour_array['response']->filtered : '';
            $web_colour_raw = (isset($res_web_colour_array['response']->raw)) ? $res_web_colour_array['response']->raw : '';
            foreach($web_colour_category as $key => $value){
                $web_cc[] = $value;
            }

            DB::table('br_web_colour_category')
                 ->updateOrInsert(['br_id' => $br_Id],
                    ['vibrant' => $web_cc[2],
                    'dark' => $web_cc[0],
                    'light' => $web_cc[1],
                    'br_cat_id' => 1,
                    'created_at' => $datetime
                ]);

            if(!empty($web_colour_raw))
            {
                foreach($web_colour_raw as $k => $v){
                    DB::table('br_web_colour_raw')
                    ->updateOrInsert(['br_id' => $br_Id,'colour_code' => $v->color],
                        ['percentage' => $v->percentage,
                        'br_cat_id' => 1,
                        'created_at' => $datetime
                    ]);
                }
            }

            $web_colour_message = "Website Color api successfull.  Data inserted to 'br_web_colour_category' and 'br_web_colour_raw'";
            Log::info($web_colour_message);

        } else {
            $web_colour_message = "Website Color api. No data fetched";
            Log::error($web_colour_message);
        }

        /*--- API to fetch Logo Colour ---*/
        if($web_logo){
            $logo_colour_message = "";

            $logo_colour_api_url = 'https://api.brandfetch.io/v1/color-api/get-colors-from-logo?';
            $logo_url = $web_logo;
            $logo_data =  json_encode(array("image" => $logo_url));

            $ch_logo_colour = curl_init();
            curl_setopt($ch_logo_colour, CURLOPT_POST, 1);
            curl_setopt($ch_logo_colour, CURLOPT_POSTFIELDS, $logo_data);
            curl_setopt($ch_logo_colour, CURLOPT_URL, $logo_colour_api_url);
            curl_setopt($ch_logo_colour, CURLOPT_HTTPHEADER, array(
            'x-api-key: oouXaXg0zf77frXoUmsWp9ncqgdHwyaT6BlOaB3a',
            'Content-Type: application/json',
            ));
            curl_setopt($ch_logo_colour, CURLOPT_TIMEOUT, 400);
            curl_setopt($ch_logo_colour, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch_logo_colour, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

            $result_logo_colour = curl_exec($ch_logo_colour);
            curl_close($ch_logo_colour);

            $res_logo_colour_array = json_decode($result_logo_colour);
            $res_logo_colour_array = get_object_vars($res_logo_colour_array);

            if(isset($res_logo_colour_array['response']->background))
            {
                $logo_colour_category = $res_logo_colour_array['response']->filtered;
                $logo_colour_raw = $res_logo_colour_array['response']->raw;
                $logo_colour_bg = $res_logo_colour_array['response']->background;

                foreach($logo_colour_category as $key => $value){
                    $logo_cc[] = $value;
                }
                $logo_bg_colour = "";

                if(is_object($logo_colour_bg))
                {
                    foreach($logo_colour_bg as $k => $v){
                        $logo_bg[] = $v;
                    }

                    $logo_bg_colour = $logo_bg[0];
                } else {
                    $logo_bg_colour = $logo_colour_bg;
                }
                DB::table('br_logo_colour_category')
                    ->updateOrInsert(['br_id' => $br_Id],
                    ['vibrant' => $logo_cc[0],
                        'dark' => $logo_cc[1],
                        'light' => $logo_cc[2],
                        'br_cat_id' => 1,
                        'created_at' => $datetime
                    ]);

                foreach($logo_colour_raw as $k => $v){
                    DB::table('br_logo_colour_raw')
                        ->updateOrInsert(['br_id' => $br_Id,'l_colour_code' => $v->color],
                        ['l_percentage' => $v->percentage,
                        'br_cat_id' => 1,
                        'created_at' => $datetime
                    ]);
                }

                DB::table('br_logo_background')
                    ->updateOrInsert(['br_id' => $br_Id],
                        ['bg_color' => $logo_bg_colour,
                        'br_cat_id' => 1,
                        'created_at' => $datetime
                    ]);

                $logo_colour_message = "Logo Color api successfull.  Data inserted to 'br_logo_colour_category','br_logo_colour_raw' and 'br_logo_background'";
                Log::info($logo_colour_message);
            } else {
                $logo_colour_message = "Logo Color api. No data fetched";
                Log::error($logo_colour_message);
            }
        }

        // / --- API to fetch Wesite Fonts --- /

        $web_font_api_url = 'https://api.brandfetch.io/v1/font';
        $font_data =  json_encode(array("domain" => $web_url,"fresh" => false));

        $ch_web_font = curl_init();
        curl_setopt($ch_web_font, CURLOPT_POST, 1);
        curl_setopt($ch_web_font, CURLOPT_POSTFIELDS, $font_data);
        curl_setopt($ch_web_font, CURLOPT_URL, $web_font_api_url);
        curl_setopt($ch_web_font, CURLOPT_HTTPHEADER, array(
          'x-api-key: oouXaXg0zf77frXoUmsWp9ncqgdHwyaT6BlOaB3a',
          'Content-Type: application/json',
        ));
        curl_setopt($ch_web_font, CURLOPT_TIMEOUT, 400);
        curl_setopt($ch_web_font, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch_web_font, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        $result_web_font = curl_exec($ch_web_font);
        curl_close($ch_web_font);

        $res_web_font_array = json_decode($result_web_font);
        $res_web_font_array = get_object_vars($res_web_font_array);

        if(isset($res_web_font_array)&&!empty($res_web_font_array)){
            if ($res_web_font_array['statusCode'] ==200) {
                $font_array = json_decode(json_encode($res_web_font_array['response']), True);
                DB::table('br_website_font')
                    ->updateOrInsert(['br_id' => $br_Id],
                        ['title_tag' => isset($font_array["title"]["font"])?$font_array["title"]["font"]:"",
                        'primary_font' =>isset($font_array["paragraph"]["font"])?$font_array["paragraph"]["font"]:"",
                        'br_cat_id' => 2,
                        'created_at' => $datetime,
                    ]);
            }

        }

        /*--- API to fetch Wesite Images ---*/

        $web_images_api_url = 'https://api.brandfetch.io/v1/image';
        $web_img_data =  json_encode(array("domain" => $web_url,"fresh" => false));

        $ch_web_images = curl_init();
        curl_setopt($ch_web_images, CURLOPT_POST, 1);
        curl_setopt($ch_web_images, CURLOPT_POSTFIELDS, $web_img_data);
        curl_setopt($ch_web_images, CURLOPT_URL, $web_images_api_url);
        curl_setopt($ch_web_images, CURLOPT_HTTPHEADER, array(
          'x-api-key: oouXaXg0zf77frXoUmsWp9ncqgdHwyaT6BlOaB3a',
          'Content-Type: application/json',
        ));
        curl_setopt($ch_web_images, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch_web_images, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch_web_images, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        $result_web_images = curl_exec($ch_web_images);
        curl_close($ch_web_images);

        $res_web_images_array = json_decode($result_web_images);
        $res_web_images_array = get_object_vars($res_web_images_array);

        if($res_web_images_array['statusCode'] != "404" && isset($res_web_images_array['response'][0]->image))
        {

            $websiteimages="";
            $i=1;
            DB::table('br_website_images')->where('br_id',$br_Id)->delete();
            foreach($res_web_images_array['response'] as $key => $value){

                $web_img_array = json_decode(json_encode($value), True);
                /* Uploading the website images to cloudinary in `$comp_name->cname` folder */
                $array_webimages = @get_headers($web_img_array['image']);
                $string_webimages = $array_webimages[0];

                if(strpos($string_webimages, "200"))
                {
                    $comp_name->cname = preg_replace('~[?&#\%<>]~','-',$comp_name->cname);
                    $upload_webimg = Cloudder::upload($web_img_array['image'],'Joshua/'.$comp_name->cname."_Joshua/web_img_br_".uniqid());

                    if($upload_webimg)
                    {
                        $brwebimg_picId = Cloudder::getPublicId();
                        $tag = str_replace(' ', '_', $comp_name->cname);
                        Cloudder::addTag($tag, $brwebimg_picId);
                    } else
                    {
                        $brwebimg_picId = "Not uploaded";
                    }
                } else
                {
                    $brwebimg_picId = "Broken URL";
                }

                if($i>1){
                    $websiteimages=$websiteimages.",".$web_img_array['image'];
                }
                else{
                    $websiteimages=$web_img_array['image'];
                }


                DB::table('br_website_images')
                    ->updateOrInsert(['br_id' => $br_Id,'image_url' => $web_img_array['image']],
                        ['br_cat_id' => 1,
                        'webimg_pic_id' => $brwebimg_picId,
                        'created_at' => $datetime
                    ]);

                $i++;
            }

            $filepath = $comp_name->cname.'_'.time().'_'."_Joshua.txt";

            Storage::put($filepath, $websiteimages);

           // $file_access_path=$_SERVER['SERVER_NAME']."/joshua/smartoneapp/storage/app/".$filepath;

                     DB::table('br_image_path')
                            ->insert([
                                'br_id' => $br_Id,
                                'file_path' => $filepath
                            ]);
        }

        /*--- API to fetch Wesite Screenshot ---*/

        $web_scn_api_url = 'https://api.brandfetch.io/v1/screenshot';
        $web_scn_data =  json_encode(array("url" => $web_url,"fresh" => false));

        $ch_web_scn_images = curl_init();
        curl_setopt($ch_web_scn_images, CURLOPT_POST, 1);
        curl_setopt($ch_web_scn_images, CURLOPT_POSTFIELDS, $web_scn_data);
        curl_setopt($ch_web_scn_images, CURLOPT_URL, $web_scn_api_url);
        curl_setopt($ch_web_scn_images, CURLOPT_HTTPHEADER, array(
          'x-api-key: oouXaXg0zf77frXoUmsWp9ncqgdHwyaT6BlOaB3a',
          'Content-Type: application/json',
        ));
        curl_setopt($ch_web_scn_images, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch_web_scn_images, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch_web_scn_images, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);


        $result_web_scn_images = curl_exec($ch_web_scn_images);
        curl_close($ch_web_scn_images);

        $res_web_scn_images_array = json_decode($result_web_scn_images);
        $res_web_scn_images_array = get_object_vars($res_web_scn_images_array);
        // print_r($res_web_scn_images_array);
        if(isset($res_web_scn_images_array['statusCode']) && $res_web_scn_images_array['statusCode'] != '200'){
            Log::error($res_web_scn_images_array['response']." for ".$comp_name->cname);
            $r['error'] = $res_web_scn_images_array['response']." for ".$comp_name->cname;
        }
        if($res_web_scn_images_array['statusCode']==200)
        {
            /* Uploading the website screenshot to cloudinary in `$comp_name->cname` folder */

            $array_webscnimages = @get_headers($res_web_scn_images_array['response']->image);
            $string_webscnimages = $array_webscnimages[0];

            if(strpos($string_webscnimages, "200"))
            {
                $comp_name->cname = preg_replace('~[?&#\%<>]~','-',$comp_name->cname);
                $upload_websc = Cloudder::upload($res_web_scn_images_array['response']->image,'Joshua/'.$comp_name->cname."_Joshua/web_scn_br_".uniqid());

                if($upload_websc)
                {
                    $brwebscn_picId = Cloudder::getPublicId();
                    $tag = str_replace(' ', '_', $comp_name->cname);
                    Cloudder::addTag($tag, $brwebscn_picId);
                } else
                {
                    $brwebscn_picId = "Not uploaded";
                }
            } else
            {
                $brwebscn_picId = "Broken URL";
            }
                DB::table('br_website_screenshot')
                    ->updateOrInsert(['br_id' => $br_Id],
                        ['scn_url' => $res_web_scn_images_array['response']->image,
                        'br_cat_id' => 1,
                        'webscn_pic_id' => $brwebscn_picId,
                        'created_at' => $datetime
                    ]);

        }

        /*--- API to fetch Wesite Metadata ---*/

        $web_meta_api_url = 'https://api.brandfetch.io/v1/company';
        $web_meta_data =  json_encode(array("domain" => $web_url,"fresh" => true,"renderJS" => true));

        $ch_web_meta = curl_init();
        curl_setopt($ch_web_meta, CURLOPT_POST, 1);
        curl_setopt($ch_web_meta, CURLOPT_POSTFIELDS, $web_meta_data);
        curl_setopt($ch_web_meta, CURLOPT_URL, $web_meta_api_url);
        curl_setopt($ch_web_meta, CURLOPT_HTTPHEADER, array(
          'x-api-key: oouXaXg0zf77frXoUmsWp9ncqgdHwyaT6BlOaB3a',
          'Content-Type: application/json',
        ));
        curl_setopt($ch_web_meta, CURLOPT_TIMEOUT, 280);
        curl_setopt($ch_web_meta, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch_web_meta, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch_web_meta, CURLOPT_CONNECTTIMEOUT, 0);

        $result_web_meta = curl_exec($ch_web_meta);
        curl_close($ch_web_meta);

        $res_web_meta_array = json_decode($result_web_meta);
        $res_web_meta_array = get_object_vars($res_web_meta_array);

        //if($res_web_meta_array['statusCode'] != "404")

        if(isset($res_web_meta_array['response']) && $res_web_meta_array['statusCode']==200)
        {
            // print_r($res_web_meta_array);
            DB::table('br_website_meta_data')
            ->updateOrInsert(['br_id' => $br_Id],
                ['title' => $res_web_meta_array['response']->name,
                // 'summary' => $res_web_meta_array['response']->summary,
                'description' => $res_web_meta_array['response']->description,
                'keywords' => $res_web_meta_array['response']->keywords,
                'language' => $res_web_meta_array['response']->language,
                'br_cat_id' => 3,
                'created_at' => $datetime
            ]);

            if(isset($res_web_meta_array['response']->twitter->url)) { $twitter = $res_web_meta_array['response']->twitter->url; } else { $twitter = ""; }
            if(isset($res_web_meta_array['response']->linkedin->url)) { $linkedin = $res_web_meta_array['response']->linkedin->url; } else { $linkedin = ""; }
            if(isset($res_web_meta_array['response']->github->url)) { $github = $res_web_meta_array['response']->github->url; } else { $github = ""; }
            if(isset($res_web_meta_array['response']->instagram->url)) { $instagram = $res_web_meta_array['response']->instagram->url; } else { $instagram = ""; }
            if(isset($res_web_meta_array['response']->facebook->url)) { $facebook = $res_web_meta_array['response']->facebook->url; } else { $facebook = ""; }
            if(isset($res_web_meta_array['response']->youtube->url)) { $youtube = $res_web_meta_array['response']->youtube->url; } else { $youtube = ""; }
            if(isset($res_web_meta_array['response']->pinterest->url)) { $pinterest = $res_web_meta_array['response']->pinterest->url; } else { $pinterest = ""; }
            if(isset($res_web_meta_array['response']->crunchbase->url)) { $crunchbase = $res_web_meta_array['response']->crunchbase->url; } else { $crunchbase = ""; }

            DB::table('br_company_social_media')
                ->updateOrInsert(['br_id' => $br_Id],
                        ['twitter' => $twitter,
                        'linkedin' => $linkedin,
                        'github' => $github,
                        'instagram' => $instagram,
                        'facebook' => $facebook,
                        'youtube' => $youtube,
                        'pinterest' => $pinterest,
                        'crunchbase' => $crunchbase,
                        'br_cat_id' => 4,
                        'created_at' => $datetime
                    ]);
        }


        /* ------ Silktide Prospect API ----- */
        $data = json_encode(array('url' => $webUrl ));
        $url = 'https://api.prospect.silktide.com/api/v1/report';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
          'api-key: 398a2e9cb7fc76f58678cc5bb41bfb5bd8f839c4',
          'Content-Type: application/json',
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);


        $result = curl_exec($curl);
        $result = json_decode($result,true);
        if(!$result || $result['status']=='error') {
            if($result['issue']=='redirect'){
                $r['error'] = "The web address you entered redirects to another website. If you'd like to test this website instead, change your url to <a href=".$result['url'].">".$result['url']."</a>";
                Log::error("The web address you entered redirects to another website. If you'd like to test this website instead, change your url to ".$result['url']);
                return $r;
            }
            $r['error'] = $result['error'];
            return $r;
        }
        curl_close($curl);

        $reportId = $result['reportId'];

        $detail_url = "https://api.prospect.silktide.com/api/v1/report/".$reportId;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $detail_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'api-key: 398a2e9cb7fc76f58678cc5bb41bfb5bd8f839c4',
          'Content-Type: application/json',
        ));

        $fetch_data = curl_exec($ch);
        $fetch_data = json_decode($fetch_data,true);
        //echo '<pre>'; print_r($fetch_data);
        //echo implode(',',$fetch_data['report']['paid_search']['adwords_keywords']);

        /* --- Save to 'silktide_prospect' table -- */
        // $exists = DB::table('silktide_prospect')->where('url',$webUrl)->first();
        // if(empty($exists)){
            $st_Id = DB::table('silktide_prospect')
                ->insertGetId([
                    'report_id' => $reportId,
                    'account_id' => $fetch_data['report']['account_id'],
                    'company_id' => $company_id,
                    'url' => $webUrl,
                    'user_id' => $user_id,
                    'created_at' => $datetime
                ]);
        // }else{
        //     $st_Id = $exists->st_id;
        // }

        /* --- Save to 'st_sitecontent' table --- */

        DB::table('st_sitecontent')
            ->updateOrInsert(['st_id' => $st_Id],
                ['average_words_per_page' => (isset($fetch_data['report']['amount_of_content']['average_words_per_page'])) ? $fetch_data['report']['amount_of_content']['average_words_per_page'] : 0,
                'pages_found' => (isset($fetch_data['report']['amount_of_content']['pages_found'])) ? $fetch_data['report']['amount_of_content']['pages_found'] : 0,
                'total_word_count' => (isset($fetch_data['report']['amount_of_content']['total_word_count'])) ? $fetch_data['report']['amount_of_content']['total_word_count'] : 0,
                'cms_solution' => (isset($fetch_data['report']['content_management_system']['cms_solution'])&&!empty($fetch_data['report']['content_management_system']['cms_solution'])) ? implode(',',$fetch_data['report']['content_management_system']['cms_solution']) : '',
                'has_cms' => (isset($fetch_data['report']['content_management_system']['has_cms'])) ? $fetch_data['report']['content_management_system']['has_cms'] : 0,
                'pages_discovered_count' => (isset($fetch_data['report']['page_count']['pages_discovered_count'])) ? $fetch_data['report']['page_count']['pages_discovered_count'] : 0,
                'created_at' => $datetime
            ]);

        /* --- Save to 'st_vitals' table --- */

        DB::table('st_vitals')
        ->updateOrInsert(['st_id' => $st_Id],
            ['pages_tested' => (isset($fetch_data['report']['amount_of_content']['pages_tested'])) ? $fetch_data['report']['amount_of_content']['pages_tested'] : 0,
            'analytics_tool' => (isset($fetch_data['report']['analytics']['analytics_tool'])) ? $fetch_data['report']['analytics']['analytics_tool'] : '',
            'has_analytics' => (isset($fetch_data['report']['analytics']['has_analytics'])) ? $fetch_data['report']['analytics']['has_analytics'] : 0,
            'domain' => (isset($fetch_data['report']['domain'])) ? $fetch_data['report']['domain'] : '',
            'ecommerce_name' => (isset($fetch_data['report']['ecommerce']['ecommerce_name'])) ? $fetch_data['report']['ecommerce']['ecommerce_name'] : '',
            'has_ecommerce' => (isset($fetch_data['report']['ecommerce']['has_ecommerce'])) ? $fetch_data['report']['ecommerce']['has_ecommerce'] : 0,
            'has_pixel' => (isset($fetch_data['report']['facebook_retargeting']['has_pixel'])) ? $fetch_data['report']['facebook_retargeting']['has_pixel'] : 0,
            'address_details_provided' => (isset($fetch_data['report']['local_presence']['address_details_provided'])) ? $fetch_data['report']['local_presence']['address_details_provided'] : 0,
            'has_result' => (isset($fetch_data['report']['local_presence']['has_result'])) ? $fetch_data['report']['local_presence']['has_result'] : 0,
            'yext_api_success' => (isset($fetch_data['report']['local_presence']['yext_api_success'])) ? $fetch_data['report']['local_presence']['yext_api_success'] : 0,
            'has_mobile_site' => (isset($fetch_data['report']['mobile']['has_mobile_site'])) ? $fetch_data['report']['mobile']['has_mobile_site'] : 0,
            'is_mobile' => (isset($fetch_data['report']['mobile']['is_mobile'])) ? $fetch_data['report']['mobile']['is_mobile'] : 0,
            'is_tablet' => (isset($fetch_data['report']['mobile']['is_tablet'])) ? $fetch_data['report']['mobile']['is_tablet'] : 0,
            'mobile_screenshot_url' => (isset($fetch_data['report']['mobile']['mobile_screenshot_url'])) ? $fetch_data['report']['mobile']['mobile_screenshot_url'] : '',
            'mobile_site_url' => (isset($fetch_data['report']['mobile']['mobile_site_url'])) ? $fetch_data['report']['mobile']['mobile_site_url'] : '',
            'average_monthly_traffic' => (isset($fetch_data['report']['organic_search']['average_monthly_traffic'])) ? $fetch_data['report']['organic_search']['average_monthly_traffic'] : 0,
            'adwords_keywords' => (isset($fetch_data['report']['paid_search']['adwords_keywords'])&&!empty($fetch_data['report']['paid_search']['adwords_keywords'])) ? implode(',', $fetch_data['report']['paid_search']['adwords_keywords']) : '',
            'has_adwords_spend' => (isset($fetch_data['report']['paid_search']['has_adwords_spend'])) ? $fetch_data['report']['paid_search']['has_adwords_spend'] : 0,
            'has_sitemap' => (isset($fetch_data['report']['sitemap']['has_sitemap'])) ? $fetch_data['report']['sitemap']['has_sitemap'] : 0,
            'sitemap_issues' => (isset($fetch_data['report']['sitemap']['sitemap_issues'])) ? $fetch_data['report']['sitemap']['sitemap_issues'] : 0,
            'stale_analysis' => (isset($fetch_data['report']['stale_analysis'])) ? $fetch_data['report']['stale_analysis'] : 0,
            'created_at' => $datetime

        ]);


        /* --- Save to 'st_discovered' table --- */

        DB::table('st_discovered')
        ->updateOrInsert(['st_id' => $st_Id],
            ['email' => (isset($fetch_data['report']['contact_details']['email'])&&!empty($fetch_data['report']['contact_details']['email'])) ? implode(',',$fetch_data['report']['contact_details']['email']) : '',
            'emails' => (isset($fetch_data['report']['contact_details']['emails'])&&!empty($fetch_data['report']['contact_details']['emails'])) ? implode(',',$fetch_data['report']['contact_details']['emails']) : '',
            'phone' => (isset($fetch_data['report']['contact_details']['phone'])&&!empty($fetch_data['report']['contact_details']['phone'])) ? implode(',',$fetch_data['report']['contact_details']['phone']) : '',
            'phones' => (isset($fetch_data['report']['contact_details']['phones'])&&!empty($fetch_data['report']['contact_details']['phones'])) ? implode(',',$fetch_data['report']['contact_details']['phones']) : '',
            'created_at' => $datetime
        ]);

        /* --- Save to 'st_domain_info' table --- */

        DB::table('st_domain_info')
        ->updateOrInsert(['st_id' => $st_Id],
            ['domain_age_days' => (isset($fetch_data['report']['domain_age']['domain_age_days'])) ? $fetch_data['report']['domain_age']['domain_age_days'] : '',
            'expiry_date' => (isset($fetch_data['report']['domain_age']['expiry_date'])) ? $fetch_data['report']['domain_age']['expiry_date'] : '',
            'registered_date' => (isset($fetch_data['report']['domain_age']['registered_date'])) ? $fetch_data['report']['domain_age']['registered_date'] : '',
            'created_at' => $datetime
        ]);


        /* --- Save to 'st_domain' table --- */
            // print_r($fetch_data['report']);
        DB::table('st_domain')
        ->updateOrInsert(['st_id' => $st_Id],
            ['is_parked' => (isset($fetch_data['report']['parked_domain_detection']['is_parked'])) ? $fetch_data['report']['parked_domain_detection']['is_parked'] : '0',
            'has_ssl' => (isset($fetch_data['report']['ssl']['has_ssl'])) ? $fetch_data['report']['ssl']['has_ssl'] : '0',
            'ssl_expired' => (isset($fetch_data['report']['ssl']['ssl_expired'])) ? $fetch_data['report']['ssl']['ssl_expired'] : '0',
            'ssl_redirect' => (isset($fetch_data['report']['ssl']['ssl_redirect'])) ? $fetch_data['report']['ssl']['ssl_redirect'] : '0',
            'ssl_valid' => (isset($fetch_data['report']['ssl']['ssl_valid'])) ? $fetch_data['report']['ssl']['ssl_valid'] : '0',
            'created_at' => $datetime
        ]);

        /* --- Save to 'st_seo' table --- */
        // echo '<pre>'; print_r($fetch_data['report']);

        DB::table('st_seo')
        ->updateOrInsert(['st_id' => $st_Id],
            ['good_headings' => (isset($fetch_data['report']['headings']['good_headings'])) ? $fetch_data['report']['headings']['good_headings'] : 0,
            'has_content_for_every_heading' => (isset($fetch_data['report']['headings']['has_content_for_every_heading'])) ? $fetch_data['report']['headings']['has_content_for_every_heading'] : 0,
            'has_hierarchical_headings' => (isset($fetch_data['report']['headings']['has_hierarchical_headings'])) ? $fetch_data['report']['headings']['has_hierarchical_headings'] : 0,
            'has_single_h1_on_each_page' => (isset($fetch_data['report']['headings']['has_single_h1_on_each_page'])) ? $fetch_data['report']['headings']['has_single_h1_on_each_page'] : 0,
            'analysis_country' => (isset($fetch_data['report']['meta']['analysis_country'])) ? $fetch_data['report']['meta']['analysis_country'] : '',
            'detected_address' => (isset($fetch_data['report']['meta']['detected_address'])) ? $fetch_data['report']['meta']['detected_address'] : '',
            'detected_name' => (isset($fetch_data['report']['meta']['detected_name'])) ? $fetch_data['report']['meta']['detected_name'] : '',
            'detected_phone' => (isset($fetch_data['report']['meta']['detected_phone'])) ? $fetch_data['report']['meta']['detected_phone'] : '',
            'num_keywords_ranked_for' => (isset($fetch_data['report']['organic_search']['num_keywords_ranked_for'])) ? $fetch_data['report']['organic_search']['num_keywords_ranked_for'] : 0,
            'top_keywords_ranked_for' => (isset($fetch_data['report']['organic_search']['top_keywords_ranked_for'])) ? json_encode($fetch_data['report']['organic_search']['top_keywords_ranked_for']) : '',
            'duplicated_items' => (isset($fetch_data['report']['page_titles_and_descriptions']['duplicated_items'])) ? $fetch_data['report']['page_titles_and_descriptions']['duplicated_items'] : 0,
            'homepage_title_tag' => (isset($fetch_data['report']['page_titles_and_descriptions']['homepage_title_tag'])) ? $fetch_data['report']['page_titles_and_descriptions']['homepage_title_tag'] : '',
            'missing_items' => (isset($fetch_data['report']['page_titles_and_descriptions']['missing_items'])) ? $fetch_data['report']['page_titles_and_descriptions']['missing_items'] : '',
            'pages_duplicated_description_count' => (isset($fetch_data['report']['page_titles_and_descriptions']['pages_duplicated_description_count'])) ? $fetch_data['report']['page_titles_and_descriptions']['pages_duplicated_description_count'] : 0,
            'pages_duplicated_title_count' => (isset($fetch_data['report']['page_titles_and_descriptions']['pages_duplicated_title_count'])) ? $fetch_data['report']['page_titles_and_descriptions']['pages_duplicated_title_count'] : 0,
            'pages_missing_description_count' => (isset($fetch_data['report']['page_titles_and_descriptions']['pages_missing_description_count'])) ? $fetch_data['report']['page_titles_and_descriptions']['pages_missing_description_count'] : 0,
            'pages_missing_title_count' => (isset($fetch_data['report']['page_titles_and_descriptions']['pages_missing_title_count'])) ? $fetch_data['report']['page_titles_and_descriptions']['pages_missing_title_count'] : 0,
            'percent_duplicated_descriptions' => (isset($fetch_data['report']['page_titles_and_descriptions']['percent_duplicated_descriptions'])) ? $fetch_data['report']['page_titles_and_descriptions']['percent_duplicated_descriptions'] : 0,
            'percent_duplicated_titles' => (isset($fetch_data['report']['page_titles_and_descriptions']['percent_duplicated_titles'])) ? $fetch_data['report']['page_titles_and_descriptions']['percent_duplicated_titles'] : 0,
            'percent_missing_descriptions' => (isset($fetch_data['report']['page_titles_and_descriptions']['percent_missing_descriptions'])) ? $fetch_data['report']['page_titles_and_descriptions']['percent_missing_descriptions'] : 0,
            'percent_missing_titles' => (isset($fetch_data['report']['page_titles_and_descriptions']['percent_missing_titles'])) ? $fetch_data['report']['page_titles_and_descriptions']['percent_missing_titles'] : 0,
            'created_at' => $datetime
        ]);

        /* --- Save to 'st_images' table --- */

        DB::table('st_images')
        ->updateOrInsert(['st_id' => $st_Id],
            ['image_count' => (isset($fetch_data['report']['images']['image_count'])) ? $fetch_data['report']['images']['image_count'] : 0,
            'non_web_friendly_count' => (isset($fetch_data['report']['images']['non_web_friendly_count'])) ? $fetch_data['report']['images']['non_web_friendly_count'] : 0,
            'percent_images_sized' => (isset($fetch_data['report']['images']['percent_images_sized'])) ? $fetch_data['report']['images']['percent_images_sized'] : 0,
            'stretched_image_count' => (isset($fetch_data['report']['images']['stretched_image_count'])) ? $fetch_data['report']['images']['stretched_image_count'] : 0,
            'created_at' => $datetime
        ]);

        /* --- Save to 'st_social' table --- */

        DB::table('st_social')
        ->updateOrInsert(['st_id' => $st_Id],
            ['has_instagram' => (isset($fetch_data['report']['instagram_account']['has_instagram'])) ? $fetch_data['report']['instagram_account']['has_instagram'] : 0,
            'days_since_update' => (isset($fetch_data['report']['last_updated']['days_since_update'])) ? $fetch_data['report']['last_updated']['days_since_update'] : 0,
            'last_updated_date' => (isset($fetch_data['report']['last_updated']['last_updated_date'])) ? $fetch_data['report']['last_updated']['last_updated_date'] : '',
            'twitter_found' => (isset($fetch_data['report']['twitter']['found'])) ? $fetch_data['report']['twitter']['found'] : 0,
            'created_at' => $datetime
        ]);

        /* --- Save to 'st_video' table --- */

        DB::table('st_video')
        ->updateOrInsert(['st_id' => $st_Id],
            ['vendor_vendor' => (isset($fetch_data['report']['vendor']['vendor'])) ? $fetch_data['report']['vendor']['vendor']: '',
            'has_video' => (isset($fetch_data['report']['video']['has_video'])) ? $fetch_data['report']['video']['has_video'] : 0,
            'video_vendor' => (isset($fetch_data['report']['video']['vendor'])) ? $fetch_data['report']['video']['vendor'] : '',
            'website_traffic' => (isset($fetch_data['report']['website_traffic']['website_traffic'])) ? $fetch_data['report']['website_traffic']['website_traffic'] : 0,
            'created_at' => $datetime
        ]);

        DB::table('company_reports')
            ->insert([
                'company_id' => $company_id,
                'url' => $webUrl,
                'user' => $user_id,
                'st_id' => $st_Id,
                'br_id' => $br_Id,
                'created_at' => $datetime
            ]);

        return '1';
    }
    public function exporttopdf($id)
    {
        $curr_time = DB::table('company_reports')
                        ->where('id',$id)
                        ->value('created_at');
        $curr_time = date('m-d-Y', strtotime($curr_time));
        $id = DB::table('company_reports')
                    ->where('id',$id)
                    ->value('company_id');
        $company_name = DB::table('company')
                        ->where('company_id',$id)
                        ->value('cname');

        $company = DB::table('company')
                        ->leftjoin('company_reports','company_reports.company_id','company.company_id')
                        ->select('company.cname as company_name', 'company.address1', 'company.address2', 'company.city','company.state_name','company.zip','company.country','company.email','company.phone','company.company_url','company.media_partner','company_reports.created_at')
                        ->where('company.company_id', $id)
                        ->first();

        $uploadWidget = DB::table('cloudwidget')
                            ->select('widget_url as uploadWidget')
                            ->where('cl_cid', '=', $id)
                            ->where('widget_type', 1)
                            ->first();
        if($uploadWidget){
            $uploadWidget->uploadWidget = url('/').'/'.$uploadWidget->uploadWidget;
            $uploadWidget->uploadWidget = "<iframe id='myFrame' src=$uploadWidget->uploadWidget max-width=100% width=100% height=650px frameborder='0'></iframe>";
        }

        $mediaLibrary = DB::table('cloudwidget')
                            ->select('widget_url as mediaLibrary')
                            ->where('cl_cid', '=', $id)
                            ->where('widget_type', 2)
                            ->first();
        if($mediaLibrary){
            $mediaLibrary->mediaLibrary = url('/').'/'.$mediaLibrary->mediaLibrary;
            $mediaLibrary->mediaLibrary = "<iframe id='myFrame' src=$mediaLibrary->mediaLibrary max-width=100% width=100% height=650px frameborder='0'></iframe>";
        }

         $design    = DB::table('brandfetch_primary')
                        ->leftjoin('br_logo_colour_raw', 'brandfetch_primary.br_id', '=', 'br_logo_colour_raw.br_id')
                        ->leftjoin('br_website_font', 'brandfetch_primary.br_id', '=', 'br_website_font.br_id')
                        ->leftjoin('br_web_colour_category', 'brandfetch_primary.br_id', '=', 'br_web_colour_category.br_id')
                        ->select('br_logo_colour_raw.l_colour_code as logoColor', 'br_website_font.primary_font as siteFont', 'br_web_colour_category.dark as primaryColor', 'br_web_colour_category.light as secondaryColor')
                        ->where('company_id', $id)
                        ->first();

        $pageUrls   = DB::table('company_sitedata')
                        ->select('page_count', 'page_url')
                        ->where('company_id', '=', $id)
                        ->first();
        $hours      = DB::table('company_hours')
                            ->select('weekday_text as hours')
                            ->where('company_id', '=', $id)
                            ->first();

        $social = DB::table('brandfetch_primary')
                        ->leftjoin('br_company_social_media', 'brandfetch_primary.br_id', '=', 'br_company_social_media.br_id')
                        ->select('br_company_social_media.twitter', 'br_company_social_media.linkedin', 'br_company_social_media.github', 'br_company_social_media.facebook', 'br_company_social_media.youtube', 'br_company_social_media.pinterest', 'br_company_social_media.instagram')
                        ->where('company_id', $id)
                        ->first();

        $metadata = DB::table('brandfetch_primary')
                        ->leftjoin('br_website_meta_data', 'brandfetch_primary.br_id', '=', 'br_website_meta_data.br_id')
                        ->select('br_website_meta_data.title', 'br_website_meta_data.summary', 'br_website_meta_data.description', 'br_website_meta_data.keywords')
                        ->where('company_id', $id)
                        ->first();

        $images = DB::table('brandfetch_primary')
                        ->leftjoin('br_website_images', 'brandfetch_primary.br_id', '=', 'br_website_images.br_id')
                        ->where('company_id', $id)
                        ->pluck('br_website_images.image_url')
                        ->all();
        if(isset($images)){
            $links['images'] = $images;
        }

        $company        = (isset($company)) ? json_decode(json_encode($company), true) : [];
        $uploadWidget   = (isset($uploadWidget)) ? json_decode(json_encode($uploadWidget), true) : [];
        $mediaLibrary   = (isset($mediaLibrary)) ? json_decode(json_encode($mediaLibrary), true) : [];
        $design         = (isset($design)) ? json_decode(json_encode($design), true) : [];
        $pageUrls       = (isset($pageUrls)) ? json_decode(json_encode($pageUrls), true) : [];
        $hours          = (isset($hours)) ? json_decode(json_encode($hours), true) : [];
        $social         = (isset($social)) ? json_decode(json_encode($social), true) : [];
        $metadata       = (isset($metadata)) ? json_decode(json_encode($metadata), true) : [];
        $links          = (isset($links)) ? json_decode(json_encode($links), true) : [];
        $data           = array_merge($company, $uploadWidget,$mediaLibrary,$design,$pageUrls,$hours,$social,$metadata,$links);
        // $data = $data->toArray();
        // print_r($data);die();

        $pdf = PDF::loadView('companyreportpdf', $data);

        return $pdf->download($company_name.'_'.$curr_time.'.pdf');
    }
    public function exportqa($id)
    {
        $curr_time = DB::table('qareport')
                        ->where('company_id',$id)
                        ->value('created_at');
        $curr_time = date('m-d-Y', strtotime($curr_time));
        $id = $id;

        $company_name = DB::table('company')
                        ->where('company_id',$id)
                        ->value('cname');

        $company = DB::table('company')
                        ->leftjoin('company_reports','company_reports.company_id','company.company_id')
                        ->select('company.cname as company_name', 'company.address1', 'company.address2', 'company.city','company.state_name','company.zip','company.country','company.email','company.phone','company.company_url','company.media_partner','company_reports.created_at')
                        ->where('company.company_id', $id)
                        ->first();

        $social = DB::table('brandfetch_primary')
        ->leftjoin('br_company_social_media', 'brandfetch_primary.br_id', '=', 'br_company_social_media.br_id')
        ->select('br_company_social_media.twitter', 'br_company_social_media.linkedin', 'br_company_social_media.github', 'br_company_social_media.facebook', 'br_company_social_media.youtube', 'br_company_social_media.pinterest', 'br_company_social_media.instagram')
        ->where('company_id', $id)
        ->first();

        $exists = DB::table('silktide_prospect')->where('url',$company->company_url)->first();
        $pages=DB::table('st_sitecontent')
                        ->select('pages_found')
                        ->where('st_id',$exists->st_id)->first();
        $st_vitals=DB::table('st_vitals')
                            ->select('has_mobile_site','has_analytics')
                            ->where('st_id',$exists->st_id)->first();
        $images=DB::table('st_images')
                    ->select('image_count','stretched_image_count','non_web_friendly_count')
                    ->where('st_id',$exists->st_id)->first();
        $st_seo=DB::table('st_seo')
                    ->select('good_headings','homepage_title_tag','has_content_for_every_heading','has_single_h1_on_each_page','detected_address')
                    ->where('st_id',$exists->st_id)->first();
        $qareport=DB::table('qareport')
                    ->select('broken_url_count','broken_urls_list','broken_images_count','broken_images_list','title_less','title_less_count','lorem_pages','lorem_count')
                    ->where('company_id',$id)->first();

        $company        =   (isset($company)) ? json_decode(json_encode($company), true) : [];
        $social         =   (isset($social)) ? json_decode(json_encode($social), true) : [];
        $pages          =   (isset($pages)) ? json_decode(json_encode($pages),true):[];
        $vitals         =   (isset($st_vitals)) ? json_decode(json_encode($st_vitals),true):[];
        $images         =   (isset($images)) ? json_decode(json_encode($images),true):[];
        $seo            =   (isset($st_seo)) ? json_decode(json_encode($st_seo),true):[];
        $qareport       =   (isset($qareport)) ? json_decode(json_encode($qareport),true):[];

        $data           = array_merge($company,$images,$qareport,$social,$pages,$vitals,$seo);

        $pdf = PDF::loadView('qareportpdf', $data);

        return $pdf->download($company_name.'_'.$curr_time.'.pdf');
    }
    public function qascan()
    {
        $companies = DB::table('company')
                    ->select('company_id','cname')
                    ->where('status','=',0)
                    ->get();

        $reports = DB::table('qareport')
                    ->select('id','company_id','url','created_at')
                    ->get();
        return view('qascan',compact('reports','companies'));
    }
    public function storeqa(Request $request)
    {
        $datetime = date('Y-m-d H:i:s');
        set_time_limit(300);
        $user = auth()->user();
        $user_id = $user->id;

        $webUrl = $request->web_url;
        $web_url    = $request->web_url;
        $company_id = $request->company_id;

        $comp_name  = DB::table('company')
                        ->select('cname')
                        ->where('company_id', '=', $company_id)
                        ->first();

        $hour_arr = array();
         /* Place Search API starts */
         $company_name = str_replace(' ', '%20', $comp_name->cname);
         $url = 'https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=' . $company_name . '&inputtype=textquery&key=AIzaSyAVzWibWlEc43MAKZk2N1PG6suW50uTnI4';

         $curl = curl_init();
         curl_setopt($curl, CURLOPT_URL, $url);
         curl_setopt($curl, CURLOPT_HTTPHEADER, array(
             'api-key: 398a2e9cb7fc76f58678cc5bb41bfb5bd8f839c4',
             'Content-Type: application/json',
         ));
         curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
         curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

         $result = curl_exec($curl);
         curl_close($curl);

         if (!$result) {
             $hour_arr = '';
         } else {
             $result = json_decode($result, true);

             if (isset($result['candidates']) && !empty($result['candidates'])) {
                 $place_id = $result['candidates'][0]['place_id'];

                 /* Place Search API ends */

                 /* Place Details API starts */

                 $hour_url = 'https://maps.googleapis.com/maps/api/place/details/json?place_id=' . $place_id . '&fields=opening_hours/weekday_text&key=AIzaSyAVzWibWlEc43MAKZk2N1PG6suW50uTnI4';

                 $ch = curl_init();
                 curl_setopt($ch, CURLOPT_URL, $hour_url);
                 curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                     'api-key: 398a2e9cb7fc76f58678cc5bb41bfb5bd8f839c4',
                     'Content-Type: application/json',
                 ));
                 curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                 curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

                 $res = curl_exec($ch);
                 curl_close($ch);

                 if (!$res) {
                 }else{
                     $res = json_decode($res, true);
                     if (isset($res['result']) && !empty($res['result'])) {
                         $hour_arr = $res['result']['opening_hours']['weekday_text'];
                         DB::table('company_hours')
                             ->updateOrInsert(['company_id' => $company_id],
                                 ['weekday_text' => json_encode($hour_arr),
                             ]);
                     }
                 }
                 /* Place Details API ends */
             }
         }

         $data           =  json_encode(array("domain" => $web_url));
         $logo_api_url   = 'https://api.brandfetch.io/v1/logo';

         $curl = curl_init();
         curl_setopt($curl, CURLOPT_POST, 1);
         curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
         curl_setopt($curl, CURLOPT_URL, $logo_api_url);
         curl_setopt($curl, CURLOPT_HTTPHEADER, array(
           'x-api-key: oouXaXg0zf77frXoUmsWp9ncqgdHwyaT6BlOaB3a',
           'Content-Type: application/json',
         ));
         curl_setopt($curl, CURLOPT_TIMEOUT, 400);
         curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
         curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

         $result = curl_exec($curl);
         curl_close($curl);
         $res_array = json_decode($result);
         $res_array = get_object_vars($res_array);

         /*--- Save to 'brandfetch_primary' table ---*/
         $exists = DB::table('brandfetch_primary')->where('web_url',$web_url)->first();
         if(empty($exists)){
              $br_Id = DB::table('brandfetch_primary')
                  ->insertGetId([
                      'web_url' => $web_url,
                      'created_at' => $datetime,
                      'company_id' => $company_id,
                      'user_id' => $user_id
                  ]);
         }else{
             $br_Id = $exists->br_id;
         }
         if($res_array['statusCode'] == 200)
         {
             $web_logo='';
             if(!empty($res_array['response']->logo->image)){
             $web_logo = $res_array['response']->logo->image;
             }
             else if($res_array['response']->icon->image){
                $web_logo = $res_array['response']->icon->image;
             }

             // Getting page header data
             $array_weblogo = @get_headers($web_logo);
             $string_logo = $array_weblogo[0];
             if(strpos($string_logo, "200"))
             {
                 $comp_name->cname = preg_replace('~[?&#\%<>]~','-',$comp_name->cname);
                 $upload = Cloudder::upload($web_logo,'Joshua/'.$comp_name->cname."_Joshua/logo_br_".uniqid());
                 if($upload){
                     $brlogo_picId = Cloudder::getPublicId();
                     $tag = str_replace(' ', '_', $comp_name->cname);
                     Cloudder::addTag($tag, $brlogo_picId);
                 } else{
                     $brlogo_picId = "Not uploaded";
                 }
             } else{
                 $brlogo_picId = "Broken URL";
             }

             $web_log_message = "";
             $message = "";

             $message = "Saved primary data to bradfetch_primary. ID: ".$br_Id;
             Log::info($message);

             /*--- Save to 'br_logo' table ---*/
             if(!empty($res_array)){
                 DB::table('br_logo')
                     ->updateOrInsert(['br_id' => $br_Id],
                         ['web_logo' => $web_logo,
                         'br_cat_id' => 1,
                         'pic_id'=>$brlogo_picId,
                         'created_at' => $datetime
                     ]);

                 $web_log_message = "Api to fetch website logo successfull. Saved to 'br_logo'";
                 Log::info($message);
             }
         }else {
             $web_logo = "";
             $web_log_message = "Api to fetch website logo. No data retireved";
             Log::error($web_log_message);
         }

         /*--- API to fetch Website colour ---*/
         $web_colour_message = "";
         $web_colour_api_url = 'https://api.brandfetch.io/v1/color';

         $ch_web_colour = curl_init();
         curl_setopt($ch_web_colour, CURLOPT_POST, 1);
         curl_setopt($ch_web_colour, CURLOPT_POSTFIELDS, $data);
         curl_setopt($ch_web_colour, CURLOPT_URL, $web_colour_api_url);
         curl_setopt($ch_web_colour, CURLOPT_HTTPHEADER, array(
           'x-api-key: oouXaXg0zf77frXoUmsWp9ncqgdHwyaT6BlOaB3a',
           'Content-Type: application/json',
         ));
         curl_setopt($ch_web_colour, CURLOPT_TIMEOUT, 400);
         curl_setopt($ch_web_colour, CURLOPT_RETURNTRANSFER, 1);
         curl_setopt($ch_web_colour, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);


         $result_web_colour = curl_exec($ch_web_colour);
         curl_close($ch_web_colour);

         $res_web_colour_array = json_decode($result_web_colour);
         $res_web_colour_array = get_object_vars($res_web_colour_array);
         if($res_web_colour_array['statusCode']==200 && !isset($res_web_colour_array['errorMessage']) && $res_web_colour_array['statusCode'] == 200){

             $web_colour_category = (isset($res_web_colour_array['response']->filtered)) ? $res_web_colour_array['response']->filtered : '';
             $web_colour_raw = (isset($res_web_colour_array['response']->raw)) ? $res_web_colour_array['response']->raw : '';
             foreach($web_colour_category as $key => $value){
                 $web_cc[] = $value;
             }

             DB::table('br_web_colour_category')
                  ->updateOrInsert(['br_id' => $br_Id],
                     ['vibrant' => $web_cc[2],
                     'dark' => $web_cc[0],
                     'light' => $web_cc[1],
                     'br_cat_id' => 1,
                     'created_at' => $datetime
                 ]);

             if(!empty($web_colour_raw))
             {
                 foreach($web_colour_raw as $k => $v){
                     DB::table('br_web_colour_raw')
                     ->updateOrInsert(['br_id' => $br_Id,'colour_code' => $v->color],
                         ['percentage' => $v->percentage,
                         'br_cat_id' => 1,
                         'created_at' => $datetime
                     ]);
                 }
             }

             $web_colour_message = "Website Color api successfull.  Data inserted to 'br_web_colour_category' and 'br_web_colour_raw'";
             Log::info($web_colour_message);

         } else {
             $web_colour_message = "Website Color api. No data fetched";
             Log::error($web_colour_message);
         }

         /*--- API to fetch Logo Colour ---*/
         if($web_logo){
             $logo_colour_message = "";

             $logo_colour_api_url = 'https://api.brandfetch.io/v1/color-api/get-colors-from-logo?';
             $logo_url = $web_logo;
             $logo_data =  json_encode(array("image" => $logo_url));

             $ch_logo_colour = curl_init();
             curl_setopt($ch_logo_colour, CURLOPT_POST, 1);
             curl_setopt($ch_logo_colour, CURLOPT_POSTFIELDS, $logo_data);
             curl_setopt($ch_logo_colour, CURLOPT_URL, $logo_colour_api_url);
             curl_setopt($ch_logo_colour, CURLOPT_HTTPHEADER, array(
             'x-api-key: oouXaXg0zf77frXoUmsWp9ncqgdHwyaT6BlOaB3a',
             'Content-Type: application/json',
             ));
             curl_setopt($ch_logo_colour, CURLOPT_TIMEOUT, 400);
             curl_setopt($ch_logo_colour, CURLOPT_RETURNTRANSFER, 1);
             curl_setopt($ch_logo_colour, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

             $result_logo_colour = curl_exec($ch_logo_colour);
             curl_close($ch_logo_colour);

             $res_logo_colour_array = json_decode($result_logo_colour);
             $res_logo_colour_array = get_object_vars($res_logo_colour_array);

             if(isset($res_logo_colour_array['response']->background))
             {
                 $logo_colour_category = $res_logo_colour_array['response']->filtered;
                 $logo_colour_raw = $res_logo_colour_array['response']->raw;
                 $logo_colour_bg = $res_logo_colour_array['response']->background;

                 foreach($logo_colour_category as $key => $value){
                     $logo_cc[] = $value;
                 }
                 $logo_bg_colour = "";

                 if(is_object($logo_colour_bg))
                 {
                     foreach($logo_colour_bg as $k => $v){
                         $logo_bg[] = $v;
                     }

                     $logo_bg_colour = $logo_bg[0];
                 } else {
                     $logo_bg_colour = $logo_colour_bg;
                 }


                 DB::table('br_logo_colour_category')
                     ->updateOrInsert(['br_id' => $br_Id],
                     ['vibrant' => $logo_cc[0],
                         'dark' => $logo_cc[1],
                         'light' => $logo_cc[2],
                         'br_cat_id' => 1,
                         'created_at' => $datetime
                     ]);

                 foreach($logo_colour_raw as $k => $v){
                     DB::table('br_logo_colour_raw')
                         ->updateOrInsert(['br_id' => $br_Id,'l_colour_code' => $v->color],
                         ['l_percentage' => $v->percentage,
                         'br_cat_id' => 1,
                         'created_at' => $datetime
                     ]);
                 }

                 DB::table('br_logo_background')
                     ->updateOrInsert(['br_id' => $br_Id],
                         ['bg_color' => $logo_bg_colour,
                         'br_cat_id' => 1,
                         'created_at' => $datetime
                     ]);

                 $logo_colour_message = "Logo Color api successfull.  Data inserted to 'br_logo_colour_category','br_logo_colour_raw' and 'br_logo_background'";
                 Log::info($logo_colour_message);
             } else {
                 $logo_colour_message = "Logo Color api. No data fetched";
                 Log::error($logo_colour_message);
             }
         }

         // / --- API to fetch Wesite Fonts --- /

         $web_font_api_url = 'https://api.brandfetch.io/v1/font';
         $font_data =  json_encode(array("domain" => $web_url,"fresh" => false));

         $ch_web_font = curl_init();
         curl_setopt($ch_web_font, CURLOPT_POST, 1);
         curl_setopt($ch_web_font, CURLOPT_POSTFIELDS, $font_data);
         curl_setopt($ch_web_font, CURLOPT_URL, $web_font_api_url);
         curl_setopt($ch_web_font, CURLOPT_HTTPHEADER, array(
           'x-api-key: oouXaXg0zf77frXoUmsWp9ncqgdHwyaT6BlOaB3a',
           'Content-Type: application/json',
         ));
         curl_setopt($ch_web_font, CURLOPT_TIMEOUT, 400);
         curl_setopt($ch_web_font, CURLOPT_RETURNTRANSFER, 1);
         curl_setopt($ch_web_font, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

         $result_web_font = curl_exec($ch_web_font);
         curl_close($ch_web_font);
         $res_web_font_array = json_decode($result_web_font);
         $res_web_font_array = get_object_vars($res_web_font_array);

         if(isset($res_web_font_array)&&!empty($res_web_font_array)){
             if ($res_web_font_array['statusCode'] ==200) {
                 $font_array = json_decode(json_encode($res_web_font_array['response']), True);
                 DB::table('br_website_font')
                     ->updateOrInsert(['br_id' => $br_Id],
                         ['title_tag' => !empty($font_array["title"]["font"])?!empty($font_array["title"]["font"]):'',
                         'primary_font' =>$font_array["paragraph"]["font"],
                         'br_cat_id' => 2,
                         'created_at' => $datetime,
                     ]);
             }

         }

         /*--- API to fetch Wesite Images ---*/

         $web_images_api_url = 'https://api.brandfetch.io/v1/image';
         $web_img_data =  json_encode(array("domain" => $web_url,"fresh" => false));

         $ch_web_images = curl_init();
         curl_setopt($ch_web_images, CURLOPT_POST, 1);
         curl_setopt($ch_web_images, CURLOPT_POSTFIELDS, $web_img_data);
         curl_setopt($ch_web_images, CURLOPT_URL, $web_images_api_url);
         curl_setopt($ch_web_images, CURLOPT_HTTPHEADER, array(
           'x-api-key: oouXaXg0zf77frXoUmsWp9ncqgdHwyaT6BlOaB3a',
           'Content-Type: application/json',
         ));
         curl_setopt($ch_web_images, CURLOPT_TIMEOUT, 0);
         curl_setopt($ch_web_images, CURLOPT_RETURNTRANSFER, 1);
         curl_setopt($ch_web_images, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

         $result_web_images = curl_exec($ch_web_images);
         curl_close($ch_web_images);

         $res_web_images_array = json_decode($result_web_images);
         $res_web_images_array = get_object_vars($res_web_images_array);

         if($res_web_images_array['statusCode'] != "404" && isset($res_web_images_array['response'][0]->image))
         {

             $websiteimages="";
             $i=1;
             DB::table('br_website_images')->where('br_id',$br_Id)->delete();
             foreach($res_web_images_array['response'] as $key => $value){

                 $web_img_array = json_decode(json_encode($value), True);
                 /* Uploading the website images to cloudinary in `$comp_name->cname` folder */
                 $array_webimages = @get_headers($web_img_array['image']);
                 $string_webimages = $array_webimages[0];

                 if(strpos($string_webimages, "200"))
                 {
                     $comp_name->cname = preg_replace('~[?&#\%<>]~','-',$comp_name->cname);
                     $upload_webimg = Cloudder::upload($web_img_array['image'],'Joshua/'.$comp_name->cname."_Joshua/web_img_br_".uniqid());

                     if($upload_webimg)
                     {
                         $brwebimg_picId = Cloudder::getPublicId();
                         $tag = str_replace(' ', '_', $comp_name->cname);
                         Cloudder::addTag($tag, $brwebimg_picId);
                     } else
                     {
                         $brwebimg_picId = "Not uploaded";
                     }
                 } else
                 {
                     $brwebimg_picId = "Broken URL";
                 }

                 if($i>1){
                     $websiteimages=$websiteimages.",".$web_img_array['image'];
                 }
                 else{
                     $websiteimages=$web_img_array['image'];
                 }


                 DB::table('br_website_images')
                     ->updateOrInsert(['br_id' => $br_Id,'image_url' => $web_img_array['image']],
                         ['br_cat_id' => 1,
                         'webimg_pic_id' => $brwebimg_picId,
                         'created_at' => $datetime
                     ]);

                 $i++;
             }

             $filepath = $comp_name->cname.'_'.time().'_'."_Joshua.txt";

             Storage::put($filepath, $websiteimages);

            // $file_access_path=$_SERVER['SERVER_NAME']."/joshua/smartoneapp/storage/app/".$filepath;

                      DB::table('br_image_path')
                             ->insert([
                                 'br_id' => $br_Id,
                                 'file_path' => $filepath
                             ]);
         }

         /*--- API to fetch Wesite Screenshot ---*/

         $web_scn_api_url = 'https://api.brandfetch.io/v1/screenshot';
         $web_scn_data =  json_encode(array("url" => $web_url,"fresh" => false));

         $ch_web_scn_images = curl_init();
         curl_setopt($ch_web_scn_images, CURLOPT_POST, 1);
         curl_setopt($ch_web_scn_images, CURLOPT_POSTFIELDS, $web_scn_data);
         curl_setopt($ch_web_scn_images, CURLOPT_URL, $web_scn_api_url);
         curl_setopt($ch_web_scn_images, CURLOPT_HTTPHEADER, array(
           'x-api-key: oouXaXg0zf77frXoUmsWp9ncqgdHwyaT6BlOaB3a',
           'Content-Type: application/json',
         ));
         curl_setopt($ch_web_scn_images, CURLOPT_TIMEOUT, 0);
         curl_setopt($ch_web_scn_images, CURLOPT_RETURNTRANSFER, 1);
         curl_setopt($ch_web_scn_images, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);


         $result_web_scn_images = curl_exec($ch_web_scn_images);
         curl_close($ch_web_scn_images);

         $res_web_scn_images_array = json_decode($result_web_scn_images);
         $res_web_scn_images_array = get_object_vars($res_web_scn_images_array);
         // print_r($res_web_scn_images_array);
         if(isset($res_web_scn_images_array['statusCode']) && $res_web_scn_images_array['statusCode'] != '200'){
             Log::error($res_web_scn_images_array['response']." for ".$comp_name->cname);
             $r['error'] = $res_web_scn_images_array['response']." for ".$comp_name->cname;
         }
         if($res_web_scn_images_array['statusCode']==200)
         {
             /* Uploading the website screenshot to cloudinary in `$comp_name->cname` folder */

             $array_webscnimages = @get_headers($res_web_scn_images_array['response']->image);
             $string_webscnimages = $array_webscnimages[0];

             if(strpos($string_webscnimages, "200"))
             {
                 $comp_name->cname = preg_replace('~[?&#\%<>]~','-',$comp_name->cname);
                 $upload_websc = Cloudder::upload($res_web_scn_images_array['response']->image,'Joshua/'.$comp_name->cname."_Joshua/web_scn_br_".uniqid());

                 if($upload_websc)
                 {
                     $brwebscn_picId = Cloudder::getPublicId();
                     $tag = str_replace(' ', '_', $comp_name->cname);
                     Cloudder::addTag($tag, $brwebscn_picId);
                 } else
                 {
                     $brwebscn_picId = "Not uploaded";
                 }
             } else
             {
                 $brwebscn_picId = "Broken URL";
             }
                 DB::table('br_website_screenshot')
                     ->updateOrInsert(['br_id' => $br_Id],
                         ['scn_url' => $res_web_scn_images_array['response']->image,
                         'br_cat_id' => 1,
                         'webscn_pic_id' => $brwebscn_picId,
                         'created_at' => $datetime
                     ]);

         }

         /*--- API to fetch Wesite Metadata ---*/

         $web_meta_api_url = 'https://api.brandfetch.io/v1/company';
         $web_meta_data =  json_encode(array("domain" => $web_url,"fresh" => true,"renderJS" => true));

         $ch_web_meta = curl_init();
         curl_setopt($ch_web_meta, CURLOPT_POST, 1);
         curl_setopt($ch_web_meta, CURLOPT_POSTFIELDS, $web_meta_data);
         curl_setopt($ch_web_meta, CURLOPT_URL, $web_meta_api_url);
         curl_setopt($ch_web_meta, CURLOPT_HTTPHEADER, array(
           'x-api-key: oouXaXg0zf77frXoUmsWp9ncqgdHwyaT6BlOaB3a',
           'Content-Type: application/json',
         ));
         curl_setopt($ch_web_meta, CURLOPT_TIMEOUT, 280);
         curl_setopt($ch_web_meta, CURLOPT_RETURNTRANSFER, 1);
         curl_setopt($ch_web_meta, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
         curl_setopt($ch_web_meta, CURLOPT_CONNECTTIMEOUT, 0);

         $result_web_meta = curl_exec($ch_web_meta);
         curl_close($ch_web_meta);

         $res_web_meta_array = json_decode($result_web_meta);
         $res_web_meta_array = get_object_vars($res_web_meta_array);

         //if($res_web_meta_array['statusCode'] != "404")

         if(isset($res_web_meta_array['response']) && $res_web_meta_array['statusCode']==200)
         {
           //  print_r($res_web_meta_array);
             DB::table('br_website_meta_data')
             ->updateOrInsert(['br_id' => $br_Id],
                 ['title' => !empty($res_web_meta_array['response']->name)?$res_web_meta_array['response']->name:'',
                 // 'summary' => $res_web_meta_array['response']->summary,
                 'description' => !empty($res_web_meta_array['response']->description)?$res_web_meta_array['response']->description:'',
                 'keywords' => !empty($res_web_meta_array['response']->keywords)?$res_web_meta_array['response']->keywords:'',
                 'language' => !empty($res_web_meta_array['response']->language)?$res_web_meta_array['response']->language:'',
                 'br_cat_id' => 3,
                 'created_at' => $datetime
             ]);

             if(isset($res_web_meta_array['response']->twitter->url)) { $twitter = $res_web_meta_array['response']->twitter->url; } else { $twitter = ""; }
             if(isset($res_web_meta_array['response']->linkedin->url)) { $linkedin = $res_web_meta_array['response']->linkedin->url; } else { $linkedin = ""; }
             if(isset($res_web_meta_array['response']->github->url)) { $github = $res_web_meta_array['response']->github->url; } else { $github = ""; }
             if(isset($res_web_meta_array['response']->instagram->url)) { $instagram = $res_web_meta_array['response']->instagram->url; } else { $instagram = ""; }
             if(isset($res_web_meta_array['response']->facebook->url)) { $facebook = $res_web_meta_array['response']->facebook->url; } else { $facebook = ""; }
             if(isset($res_web_meta_array['response']->youtube->url)) { $youtube = $res_web_meta_array['response']->youtube->url; } else { $youtube = ""; }
             if(isset($res_web_meta_array['response']->pinterest->url)) { $pinterest = $res_web_meta_array['response']->pinterest->url; } else { $pinterest = ""; }
             if(isset($res_web_meta_array['response']->crunchbase->url)) { $crunchbase = $res_web_meta_array['response']->crunchbase->url; } else { $crunchbase = ""; }

             DB::table('br_company_social_media')
                 ->updateOrInsert(['br_id' => $br_Id],
                         ['twitter' => $twitter,
                         'linkedin' => $linkedin,
                         'github' => $github,
                         'instagram' => $instagram,
                         'facebook' => $facebook,
                         'youtube' => $youtube,
                         'pinterest' => $pinterest,
                         'crunchbase' => $crunchbase,
                         'br_cat_id' => 4,
                         'created_at' => $datetime
                     ]);
         }
        // /* ------ Silktide Prospect API ----- */
        $data = json_encode(array('url' => $webUrl ));
        $url = 'https://api.prospect.silktide.com/api/v1/report';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
          'api-key: 398a2e9cb7fc76f58678cc5bb41bfb5bd8f839c4',
          'Content-Type: application/json',
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);


        $result = curl_exec($curl);
        $result = json_decode($result,true);
        if(!$result || $result['status']=='error') {
            if($result['issue']=='redirect'){
                $r['error'] = "The web address you entered redirects to another website. If you'd like to test this website instead, change your url to <a href=".$result['url'].">".$result['url']."</a>";
                Log::error("The web address you entered redirects to another website. If you'd like to test this website instead, change your url to ".$result['url']);
                return $r;
            }
            $r['error'] = $result['error'];
            return $r;
        }
        curl_close($curl);

        $reportId = $result['reportId'];

        $detail_url = "https://api.prospect.silktide.com/api/v1/report/".$reportId;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $detail_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'api-key: 398a2e9cb7fc76f58678cc5bb41bfb5bd8f839c4',
          'Content-Type: application/json',
        ));

        $fetch_data = curl_exec($ch);
        $fetch_data = json_decode($fetch_data,true);
        //echo '<pre>'; print_r($fetch_data);
        //echo implode(',',$fetch_data['report']['paid_search']['adwords_keywords']);

        /* --- Save to 'silktide_prospect' table -- */
        $exists = DB::table('silktide_prospect')->where('url',$webUrl)->first();
        if(empty($exists)){
            $st_Id = DB::table('silktide_prospect')
                ->insertGetId([
                    'report_id' => $reportId,
                    'account_id' => $fetch_data['report']['account_id'],
                    'company_id' => $company_id,
                    'url' => $webUrl,
                    'user_id' => $user_id,
                    'created_at' => $datetime
                ]);
        }else{
            $st_Id = $exists->st_id;
        }

        /* --- Save to 'st_sitecontent' table --- */

        DB::table('st_sitecontent')
            ->updateOrInsert(['st_id' => $st_Id],
                ['average_words_per_page' => (isset($fetch_data['report']['amount_of_content']['average_words_per_page'])) ? $fetch_data['report']['amount_of_content']['average_words_per_page'] : 0,
                'pages_found' => (isset($fetch_data['report']['amount_of_content']['pages_found'])) ? $fetch_data['report']['amount_of_content']['pages_found'] : 0,
                'total_word_count' => (isset($fetch_data['report']['amount_of_content']['total_word_count'])) ? $fetch_data['report']['amount_of_content']['total_word_count'] : 0,
                'cms_solution' => (isset($fetch_data['report']['content_management_system']['cms_solution'])&&!empty($fetch_data['report']['content_management_system']['cms_solution'])) ? implode(',',$fetch_data['report']['content_management_system']['cms_solution']) : '',
                'has_cms' => (isset($fetch_data['report']['content_management_system']['has_cms'])) ? $fetch_data['report']['content_management_system']['has_cms'] : 0,
                'pages_discovered_count' => (isset($fetch_data['report']['page_count']['pages_discovered_count'])) ? $fetch_data['report']['page_count']['pages_discovered_count'] : 0,
                'created_at' => $datetime
            ]);

        /* --- Save to 'st_vitals' table --- */

        DB::table('st_vitals')
        ->updateOrInsert(['st_id' => $st_Id],
            ['pages_tested' => (isset($fetch_data['report']['amount_of_content']['pages_tested'])) ? $fetch_data['report']['amount_of_content']['pages_tested'] : 0,
            'analytics_tool' => (isset($fetch_data['report']['analytics']['analytics_tool'])) ? $fetch_data['report']['analytics']['analytics_tool'] : '',
            'has_analytics' => (isset($fetch_data['report']['analytics']['has_analytics'])) ? $fetch_data['report']['analytics']['has_analytics'] : 0,
            'domain' => (isset($fetch_data['report']['domain'])) ? $fetch_data['report']['domain'] : '',
            'ecommerce_name' => (isset($fetch_data['report']['ecommerce']['ecommerce_name'])) ? $fetch_data['report']['ecommerce']['ecommerce_name'] : '',
            'has_ecommerce' => (isset($fetch_data['report']['ecommerce']['has_ecommerce'])) ? $fetch_data['report']['ecommerce']['has_ecommerce'] : 0,
            'has_pixel' => (isset($fetch_data['report']['facebook_retargeting']['has_pixel'])) ? $fetch_data['report']['facebook_retargeting']['has_pixel'] : 0,
            'address_details_provided' => (isset($fetch_data['report']['local_presence']['address_details_provided'])) ? $fetch_data['report']['local_presence']['address_details_provided'] : 0,
            'has_result' => (isset($fetch_data['report']['local_presence']['has_result'])) ? $fetch_data['report']['local_presence']['has_result'] : 0,
            'yext_api_success' => (isset($fetch_data['report']['local_presence']['yext_api_success'])) ? $fetch_data['report']['local_presence']['yext_api_success'] : 0,
            'has_mobile_site' => (isset($fetch_data['report']['mobile']['has_mobile_site'])) ? $fetch_data['report']['mobile']['has_mobile_site'] : 0,
            'is_mobile' => (isset($fetch_data['report']['mobile']['is_mobile'])) ? $fetch_data['report']['mobile']['is_mobile'] : 0,
            'is_tablet' => (isset($fetch_data['report']['mobile']['is_tablet'])) ? $fetch_data['report']['mobile']['is_tablet'] : 0,
            'mobile_screenshot_url' => (isset($fetch_data['report']['mobile']['mobile_screenshot_url'])) ? $fetch_data['report']['mobile']['mobile_screenshot_url'] : '',
            'mobile_site_url' => (isset($fetch_data['report']['mobile']['mobile_site_url'])) ? $fetch_data['report']['mobile']['mobile_site_url'] : '',
            'average_monthly_traffic' => (isset($fetch_data['report']['organic_search']['average_monthly_traffic'])) ? $fetch_data['report']['organic_search']['average_monthly_traffic'] : 0,
            'adwords_keywords' => (isset($fetch_data['report']['paid_search']['adwords_keywords'])&&!empty($fetch_data['report']['paid_search']['adwords_keywords'])) ? implode(',', $fetch_data['report']['paid_search']['adwords_keywords']) : '',
            'has_adwords_spend' => (isset($fetch_data['report']['paid_search']['has_adwords_spend'])) ? $fetch_data['report']['paid_search']['has_adwords_spend'] : 0,
            'has_sitemap' => (isset($fetch_data['report']['sitemap']['has_sitemap'])) ? $fetch_data['report']['sitemap']['has_sitemap'] : 0,
            'sitemap_issues' => (isset($fetch_data['report']['sitemap']['sitemap_issues'])) ? $fetch_data['report']['sitemap']['sitemap_issues'] : 0,
            'stale_analysis' => (isset($fetch_data['report']['stale_analysis'])) ? $fetch_data['report']['stale_analysis'] : 0,
            'created_at' => $datetime

        ]);


        /* --- Save to 'st_discovered' table --- */

        DB::table('st_discovered')
        ->updateOrInsert(['st_id' => $st_Id],
            ['email' => (isset($fetch_data['report']['contact_details']['email'])&&!empty($fetch_data['report']['contact_details']['email'])) ? implode(',',$fetch_data['report']['contact_details']['email']) : '',
            'emails' => (isset($fetch_data['report']['contact_details']['emails'])&&!empty($fetch_data['report']['contact_details']['emails'])) ? implode(',',$fetch_data['report']['contact_details']['emails']) : '',
            'phone' => (isset($fetch_data['report']['contact_details']['phone'])&&!empty($fetch_data['report']['contact_details']['phone'])) ? implode(',',$fetch_data['report']['contact_details']['phone']) : '',
            'phones' => (isset($fetch_data['report']['contact_details']['phones'])&&!empty($fetch_data['report']['contact_details']['phones'])) ? implode(',',$fetch_data['report']['contact_details']['phones']) : '',
            'created_at' => $datetime
        ]);

        /* --- Save to 'st_domain_info' table --- */

        DB::table('st_domain_info')
        ->updateOrInsert(['st_id' => $st_Id],
            ['domain_age_days' => (isset($fetch_data['report']['domain_age']['domain_age_days'])) ? $fetch_data['report']['domain_age']['domain_age_days'] : '',
            'expiry_date' => (isset($fetch_data['report']['domain_age']['expiry_date'])) ? $fetch_data['report']['domain_age']['expiry_date'] : '',
            'registered_date' => (isset($fetch_data['report']['domain_age']['registered_date'])) ? $fetch_data['report']['domain_age']['registered_date'] : '',
            'created_at' => $datetime
        ]);


        /* --- Save to 'st_domain' table --- */
            // print_r($fetch_data['report']);
        DB::table('st_domain')
        ->updateOrInsert(['st_id' => $st_Id],
            ['is_parked' => (isset($fetch_data['report']['parked_domain_detection']['is_parked'])) ? $fetch_data['report']['parked_domain_detection']['is_parked'] : '0',
            'has_ssl' => (isset($fetch_data['report']['ssl']['has_ssl'])) ? $fetch_data['report']['ssl']['has_ssl'] : '0',
            'ssl_expired' => (isset($fetch_data['report']['ssl']['ssl_expired'])) ? $fetch_data['report']['ssl']['ssl_expired'] : '0',
            'ssl_redirect' => (isset($fetch_data['report']['ssl']['ssl_redirect'])) ? $fetch_data['report']['ssl']['ssl_redirect'] : '0',
            'ssl_valid' => (isset($fetch_data['report']['ssl']['ssl_valid'])) ? $fetch_data['report']['ssl']['ssl_valid'] : '0',
            'created_at' => $datetime
        ]);

        /* --- Save to 'st_seo' table --- */
        // echo '<pre>'; print_r($fetch_data['report']);

        DB::table('st_seo')
        ->updateOrInsert(['st_id' => $st_Id],
            ['good_headings' => (isset($fetch_data['report']['headings']['good_headings'])) ? $fetch_data['report']['headings']['good_headings'] : 0,
            'has_content_for_every_heading' => (isset($fetch_data['report']['headings']['has_content_for_every_heading'])) ? $fetch_data['report']['headings']['has_content_for_every_heading'] : 0,
            'has_hierarchical_headings' => (isset($fetch_data['report']['headings']['has_hierarchical_headings'])) ? $fetch_data['report']['headings']['has_hierarchical_headings'] : 0,
            'has_single_h1_on_each_page' => (isset($fetch_data['report']['headings']['has_single_h1_on_each_page'])) ? $fetch_data['report']['headings']['has_single_h1_on_each_page'] : 0,
            'analysis_country' => (isset($fetch_data['report']['meta']['analysis_country'])) ? $fetch_data['report']['meta']['analysis_country'] : '',
            'detected_address' => (isset($fetch_data['report']['meta']['detected_address'])) ? $fetch_data['report']['meta']['detected_address'] : '',
            'detected_name' => (isset($fetch_data['report']['meta']['detected_name'])) ? $fetch_data['report']['meta']['detected_name'] : '',
            'detected_phone' => (isset($fetch_data['report']['meta']['detected_phone'])) ? $fetch_data['report']['meta']['detected_phone'] : '',
            'num_keywords_ranked_for' => (isset($fetch_data['report']['organic_search']['num_keywords_ranked_for'])) ? $fetch_data['report']['organic_search']['num_keywords_ranked_for'] : 0,
            'top_keywords_ranked_for' => (isset($fetch_data['report']['organic_search']['top_keywords_ranked_for'])) ? json_encode($fetch_data['report']['organic_search']['top_keywords_ranked_for']) : '',
            'duplicated_items' => (isset($fetch_data['report']['page_titles_and_descriptions']['duplicated_items'])) ? $fetch_data['report']['page_titles_and_descriptions']['duplicated_items'] : 0,
            'homepage_title_tag' => (isset($fetch_data['report']['page_titles_and_descriptions']['homepage_title_tag'])) ? $fetch_data['report']['page_titles_and_descriptions']['homepage_title_tag'] : '',
            'missing_items' => (isset($fetch_data['report']['page_titles_and_descriptions']['missing_items'])) ? $fetch_data['report']['page_titles_and_descriptions']['missing_items'] : '',
            'pages_duplicated_description_count' => (isset($fetch_data['report']['page_titles_and_descriptions']['pages_duplicated_description_count'])) ? $fetch_data['report']['page_titles_and_descriptions']['pages_duplicated_description_count'] : 0,
            'pages_duplicated_title_count' => (isset($fetch_data['report']['page_titles_and_descriptions']['pages_duplicated_title_count'])) ? $fetch_data['report']['page_titles_and_descriptions']['pages_duplicated_title_count'] : 0,
            'pages_missing_description_count' => (isset($fetch_data['report']['page_titles_and_descriptions']['pages_missing_description_count'])) ? $fetch_data['report']['page_titles_and_descriptions']['pages_missing_description_count'] : 0,
            'pages_missing_title_count' => (isset($fetch_data['report']['page_titles_and_descriptions']['pages_missing_title_count'])) ? $fetch_data['report']['page_titles_and_descriptions']['pages_missing_title_count'] : 0,
            'percent_duplicated_descriptions' => (isset($fetch_data['report']['page_titles_and_descriptions']['percent_duplicated_descriptions'])) ? $fetch_data['report']['page_titles_and_descriptions']['percent_duplicated_descriptions'] : 0,
            'percent_duplicated_titles' => (isset($fetch_data['report']['page_titles_and_descriptions']['percent_duplicated_titles'])) ? $fetch_data['report']['page_titles_and_descriptions']['percent_duplicated_titles'] : 0,
            'percent_missing_descriptions' => (isset($fetch_data['report']['page_titles_and_descriptions']['percent_missing_descriptions'])) ? $fetch_data['report']['page_titles_and_descriptions']['percent_missing_descriptions'] : 0,
            'percent_missing_titles' => (isset($fetch_data['report']['page_titles_and_descriptions']['percent_missing_titles'])) ? $fetch_data['report']['page_titles_and_descriptions']['percent_missing_titles'] : 0,
            'created_at' => $datetime
        ]);

        /* --- Save to 'st_images' table --- */

        DB::table('st_images')
        ->updateOrInsert(['st_id' => $st_Id],
            ['image_count' => (isset($fetch_data['report']['images']['image_count'])) ? $fetch_data['report']['images']['image_count'] : 0,
            'non_web_friendly_count' => (isset($fetch_data['report']['images']['non_web_friendly_count'])) ? $fetch_data['report']['images']['non_web_friendly_count'] : 0,
            'percent_images_sized' => (isset($fetch_data['report']['images']['percent_images_sized'])) ? $fetch_data['report']['images']['percent_images_sized'] : 0,
            'stretched_image_count' => (isset($fetch_data['report']['images']['stretched_image_count'])) ? $fetch_data['report']['images']['stretched_image_count'] : 0,
            'created_at' => $datetime
        ]);

        /* --- Save to 'st_social' table --- */

        DB::table('st_social')
        ->updateOrInsert(['st_id' => $st_Id],
            ['has_instagram' => (isset($fetch_data['report']['instagram_account']['has_instagram'])) ? $fetch_data['report']['instagram_account']['has_instagram'] : 0,
            'days_since_update' => (isset($fetch_data['report']['last_updated']['days_since_update'])) ? $fetch_data['report']['last_updated']['days_since_update'] : 0,
            'last_updated_date' => (isset($fetch_data['report']['last_updated']['last_updated_date'])) ? $fetch_data['report']['last_updated']['last_updated_date'] : '',
            'twitter_found' => (isset($fetch_data['report']['twitter']['found'])) ? $fetch_data['report']['twitter']['found'] : 0,
            'created_at' => $datetime
        ]);

        /* --- Save to 'st_video' table --- */

        DB::table('st_video')
        ->updateOrInsert(['st_id' => $st_Id],
            ['vendor_vendor' => (isset($fetch_data['report']['vendor']['vendor'])) ? $fetch_data['report']['vendor']['vendor']: '',
            'has_video' => (isset($fetch_data['report']['video']['has_video'])) ? $fetch_data['report']['video']['has_video'] : 0,
            'video_vendor' => (isset($fetch_data['report']['video']['vendor'])) ? $fetch_data['report']['video']['vendor'] : '',
            'website_traffic' => (isset($fetch_data['report']['website_traffic']['website_traffic'])) ? $fetch_data['report']['website_traffic']['website_traffic'] : 0,
            'created_at' => $datetime
        ]);

        //broken links
        $links=$this->getUrls($web_url);
        $broken_links='';
        $working_links=[];
        $broken_links_count=0;
        foreach($links as $link){
            if($this->check_url($link)==false){
                $broken_links.='====!'.$link;
                $broken_links_count++;
            }
            else{
                array_push($working_links,$link);
            }
        }

       //broken images
       $Images=$this->getImgs($web_url);
       $broken_images='';
       $broken_img_count=0;
       foreach($Images as $link){
            if($this->check_url($link)==false){
                $broken_images.='====!'.$link;
                $broken_img_count++;
            }
        }

        //check word in pages
        $word_pages='';
        $words_count=0;
        $title_pages='';
        $title_count=0;
        foreach($working_links as $link){
            if($this->check_word($link,"lorem")=="1"){
                $word_pages.='====!'.$link;
                $words_count++;
            }
            //check for missing title
            if($this->check_title($link)=="1"){
                $title_pages.='====!'.$link;
                $title_count++;
            }
        }
        DB::table('qareport')
        ->updateOrInsert(['company_id' => $company_id],
           ['url' => $web_url,
           'broken_url_count' => $broken_links_count,
           'broken_urls_list' => $broken_links,
           'broken_images_count' => $broken_img_count,
           'broken_images_list' => $broken_images,
           'title_less'=>$title_pages,
           'title_less_count'=>$title_count,
           'lorem_pages'=>$word_pages,
           'lorem_count'=>$words_count,
           'created_at'=>$datetime
       ]);
        return 1;
    }
    function getUrls($url,$depth=5){

        $client=new Client();
        $crawler=$client->request('GET',$url);
        $anchors=$crawler->filter('a')->links();
        $links=[];

        foreach ($anchors as $anchor) {
            if (!(strpos($anchor->getUri(), $url))) {
                $path = ltrim($anchor->getUri(), '/');

                //exclude social media pages
                if(strpos($path,'google.com')||strpos($path,'linkedin.com')||strpos($path,'facebook.com')||strpos($path,'youtube.com')||strpos($path,'twitter.com')||strpos($path,'instagram.com')){
                continue;
                }
                else{
                    array_push($links,$path);
                }
            }
        }
        $links=array_unique($links);
        return $links;
    }

    function getImgs($url){
        $client=new Client();
        $crawler=$client->request('GET',$url);
        $images=$crawler->filter('img')->each(function($node){
            return $node->attr('src');
        });

        $images=array_unique($images);
        return $images;
    }
    function check_url($url) {

        $ch = curl_init();

        $headers = array();
        $headers[] = 'Content-type: charset=utf-8';
        $headers[] = 'Connection: Keep-Alive';
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_exec($ch);
        $status=curl_getinfo($ch,CURLINFO_HTTP_CODE);
        //print curl_error($ch);
        curl_close($ch);
        if ( $status=='0'||$status=='404'){
            return false;
        }
       return true;
    }
    function check_word($url,$word) {
        $client=new Client();
        $crawler=$client->request('GET',$url);
        try{
            if(strpos(json_encode($crawler->html()),$word)){
                return '1';
        }
        }
        catch(InvalidArgumentException $e){
            return '0';
        }
        return '0';
    }
    function check_title($url) {
        $client=new Client();
        $crawler=$client->request('GET',$url);
        try{
            $title=$crawler->filter('title')->text();
            if(empty($crawler->filter('title')->text())){
                return '1';
            }
        }
        catch(InvalidArgumentException $e){
            return '0';
        }
        return '0';
    }
}

