<?php

namespace App\Http\Controllers;

//use App\Permission;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;
use App\User;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index()
    {
        // return auth()->user()->permissions;
        // die();
        //auth()->user()->revokePermissionTo('api center');

        $permissions = DB::table('permissions')
                            ->where('pstatus','=',0)
                            ->get();
        return view('permissions',compact('permissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('addpermissions');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|unique:permissions|max:255',
        ]);
        Permission::create(['name' => $request->name]);
        return redirect('permissions')->with('status', 'Permission Saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function show(Permission $permission)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function edit(Permission $permission,$id)
    {
        $permissions = DB::table('permissions')
                    ->where('id',$id)
                    ->first();

        return view('editpermissions',compact('permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Permission $permission,$id)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
        ]);

        DB::table('permissions')
            ->where('id', $id)
            ->update($validatedData);
        return redirect('permissions')->with('status', 'Permission Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function destroy(Permission $permission,Request $request)
    {
        $id = $request->permissionid;
        Permission::destroy($id);
        // DB::table('permissions')
        //     ->where('id', $id)
        //     ->delete(['pstatus' => 1]);
         return "1";
    }

    public function assignPermission($id)
    {
        // $roles = DB::table('roles')
        //             ->where('id',$id)
        //             ->first();

        $permissions = DB::table('permissions')
                            ->where('pstatus','=','0')
                            ->get();

        $users = DB::table('users')
                            ->leftjoin('roles','roles.id','=','users.role_id')
                            ->select('users.id','users.name','roles.id as role_id','roles.name as role_name')
                            ->where('users.id',$id)
                            ->first();

        $role_id = $users->role_id;
        $user_id = $users->id;
        //echo '<pre>'; print_r($users); die();


        $assignPermissions = DB::table('role_has_permissions')
                                ->leftjoin('model_has_permissions','role_has_permissions.permission_id','=','model_has_permissions.permission_id')
                                ->select('role_has_permissions.permission_id')
                                ->where('model_has_permissions.model_id','=',$user_id)
                                ->get()
                                ->toArray();


        $assignPermissionsArr[] = "";

        if(!empty($assignPermissions)){
        foreach ($assignPermissions as $value) {
            $assignPermissionsArr[] = $value->permission_id;
        }
    }


        return view('assignpermissions',compact('permissions','users','assignPermissionsArr'));
    }

    public function savePermissions(Request $request)
    {
        $role = Role::findById($request->roleid);
        $user_id = $request->userid;
        $rolename = $request->name;
        $permissions = $request->asperms;


        $user = User::where('id', '=', $user_id)->first();

        $username = $user->name;

        $permission_data = DB::table('role_has_permissions')
                                ->leftjoin('permissions','role_has_permissions.permission_id','=','permissions.id')
                                ->select('role_has_permissions.permission_id','permissions.name')
                                ->where('role_has_permissions.role_id','=',$request->roleid)
                                ->get();

        $permission_count = count((array)$permission_data);

        if($permission_count>0){
            //auth()->user()->revokePermissionTo('api center');
            DB::table('role_has_permissions')->where('role_id','=',$request->roleid)->delete();
            foreach($permission_data as $kp => $pvalue){
                $user->revokePermissionTo($pvalue->name);
            }
        }

        if(!empty($permissions))
        {
            foreach($permissions as $key => $value)
            {
                $role->givePermissionTo($value);
                $permission_name = DB::table('permissions')
                                    ->select('id','name','guard_name')
                                    ->where('id',$value)
                                    ->first();

                $user->givePermissionTo($permission_name->name);
                $user->assignRole($rolename);

            }
        }


        return redirect('users')->with('status', 'Permission assigned to '.$username.'!');

    }
}
