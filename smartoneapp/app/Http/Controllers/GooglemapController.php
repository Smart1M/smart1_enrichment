<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use DB;
use Excel;
use Storage;
use Carbon;
use ErrorException;

class GooglemapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('exportgooglemapdata');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function importExcel(Request $request)
    {
        if(Input::hasFile('google_export')){
            //$path = Input::file('google_export')->getRealPath();
            $filename = Input::file('google_export')->getClientOriginalName();
            $extension = Input::file('google_export')->getClientOriginalExtension();
            $tempPath = Input::file('google_export')->getRealPath();
            $fileSize = Input::file('google_export')->getSize();
            $mimeType = Input::file('google_export')->getMimeType();

            $current_date = date('Y-m-d H:i:s');

            $filename = pathinfo($filename, PATHINFO_FILENAME);
            $filename = trim($filename).'_'.time().'.'.$extension;

            $valid_extension = array("csv");
            $maxFileSize = 2097152;
            $knackUpdate=Input::get('updateKnack');
            if(in_array(strtolower($extension),$valid_extension)){
            
            // Check file size
            if($fileSize <= $maxFileSize){

                // File upload location
                $location = 'uploads';

                // Upload file
                Input::file('google_export')->storeAs('uploads',$filename);

                // Import CSV to Database
                $filepath =Storage::files('uploads');

                // echo '<pre>'; print_r($filepath); die();

                // Reading file
                $file = fopen(public_path().'/uploads/'.$filename,"r");

                // $url = storage_path('me.txt');
                // print_r($url);die();
                // $file = fopen($url,"r");
                
                $importData_arr = array();
                $i = 0;
                // echo print_r($filedata);die();
                while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                    $num = count($filedata );
                    
                    // Skip first row (Remove below comment if you want to skip the first row)
                    if($i == 0){
                        $i++;
                        continue;
                    }
                    for ($c=0; $c < $num; $c++) {
                        $importData_arr[$i][] = $filedata [$c];
                    }
                    $i++;
                }
                fclose($file);

                try{
                    //echo '<pre>'; print_r($importData_arr); die();
                    foreach ($importData_arr as $key => $value) {

                        $cname = isset($value['1']) ? $value['1'] : 'Company Name';
                        $email = empty($value['10'])?'you@example.com':$value['10'];
                        $company_url = empty($value['8'])? 'http://example.com/':$value['8'] ;
                        $phone = isset($value['9']) ? $value['9'] : '0123456789';
                        $address1= isset($value['2']) ? $value['2'] : 'Address here';
                        $city = isset($value['4']) ? $value['4'] : 'City here';
                        $state_id = isset($value['5']) ? $value['5'] : 'OH';
                        $zip = isset($value['6']) ? $value['6'] :'43110';
                        $plus_code = isset($value['7'])?$value['7']:'';
                        $latitude = isset($value['11'])?$value['11']:'';
                        $longitude = isset($value['12'])?$value['12']:'';
                        $virtual_tour = isset($value['13'])?$value['13']:'';
                        $claimed = isset($value['14'])?$value['14']:'';
                        $category = isset($value['15'])?$value['15']:'';
                        $rating = isset($value['16'])?$value['16']:'';
                        $review_count = isset($value['17'])?$value['17']:'';
                        $amenities = isset($value['18'])?$value['18']:'';
                        $photo_count = isset($value['19'])?$value['19']:'';
                        $main_image = isset($value['20'])?$value['20']:'';
                        $sub_title = isset($value['21'])?$value['21']:'';
                        $google_url = isset($value['29'])?$value['29']:'';
                        $media_partner = isset($value['30']) ? $value['30'] : '';
                        $Client_number= isset($value['31']) ? $value['31'] : '';
                        $password= isset($value['32']) ? $value['32'] : '';
                        $Development_link= isset($value['33']) ? $value['33'] : '';

                        /* Hours */
                        $monday = isset($value['22']) ? $value['22'] : '';
                        $tuesday = isset($value['23']) ? $value['23'] : '';
                        $wednesday = isset($value['24']) ? $value['24'] : '';
                        $thursday = isset($value['25']) ? $value['25'] : '';
                        $friday = isset($value['26']) ? $value['26'] : '';
                        $saturday = isset($value['27']) ? $value['27'] : '';
                        $sunday = isset($value['28']) ? $value['28'] : '';
                    
                        $state_db = DB::table('states_db')
                                    ->select('state_name')
                                    ->where('state_code','=',$state_id)
                                    ->first();
                        if(!empty($state_db)){
                            $state_name = $state_db->state_name;
                        }
                        else{
                            $state_name = "";
                        }
                        if(!empty($value['0'])){
                            $company_id = DB::table('company')
                                        ->insertGetId([
                                            'cname' =>$cname,
                                            'email'=>$email,
                                            'company_url'=>$company_url,
                                            'phone'=> $phone,
                                            'address1'=>$address1,
                                            'city'=>$city,
                                            'state_name'=>$state_name,
                                            'zip'=>$zip,
                                            'media_partner'=>$media_partner,
                                            'plus_code'=>$plus_code,
                                            'latitude'=>$latitude,
                                            'longitude'=>$longitude,
                                            'virtual_tour'=>$virtual_tour,
                                            'claimed'=>$claimed,
                                            'category'=>$category,
                                            'rating'=>$rating,
                                            'review_count'=>$review_count,
                                            'amenities'=>$amenities,
                                            'photo_count'=>$photo_count,
                                            'main_image'=>$main_image,
                                            'sub_title'=>$sub_title,
                                            'Client_number'=>$Client_number,
                                            'password'=>$password,
                                            'Development_link'=>$Development_link
                                        ]);
                            
                            //post company to Knack
                            if($knackUpdate){
                                $data=json_encode(array(
                                    'field_1'=>$company_id,
                                    'field_2'=>$cname,
                                    'field_3'=>$address1,
                                    'field_4'=>$email,
                                    'field_5'=>$phone,
                                    'field_6'=>$company_url,
                                    'field_7'=>$media_partner,
                                    'field_8'=>$current_date,
                                    'field_337'=>$Development_link,
                                    'field_338'=>$Client_number,
                                    'field_339'=>$password,
                                ));
                                
                                $curl = curl_init();

                                curl_setopt_array($curl, array(
                                CURLOPT_URL => "https://api.knack.com/v1/objects/object_1/records",
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_ENCODING => "",
                                CURLOPT_MAXREDIRS => 10,
                                CURLOPT_TIMEOUT => 0,
                                CURLOPT_FOLLOWLOCATION => true,
                                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                CURLOPT_CUSTOMREQUEST => "POST",
                                CURLOPT_POSTFIELDS =>$data,
                                CURLOPT_HTTPHEADER => array(
                                    "X-Knack-Application-Id: 5f7e2af82c55b5001636420b",
                                    "X-Knack-REST-API-Key: d2541fdd-43c2-4d4d-9350-c1b787bfe87c",
                                    "Content-Type: application/json",
                                    "Cookie: connect.sid=s%3A-599MYE7MII1h8OXi_XdmlzFcZ3oGfdp.v%2BU8UBVXsphpnw5W6ymm6%2Ft0RyKKY%2FXgL8FkXrNvDdY"
                                ),
                                ));
                                $response = curl_exec($curl);
                                curl_close($curl);

                                if(isset(json_decode($response)->id)){
                                    DB::table('company')
                                    ->where('company_id', $company_id)
                                    ->update(['knackID' =>json_decode($response)->id]);
                                }
                                else{
                                    return redirect('googlemapdata')->with('error', 'Mandatory fields in Knack cannot be empty'); 
                                }
                            }
                            
                            /* Hours */
                            DB::table('company_hours')
                            ->insert([
                                'company_id' =>$company_id,
                                'monday'=>$monday,
                                'tuesday'=>$tuesday,
                                'wednesday'=> $wednesday,
                                'thursday'=>$thursday,
                                'friday'=>$friday,
                                'saturday'=>$saturday,
                                'sunday'=>$sunday
                            ]);
                        }
                    }
                return redirect('company')->with('status', 'Company Details Saved!');
                }
                catch(ErrorException $e){
                    return redirect('googlemapdata')->with('error', 'Data must be seperated with commas');            
                }
            }
            else{
                //Session::flash('message','File too large. File must be less than 2MB.');
                return redirect('googlemapdata')->with('error', 'File too large. File must be less than 2MB.');            
            }
        }
        else{
            return redirect('googlemapdata')->with('error', 'Invalid File Extension.');
        }
        }else{
            echo "Hello";
        }

    }
}
