<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class MediapartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mediapartner = DB::table('mediapartners')
                        ->where('status','=',0)
                        ->orderBy('media_id','desc')
                        ->get();
        return view('mediapartner',compact('mediapartner'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('addmediapartner');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('mediapartners')
            ->insert(['media_partner' => $request->mname,'rate'=>$request->mrate,'internal_rep'=>$request->internal_rep]);
        return redirect('mediapartner')->with('status', 'Media Partner Saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mediapartner_det = DB::table('mediapartners')
                            ->select('media_id','media_partner','rate','internal_rep')
                            ->where('media_id','=',$id)
                            ->first();

        return view('editmediapartner',compact('mediapartner_det'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('mediapartners')
            ->where('media_id', $id)
            ->update(['media_partner' => $request->mname,'rate'=>$request->mrate,'internal_rep'=>$request->internal_rep]);
         return redirect('mediapartner')->with('status', 'Media Partner Details Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->mediaid;
        DB::table('mediapartners')
            ->where('media_id', $id)
            ->update(['status' => 1]);
         return "1";
    }
}
