<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Storage;
use File;
use DB;
use Cloudder;

class CloudinaryWidgetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cloudinarywidget');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $current_date = date('Y-m-d H:i:s'); 
        $cl_name = $request->cl_name;
        $cl_email = $request->email; 
        $web_url = $request->company_url;
        $cl_phone = $request->phone;  
        $cl_cid = $request->cl_cid;  
        $widgetflag = $request->widgetflag;
        $unixtime = time();
        $fileprefix = 'none';


        $checkWidgetexist = DB::table('cloudwidget')
                    ->select('cloud_id')
                    ->where('cl_cid', '=', $cl_cid)
                    ->where('widget_type', '=', $widgetflag)
                    ->first();
                       
        if(!empty($checkWidgetexist))
        {   
            $cloud_id = $checkWidgetexist->cloud_id;
            $widgetDet = DB::table('cloudwidget')
                        ->select('widget_url')
                        ->where('cloud_id', '=', $cloud_id)
                        ->first();
            echo $widgetDet->widget_url;
        } else
        {
            $josua_logo = public_path('css/images/logo.png');

            $upload = Cloudder::upload($josua_logo,'Joshua/'.$cl_name."_Joshua/joshua_logo".uniqid());
                    

            if($upload)
            {
                $defualtlogo_picId = Cloudder::getPublicId();
                $tag = str_replace(' ', '_', $cl_name);
                Cloudder::addTag($tag, $defualtlogo_picId);
            } else  
            {
                $defualtlogo_picId = "Not uploaded"; 
            }


            $cl_Id = DB::table('cloudwidget')
            ->insertGetId([
                'cl_cid' => $cl_cid,
                'cl_name' => $cl_name,
                'cl_email' => $cl_email,
                'cl_phone' => $cl_phone,
                'web_url' => $web_url,
                'widget_type' => $widgetflag,
                'created_date' => $current_date
            ]);

            

            $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'; 
            $uniqstr = substr(str_shuffle($str_result),0, 5); 

            if($widgetflag==1)
            {
                $fileprefix = 'cloud_';
                $cloudImagetag=str_replace(' ', '_', $cl_name);

                $html  = '<!Doctype html>';
                $html .= '<html><head>';
                $html .= '<meta charset="utf-8">';
                $html .= '<meta name="viewport" content="width=device-width, initial-scale=1">';
                $html .= '<title>'.$cl_name.' - Upload Widget</title>';
                $html .= '<style type="text/css">.myBtn {  background-color: #1e2b57; /* Green */   border: none;   color: white;   padding: 15px 32px;  text-align: center;  text-decoration: none;  display: inline-block;  font-size: 16px; cursor:pointer;}</style>';
                $html .= '</head><body>';
                // $html .= '<div style="margin: auto;width: 60%;border: 3px solid #73AD21;padding: 10px;">Please wait, widget will load automatically for the first time.<p><button id="open-btn" class="myBtn">Open Widget</button></p></div>';
                $html .='<button id="upload_widget" class="cloudinary-button">Upload files</button>';
                $html .= '</body>';
                $html .= '<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>';
                $html .= '<script src="https://widget.cloudinary.com/v2.0/global/all.js" type="text/javascript"></script>';
                $html .= '<script src="https://cdnjs.cloudflare.com/ajax/libs/es6-promise/4.1.1/es6-promise.auto.min.js"></script>';
                $html .= '<script type="text/javascript">';
                $html .= '$(document).ready(function() {';
                $html .= ' var cname = "'.$cl_name.'";';
                // $html .= ' var publicId = "'.$file_name.'";';
                $html .= ' var myWidget = cloudinary.createUploadWidget({';
                $html .= 'cloud_name: "smart1snap",';
                $html .= 'tags:["'.$cloudImagetag.'"],';
                $html .= 'uploadPreset: "monteply",folder: "Joshua/'.$cl_name.'_Joshua"},';
                $html .= '(error, result) => {';
                $html .= 'if (!error && result && result.event === "success") {';
                $html .= 'console.log("Done! Here is the image info:", result.info);';
                $html .= '}});  ';
                $html .= 'document.getElementById("upload_widget").addEventListener("click", function(){';
                $html .= 'myWidget.open();';
                $html .= '}, false);})';
                $html .= '</script>';
                $html .= '</html>';
            } else if($widgetflag==2)
            {
                $fileprefix = 'media_';

                $html  = '<!Doctype html>';
                $html .= '<html><head>';
                $html .= '<meta charset="utf-8">';
                $html .= '<meta name="viewport" content="width=device-width, initial-scale=1">';
                $html .= '<title>'.$cl_name.' - Media Library Widget</title>';
                $html .= '<style type="text/css">.myBtn {  background-color: #1e2b57; border: none;   color: white;   padding: 12px 20px;  text-align: center;  text-decoration: none;  display: inline-block;  font-size: 16px; cursor:pointer;}</style>';
                $html .= '</head><body>';
                $html .= '<div style="margin: auto;padding: 10px;"><button id="open-btn" class="myBtn">Open Media Library</button></div>';
                $html .= '</body>';
                $html .= '<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>';
                $html .= '<script src="https://media-library.cloudinary.com/global/all.js"></script>';
                $html .= '<script src="https://cdnjs.cloudflare.com/ajax/libs/es6-promise/4.1.1/es6-promise.auto.min.js"></script>';
                $html .= '<script src="../js/cloudwidget.js"></script>';
                $html .= '<script type="text/javascript">';
                $html .= '$("#open-btn").click(function(){';
                $html .= ' var cname = "'.$cl_name.'";';
                $html .= ' var unixtime = widget().timestamp;';
                $html .= ' var auth = widget().signature;';
                $html .= ' window.ml = cloudinary.createMediaLibrary({';
                $html .= 'cloud_name: "smart1snap",';
                $html .= 'api_key: "872488644391297",';
                $html .= 'username: "toddswickard@smart1marketing.com",';
                $html .= 'timestamp: unixtime,';
                $html .= 'signature:auth,';
                $html .= 'z_index: 99999,';
                $html .= 'button_class: "myBtn btn btn-primary",';
                $html .= 'button_caption: "Open Media Library",';
                $html .= 'insert_transformation: true,';
                $html .= '}, {';
                $html .= 'insertHandler: function (data) {';
                $html .= 'data.assets.forEach(asset => { console.log("Inserted asset:",';
                $html .= 'JSON.stringify(asset, null, 2)) })';
                $html .= '} },';
                $html .= 'document.getElementById("open-btn")';
                $html .= '); window.ml.show({folder: {path: "Joshua/'.$cl_name.'_Joshua"}});';
                $html .= ' });';
                $html .= '</script>';
                $html .= '</html>';
            }
            else if($widgetflag==3)
            {
                $fileprefix = 'customer_';
                $cloudImagetag=str_replace(' ', '_', $cl_name);
                $html  = '<!Doctype html>';
                $html .= '<html><head>';
                $html .= '<meta charset="utf-8">';
                $html .= '<meta name="viewport" content="width=device-width, initial-scale=1">';
                $html .= '<title>'.$cl_name.' - Customer Gallery</title>';
                $html = '<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>';
                $html .= '<script src="https://media-library.cloudinary.com/global/all.js"></script>';
                $html .= '<script src="https://widget.cloudinary.com/v2.0/global/all.js" type="text/javascript"></script>';
                $html .= '</head><body>';
                $html.=' <script src="https://product-gallery.cloudinary.com/all.js" type="text/javascript"></script>';
                $html.=' <div id="product-gallery"></div>';
                $html .= '<script type="text/javascript">';
                $html .= 'const gallery = cloudinary.galleryWidget({';
                $html .= 'container: "#product-gallery",';
                $html .= 'cloudName: "smart1snap",';
                $html .= 'mediaAssets: [{';
                $html .= 'tag:"'.$cloudImagetag.'",';
                $html .= 'mediaType: "image"';
                $html .= '}, {';
                $html .= 'tag:"'.$cloudImagetag.'",';
                $html .= 'mediaType: "video"';
                $html .= '}],';
                $html .= 'carouselStyle: "thumbnails",';
                $html .= 'carouselLocation: "bottom",';
                $html .= 'navigation: "always",';
                $html .= 'aspectRatio: "16:9",';
                $html .= 'thumbnailProps: {';
                $html .= 'width: 120,';
                $html .= 'height: 96,';
                $html .= 'mediaSymbolPosition: "center",';
                $html .= 'mediaSymbolBgShape: "radius",';
                $html .= 'selectedBorderColor: "#d0021b",';
                $html .= 'selectedBorderPosition: "all",';
                $html .= 'mediaSymbolBgOpacity: 0.8,';
                $html .= 'navigationShape: "round"';
                $html .='},';
                $html .= 'zoom: true,';
                $html .= 'zoomProps: {';
                $html .= 'level: 2.5,';
                $html .= 'type: "popup",';
                $html .= 'trigger: "click"';
                $html .= '},';
                $html .= 'themeProps: {';
                $html .= 'primary: "#0693e3",';
                $html .= 'onPrimary: "#ffffff",';
                $html .= 'active: "#0693e3"';
                $html .= '},';
                $html .= 'transition: "fade",';
                $html .= 'viewportBreakpoints: [{';
                $html .= 'breakpoint: 576,';
                $html .= 'navigation: "always",';
                $html .= 'carouselStyle: "indicators",';
                $html .= 'carouselLocation: "bottom",';
                $html .= 'onPrimary: "#d0021b",';
                $html .= 'navigationProps: {';
                $html .= 'iconColor: "#d0021b"';
                $html .= '},';
                $html .= 'indicatorProps: {';
                $html .= 'selectedColor: "#d0021b",';
                $html .= 'color: "#E7808D"';
                $html .= '}';
                $html .= '}]';
                $html .= '});';
                $html .= 'gallery.render();';
                $html.=' setInterval(function () { gallery.render()}, 10000);';
                $html.='</script>';
                $html .= '</body>';
                $html.='</html>';
            }
            else if($widgetflag==4)
            {
                $fileprefix = 'ma_';
                $cloudImagetag=str_replace(' ', '_', $cl_name);

                $html  = '<!Doctype html>';
                $html .= '<html><head>';
                $html .= '<meta charset="utf-8">';
                $html .= '<meta name="viewport" content="width=device-width, initial-scale=1">';
                $html .= '<title>'.$cl_name.' - Upload Widget</title>';
                $html .= '<style type="text/css">.myBtn {  background-color: #1e2b57; /* Green */   border: none;   color: white;   padding: 15px 32px;  text-align: center;  text-decoration: none;  display: inline-block;  font-size: 16px; cursor:pointer;}</style>';
                $html .= '</head><body>';
                // $html .= '<div style="margin: auto;width: 60%;border: 3px solid #73AD21;padding: 10px;">Please wait, widget will load automatically for the first time.<p><button id="open-btn" class="myBtn">Open Widget</button></p></div>';
                $html .='<button id="upload_widget" class="cloudinary-button">Upload files</button>';
                $html .= '</body>';
                $html .= '<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>';
                $html .= '<script src="https://widget.cloudinary.com/v2.0/global/all.js" type="text/javascript"></script>';
                $html .= '<script src="https://cdnjs.cloudflare.com/ajax/libs/es6-promise/4.1.1/es6-promise.auto.min.js"></script>';
                $html .= '<script type="text/javascript">';
                $html.='const MAuploadTag=JSON.stringify("ma"+"'.$cloudImagetag.'");';
                $html .= '$(document).ready(function() {';
                $html .= ' var cname = "'.$cl_name.'";';
                // $html .= ' var publicId = "'.$file_name.'";';
                $html .= ' var myWidget = cloudinary.createUploadWidget({';
                $html .= 'cloud_name: "smart1snap",';
                $html.='tags: [MAuploadTag],';
                $html .= 'uploadPreset: "monteply",folder: "Joshua/'.$cl_name.'_Joshua/MarketingAssets"},';
                $html .= '(error, result) => {';
                $html .= 'if (!error && result && result.event === "success") {';
                $html .= 'console.log("Done! Here is the image info:", result.info);';
                $html .= '}});  ';
                $html .= 'document.getElementById("upload_widget").addEventListener("click", function(){';
                $html .= 'myWidget.open();';
                $html .= '}, false);})';
                $html .= '</script>';
                $html .= '</html>';
            }
            else if($widgetflag==5)
            {
                $fileprefix = 'mag_';
                $cloudImagetag=str_replace(' ', '_', $cl_name);
                $html  = '<!Doctype html>';
                $html .= '<html><head>';
                $html .= '<meta charset="utf-8">';
                $html .= '<meta name="viewport" content="width=device-width, initial-scale=1">';
                $html .= '<title>'.$cl_name.' - Customer Gallery</title>';
                $html = '<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>';
                $html .= '<script src="https://media-library.cloudinary.com/global/all.js"></script>';
                $html .= '<script src="https://widget.cloudinary.com/v2.0/global/all.js" type="text/javascript"></script>';
                $html .= '</head><body>';
                $html.=' <script src="https://product-gallery.cloudinary.com/all.js" type="text/javascript"></script>';
                $html.=' <div id="product-gallery"></div>';
                $html .= '<script type="text/javascript">';
                $html.='const MAuploadTag=JSON.stringify("ma"+"'.$cloudImagetag.'");';
                $html .= 'const gallery = cloudinary.galleryWidget({';
                $html .= 'container: "#product-gallery",';
                $html .= 'cloudName: "smart1snap",';
                $html .= 'mediaAssets: [{';
                $html .= 'tag:MAuploadTag,';
                $html .= 'mediaType: "image"';
                $html .= '}, {';
                $html .= 'tag:MAuploadTag,';
                $html .= 'mediaType: "video"';
                $html .= '}],';
                $html .= 'carouselStyle: "thumbnails",';
                $html .= 'carouselLocation: "bottom",';
                $html .= 'navigation: "always",';
                $html .= 'aspectRatio: "16:9",';
                $html.='displayProps: {';
                $html.='mode: "expanded",';
                $html.='spacing: 50,';
                $html.='columns: 4,';
                $html.='topOffset: 70';
                $html.='},';
                $html .= 'thumbnailProps: {';
                $html .= 'width: 120,';
                $html .= 'height: 96,';
                $html .= 'mediaSymbolPosition: "center",';
                $html .= 'mediaSymbolBgShape: "radius",';
                $html .= 'selectedBorderColor: "#d0021b",';
                $html .= 'selectedBorderPosition: "all",';
                $html .= 'mediaSymbolBgOpacity: 0.8,';
                $html .= 'navigationShape: "round"';
                $html .='},';
                $html .= 'zoom: true,';
                $html .= 'zoomProps: {';
                $html .= 'level: 2.5,';
                $html .= 'type: "popup",';
                $html .= 'trigger: "click"';
                $html .= '},';
                $html .= 'themeProps: {';
                $html .= 'primary: "#0693e3",';
                $html .= 'onPrimary: "#ffffff",';
                $html .= 'active: "#0693e3"';
                $html .= '},';
                $html .= 'transition: "fade",';
                $html .= 'viewportBreakpoints: [{';
                $html .= 'breakpoint: 576,';
                $html .= 'navigation: "always",';
                $html .= 'carouselStyle: "indicators",';
                $html .= 'carouselLocation: "bottom",';
                $html .= 'onPrimary: "#d0021b",';
                $html .= 'navigationProps: {';
                $html .= 'iconColor: "#d0021b"';
                $html .= '},';
                $html .= 'indicatorProps: {';
                $html .= 'selectedColor: "#d0021b",';
                $html .= 'color: "#E7808D"';
                $html .= '}';
                $html .= '}]';
                $html .= '});';
                $html .= 'gallery.render();';
                $html.=' setInterval(function () { gallery.render()}, 10000);';
                $html.='</script>';
                $html .= '</body>';
                $html.='</html>';
            }
            else if($widgetflag==6)
            {
                $fileprefix = 'cv_';
                $cloudImagetag=str_replace(' ', '_', $cl_name);
                
                $html  = '<!Doctype html>';
                $html .= '<html><head>';
                $html .= '<meta charset="utf-8">';
                $html .= '<meta name="viewport" content="width=device-width, initial-scale=1">';
                $html .= '<title>'.$cl_name.' - Upload Widget</title>';
                $html .= '<style type="text/css">.myBtn {  background-color: #1e2b57; /* Green */   border: none;   color: white;   padding: 15px 32px;  text-align: center;  text-decoration: none;  display: inline-block;  font-size: 16px; cursor:pointer;}</style>';
                $html .= '</head><body>';
                // $html .= '<div style="margin: auto;width: 60%;border: 3px solid #73AD21;padding: 10px;">Please wait, widget will load automatically for the first time.<p><button id="open-btn" class="myBtn">Open Widget</button></p></div>';
                $html .='<button id="upload_widget" class="cloudinary-button">Upload files</button>';
                $html .= '</body>';
                $html .= '<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>';
                $html .= '<script src="https://widget.cloudinary.com/v2.0/global/all.js" type="text/javascript"></script>';
                $html .= '<script src="https://cdnjs.cloudflare.com/ajax/libs/es6-promise/4.1.1/es6-promise.auto.min.js"></script>';
                $html .= '<script type="text/javascript">';
                $html.='const MAuploadTag=JSON.stringify("cv"+"'.$cloudImagetag.'");';
                $html .= '$(document).ready(function() {';
                $html .= ' var cname = "'.$cl_name.'";';
                // $html .= ' var publicId = "'.$file_name.'";';
                $html .= ' var myWidget = cloudinary.createUploadWidget({';
                $html .= 'cloud_name: "smart1snap",';
                $html.='tags: [MAuploadTag],';
                $html .= 'uploadPreset: "monteply",folder: "Joshua/'.$cl_name.'_Joshua/MarketingAssets/CustomerView/"},';
                $html .= '(error, result) => {';
                $html .= 'if (!error && result && result.event === "success") {';
                $html .= 'console.log("Done! Here is the image info:", result.info);';
                $html .= '}});  ';
                $html .= 'document.getElementById("upload_widget").addEventListener("click", function(){';
                $html .= 'myWidget.open();';
                $html .= '}, false);})';
                $html .= '</script>';
                $html .= '</html>';
            }
            else if($widgetflag==7)
            {
                $fileprefix = 'cvg_';
                $cloudImagetag=str_replace(' ', '_', $cl_name);
                $html  = '<!Doctype html>';
                $html .= '<html><head>';
                $html .= '<meta charset="utf-8">';
                $html .= '<meta name="viewport" content="width=device-width, initial-scale=1">';
                $html .= '<title>'.$cl_name.' - Customer Gallery</title>';
                $html = '<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>';
                $html .= '<script src="https://media-library.cloudinary.com/global/all.js"></script>';
                $html .= '<script src="https://widget.cloudinary.com/v2.0/global/all.js" type="text/javascript"></script>';
                $html .= '</head><body>';
                $html.=' <script src="https://product-gallery.cloudinary.com/all.js" type="text/javascript"></script>';
                $html.=' <div id="product-gallery"></div>';
                $html .= '<script type="text/javascript">';
                $html.='const MAuploadTag=JSON.stringify("cv"+"'.$cloudImagetag.'");';
                $html .= 'const gallery = cloudinary.galleryWidget({';
                $html .= 'container: "#product-gallery",';
                $html .= 'cloudName: "smart1snap",';
                $html .= 'mediaAssets: [{';
                $html .= 'tag:MAuploadTag,';
                $html .= 'mediaType: "image"';
                $html .= '}, {';
                $html .= 'tag:MAuploadTag,';
                $html .= 'mediaType: "video"';
                $html .= '}],';
                $html .= 'carouselStyle: "thumbnails",';
                $html .= 'carouselLocation: "bottom",';
                $html .= 'navigation: "always",';
                $html .= 'aspectRatio: "16:9",';
                $html.='displayProps: {';
                $html.='mode: "expanded",';
                $html.='spacing: 50,';
                $html.='columns: 4,';
                $html.='topOffset: 70';
                $html.='},';
                $html .= 'thumbnailProps: {';
                $html .= 'width: 120,';
                $html .= 'height: 96,';
                $html .= 'mediaSymbolPosition: "center",';
                $html .= 'mediaSymbolBgShape: "radius",';
                $html .= 'selectedBorderColor: "#d0021b",';
                $html .= 'selectedBorderPosition: "all",';
                $html .= 'mediaSymbolBgOpacity: 0.8,';
                $html .= 'navigationShape: "round"';
                $html .='},';
                $html .= 'zoom: true,';
                $html .= 'zoomProps: {';
                $html .= 'level: 2.5,';
                $html .= 'type: "popup",';
                $html .= 'trigger: "click"';
                $html .= '},';
                $html .= 'themeProps: {';
                $html .= 'primary: "#0693e3",';
                $html .= 'onPrimary: "#ffffff",';
                $html .= 'active: "#0693e3"';
                $html .= '},';
                $html .= 'transition: "fade",';
                $html .= 'viewportBreakpoints: [{';
                $html .= 'breakpoint: 576,';
                $html .= 'navigation: "always",';
                $html .= 'carouselStyle: "indicators",';
                $html .= 'carouselLocation: "bottom",';
                $html .= 'onPrimary: "#d0021b",';
                $html .= 'navigationProps: {';
                $html .= 'iconColor: "#d0021b"';
                $html .= '},';
                $html .= 'indicatorProps: {';
                $html .= 'selectedColor: "#d0021b",';
                $html .= 'color: "#E7808D"';
                $html .= '}';
                $html .= '}]';
                $html .= '});';
                $html .= 'gallery.render();';
                $html.=' setInterval(function () { gallery.render()}, 10000);';
                $html.='</script>';
                $html .= '</body>';
                $html.='</html>';
            }
            //error_log($html);
            
            File::put(public_path('cloudwidget/'.$fileprefix.str_replace(' ', '',$cl_name).'_'.time().'.html'),  $html);

            $widget_url = 'cloudwidget/'.$fileprefix.str_replace(' ', '',$cl_name).'_'.time().'.html';

            DB::table('cloudwidget')
                ->where('cloud_id', $cl_Id)
                ->update(['widget_url' => $widget_url]);

            echo 'cloudwidget/'.$fileprefix.str_replace(' ', '',$cl_name).'_'.time().'.html';
        }

        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function widgetlist()
    {
        $uploadw = DB::table('cloudwidget')
                    ->orderBy('created_date','desc')
                    ->get();
        return view('uploadwidgetlist',compact('uploadw'));
    }

    public function fetchUrl(Request $request)
    {
        $cloud_id = $request->cloud_id;
        $widgetdet = DB::table('cloudwidget')
                    ->select('widget_url')
                    ->where('cloud_id', '=', $cloud_id)
                    ->first();

        echo $widgetdet->widget_url;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
