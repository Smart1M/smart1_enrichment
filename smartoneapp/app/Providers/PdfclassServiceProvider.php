<?php

namespace App\Providers;
use App\Pdfclass;

use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class PdfclassServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('pdfclass',function(){

        return new Pdfclass();

        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
