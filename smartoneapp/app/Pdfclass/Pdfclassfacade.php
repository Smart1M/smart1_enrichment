<?php
namespace App\Pdfclass;
use Illuminate\Support\Facades\Facade;

class PdfclassFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'pdfclass';
    }
}

?>