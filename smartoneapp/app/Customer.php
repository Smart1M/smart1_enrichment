<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'fname','surname','lastname','address','dob','mob','email','company',
    ];
}
