<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColToCompanyRportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_reports', function (Blueprint $table) {
            $table->integer('br_id')->after('user')->nullable();
            $table->integer('st_id')->after('user')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_reports', function (Blueprint $table) {
            $table->dropColumn('br_id');
            $table->dropColumn('st_id');
        });
    }
}
