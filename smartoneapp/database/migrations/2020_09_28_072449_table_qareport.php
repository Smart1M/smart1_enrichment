<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableQareport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qareport', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('company_id');
            $table->string('url');
            $table->integer('broken_url_count');
            $table->longText('broken_urls_list');
            $table->integer('broken_images_count');
            $table->longText('broken_images_list');
            $table->longText('title_less');
            $table->integer('title_less_count');
            $table->longText('lorem_pages');
            $table->integer('lorem_count');
            $table->dateTime('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qareport');
    }
}
