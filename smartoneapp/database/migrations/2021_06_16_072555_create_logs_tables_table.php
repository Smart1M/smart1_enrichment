<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs_tables', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('uniqueId')->nullable();
            $table->integer('userId')->nullable();
            $table->integer('companyId')->nullable();
            $table->string('apiName')->nullable();
            $table->string('api')->nullable();
            $table->string('content')->nullable();
            $table->string('level')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs_tables');
    }
}
