<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', 'HomeController@index');

// Route::get('/login', function () {
//     return view('login');
// });
Route::get('/customergallery','CompanyGalleryController@customergallery')->name('company.customergallery');
Route::get('/marketingassets','CompanyGalleryController@seeMarketingAssets')->name('company.marketingassets');
Route::get('/customerview','CompanyGalleryController@customerview')->name('company.customerview');
Auth::routes();

Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
Route::post('/password/reset', 'Auth\ResetPasswordController@reset')->name('password.email');
Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');




Route::group(['middleware' => ['auth']], function() {
    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/roles', 'RoleController@index');
    Route::get('/roles/add', 'RoleController@create');
    Route::post('/roles/save', 'RoleController@store');
    Route::get('/roles/edit/{id}', 'RoleController@edit');
    Route::post('/roles/update/{id}', 'RoleController@update');
    Route::post('/roles/delete', 'RoleController@destroy');


    Route::get('/users', 'UserController@index');
    Route::get('/users/add', 'UserController@create');
    Route::get('/users/edit/{id}', 'UserController@edit');
    Route::post('/users/update/{id}', 'UserController@update');
    Route::post('/users/delete', 'UserController@destroy');
    Route::get('/changepassword', 'UserController@changepassword');
    Route::post('/users/updatepassword', 'UserController@updatepassword');
    Route::post('/users/checkemail', 'UserController@checkemail');
    Route::post('/users/editcheckemail', 'UserController@editcheckemail');

    Route::get('/permissions', 'PermissionController@index');
    Route::get('/permissions/add', 'PermissionController@create');
    Route::post('/permissions/save', 'PermissionController@store');
    Route::get('/permissions/edit/{id}', 'PermissionController@edit');
    Route::post('/permissions/update/{id}', 'PermissionController@update');
    Route::post('/permissions/delete', 'PermissionController@destroy');
    Route::get('permissions/assignpermission/{id}', 'PermissionController@assignPermission');
    Route::post('/permissions/savepermissions', 'PermissionController@savePermissions');

    Route::get('/company', 'CompanyController@index');
    Route::get('/company/add', 'CompanyController@create');
    Route::post('/company/save', 'CompanyController@store');
    Route::get('/company/edit/{id}', 'CompanyController@edit');
    Route::post('/company/update/{id}', 'CompanyController@update');
    Route::post('/company/delete', 'CompanyController@destroy');

    Route::get('/company/playgame/{id}', 'CompanyController@playgame');
    Route::get('/playgame/logs/{id}', 'CompanyController@log');
    Route::get('/api/logs', 'CompanyController@log');
    Route::post('/company/fetchapi', 'CompanyController@fetchapires');
    Route::post('company/fetchCloudinarymedia', 'CompanyController@fetchCloudinarymedia');
    Route::post('/company/fetchlink', 'CompanyController@show');
    Route::get('/company/pdf/{compid}/{basicid}', 'CompanyController@scanpdf');
    Route::get('/company/excel/{compid}/{basicid}', 'CompanyController@excel');
    Route::post('/company/sendmail', 'CompanyController@sendmail');
    Route::post('/company/checkemail', 'CompanyController@checkemail');
    Route::post('/company/editcheckemail', 'CompanyController@editcheckemail');


    Route::get('/menu', 'MenuController@index');
    Route::get('/menu/add', 'MenuController@create');
    Route::post('/menu/save', 'MenuController@store');
    Route::get('/menu/edit/{id}', 'MenuController@edit');
    Route::post('/menu/update', 'MenuController@update');
    Route::post('/menu/delete', 'MenuController@destroy');
    Route::post('/menu/submenu', 'MenuController@fetchsubmenu');

    // Route::get('/customers', 'CustomerController@index');
    // Route::get('/customers/add', 'CustomerController@create');
    // Route::post('/customers/save', 'CompanyController@store');
    // Route::get('/customers/edit/{id}', 'CompanyController@edit');
    // Route::post('/customers/update/{id}', 'CompanyController@update');
    // Route::post('/customers/delete', 'CompanyController@destroy');

    Route::get('company/report', 'CompanyReportController@index');
    Route::get('company/reports/{id}', 'CompanyReportController@reports');
    Route::post('company/report/fetch', 'CompanyReportController@store');
    Route::get('company/report/export/{id}', 'CompanyReportController@exporttopdf');
    Route::get('company/report/exportqa/{id}', 'CompanyReportController@exportqa');
    Route::get('company/qascan', 'CompanyReportController@qascan');
    Route::post('company/storeqa', 'CompanyReportController@storeqa');

    Route::get('/silktide', 'SilktideController@index');
    Route::get('/silktide/reports/{id}', 'SilktideController@reports');
    Route::post('/silktide/fetch', 'SilktideController@store');
    Route::get('/silktide/export/{id}', 'SilktideController@exporttopdf');

    Route::get('/brandfetch', 'BrandfetchController@index');
    Route::get('/brandfetch/reports/{id}', 'BrandfetchController@reports');
    Route::post('/brandfetch/fetch', 'BrandfetchController@store');
    Route::get('/brandfetch/export/{id}','BrandfetchController@exporttopdf');


    Route::get('/cloudinarywidget','CloudinaryWidgetController@index');
    Route::get('/cloud/uploadwidget','CloudinaryWidgetController@widgetlist');
    Route::post('/cloudinarywidget/save','CloudinaryWidgetController@store');
    Route::post('/cloudinarywidget/fetch','CloudinaryWidgetController@fetchUrl');


    Route::get('/mediapartner', 'MediapartnerController@index');
    Route::get('/mediapartner/add', 'MediapartnerController@create');
    Route::post('/mediapartner/save', 'MediapartnerController@store');
    Route::get('/mediapartner/edit/{id}', 'MediapartnerController@edit');
    Route::post('/mediapartner/update/{id}', 'MediapartnerController@update');
    Route::post('/mediapartner/delete', 'MediapartnerController@destroy');


    Route::get('/googlemapdata', 'GooglemapController@index');
    Route::post('/googledata/importfile', 'GooglemapController@importExcel');


    Route::get('/testpage', 'TestController@index');
});
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

// Route::get('/generator', function() {
//   return File::get(public_path() . '/generator/index.php');
// });


    // Moving here will ensure that sessions, csrf, etc. is included in all these routes

//company auth routes
Route::get('/{cname}', 'Auth\CompanyLoginController@showLoginForm')->name('company.showlogin');
Route::post('/companylogin', 'Auth\CompanyLoginController@login')->name('company.login');
Route::post('/clientlogout', 'Auth\CompanyLoginController@logout')->name('company.logout');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
