<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/key', function (Request $request) {
    return env('API_KEY');
});
Route::get('companies', 'ApiController@index');
Route::post('companies/store', 'ApiController@storeCompany');
Route::post('report', 'ApiController@companyReport');
Route::post('brandfetch', 'ApiController@brandFetchReport');
Route::post('silktide', 'ApiController@silktideReport');
