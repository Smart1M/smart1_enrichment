@extends('layouts.mediapartnerlist')

@section('content')

@if ($errors->any())
  <div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
    </ul>
  </div><br />
@endif
<div class="dash-main-body">
<div class="dash-main-form">
  <form action="{{ url('mediapartner/save') }}" method="post" name="mediapartneradd">
    @csrf
      <div class="col-lg-12 row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Name <span style="color: red;">*</span>
                  </label> 
              <input type="text" name="mname" id="mname"  autocomplete="mname">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Rate <span style="color: red;">*</span>
                  </label> 
              <input type="text" name="mrate" id="mrate" autocomplete="mrate">
            </div>
      </div>
       <div class="col-lg-12 row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Internal Rep <span style="color: red;">*</span>
                  </label> 
                <input type="text" name="internal_rep" id="internal_rep"  autocomplete="internal_rep">
            </div>
      </div>
      
      
  <div class="col-lg-12 row">    
      <div class="col-lg-12">
            <input type="submit" class="dash-main-form-btn" value="Submit">
            <a href="{{ url('/mediapartner') }}"><input type="button" class="dash-main-form-btn" value="Cancel" ></a>
      </div>
  </div>
</form>
</div>
</div>

@endsection

<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script type="text/javascript">
// $(document).ready(function() {
    
//     var regExp = /[a-z]/i;
//     $('#phone').on('keydown keyup', function(e) {
//       var value = String.fromCharCode(e.which) || e.key;

      
//       if (regExp.test(value)) {
//         e.preventDefault();
//         return false;
//       }
//     });
// });

function Validate() {
    var isValid = false;
    var regex = /^[0-9-+()' ']*$/;
    isValid = regex.test(document.getElementById("phone").value);
    document.getElementById("spnError").style.display = !isValid ? "block" : "none";
    
    if(isValid==false)
    {
      $("#phone").val("");
    }
    return isValid;
}

// function ValidateEmail() 
// {
//   var mail = document.getElementById('email').value;
//  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
//   {
//     return true;
//   }
//     alert("You have entered an invalid email address!")
//     return false;
// }

function checkemailexist()
{
  var mail = document.getElementById('email').value;
  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
  {

    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
 
       $.ajax({
          url: "{{ url('/company/checkemail') }}",
          method: 'post',
          data: {
             email: mail
          },
          success: function(result){ 
            
             if(result=='1'){
                
                $("#email_err").text('Email Id already exist');
                $("#email").val("");
                
              
             } else {
                $("#email_err").text('');
             }
          }
        });

  } else{
    //alert("You have entered an invalid email address!")
    $("#email_err").text('You have entered an invalid email address!');
    $("#email").val("");
  }
}

function validUrl()
{
  var validUrl = false;
  var web_url = document.getElementById('company_url').value;
  var pattern = /^(http|https)?:\/\/[a-zA-Z0-9-\.]+\.[a-z]{2,4}/;
 
  
  validUrl = pattern.test(web_url)
  if(validUrl==false){
    $("#company_url").val("");
    document.getElementById("urlError").style.display = !validUrl ? "block" : "none";
  } else 
  {
    document.getElementById("urlError").style.display = !validUrl ? "block" : "none";
  }
}
</script>
