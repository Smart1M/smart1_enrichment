@extends('layouts.permission')

@section('content')


<p>
@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
        
        <ul id="horizontal-list">
            <li>
                <a href="{{ url('permissions/add') }}" >
                <button type="button" class="btn btn-primary" name="create" class="">Permissions <i class="fa fa-plus"></i></button>
                </a> 
            </li>
            <li>
                <a href="{{ url('roles') }}">
                <button type="button" name="create" class="btn btn-primary">Roles <i class="fa fa-file"></i></button>
                </a> 
            </li>
        </ul>  
        

        <table id="example" class="table table-striped table-bordered" style="width:100%;">
        <thead>
        <tr>
            <th>Sl.No</th>
            <th>Permission</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>

        <?php $i=1; foreach ($permissions as $key => $value) { ?>
        <tr>
            <td>{{ $i }}</td>
            <td>{{ $value->name }}</td>
            <td><span id="status_{{ $value->id }}">@if($value->pstatus==0) Active @else Inactive @endif </span></td>
            <td><a  href="{{ url('permissions/edit/'.$value->id) }}" title="Edit" style="text-decoration: none; color: inherit;"><i class="far fa-edit"></i></a>&nbsp; <a id="{{ $value->id }}"" href="javascript:void(0)" title="Delete" style="text-decoration: none; color: inherit;" class="deletepermissions"><i class="far fa-trash-alt"></i></a></td>
        </tr>

        <?php $i++; } ?>

        </tbody>
        </table>
            
        </div>
    </div>
</div>
</p>

@endsection

<script type="text/javascript">

</script>
