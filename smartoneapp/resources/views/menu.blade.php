@extends('layouts.menulist')

@section('content')


<p>
@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
        
        <ul id="horizontal-list">
            
            <li>
                <a href="{{ url('menu/add') }}" >
                <button type="button" class="btn btn-primary" name="create" class="">Menu <i class="fa fa-plus"></i></button>
                </a> 
            </li>
            
        </ul> 

        <table id="example" class="table table-bordered" style="width:100%;">
        <thead>
        <tr>
            <th></th>
            <th>Sl.No</th>
            <th>Name</th>
            <th>Menu Type</th>
            <th>Slug</th>
            <th>Permission</th>
            <th>Order</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>

        <?php $i=1; foreach ($menu as $key => $value) { ?>
        <tr id="{{ $value->menu_id }}" >
            <td <?php if($value->menu_type==1 && $value->is_sub==1) { ?>class="details-control" <?php } ?>></td>
            <td>{{ $i }}</td>
            <td>{{ $value->menu_name }}</td>
            <td>@if($value->menu_type==1) Parent @else Sub @endif</td>
            <td>{{ $value->menu_slug }}</td>
            <td>{{ $value->name }}</td>
            <td>{{ $value->menu_order }}</td>
            <td><span id="menustatus_{{ $value->menu_id }}">@if($value->active_status==0) Active @else Inactive @endif </span></td>
            <td><a  href="{{ url('menu/edit/'.$value->menu_id) }}" title="Edit" style="text-decoration: none; color: inherit;"><i class="far fa-edit"></i></a>&nbsp; <a id="{{ $value->menu_id }}" href="javascript:void(0)" title="Delete" style="text-decoration: none; color: inherit;" class="deletemenu"><i class="far fa-trash-alt"></i></a></td>
        </tr>

        <?php $i++; } ?>

        </tbody>
        </table>
            
        </div>
    </div>
</div>
</p>



@endsection

