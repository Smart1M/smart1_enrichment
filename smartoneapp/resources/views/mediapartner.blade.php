@extends('layouts.mediapartnerlist')

@section('content')


<p>
@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
        
        <ul id="horizontal-list">
            @can('add media')
            <li>
                <a href="{{ url('mediapartner/add') }}" >
                <button type="button" class="btn btn-primary" name="create" class="">Media Partner <i class="fa fa-plus"></i></button>
                </a> 
            </li>
            @endcan
            <!-- <li>
                <a href="{{ url('customers') }}" >
                <button type="button" class="btn btn-primary" name="create" class="">Customer <i class="fa fa-user-plus"></i></button>
                </a> 
            </li> -->
        </ul> 

        <table id="example" class="table table-striped table-bordered" style="width:100%;">
        <thead>
        <tr>
            <th>Sl.No</th>
            <th>Media Partner</th>
            <th>Rate</th>
            <th>Internal Rep</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>

        <?php $i=1; foreach ($mediapartner as $key => $value) { ?>
        <tr>
            <td>{{ $i }}</td>
            <td>{{ $value->media_partner }}</td>
            <td>{{ $value->rate }}</td>
            <td>{{ $value->internal_rep }}</td>
            <td>@can('edit media')<a  href="{{ url('mediapartner/edit/'.$value->media_id) }}" title="Edit" style="text-decoration: none; color: inherit;"><i class="far fa-edit"></i></a>@endcan @can('delete media')&nbsp; <a id="{{ $value->media_id }}" href="javascript:void(0)" title="Delete" style="text-decoration: none; color: inherit;" class="deletemedia"><i class="far fa-trash-alt"></i></a>@endcan</td>
        </tr>

        <?php $i++; } ?>

        </tbody>
        </table>
            
        </div>
    </div>
</div>
</p>



@endsection

