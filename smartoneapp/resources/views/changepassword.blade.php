@extends('layouts.userlist')

@section('content')

@if (session('password_success'))
    <div class="alert alert-success">
        {{ session('password_success') }}
        <script type="text/javascript">
          document.getElementById('logout-form').submit();
        </script>
    </div>
@endif

@if (session('error_msg'))
    <div class="alert alert-danger">
        {{ session('error_msg') }}
    </div>
@endif
<div class="dash-main-body">
<div class="dash-main-form">

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
      @csrf
</form>
  <form action="{{ url('/users/updatepassword') }}" method="post" onsubmit="return validatePassword()">
    @csrf
      
            
      <div class="col-lg-12 row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Current Password <span style="color: red;">*</span>
                  </label> 
              <input type="password" name="currentpassword" id="currentpassword" style="width: 100%;" autocomplete="new-password" value="">
            </div>

            <!-- <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        New Password <span style="color: red;">*</span>
                  </label> 
              <input type="password" name="newpassword" id="newpassword" style="width: 100%;" autocomplete="new-password" value="">
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Confirm pasword <span style="color: red;">*</span>
                  </label> 
              <input id="password-confirm" type="password"  name="password_confirmation" required autocomplete="new-password" style="width: 100%" value="">
            </div> -->
      </div>

      <div class="col-lg-12 row">

            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        New Password <span style="color: red;">*</span>
                  </label> 
              <input type="password" name="newpassword" id="newpassword" style="width: 100%;" autocomplete="new-password" value="">
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Confirm password <span style="color: red;">*</span>
                  </label> 
              <input id="password_confirmation" type="password"  name="password_confirmation" required autocomplete="new-password" style="width: 100%" value="">
            </div>
      </div>
  <div class="col-lg-12 row">    
      <div class="col-lg-12">
            <input type="submit" value="Submit" >
      </div>
  </div>
</form>
</div>
</div>

@endsection

<script type="text/javascript">

  function validatePassword()
  {
    var currentpassword = document.getElementById('currentpassword').value;
    var newpassword = document.getElementById('newpassword').value;
    var confirmpassword = document.getElementById('password_confirmation').value;
    
    if (newpassword != confirmpassword)
    {
      alert("New password and confimation mismatch !!");
      return false;
    } else if(currentpassword == newpassword)
    {
      alert("New password is same as Old password. Please update !!");
      return false;
    }
    {
      return true;
    }
    
  }

</script>
