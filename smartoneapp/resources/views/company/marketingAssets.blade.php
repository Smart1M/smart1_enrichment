<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">


    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="https://kit.fontawesome.com/ef363a0a6e.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
        #contactBtn,
        #upload_widget,
        .btn-group {
            border-radius: 0px;
            border: 2px solid #928C8B;
            box-shadow: 2px 2px 2px #C4BEBE;
        }

        .btn-group .dropdown-toggle::after {
            display: none;
        }

        .assetWrapper {
            box-shadow: 2px 2px 2px #C4BEBE;
            position: relative;
            text-align: center;
            color: white;
        }

        .assetWrapper:hover {
            box-shadow: 5px 0px 5px #C4BEBE;
            border-bottom: 5px solid white;
        }

        #imgName {
            position: relative;
            text-align: center;
            color: #12ADF7;
            font-weight: bold;
            box-shadow: 5px 5px 5px #C4BEBE;
            top: 5px;
        }
    </style>
</head>

<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <!-- <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a> -->
                <div class="logo" style="margin-left: 170px;text-align:center">
                    <a class="navbar-brand" href="#"><img src="{{ asset('css/images/logo.png') }}"></a>
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                        <!-- <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li> -->
                        @if (Route::has('register'))
                        <!--  <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li> -->
                        @endif
                        @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdowns" class="nav-link dropdown-toggle text-uppercase" href="#"
                                role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ $company->cname }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                    <span>{{ __('Logout') }} <i class="fa fa-sign-out"></i></span>
                                </a>
                                <form method="POST" id="logout-form" action="{{route('company.logout')}}"
                                    style="display:none">@csrf
                                    <input type="hidden" name="cname" value="{{$company->cname}}" />
                                    <input type="hidden" name="zip" value="{{$company->zip}}" />
                                </form>
                            </div>
                        </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container">
        <div class="row">
                <div class="col">
                    <div class="d-flex justify-content-center">
                        <h4 class="pl-4 justify-content-center pt-4 text-uppercase">{{$company->cname}}'s Marketing
                            Assets</h4>
                    </div>
                </div>
            </div>
            <div class="row bg-light justify-content-center" style="border:3px solid #E3E3E3">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('company.customergallery')}}">Home</a>
                            </li>
                            <p class="pt-2">|</P>
                            <li class="nav-item">
                                <a class="nav-link" href="#" id="upload_widget" style="border:none;box-shadow:none">Upload New Marketing Asset</a>
                            </li>
                            <p class="pt-2">|</P>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('company.customerview')}}"> See My Marketing Assets </a>
                            </li>
                            <p class="pt-2">|</P>
                            <li class="nav-item">
                                <a class="nav-link" href="https://smart1.solutions/customer_media_contact"> Contact Web Support</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <div class="row" style="border:3px solid #E3E3E3;border-top:none;">
                <div class="col">
                    <script src="https://product-gallery.cloudinary.com/all.js" type="text/javascript"></script>
                    <div id="product-gallery" style="max-width:100%;margin:auto;padding-top:50px;" zoomed="false">
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://media-library.cloudinary.com/global/all.js"></script>
<script src="https://widget.cloudinary.com/v2.0/global/all.js" type="text/javascript">
</script>
<script type="text/javascript">
    //product gallery
    const MAuploadTag=JSON.stringify('ma'+'{{$cloudImagetag}}');
    const gallery = cloudinary.galleryWidget({
        container: "#product-gallery",
        cloudName: "smart1snap",
        mediaAssets: [{
            tag:MAuploadTag,
            mediaType: "image",
        }],
        carouselStyle: "thumbnails",
        carouselLocation: "bottom",
        navigation: "always",
        aspectRatio: "16:9",
        displayProps: {
            mode: "expanded", //change to classic for a different view mode
            spacing: 50,
            columns: 4,
            topOffset: 70 // to account for the menu element at the top of this documentation page
        },
        thumbnailProps: {
            width: 120,
            height: 96,
            mediaSymbolPosition: "center",
            mediaSymbolBgShape: "radius",
            selectedStyle: "border",
            selectedBorderColor: "#d0021b",
            selectedBorderPosition: "all",
            mediaSymbolBgOpacity: 0.8,
            navigationShape: "round"
        },
        zoom: true,
        zoomProps: {
            level: 2.5,
            type: "popup",
            trigger: "click"
        },
        themeProps: {
            primary: "#0693e3",
            onPrimary: "#ffffff",
            active: "#0693e3"
        },
        transition: "fade",
        viewportBreakpoints: [{
            breakpoint: 576,
            navigation: "always",
            carouselStyle: "indicators",
            carouselLocation: "bottom",
            onPrimary: "#d0021b",
            navigationProps: {
                iconColor: "#d0021b"
            },
            indicatorProps: {
                selectedColor: "#d0021b",
                color: "#E7808D"
            }
        }]
    });
    gallery.render();

    //upload  to main directory
    var uploadWidget = cloudinary.createUploadWidget({
            cloudName: 'smart1snap',
            uploadPreset: 'monteply',
            tags: [MAuploadTag],
            folder: 'Joshua/' + '{{$company->cname}}' + '_Joshua/MarketingAssets/'
        },
        (error, result) => {
            if (!error && result && result.event === "success") {
                console.log('Done! Here is the image info: ', result.info);
            }
        }
    )
    $(document).on('click', '#upload_widget', function () {
        uploadWidget.open();
    });

    //stop reload while image's zoomed
    $(".assetWrapper").click(function () {
        $("#product-gallery").attr("zoomed", 'true');
    });
    $(document).on('click', '.assetWrapper', function () {
        document.getElementById('product-gallery').setAttribute('zoomed', 'true');
    });
    $(document).on('click', '.css-1n3cswg', function () {
        document.getElementById('product-gallery').setAttribute('zoomed', 'false');
    });

    //reload product-gallery 
    setInterval(function () {
        if ($('#product-gallery').attr('zoomed') === "false") {
            gallery.render();
        }
    }, 5000);

    //display image name
    $(document).on('mouseover', '.assetWrapper', function () {
        var filepath = $(this).find('img').attr('src');
        var filename = filepath.replace(/^.*[\\\/]/, '').split('?');
        $('#imgName').remove();
        $(this).append('<div id="imgName" ><p>' + filename[0] + '</p></div>');
    });
</script>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script type="text/javascript">
    $(document).on('keydown', '#email', function (e) {
        if (e.keyCode == 32) return false;
    });

    $(document).on('keydown', '#password', function (e) {
        if (e.keyCode == 32) return false;
    });
</script>

</html>