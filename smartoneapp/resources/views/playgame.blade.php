@extends('layouts.companylist')

@section('content')


<p>
@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif

<div class="loading">Loading&#8230;</div>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
        <?php
            if(!empty($company->company_url)){
        ?>
        <ul id="horizontal-list">
            <li>

                {{ $company->cname }} Checklist  &nbsp;<i class="fa fa-check"></i>
            </li>
        </ul>
        <hr>
        <form>
            @csrf
            <input type="hidden" value="{{ $company->company_url }}" name="company_url" id="company_url">
            <input type="hidden" value="{{ $company->company_id }}" name="company_id" id="company_id">

            <ul style="list-style: none;">
                <li><input type="checkbox" id="select_all" /> Select all</li>
                <ul style="list-style: none;">
                    <li>
                        <input type="checkbox" name="checklist_advertisers" id="checklist_advertisers" value="12" class="checkbox" checked=""> Advertisers
                    </li>
                    <li>
                        <input type="checkbox" name="checklist_companysocialmedia" id="checklist_companysocialmedia" value="8" class="checkbox" checked=""> Company Social Media
                    </li>
                    <li>
                        <input type="checkbox" name="checklist_dispadvertisingreports" id="checklist_dispadvertisingreports" value="11" class="checkbox" checked=""> Display Ad Reports
                    </li>
                    <li>
                        <input type="checkbox" name="checklist_localdata" id="checklist_localdata" value="15" class="checkbox" checked=""> Local Data
                    </li>
                    <li>
                        <input type="checkbox" name="checklist_marketingoverview" id="checklist_marketingoverview" value="14" class="checkbox" checked=""> Marketing Overview
                    </li>
                    <li>
                        <input type="checkbox" name="checklist_publisher_rank" id="checklist_publisher_rank" value="13" class="checkbox" checked=""> Publisher Rank
                    </li>
                    <li>
                        <input type="checkbox" name="checklist_sitecontent" id="checklist_sitecontent" value="16" class="checkbox" checked=""> Site Content
                    </li>
                    <li>
                        <input type="checkbox" name="checklist_websitecolors" id="checklist_websitecolors" value="3" class="checkbox" checked=""> Website Colors
                    </li>
                    <li>
                        <input type="checkbox" name="checklist_websitefont" id="checklist_websitefont" value="6" class="checkbox" checked=""> Website Fonts
                    </li>
                    <li>
                        <input type="checkbox" name="checklist_websiteimages" id="checklist_websiteimages" value="9" class="checkbox" checked=""> Website Images
                    </li>
                    <li>
                            <input type="checkbox" name="checklist_weblogo" id="checklist_weblogo" value="1" class="checkbox" checked=""> Website Logo
                    </li>
                    <li>
                        <input type="checkbox" name="checklist_colorsfromlogo" id="checklist_colorsfromlogo" value="4" class="checkbox" checked=""> Website Logo Colors
                    </li>
                    <li>
                        <input type="checkbox" name="checklist_websitemetadata" id="checklist_websitemetadata" value="10" class="checkbox" checked=""> Website Metadata
                    </li>
                    <li>
                        <input type="checkbox" name="checklist_websitescreenshot" id="checklist_websitescreenshot" value="7" class="checkbox" checked=""> Website Screenshot
                    </li>
                        <!-- <li>
                            <input type="checkbox" name="checklist_socialmedialogo" id="checklist_socialmedialogo" value="2" class="" checked=""> Social Media Logo
                        </li> -->
                        <!-- <li>
                            <input type="checkbox" name="checklist_alternativecolors" id="checklist_alternativecolors" value="5" class="" checked=""> Alternative Colors
                        </li> -->
                    </ul>


                </ul>


                <a href="{{ url('company') }}" ><input type="button" name="proceed" value="Back" class="btn btn-secondary" style="margin: 15px 0 0 35px;"></a>
                <input type="button" name="proceed" id="proceedapi" value="Proceed" class="btn btn-primary" style="margin: 15px 0 0 25px;">

            </form>
        <?php } else { ?>

            <ul id="horizontal-list">
                <li>

                    {{ $company->cname }} Checklist  &nbsp;<i class="fa fa-check"></i>
                </li>
            </ul>
            <div class="alert alert-danger">
                No website URL found.!! <a  href="{{ url('company/edit/'.$company->company_id) }}" title="Update URL" style="text-decoration: none; color: inherit;"><b>Click here to update</b></a>
            </div>

        <?php } ?>

        </div>
    </div>
</div>
</p>



@endsection



