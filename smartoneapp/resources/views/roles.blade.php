@extends('layouts.list')

@section('content')


<p>
@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            
        <ul id="horizontal-list">
            @can('add roles')
            <li>
                <a href="{{ url('roles/add') }}" >
                <button type="button" class="btn btn-primary" name="create" class="">Role <i class="fa fa-plus"></i></button>
                </a> 
            </li>
            @endcan
            @can('view permissions')
            <li>
                <a href="{{ url('permissions') }}">
                <button type="button" name="create" class="btn btn-primary">Permissions <i class="fa fa-cog"></i></button>
                </a> 
            </li>
            @endcan
        </ul>      
        

        <table id="example" class="table table-striped table-bordered" style="width:100%;">
        <thead>
        <tr>
            <th>Sl.No</th>
            <th>Role</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>

        <?php $i=1; foreach ($roles as $key => $value) { ?>
        <tr>
            <td>{{ $i }}</td>
            <td>{{ $value->name }}</td>
            <td><span id="status_{{ $value->id }}">@if($value->rstatus==0) Active @else Inactive @endif </span></td>
            <td>@can('edit roles')<a  href="{{ url('roles/edit/'.$value->id) }}" title="Edit" style="text-decoration: none; color: inherit;"><i class="far fa-edit"></i></a>@endcan @can('delete roles')&nbsp; <a id="{{ $value->id }}" href="javascript:void(0)" title="Delete" style="text-decoration: none; color: inherit;" class="deleteroles"><i class="far fa-trash-alt"></i></a>@endcan&nbsp; <!-- <a  href="{{ url('permissions/assignpermission/'.$value->id) }}" title="Assign Permission" style="text-decoration: none; color: inherit;"><i class="fa fa-link"></i></a> --></td>
        </tr>

        <?php $i++; } ?>

        </tbody>
        </table>
            
        </div>
    </div>
</div>
</p>

@endsection

<script type="text/javascript">

</script>
