<style type="text/css">
table {
  border-collapse: collapse;
  width: 100%;
}

table, th, td {
  border: 1px solid black;
}
th {
  text-align: center;
  height: 50px;
}
td {
  text-align: center;
  height: 25px;
  width: 25%;
}
td.break-word {
  word-wrap: break-word;
}
.dot {
    height: 15px;
    width: 15px;

    border-radius: 50%;
    display: inline-block;
  }

</style>

<h2><u><center> Report @if(!empty($company_name)) of {{$company_name}} @endif </center></u></h2>
{{-- <center><b> @if(!empty($created_at)) {{$created_at}} @endif</b></center> --}}
<table>
  <thead>
    <tr>
    <th colspan="2">General Info</th>
  </tr>
  </thead>
  <tbody>
      <tr>
        <td><b>Company Name</b></td>
        <td>@if(!empty($company_name)) {{$company_name}} @else - @endif</td>
      </tr>
      <tr>
        <td><b>Address</b></td>
        <td>@if(!empty($address1) || !empty($address2)) {{$address1}} @if(!empty($address2)) <br> {{$address2}} @endif @else - @endif</td>
      </tr>
      <tr>
        <td><b>City</b></td>
        <td>@if(!empty($city)) {{$city}} @else - @endif</td>
      </tr>
      <tr>
        <td><b>State</b></td>
        <td>@if(!empty($state_name)) {{$state_name}} @else - @endif</td>
      </tr>
      <tr>
        <td><b>Zip code</b></td>
        <td>@if(!empty($zip)) {{$zip}} @else - @endif</td>
      </tr>
       <tr>
        <td><b>Country</b></td>
        <td>@if(!empty($country)) {{$country}} @else - @endif</td>
      </tr>
       <tr>
        <td><b>Email</b></td>
        <td>@if(!empty($email)) {{$email}} @else - @endif</td>
      </tr>
      <tr>
        <td><b>Phone</b></td>
        <td>@if(!empty($phone)) {{$phone}} @else - @endif</td>
      </tr>
      <tr>
        <td><b>Website url</b></td>
        <td>@if(!empty($company_url)) {{$company_url}} @else - @endif</td>
      </tr>
      <tr>
        <td><b>Media partner</b></td>
        <td>@if(!empty($media_partner)) {{$media_partner}} @else - @endif</td>
      </tr>
  </tbody>
</table>

<br>

<table>
  <thead>
    <tr>
    <th colspan="1">Image Info</th>
  </tr>
  </thead>
  <tbody>
      <tr>
        <td><b>Upload URL for client</b></td>
      </tr>
      <tr>
        <td>@if(!empty($uploadWidget)) {{$uploadWidget}} @else - @endif</td>
      </tr>
      <tr>
        <td><b>Media library link for customer</b></td>
      </tr>
      <tr>
        <td>@if(!empty($mediaLibrary)) {{$mediaLibrary}} @else - @endif</td>
      </tr>
  </tbody>
</table>

<br>
<table>
    <thead>
      <tr>
      <th colspan="1">Images</th>
    </tr>
    </thead>
    <tbody>
        @isset($images)
        @foreach ($images as $value)
            <tr>
                <td><a href="{{ $value }}">{{ $value }}</a></td>
            </tr>
        @endforeach

        @endisset
        @empty($images)
            <tr><td>-</td></tr>
        @endempty
    </tbody>
  </table>

  <br>
<table>
  <thead>
    <tr>
    <th colspan="2">Design Specs</th>
  </tr>
  </thead>
  <tbody>
      <tr>
        <td><b>Logo Color</b></td>
        <td>@if(!empty($logoColor))  &nbsp; <span class="dot" style="background-color: {{ $logoColor }};"></span> {{$logoColor}} @else - @endif</td>
      </tr>
      <tr>
        <td><b>Site font</b></td>
        <td>@if(!empty($siteFont))  {{$siteFont}} @else - @endif</td>
      </tr>
      <tr>
        <td><b>Primary color</b></td>
        <td>@if(!empty($primaryColor)) &nbsp; <span class="dot" style="background-color: {{ $primaryColor }};"></span> {{$primaryColor}} @else - @endif</td>
      </tr>
      <tr>
        <td><b>Secondary color</b></td>
        <td>@if(!empty($secondaryColor))  &nbsp; <span class="dot" style="background-color: {{ $secondaryColor }};"></span> {{$secondaryColor}} @else - @endif</td>
      </tr>
  </tbody>
</table>

<br>

<table style="table-layout: fixed">
  <thead>
    <tr>
    <th colspan="2">Page Info</th>
  </tr>
  </thead>
  <tbody>
      <tr>
        <td><b>Total number of pages</b></td>
        <td>@if(!empty($page_count)) {{$page_count}} @else - @endif</td>
      </tr>
      @if(!empty($page_url))
      <tr>
        <td colspan="2"><b>Page Urls</b></td>
      </tr>
        @php $page_url = explode(',',$page_url); @endphp
        @foreach($page_url as  $value)
        <tr>
          <td colspan="2" class="break-word">{{$value}}</td>
        </tr>
        @endforeach
      @endif
  </tbody>
</table>

<br>

<table>
  <thead>
    <tr>
    <th colspan="2">Hours</th>
  </tr>
  </thead>
  <tbody>
    @if(!empty($hours))
      @php $hours = json_decode($hours); @endphp
      @foreach($hours as  $value)
      <tr>
        <td colspan="2">{{$value}}</td>
      </tr>
      @endforeach
    @else
      <tr>
        <td colspan="2"> - </td>
      </tr>
    @endif
  </tbody>
</table>

<br>

<table>
  <thead>
    <tr>
    <th colspan="2">Social Info</th>
  </tr>
  </thead>
  <tbody>
      <tr>
        <td><b>Facebook</b></td>
        <td>@if(!empty($facebook))<a href="{{ $facebook }}"><img src="../public/socialmedia/facebook.png"></a> {{$facebook}} @else - @endif</td>
      </tr>
      <tr>
        <td><b>Instagram</b></td>
        <td>@if(!empty($instagram))<a href="{{ $instagram }}"><img src="../public/socialmedia/instagram.png"></a>  {{$instagram}} @else - @endif</td>
      </tr>
      <tr>
        <td><b>Twitter</b></td>
        <td>@if(!empty($twitter))<a href="{{ $twitter }}"><img src="../public/socialmedia/twitter.png"></a> {{$twitter}} @else - @endif</td>
      </tr>
      <tr>
        <td><b>Linkedin</b></td>
        <td>@if(!empty($linkedin))<a href="{{ $linkedin }}"><img src="../public/socialmedia/linkedin.png"></a> {{$linkedin}} @else - @endif</td>
      </tr>
      <tr>
        <td><b>Pinterest</b></td>
        <td>@if(!empty($pinterest))<a href="{{ $pinterest }}"><img src="../public/socialmedia/pinterest.png"></a> {{$pinterest}} @else - @endif</td>
      </tr>
       <tr>
        <td><b>Youtube</b></td>
        <td>@if(!empty($youtube))<a href="{{ $youtube }}"><img src="../public/socialmedia/youtube.png"></a> {{$youtube}} @else - @endif</td>
      </tr>
  </tbody>
</table>

<br>

<table>
  <thead>
    <tr>
    <th colspan="2">Meta Data</th>
  </tr>
  </thead>
  <tbody>
      <tr>
        <td><b>Title</b></td>
        <td>@if(!empty($title)) {{$title}} @else - @endif</td>
      </tr>
      <tr>
        <td><b>Summary</b></td>
        <td>@if(!empty($summary))  {{$summary}} @else - @endif</td>
      </tr>
      <tr>
        <td><b>Description</b></td>
        <td>@if(!empty($description)) {{$description}} @else - @endif</td>
      </tr>
      <tr>
        <td><b>Keywords</b></td>
        <td>@if(!empty($keywords)) {{$keywords}} @else - @endif</td>
      </tr>
  </tbody>
</table>

<br>
