@extends('layouts.cloudwidget')

@section('content')


<p>
@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

        <ul id="horizontal-list">

                <a href="{{ url('cloudinarywidget') }}" >
                <button type="button" class="btn btn-primary" name="create" class="">Create <i class="fa fa-plus"></i></button>
                </a>
            </li>

            <!-- <li>
                <a href="{{ url('customers') }}" >
                <button type="button" class="btn btn-primary" name="create" class="">Customer <i class="fa fa-user-plus"></i></button>
                </a>
            </li> -->
        </ul>

        <table id="example" class="table table-striped table-bordered" style="width:100%;">
        <thead>
        <tr>
            <th>Sl.No</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Website</th>
            <th>Code</th>
            <th>Widget</th>
        </tr>
        </thead>
        <tbody>

    <?php $i=1;
    foreach ($uploadw as $key => $value) {
             $value->cl_email = str_replace(',',', ',$value->cl_email);
            //$iframe_content = "<iframe id='myFrame' src=".base_url().'/'.$value->widget_url" max-width=100% width=100% height=650px frameborder='0'></iframe>";
        ?>
        <tr>
            <td>{{ $i }}</td>
            <td>{{ $value->cl_name }}</td>
            <td>{{ $value->cl_email }}</td>
            <td>{{ $value->cl_phone }}</td>
            <td>{{ $value->web_url }}</td>
            <td><button type="button" class="btn btn-primary" onclick="showCode({{$value->cloud_id}})">Code</button></td>
            <td>@if($value->widget_url)<a href="{{ url($value->widget_url) }}" style="text-decoration: none;" target="_blank"><?php if($value->widget_type==1){ echo "Upload"; } else if($value->widget_type==2) { echo "Media"; } else if($value->widget_type==3){echo "Gallery";}?> &nbsp; <i class="fa fa-external-link"></i></a> @endif</td>
        </tr>

        <?php $i++;
    } ?>

        </tbody>
        </table>

        </div>
    </div>
</div>
</p>



@endsection

