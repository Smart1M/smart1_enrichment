@extends('layouts.companylist')

@section('content')

<p>
    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12" style="width:100%;">

                <ul id="horizontal-list">
                    @can('add company')
                    <li>
                        <a href="{{ url('company/add') }}">
                            <button type="button" class="btn btn-primary" name="create" class="">Company <i
                                    class="fa fa-plus"></i></button>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('googlemapdata') }}">
                            <button type="button" class="btn btn-primary" name="create" class="">Upload Sheet <i
                                    class="fa fa-upload"></i></button>
                        </a>
                    </li>
                    @endcan
                    <!-- <li>
                <a href="{{ url('customers') }}" >
                <button type="button" class="btn btn-primary" name="create" class="">Customer <i class="fa fa-user-plus"></i></button>
                </a>
            </li> -->
                </ul>

                <table id="example" class="table table-striped table-bordered" style="width:100%;">
                    <thead>
                        <tr>
                            <th>Sl.No</th>
                            <th>Partner</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Last Scan</th>
                            <th>Status</th>
                            <th>Actions</th>
                            @can('play game')<th>Play Game</th>@endcan
                        </tr>
                    </thead>
                    <tbody>

                        <?php $i=1; foreach ($company as $key => $value) { $value->email = str_replace(',', ', ', $value->email);?>
                        <tr>
                            <td>{{ $i }}</td>
                            <td>{{ $value->media_partner }}</td>
                            <td>{{ $value->cname }}</td>
                            <td>{{ $value->email }}</td>
                            <td><span>@foreach($scan as $row) @if($row->company_id===$value->company_id) {{$row->created_at}} @else &nbsp; @endif @endforeach</span></td>
                            <td><span id="status_{{ $value->company_id }}">@if($value->status==0) Active @else Inactive
                                    @endif </span></td>
                            <td>@can('edit company')<a href="{{ url('company/edit/'.$value->company_id) }}" title="Edit"
                                    style="text-decoration: none; color: inherit;"><i
                                        class="far fa-edit"></i></a>@endcan @can('delete company')&nbsp; <a
                                    id="{{ $value->company_id }}" href="javascript:void(0)" title="Delete"
                                    style="text-decoration: none; color: inherit;" class="deletecompany"><i
                                        class="far fa-trash-alt"></i></a>@endcan</td>
                            @can('play game')<td><a class="btn btn-primary"
                                    href="{{ url('company/playgame/'.$value->company_id) }}"
                                    style="width: 150px; text-decoration: none;"><i class="fas fa-gamepad"></i> Play a
                                    game</a></td>@endcan
                        </tr>

                        <?php $i++; } ?>

                    </tbody>
                </table>

            </div>
        </div>
    </div>
</p>

@endsection
