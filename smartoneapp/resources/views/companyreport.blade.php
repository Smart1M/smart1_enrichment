@extends('layouts.companyreport')

@section('content')

@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div><br/>
@endif

<div class="loading">Loading&#8230;</div>

<div class="dash-main-body">
    <div class="dash-main-form">
        <form>
            @csrf
            <div class="col-lg-12 row">

                <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                    <label>
                        Select Company <span style="color: red;">*</span>
                    </label>
                    <select name="company_id" id="company_id" class="js-example-basic-single" style="width: 100%; height: 30px;" required="">
                        <option value="">---Select a Company---</option>
                        <?php foreach ($companies as $key => $value) { ?>
                        <option value="{{ $value->company_id }}">{{ $value->cname }}</option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                    <label>
                        Website URL <span style="color: red;">*</span>
                    </label>
                    <input type="text" name="web_url" id="web_url" required="" disabled>
                    <span style="font-size: 11px; color: #898383;">Valid URLs : http://domain-name.com or
                        http://www.domain-name.com</span>
                </div>
            </div>

            <div class="col-lg-12 row">
                <div class="col-lg-12">
                    <input type="button" value="Submit" class="saveUrl" id="saveUrl" style="float: left;">
                    <a href="{{ url('/home') }}"><input type="button" class="dash-main-form-btn" value="Cancel"
                            style="float: left;"></a>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <table id="example" class="table table-striped table-bordered" style="width:100%;">
                <thead>
                    <tr>
                        <th>Sl.No</th>
                        <th>URL</th>
                        <th>Records</th>
                        <th>View</th>
                    </tr>
                </thead>
                <tbody>

                    <?php $i=1; foreach ($reports as $key => $value) { ?>
                    <tr>
                        <td>{{ $i }}</td>
                        <td>{{ $value->url }}</td>
                        <td>{{ $value->records }}</td>
                        <td><a href="{{ url('company/reports/'.$value->company_id) }}" style="text-decoration: none; color: inherit;"><i class="fa fa-eye  fa-lg" style="color: red;"></i></a></td>
                    </tr>

                    <?php $i++; } ?>

                </tbody>
            </table>

        </div>
    </div>
</div>

@endsection
