<style type="text/css">
    table {
        border-collapse: collapse;
        width: 100%;
    }

    table,
    th,
    td {
        border: 1px solid black;
        padding: 5px;
    }

    th {
        text-align: center;
        height: 50px;
    }

    td {
        text-align: center;
        height: 25px;
        width: 25%;
    }

    td {
        word-wrap: break-word;
    }

    .dot {
        height: 15px;
        width: 15px;

        border-radius: 50%;
        display: inline-block;
    }

    div {
        border: 1px solid black;
        word-wrap: break-word;
        width: 690px;
        position: relative;
        left: 5.5px;
        top: -22px;
        border-top: none;
    }
</style>

<h2><u>
        <center> Report @if(!empty($company_name)) of {{$company_name}} @endif </center>
    </u></h2>
{{-- <center><b> @if(!empty($created_at)) {{$created_at}} @endif</b></center> --}}
<table>
    <thead>
        <tr>
            <th colspan="2">General Info</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Company Name</b></td>
            <td>@if(!empty($company_name)) {{$company_name}} @else - @endif</td>
        </tr>
        <tr>
            <td><b>Address</b></td>
            <td>@if(!empty($address1) || !empty($address2)) {{$address1}} @if(!empty($address2)) <br> {{$address2}}
                @endif @else - @endif</td>
        </tr>
        <tr>
            <td><b>City</b></td>
            <td>@if(!empty($city)) {{$city}} @else - @endif</td>
        </tr>
        <tr>
            <td><b>State</b></td>
            <td>@if(!empty($state_name)) {{$state_name}} @else - @endif</td>
        </tr>
        <tr>
            <td><b>Zip code</b></td>
            <td>@if(!empty($zip)) {{$zip}} @else - @endif</td>
        </tr>
        <tr>
            <td><b>Country</b></td>
            <td>@if(!empty($country)) {{$country}} @else - @endif</td>
        </tr>
        <tr>
            <td><b>Email</b></td>
            <td>@if(!empty($email)) {{$email}} @else - @endif</td>
        </tr>
        <tr>
            <td><b>Phone</b></td>
            <td>@if(!empty($phone)) {{$phone}} @else - @endif</td>
        </tr>
        <tr>
            <td><b>Website url</b></td>
            <td>@if(!empty($company_url)) {{$company_url}} @else - @endif</td>
        </tr>
    </tbody>
</table>
<br>
<table>
    <tbody>

        <tr>
            <td><b>Pages found</b></td>
            <td>{{$pages_found}}</td>
        </tr>
        <tr>
            <td><b>Is Mobile Friendly</b></td>
            <td>@if($has_mobile_site==1) Yes @else No @endif</td>
        </tr>
        <tr>
            <td><b>Good Headings</b></td>
            <td>@if($good_headings==1) Yes @else No @endif</td>
        </tr>
        <tr>
            <td><b>Has Content for every Heading</b></td>
            <td>@if($has_content_for_every_heading==1) Yes @else No @endif</td>
        </tr>
        <tr>
            <td><b>Home Page Title Tag</b></td>
            <td>@if(!empty($homepage_title_tag)) {{$homepage_title_tag}} @else - @endif</td>
        </tr>
        <tr>
            <td><b>Has Single H1 on Every Page</b></td>
            <td>@if($has_single_h1_on_each_page==1) Yes @else No @endif</td>
        </tr>
        <tr>
            <td><b>Detected Address</b></td>
            <td>@if(!empty($detected_address)) {{$detected_address}} @else - @endif</td>
        </tr>
        <tr>
            <td><b>Has Analytics</b></td>
            <td>@if($has_analytics==1) Yes @else No @endif</td>
        </tr>
    </tbody>
</table>
<br>

<table>
    <thead>
        <tr>
            <th colspan="2">Images</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Image Count</b></td>
            <td>@if(!empty($image_count)) {{$image_count}} @else 0 @endif</td>
        </tr>
        <tr>
            <td><b>Stretched Images Count</b></td>
            <td>@if(!empty($stretched_image_count)) {{$stretched_image_count}} @else 0 @endif</td>
        </tr>
        <tr>
            <td><b>Non web friendly Count</b></td>
            <td>@if(!empty($non_web_friendly_count)) {{$non_web_friendly_count}} @else 0 @endif</td>
        </tr>

    </tbody>
</table>
<br>

<table>
    <thead>
        <tr>
            <th colspan="2"><b>Broken Images</b></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Broken Images Count</b></td>
            <td>@if(!empty($broken_images_count)) {{$broken_images_count}} @else 0 @endif</td>
        </tr>
        <tr>
            <td colspan="2"><b>Broken Images List</b> </td>
        </tr>
    </tbody>
</table>
<div>
    <br>
    @if(!empty($broken_images_list))
    <ol>
        @foreach(explode('====!', $broken_images_list) as $image)
        @if($loop->first) @continue @endif
        <li>@if(!empty($image)) {{$image}} @else Invalid path @endif</li> @endforeach
    </ol>
    @else
    <p style="text-align: center;">-</p>
    @endif
</div>
<br>




<br>
<table>
    <thead>
        <tr>
            <th colspan="2">Broken URL's</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Count of Broken URL's</b> </td>
            <td>@if(!empty($broken_url_count)){{$broken_url_count}} @else 0 @endif</td>
        </tr>
        <tr>
            <td colspan="2"> <b>List of Broken URL's</b> </td>
        </tr>
    </tbody>
</table>
<div>
    <br>
    @if(!empty($broken_urls_list))
    <ol>

        @foreach(explode('====!', $broken_urls_list) as $link)
        @if($loop->first) @continue @endif
        <li>@if(!empty($link)) {{$link}} @else Invalid path @endif</li> @endforeach
    </ol>
    @else
    <p style="text-align: center;">-</p>
    @endif
</div>
<br>
<table>
    <thead>
        <tr>
            <th colspan="2">Pages Missing Titles</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Count of Pages Missing Title</b> </td>
            <td>@if(!empty($title_less_count)) {{$title_less_count}} @else 0 @endif</td>
        </tr>
        <tr>
            <td colspan="2"><b>List of Pages Missing Title</b> </td>
        </tr>
    </tbody>
</table>
<div>
    <br>
    @if(!empty($title_less))
    <ol>
        @foreach(explode('====!', $title_less) as $link)
        @if($loop->first) @continue @endif
        <li>@if(!empty($link)) {{$link}} @else - @endif</li> @endforeach
    </ol>
    @else
    <p style="text-align: center;">-</p>
    @endif
</div>
<br>

<table>
    <thead>
        <tr>
            <th colspan="2"> Pages with Lorem</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td> <b>Count of Pages with Lorem</b> </td>
            <td>@if(!empty($lorem_count)) {{$lorem_count}} @else 0 @endif</td>
        </tr>
        <tr>
            <td colspan="2"><b>List of Pages with Lorem</b> </td>
        </tr>
    </tbody>
</table>
<div>
    <br>
    @if(!empty($lorem_pages))
    <ol>
        @foreach(explode('====!', $lorem_pages) as $link)
        @if($loop->first) @continue @endif
        <li>@if(!empty($link)) {{$link}} @else - @endif</li> @endforeach
    </ol>
    @else
    <p style="text-align: center;">-</p>
    @endif
</div>
<br>

<table>
    <thead>
        <tr>
            <th colspan="2">Social Info</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Facebook</b></td>
            <td>@if(!empty($facebook))<a href="{{ $facebook }}"><img src="../public/socialmedia/facebook.png"></a>
                {{$facebook}} @else - @endif</td>
        </tr>
        <tr>
            <td><b>Instagram</b></td>
            <td>@if(!empty($instagram))<a href="{{ $instagram }}"><img src="../public/socialmedia/instagram.png"></a>
                {{$instagram}} @else - @endif</td>
        </tr>
        <tr>
            <td><b>Twitter</b></td>
            <td>@if(!empty($twitter))<a href="{{ $twitter }}"><img src="../public/socialmedia/twitter.png"></a>
                {{$twitter}} @else - @endif</td>
        </tr>
        <tr>
            <td><b>Linkedin</b></td>
            <td>@if(!empty($linkedin))<a href="{{ $linkedin }}"><img src="../public/socialmedia/linkedin.png"></a>
                {{$linkedin}} @else - @endif</td>
        </tr>
        <tr>
            <td><b>Pinterest</b></td>
            <td>@if(!empty($pinterest))<a href="{{ $pinterest }}"><img src="../public/socialmedia/pinterest.png"></a>
                {{$pinterest}} @else - @endif</td>
        </tr>
        <tr>
            <td><b>Youtube</b></td>
            <td>@if(!empty($youtube))<a href="{{ $youtube }}"><img src="../public/socialmedia/youtube.png"></a>
                {{$youtube}} @else - @endif</td>
        </tr>
    </tbody>
</table>

<br>