@extends('layouts.companyreport')

@section('content')

@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div><br />
@endif

<div class="loading">Loading&#8230;</div>

<div class="dash-main-body">
    <div class="dash-main-form">
        <form>
            @csrf
            <div class="col-lg-12 row">

                <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                    <label>
                        Select Company <span style="color: red;">*</span>
                    </label>
                    <select name="company_id" id="company_id" class="js-example-basic-single"
                        style="width: 100%; height: 30px;" required="">
                        <option value="">---Select a Company---</option>
                        <?php foreach ($companies as $key => $value) { ?>
                        <option value="{{ $value->company_id }}">{{ $value->cname }}</option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                    <label>
                        Website URL <span style="color: red;">*</span>
                    </label>
                    <input type="text" name="web_url" id="web_url" required="" disabled>
                    <span style="font-size: 11px; color: #898383;">Valid URLs : http://domain-name.com or
                        http://www.domain-name.com</span>
                </div>
            </div>

            <div class="col-lg-12 row">
                <div class="col-lg-12">
                    <input type="button" value="Submit" class="saveUrl" id="saveQa" style="float: left;">
                    <a href="{{ url('/home') }}"><input type="button" class="dash-main-form-btn" value="Cancel"
                            style="float: left;"></a>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <table id="example" class="table table-striped table-bordered" style="width:100%;">
                <thead>
                    <tr>
                        <th>Sl.No</th>
                        <th>URL</th>
                        <th>Created At</th>
                        <th>Export</th>
                    </tr>
                </thead>
                <tbody>

                    <?php $i=1; foreach ($reports as $key => $value) { ?>
                    <tr>
                        <td>{{ $i }}</td>
                        <td>{{ $value->url }}</td>
                        <td>{{ date('m-d-Y H:i A', strtotime($value->created_at)) }}</td>
                        <td><a href="{{ url('company/report/exportqa/'.$value->company_id) }}" class="" id=""
                                title="Export" style="text-decoration: none; color: inherit;"><i
                                    class="fa fa-file-pdf-o fa-lg" style="color: red;"></i></a></td>
                    </tr>

                    <?php $i++; } ?>

                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script type="text/javascript">
$("#saveQa").click(function() {
            var company_id = $("#company_id").val();
            var web_url = $("#web_url").val();
            if (web_url != "" && company_id != "") {
                var pattern = /^(http|https)?:\/\/[a-zA-Z0-9-\.]+\.[a-z]{2,4}/;
                if (pattern.test(web_url)) {
                    $("#saveUrl").attr("disabled", true);
                    $(".loading").show();
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: "{{ url('/company/storeqa') }}",
                        method: 'post',
                        data: {
                            web_url: web_url,
                            company_id: company_id
                        },
                        success: function(result) {
                            var APP_URL = {!!json_encode(url('/')) !!};
                            $(".loading").hide();
                            $("#web_url").val("");
                            $("#company_id").val("");
                            $("#saveUrl").attr("disabled", false);
                            if (result == 1) {
                                Swal.fire(
                                    'Good job!',
                                    'Data has been saved!',
                                    'success'
                                )
                                window.location.reload();
                            } else if (result.error) {
                                Swal.fire(
                                    'Please try again!',
                                    result.error,
                                    'error'
                                ).then(
                                    function() {
                                        window.location.href = APP_URL + '/company/edit/' + company_id;
                                    }
                                );
                            } else {
                                Swal.fire(
                                    'Please try again!',
                                    'API was timed out before fetching the required data!',
                                    'error'
                                )
                            }
                        },
                        error: function(result, text, status) {
                            $(".loading").hide();
                            $("#saveUrl").attr("disabled", false);
                            Swal.fire(
                                'Please try again!',
                                status,
                                'error'
                            )
                        }
                    });
                } else {
                    Swal.fire(
                        'Invalid URL',
                        'Please Enter Valid URL! in Company edit',
                        'error'
                    )
                    return false;
                }
            } else {
                Swal.fire(
                    'Field Missing!',
                    'Both fields are compulsory!',
                    'error'
                )
            }
        });

    $(document).ready(function () {
        var table = $('#example').DataTable();
        $('#example tbody').on('click', 'a.exporttopdf', function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var id = $(this).attr("id");
            $.ajax({
                url: "{{ url('/company/exportqa') }}",
                method: 'post',
                data: {
                    id: id
                },
                success: function (result) {}
            });
        });
    });
</script>
@endsection