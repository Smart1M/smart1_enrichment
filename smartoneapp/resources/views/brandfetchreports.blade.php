@extends('layouts.brandfetch')

@section('content')

@if ($errors->any())
  <div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
    </ul>
  </div><br />
@endif

<div class="loading">Loading&#8230;</div>

<div class="dash-main-body">
    <div class="dash-main-form">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">

                    <table id="example" class="table table-striped table-bordered" style="width:100%;">
                        <thead>
                        <tr>
                            <th>Sl.No</th>
                            <th>URL</th>
                            <th>Created at</th>
                            <th>Export</th>

                        </tr>
                        </thead>
                        <tbody>

                        <?php $i=1; foreach ($brandfetchinfo as $key => $value) { ?>
                        <tr>
                            <td>{{ $i }}</td>
                            <td>{{ $value->web_url }}</td>
                            <td>{{ date('m-d-Y H:i A', strtotime($value->created_at)) }}</td>
                            <td><a href="{{ url('brandfetch/export/'.$value->br_id) }}" class="" id="" title="Export" style="text-decoration: none; color: inherit;"><i class="fa fa-file-pdf-o fa-lg" style="color: red;"></i></a></td>

                        </tr>

                        <?php $i++; } ?>

                        </tbody>
                        </table>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection

<script type="text/javascript">

</script>
