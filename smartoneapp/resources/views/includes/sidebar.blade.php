<div id="smartsidenav" class="sidenav-wrap">
      <div class="sidebar-head logo">
            <img src="{{ asset('css/images/logo.png') }}">
      </div>
      <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
      <div class="prof">
            <img class="prof-icon" src="{{ asset('css/images/prof.png') }}">
            <a href="javascript:void(0)"><span class="prof-label">{{ Auth::user()->name }}</span></a>
      </div>

      <ul class="sidebar-menu">
            <li class="sidebar-menu-item">
                  <a href="{{ url('/home') }}" style="text-decoration: none; color: inherit; "> &nbsp; Dashboard</a>
            </li>
            <?php $menu = Menu::get_parentmenu(); $dropicon = "";
            foreach($menu as $mkey => $mval) {
              if($mval->is_sub==1)
              {
                $dropicon = 'dropdown';
              }
            ?>
             @can($mval->name)

              <li class="sidebar-menu-item {{ $dropicon }} ">
                  <a href="{{ url('/'.$mval->menu_slug.'') }}" data-toggle="{{ $dropicon }}" style="text-decoration: none; color: inherit; display: inline-block; width: 100%;"> &nbsp; {{ $mval->menu_name }} <?php if($mval->is_sub==1) { ?><i class="icon-arrow"></i><?php } ?></a>
                  <?php if($mval->is_sub==1) {

                    $sub_menu = Menu::get_submenu($mval->menu_id);
                  ?>

                    <ul class="dropdown-menu">
                      <?php foreach($sub_menu as $smkey => $smval) { ?>

                        <li><a href="{{ url('/'.$smval->menu_slug.'') }}" style="text-decoration: none; color: inherit; display: inline-block; width: 100%;">&nbsp;{{ $smval->menu_name }}</a></li>

                      <?php } ?>

                   </ul>

                  <?php } ?>
            </li>

            @endcan

            <?php } ?>
            @role('General|Manager')
            <li class="sidebar-menu-item dropdown">
              <a href="#" data-toggle="dropdown" style="text-decoration: none; color: inherit; display: inline-block; width: 100%;"><i class="icon-arrow"></i> &nbsp; Settings</a>

                  <ul class="dropdown-menu">
                        <li><a href="{{ url('/menu/add') }}" style="text-decoration: none; color: inherit; display: inline-block; width: 100%;"> &nbsp; Menu</a></li>
                   </ul>

            </li>
           @endrole

            <li class="sidebar-menu-item">
            </li>
              <a href="{{ route('logout') }}" style="text-decoration: none; color: inherit; display: inline-block; width: 100%;" onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();"> &nbsp; {{ __('Logout') }}</a>
            </li>

      </ul>

</div>


<div class="col-lg-2 col-md-4 col-sm-12 col-12 sidebar-wrap">

    <div class="sidebar-head logo">
          <img src="{{ asset('css/images/logo.png') }}">
    </div>
    <div class="prof">
          <img class="prof-icon" src="{{ asset('css/images/prof.png') }}">
          <a href="javascript:void(0)"><span class="prof-label">{{ Auth::user()->name }}</span></a>
    </div>
    <!--   -->
    <ul class="sidebar-menu ">
           <li class="sidebar-menu-item dropdown">
                <a href="{{ url('/home') }}" data-toggle="dropdown" style="text-decoration: none; color: inherit; display: inline-block; width: 100%;"> &nbsp; Dashboard</a>
            </li>
            <?php $menu = Menu::get_parentmenu(); $dropicon = "";
            foreach($menu as $mkey => $mval) {
              if($mval->is_sub==1)
              {
                $dropicon = 'dropdown';
              }
            ?>
             @can($mval->name)
              <li class="sidebar-menu-item dropdown ">
                  <a href="{{ url('/'.$mval->menu_slug.'') }}" data-toggle="dropdown" style="text-decoration: none; color: inherit; display: inline-block; width: 100%;"> &nbsp; {{ $mval->menu_name }} <?php if($mval->is_sub==1) { ?><i class="icon-arrow"></i><?php } ?></a>
                  <?php if($mval->is_sub==1) {

                    $sub_menu = Menu::get_submenu($mval->menu_id);
                  ?>

                    <ul class="dropdown-menu">
                      <?php foreach($sub_menu as $smkey => $smval) { ?>

                        <li class="sidebar-sub-menu-item"><a href="{{ url('/'.$smval->menu_slug.'') }}" style="text-decoration: none; color: inherit; display: inline-block; width: 100%;">&nbsp;{{ $smval->menu_name }}</a></li>

                      <?php } ?>

                   </ul>

                  <?php } ?>
            </li>
            @endcan

            <?php } ?>


            <li class="sidebar-menu-item dropdown">
              <a href="#" data-toggle="dropdown" style="text-decoration: none; color: inherit; display: inline-block;
    width: 100%;"><i class="icon-arrow"></i> &nbsp; Settings</a>

                  <ul class="dropdown-menu">
                    @role('Super Admin|Admin')
                        <li><a href="{{ url('/menu') }}" style="text-decoration: none; color: inherit; display: inline-block;
    width: 100%;">&nbsp;Menu</a></li>
                    @endrole
                        <!-- <li><a href="{{ url('/changepassword') }}" style="text-decoration: none; color: inherit;">&nbspChange Password</a></li> -->
                   </ul>

            </li>


          <li class="sidebar-menu-item">
                <a href="{{ route('logout') }}" style="text-decoration: none; color: inherit; display: inline-block;
    width: 100%;" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> &nbsp; {{ __('Logout') }}</a>
          </li>

    </ul>

</div>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
</form>

