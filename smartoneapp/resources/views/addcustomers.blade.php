@extends('layouts.customerlist')

@section('content')

@if ($errors->any())
  <div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
    </ul>
  </div><br />
@endif
<div class="dash-main-body">
<div class="dash-main-form">
  <form action="{{ route('register') }}" method="post">
    @csrf

      <div class="col-lg-12 row">
            <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                  <label>
                        Firstname <span style="color: red;">*</span>
                  </label> 
              <input type="text" name="fname" id="fname" required autocomplete="fname">
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                  <label>
                        Surname <span style="color: red;">*</span>
                  </label> 
              <input type="text" name="surname" id="surname" required autocomplete="surname">
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                  <label>
                        Lastname <span style="color: red;">*</span>
                  </label> 
              <input type="text" name="lname" id="lastname" required autocomplete="lastname">
            </div>
      </div>

      <div class="col-lg-12 row">
            
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Company <span style="color: red;">*</span>
                  </label> 
              <select name="role_id" id="role_id" class="js-example-basic-single" style="width: 100%;" required="">
                <option>Select Company</option>
                <?php foreach ($company as $key => $value) { ?>
                 <option value="{{ $value->company_id }}">{{ $value->cname }}</option>
               <?php } ?>
              </select>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Dob <span style="color: red;">*</span>
                  </label> 
              <input type="text" name="dob" id="dob" autocomplete="Dob">
            </div>
      </div>
      <div class="col-lg-12 row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Email <span style="color: red;">*</span>
                  </label> 
              <input type="text" name="email" id="email" required autocomplete="email">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Mobile <span style="color: red;">*</span>
                  </label> 
              <input type="text" name="mobile" id="mobile" autocomplete="Mobile">
            </div>
      </div>
      <div class="col-lg-12 row">

            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Password <span style="color: red;">*</span>
                  </label> 
              <input type="password" name="password" id="password" style="width: 100%;" autocomplete="new-password">
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Confirm pasword <span style="color: red;">*</span>
                  </label> 
              <input id="password-confirm" type="password"  name="password_confirmation" required autocomplete="new-password" style="width: 100%";>
            </div>
      </div>
  <div class="col-lg-12 row">    
      <div class="col-lg-12">
            <input type="submit" value="Submit">
      </div>
  </div>
</form>
</div>
</div>

@endsection

<script type="text/javascript">

</script>
