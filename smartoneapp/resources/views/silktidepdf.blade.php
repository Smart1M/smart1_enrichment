<style type="text/css">
table {
  border-collapse: collapse;
  width: 100%;
}

table, th, td {
  border: 1px solid black;
}
th {
  text-align: center;
  height: 50px;
}
td {
  text-align: center;
  height: 25px;
  width: 25%;
}

</style>

<h2><u>Silktide Data of {{$url}}</u></h2>

<table>
  <thead>
    <tr>
    <th colspan="2">Site Content</th>
  </tr>
  </thead>
  <tbody>
      <tr>
        <td><b>Average words per page</b></td>
        <td>{{ $average_words_per_page }}</td>
      </tr>
      <tr>
        <td><b>Pages found</b></td>
        <td>{{ $pages_found }}</td>
      </tr>
      <tr>
        <td><b>Total word count</b></td>
        <td>{{ $total_word_count }}</td>
      </tr>
      <tr>
        <td><b>Has CMS</b></td>
        <td><?php if($has_cms==1){ ?>Yes</i> <?php } else { ?>No<?php } ?></td>
      </tr>
      <tr>
        <td><b>CMS solution</b></td>
        <td><?php if($has_cms==1){ echo  $cms_solution; } else { echo "-"; }?></td>
      </tr>
  </tbody>
</table>

<br>

<table>
  <thead>
    <tr>
    <th colspan="2">Vitals</th>
  </tr>
  </thead>
  <tbody>

      <tr>
        <td><b>Pages Tested</b></td>
        <td>{{ $pages_tested }}</td>
      </tr>
      <tr>
        <td><b>Has Analytics</b></td>
        <td><?php if($has_analytics==1){ echo "Yes"; } else { echo "No"; } ?></td>
      </tr>
      <tr>
        <td><b>Analytics Tool</b></td>
        <td><?php if($has_analytics==1){ echo $analytics_tool; } else { echo "-"; } ?></td>
      </tr>
      <tr>
        <td><b>Has Commerce</b></td>
        <td><?php if($has_ecommerce==1){ echo "Yes"; } else { echo "No"; } ?></td>
      </tr>
      <tr>
        <td><b>Ecommerce Name</b></td>
        <td><?php if($has_ecommerce==1){ echo  $ecommerce_name; } else { echo "-"; } ?></td>
      </tr>
      <tr>
        <td><b>Has Pixel</b></td>
        <td><?php if($has_pixel==1){ echo "Yes"; } else { echo "No"; } ?></td>
      </tr>
      <tr>
        <td><b>Address Details Provided</b></td>
        <td>{{ $address_details_provided }}</td>
      </tr>
      <tr>
        <td><b>Has Result</b></td>
        <td><?php if($has_result==1){ echo "Yes"; } else { echo "No"; } ?></td>
      </tr>
      <tr>
        <td><b>Local Scan</b></td>
        <td><?php if($yext_api_success==1){ echo "Success"; } else { echo "Fail"; } ?></td>
      </tr>
      <tr>
        <td><b>Mobile Friendly</b></td>
        <td><?php if($is_mobile==1){ echo "Yes"; } else { echo "No"; } ?></td>
      </tr>
      <tr>
        <td><b>Has Mobile Site</b></td>
        <td><?php if($has_mobile_site==1){ echo "Yes"; } else { echo "No"; } ?></td>
      </tr>
      <tr>
        <td><b>Mobile Site</b></td>
        <td><?php if($mobile_site_url!=""){ echo $mobile_site_url; } else { echo "-"; } ?></td>
      </tr>
      <tr>
        <td><b>Tablet Friendly</b></td>
        <td><?php if($is_tablet==1){ echo "Yes"; } else { echo "No"; } ?></td>
      </tr>
      <tr>
        <td><b>Mobile Screenshot</b></td>
        <td><?php if($mobile_screenshot_url!=""){ ?> <a href="{{ $mobile_screenshot_url }}" style="text-decoration: none;">Click here to View</a><?php } else { echo "-"; } ?></td>
      </tr>
      <tr>
        <td><b>Estimate Monthly Organic Traffic</b></td>
        <td>{{ $average_monthly_traffic }}</td>
      </tr>
      <tr>
        <td><b>Google Adwords</b></td>
        <td><?php if($adwords_keywords!=""){ echo $adwords_keywords; } else { echo "-"; } ?></td>
      </tr>
      <tr>
        <td><b>Adwords Spend</b></td>
        <td><?php if($has_adwords_spend==1){ echo "Yes"; } else { echo "No"; } ?></td>
      </tr>
      <tr>
        <td><b>Has a Sitemap</b></td>
        <td><?php if($has_sitemap==1){ echo "Yes"; } else { echo "No"; } ?></td>
      </tr>
      <tr>
        <td><b>Sitemap has Issues</b></td>
        <td><?php if($sitemap_issues==1){ echo "Yes"; } else { echo "No"; } ?></td>
      </tr>
      <tr>
        <td><b>Days since Last Update</b></td>
        <td><?php if($stale_analysis==1){ echo "Yes"; } else { echo "No"; } ?></td>
      </tr>
      <tr>
        <td><b>Estimated Site Traffic</b></td>
        <td>{{ $website_traffic }}</td>
      </tr>

  </tbody>
</table>

<br>

<table>
  <thead>
    <tr>
    <th colspan="2">Discovered</th>
  </tr>
  </thead>
  <tbody>
      <tr>
        <td><b>Emails</b></td>
        <td><?php if($emails!=""){ echo $emails; } else { echo "-"; } ?></td>
      </tr>
      <tr>
        <td><b>Phones</b></td>
        <td><?php if($phones!=""){ echo $phones; } else { echo "-"; } ?></td>
      </tr>
  </tbody>
</table>

<br>

<table>
  <thead>
    <tr>
    <th colspan="2">Domain</th>
  </tr>
  </thead>
  <tbody>
      <tr>
        <td><b>Domain Parked ?</b></td>
        <td><?php if($is_parked==1){ echo "Yes"; } else { echo "No"; } ?></td>
      </tr>
      <tr>
        <td><b>Has an SSL ?</b></td>
        <td><?php if($has_ssl==1){ echo "Yes"; } else { echo "No"; } ?></td>
      </tr>
      <tr>
        <td><b>SSL Expired ?</b></td>
        <td><?php if($ssl_expired==1){ echo "Yes"; } else { echo "No"; } ?></td>
      </tr>
      <tr>
        <td><b>SSL Redirect ?</b></td>
        <td><?php if($ssl_redirect==1){ echo "Yes"; } else { echo "No"; } ?></td>
      </tr>
      <tr>
        <td><b>SSL Valid ?</b></td>
        <td><?php if($ssl_valid==1){ echo "Yes"; } else { echo "No"; } ?></td>
      </tr>
  </tbody>
</table>

<br>

<table>
  <thead>
    <tr>
    <th colspan="2">Domain Info</th>
  </tr>
  </thead>
  <tbody>
      <tr>
        <td><b>Age of Domain</b></td>
        <td><?php if($domain_age_days!=""){ echo $domain_age_days; } else { echo "-"; } ?></td>
      </tr>
      <tr>
        <td><b>Expiry Date</b></td>
        <td>@if($expiry_date) {{ date('m/d/Y', strtotime($expiry_date)) }} @else - @endif</td>
      </tr>
      <tr>
        <td><b>Registered Date</b></td>
        <td>@if($registered_date) {{ date('m/d/Y', strtotime($registered_date)) }} @else - @endif</td>
      </tr>
  </tbody>
</table>

<br>


<table>
  <thead>
    <tr>
    <th colspan="2">SEO</th>
  </tr>
  </thead>
  <tbody>
      <tr>
        <td><b>Has Heading Tags</b></td>
        <td><?php if($good_headings==1){ echo "Yes"; } else { echo "No"; } ?></td>
      </tr>
      <tr>
        <td><b>Has Content for Headings</b></td>
        <td><?php if($has_content_for_every_heading==1){ echo "Yes"; } else { echo "No"; } ?></td>
      </tr>
      <tr>
        <td><b>Has H2,H3 Tags</b></td>
        <td><?php if($has_hierarchical_headings==1){ echo "Yes"; } else { echo "No"; } ?></td>
      </tr>
      <tr>
        <td><b>Has H1 on each Page</b></td>
        <td><?php if($has_single_h1_on_each_page==1){ echo "Yes"; } else { echo "No"; } ?></td>
      </tr>
      <tr>
        <td><b>Meta Country</b></td>
        <td><?php if($analysis_country!=""){ echo $analysis_country; } else { echo "-"; } ?></td>
      </tr>
      <tr>
        <td><b>Meta Address</b></td>
        <td><?php if($detected_address!=""){ echo $detected_address; } else { echo "-"; } ?></td>
      </tr>
      <tr>
        <td><b>Meta Name</b></td>
        <td><?php if($detected_name!=""){ echo $detected_name; } else { echo "-"; } ?></td>
      </tr>
      <tr>
        <td><b>Meta Phone</b></td>
        <td><?php if($detected_phone!=""){ echo $detected_phone; } else { echo "-"; } ?></td>
      </tr>
      <tr>
        <td><b>Number of Keywords Ranked</b></td>
        <td>{{ $num_keywords_ranked_for }}</td>
      </tr>
      <tr>
        <td><b>Top Keywords</b></td>
        <td><?php if($top_keywords_ranked_for!=""){ echo $top_keywords_ranked_for; } else { echo "-"; } ?></td>
      </tr>
      <tr>
        <td><b>Duplicated Items</b></td>
        <td>{{ $duplicated_items }}</td>
      </tr>
      <tr>
        <td><b>Home Page Tags</b></td>
        <td><?php if($homepage_title_tag!=""){ echo $homepage_title_tag; } else { echo "-"; } ?></td>
      </tr>
      <tr>
        <td><b>Missing Items</b></td>
        <td>{{ $missing_items }}</td>
      </tr>
      <tr>
        <td><b>Duplicate Descriptions</b></td>
        <td>{{ $pages_duplicated_description_count }}</td>
      </tr>
      <tr>
        <td><b>Duplicate Titles</b></td>
        <td>{{ $pages_duplicated_title_count }}</td>
      </tr>
      <tr>
        <td><b>Pages missing Descriptions</b></td>
        <td>{{ $pages_missing_description_count }}</td>
      </tr>
      <tr>
        <td><b>Pages missing Titles</b></td>
        <td>{{ $pages_missing_title_count }}</td>
      </tr>
      <tr>
        <td><b>Percentage Duplicate Descriptions</b></td>
        <td>{{ $percent_duplicated_descriptions }}</td>
      </tr>
      <tr>
        <td><b>Percentage Duplicate Tiles</b></td>
        <td>{{ $percent_duplicated_titles }}</td>
      </tr>
      <tr>
        <td><b>Percentage Missing Descriptions</b></td>
        <td>{{ $percent_missing_descriptions }}</td>
      </tr>
      <tr>
        <td><b>Percentage Missing Descriptions</b></td>
        <td>{{ $percent_missing_titles }}</td>
      </tr>


  </tbody>
</table>

<br>


<table>
  <thead>
    <tr>
    <th colspan="2">Images</th>
  </tr>
  </thead>
  <tbody>
      <tr>
        <td><b>Image Count</b></td>
        <td>{{ $image_count }}</td>
      </tr>
      <tr>
        <td><b>No web friendly images</b></td>
        <td>{{ $non_web_friendly_count }}</td>
      </tr>
      <tr>
        <td><b>Sized Images</b></td>
        <td>{{ $percent_images_sized }}</td>
      </tr>
      <tr>
        <td><b>Strected Images</b></td>
        <td>{{ $stretched_image_count }}</td>
      </tr>
  </tbody>
</table>

<br>

<table>
  <thead>
    <tr>
    <th colspan="2">Social</th>
  </tr>
  </thead>
  <tbody>
      <tr>
        <td><b>Has Instagram</b></td>
        <td><?php if($has_instagram!=""){ echo $has_instagram; } else { echo "-"; } ?></td>
      </tr>
      <tr>
        <td><b>Days since last Instagram Update</b></td>
        <td>{{ $days_since_update }}</td>
      </tr>
      <tr>
        <td><b>Last Instatgram Update</b></td>
        <td>@if($last_updated_date) {{ date('m/d/Y', strtotime($last_updated_date)) }} @else - @endif</td>
      </tr>
      <tr>
        <td><b>Twitter Info</b></td>
        <td><?php if($twitter_found==1){ echo "Yes"; } else { echo "No"; } ?></td>
      </tr>
  </tbody>
</table>

<br>

<table>
  <thead>
    <tr>
    <th colspan="2">Video</th>
  </tr>
  </thead>
  <tbody>
    <tr>
        <td><b>Has Video</b></td>
        <td><?php if($has_video==1){ echo "Yes"; } else { echo "No"; } ?></td>
      </tr>
      <tr>
        <td><b>Video Vendor</b></td>
        <td><?php if($vendor_vendor!=""){ echo $vendor_vendor; } else { echo "-"; } ?></td>
      </tr>
      <tr>
        <td><b>Video Vendor</b></td>
        <td><?php if($video_vendor!=""){ echo $video_vendor; } else { echo "-"; } ?></td>
      </tr>


  </tbody>
</table>
