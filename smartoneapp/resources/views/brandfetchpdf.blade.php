<style type="text/css">
    table {
        border-collapse: collapse;
        width: 100%;
    }

    table,
    th,
    td {
        border: 1px solid black;
    }

    th {
        text-align: center;
        height: 50px;
    }

    td {
        text-align: center;
        height: 25px;
        width: 25%;
    }

    .dot {
        height: 15px;
        width: 15px;

        border-radius: 50%;
        display: inline-block;
    }
</style>

<h2><u>BrandFetch Data @if(isset($web_url)) of {{$web_url}} @endif</u></h2>

<h4>Site Content</h4>

<table>
    <tbody>
        <tr>
            <td><b>Website</b></td>
            <td>@if(isset($web_url)) <a href="{{ $web_url }}" style="text-decoration: none; color: blue;">{{ $web_url }} @else - @endif</a></td>
        </tr>
        <tr>
            <td><b>Created Date</b></td>
            <td>@if($created_at) {{ date('m-d-Y H:i A',strtotime($created_at)) }} @else - @endif</td>
        </tr>

    </tbody>
</table>

<br>
<h4>Social Media</h4>

<table>
    <tbody>
        <tr>
            <td colspan="2"><?php if(!empty($twitter)){ ?><a href="{{ $twitter }}"><img
                        src="../public/socialmedia/twitter.png"></a><?php } else { echo "No twitter account found"; } ?>
            </td>
        </tr>
        <tr>
            <td colspan="2"><?php if(!empty($linkedin)){ ?><a href="{{ $linkedin }}"><img
                        src="../public/socialmedia/linkedin.png"></a><?php } else { echo "No LinkedIn account found"; } ?>
            </td>
        </tr>
        <tr>
            <td colspan="2"><?php if(!empty($github)){ ?><a href="{{ $github }}"><img
                        src="../public/socialmedia/github-logo.png"></a><?php } else { echo "No Github account found"; } ?>
            </td>
        </tr>
        <tr>
            <td colspan="2"><?php if(!empty($instagram)){ ?><a href="{{ $instagram }}"><img
                        src="../public/socialmedia/instagram.png"></a><?php } else { echo "No Instagram account found"; } ?>
            </td>
        </tr>
        <tr>
            <td colspan="2"><?php if(!empty($facebook)){ ?><a href="{{ $facebook }}"><img
                        src="../public/socialmedia/facebook.png"></a><?php } else { echo "No Facebook account found"; } ?>
            </td>
        </tr>
        <tr>
            <td colspan="2"><?php if(!empty($youtube)){ ?><a href="{{ $youtube }}"><img
                        src="../public/socialmedia/youtube.png"></a><?php } else { echo "No Youtube account found"; } ?>
            </td>
        </tr>
        <tr>
            <td colspan="2"><?php if(!empty($pinterest)){ ?><a href="{{ $pinterest }}"><img
                        src="../public/socialmedia/pinterest.png"></a><?php } else { echo "No Pinterest account found"; } ?>
            </td>
        </tr>
        <tr>
            <td colspan="2"><?php if(!empty($crunchbase)){ ?><a href="{{ $crunchbase }}"><img
                        src="../public/socialmedia/crunchbase.png"></a><?php } else { echo "No Crunchbase account found"; } ?>
            </td>
        </tr>

    </tbody>
</table>

<br>
<h4>Logo</h4>

<table>
    <tbody>
        <tr>
            <td><b>Logo</b></td>
            <td>@if(isset($web_logo)) <a href="{{ $web_logo }}" style="text-decoration: none; color: blue;">{{ $web_logo }}</a>@else - @endif</td>
        </tr>

    </tbody>
</table>

<br>
<h4>Logo Background</h4>

<table>
    <tbody>
        <tr>
            <td><b>Logo Background Color</b></td>
            <td>@if(isset($bg_color)){{ $bg_color }}&nbsp;  <span class="dot" style="background-color: {{ $bg_color }};"></span>@else - @endif</td>
        </tr>

    </tbody>
</table>

<br>
<h4>Logo Color Category</h4>

<table>
    <tbody>
        <tr>
            <td><b>Vibrant Color</b></td>
            <td>@if(isset($vibrant)){{ $vibrant }} &nbsp; <span class="dot" style="background-color: {{ $vibrant }};"></span>@else - @endif</td>
        </tr>

        <tr>
            <td><b>Dark Color</b></td>
            <td>@if(isset($dark)){{ $dark }} &nbsp; <span class="dot" style="background-color: {{ $dark }};"></span>@else - @endif</td>
        </tr>

        <tr>
            <td><b>Light Color</b></td>
            <td>@if(isset($light)){{ $light }} &nbsp; <span class="dot" style="background-color: {{ $light }};"></span>@else - @endif</td>
        </tr>

    </tbody>
</table>

<br>
<br>
<br>
<h4>Logo Color Raw</h4>

<table>
    <tbody>
        <tr>
            <td><b>Color Code</b></td>
            <td>@if(isset($l_colour_code)){{ $l_colour_code }} &nbsp; <span class="dot" style="background-color: {{ $l_colour_code }};"></span>@else - @endif
            </td>
        </tr>

        <tr>
            <td><b>Color Percentage</b></td>
            <td>@if(isset($l_percentage)){{ $l_percentage*100 }} % @else - @endif</td>
        </tr>

    </tbody>
</table>

<br>
<h4>Website Font</h4>

<table>
    <tbody>
        <tr>
            <td><b>Title tag</b></td>
            <td>@if(isset($title_tag)){{ $title_tag }} @else - @endif</td>
        </tr>

        <tr>
            <td><b>Primary Font</b></td>
            <td>@if(isset($primary_font)){{ $primary_font }}@else - @endif</td>
        </tr>

    </tbody>
</table>

<br>
<h4>Website Images</h4>

<table>
    <tbody>
        <tr>
            <td><b>Images</b></td>
            <td>@if(isset($file_path))<a href="{{ url('storage/app/'.$file_path) }}" style="text-decoration: none; color: blue;">{{ $file_path }}</a>@else - @endif</td>
        </tr>

    </tbody>
</table>

<br>
<h4>Website Metadata</h4>

<table>
    <tbody>
        <tr>
            <td><b>Title</b></td>
            <td>@if(isset($title)){{ $title }}@else - @endif</td>
        </tr>
        <tr>
            <td><b>Summary</b></td>
            <td>@if(isset($summary)){{ $summary }}@else - @endif</td>
        </tr>
        <tr>
            <td><b>Description</b></td>
            <td>@if(isset($description)){{ $description }}@else - @endif</td>
        </tr>
        <tr>
            <td><b>Keywords</b></td>
            <td>@if(isset($keywords)){{ $keywords }}@else - @endif</td>
        </tr>
        <tr>
            <td><b>Language</b></td>
            <td>@if(isset($language)){{ $language }}@else - @endif</td>
        </tr>

    </tbody>
</table>

<br>
<h4>Website Screenshot</h4>

<table>
    <tbody>
        <tr>
            <td><b>Screenshot url</b></td>
            <td>@if(isset($scn_url))<a href="{{ $scn_url }}" style="text-decoration: none; color: blue;">{{ $scn_url }}</a>@else - @endif</td>
        </tr>

    </tbody>
</table>

<br>
<h4>Website Color Category</h4>

<table>
    <tbody>
        <tr>
            <td><b>Vibrant</b></td>
            <td>@if(isset($vibrant)){{ $vibrant }} &nbsp; <span class="dot" style="background-color: {{ $vibrant }};"></span>@else - @endif</td>
        </tr>
        <tr>
            <td><b>Dark</b></td>
            <td>@if(isset($dark)){{ $dark }} &nbsp; <span class="dot" style="background-color: {{ $dark }};"></span>@else - @endif</td>
        </tr>
        <tr>
            <td><b>Light</b></td>
            <td>@if(isset($light)){{ $light }} &nbsp; <span class="dot" style="background-color: {{ $light }};"></span>@else - @endif</td>
        </tr>

    </tbody>
</table>

<br>
<h4>Website Color Raw</h4>

<table>
    <tbody>
        <tr>
            <td><b>Color Code</b></td>
            <td>@if(isset($colour_code)){{ $colour_code }} &nbsp; <span class="dot" style="background-color: {{ $colour_code }};"></span>@else - @endif</td>
        </tr>
        <tr>
            <td><b>Percentage</b></td>
            <td>@if(isset($percentage)){{ $percentage*100 }} % @else - @endif</td>
        </tr>

    </tbody>
</table>
