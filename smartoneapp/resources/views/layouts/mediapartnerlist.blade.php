<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <!-- <script src="{{ asset('js/app.js') }}" defer></script> -->
    <link rel="stylesheet" href="{{ asset('css/css/style.css') }}">
    <script src="https://kit.fontawesome.com/ef363a0a6e.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Datatables --->
    <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/> -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link href="{{ asset('css/css/sweetalert2.min.css') }}" rel="stylesheet">

    <!-- Fonts -->
    <!-- <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"> -->

    <!-- Styles -->
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
    <style type="text/css">
      ul#horizontal-list {
        min-width: 696px;
        list-style: none;
        padding-top: 20px;
        }
        ul#horizontal-list li {
          display: inline;
        }
        form .error {
        color: #ff0000;
        font-size: 14px;
      }
    </style>
</head>
<body>
    <div class="main-wrapper">


            @include('includes.sidebar')
            <div class="col-lg-10 col-md-8 col-sm-12 col-12 dash-main">
                  <!-- <div class="dash-main-nav">
                        <span class="openbtn" " onclick="openNav()">&#9776;</span>
                        <div class="dash-main-nav-search">
                              <i class="fas fa-search"></i><input type="text" placeholder="Search...">
                        </div>
                        <div class="dash-main-nav-settings">
                              <a href="#"><i class="fas fa-cog"></i></a>
                        </div>
                  </div> -->

                    @yield('content')

            </div>
      </div>
</body>
</html>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script> -->

<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="{{ asset('js/sweetalert2.all.min.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable();

    $('form[name="mediapartneradd"]').validate({
      rules: {
        mname: {
            required:true,
            normalizer:function(value){
                return $.trim(value);
            }
        },
        mrate: {
            required:true,
            normalizer:function(value){
                return $.trim(value);
            }
        },
        internal_rep: {
            required:true,
            normalizer:function(value){
                return $.trim(value);
            }
        }
      },
      messages: {
        mname: 'Please enter name',
        mrate: 'Please enter rate',
        internal_rep: 'Please enter internal rep'
      },
      submitHandler: function(form) {
        form.submit();
      }
    });


    $('form[name="mediapartneredit"]').validate({
      rules: {
        mname: {
            required:true,
            normalizer:function(value){
                return $.trim(value);
            }
        },
        mrate: {
            required:true,
            normalizer:function(value){
                return $.trim(value);
            }
        },
        internal_rep: {
            required:true,
            normalizer:function(value){
                return $.trim(value);
            }
        }
      },
      messages: {
        name: 'Please add a name',
        mrate: 'Please enter rate',
        internal_rep: 'Please enter internal rep'
      },
      submitHandler: function(form) {
        form.submit();
      }
    });
});



$(document).ready(function(){



var table = $('#example').DataTable();
$('#example tbody').on( 'click', 'a.deletemedia', function () {


 Swal.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.value) {

    // table
    //   .row( $(this).parents('tr') )
    //   .remove()
    //   .draw();

      $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

       var mid = $(this).attr("id");
       $.ajax({
          url: "{{ url('/mediapartner/delete') }}",
          method: 'post',
          data: {
             mediaid: mid
          },
          success: function(result){
             if(result==1){

              //$("#status_"+mid).text('Inactive');
              var APP_URL = {!! json_encode(url('/')) !!};
              Swal.fire({title: "Deleted", text: "Data has been deleted.", type: "success"}).then(

                function(){
                     window.location.href = APP_URL+'/mediapartner';
                 }
              );
             }
          },
          error : function(result)
          {

            Swal.fire(
              'Oops!',
              'Something went wrong!',
              'error'
            )
          }
        });



    // Swal.fire(
    //   'Deleted!',
    //   'Your file has been deleted.',
    //   'success'
    // )
  }





})


});


});
</script>
@include('includes.footer')
