<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <!-- <script src="{{ asset('js/app.js') }}" defer></script> -->
    <link rel="stylesheet" href="{{ asset('css/css/style.css') }}">
    <script src="https://kit.fontawesome.com/ef363a0a6e.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Datatables --->
    <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/> -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link href="{{ asset('css/css/sweetalert2.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="{{ asset('css/css/fancybox/jquery.fancybox.css?v=2.1.7') }}" rel="stylesheet">
    

    <!-- Fonts -->
    <!-- <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"> -->

    <!-- Styles -->
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->

    <style type="text/css">
      ul#horizontal-list {
        min-width: 696px;
        list-style: none;
        padding-top: 20px;
        }
        ul#horizontal-list li {
          display: inline;
        }
        .error {
            color: red;
            font-size: 14px;
        }
    </style>

</head>
<body>
    <div class="main-wrapper">
                        

            @include('includes.sidebar')
            <div class="col-lg-10 col-md-8 col-sm-12 col-12 dash-main">
                  <!-- <div class="dash-main-nav">
                        <span class="openbtn" " onclick="openNav()">&#9776;</span>
                        <div class="dash-main-nav-search">
                              <i class="fas fa-search"></i><input type="text" placeholder="Search...">
                        </div>
                        <div class="dash-main-nav-settings">
                              <a href="#"><i class="fas fa-cog"></i></a>
                        </div>
                  </div> -->
                  
                    @yield('content')
                  
            </div>
      </div>
</body>
</html>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script> -->

<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('js/sweetalert2.all.min.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable();
});

$("#menu_type").change(function(){
  var menu_type = $("#menu_type").val();
  if(menu_type==2)
  {
    $("#parentmenurow").css('display','block');
    $("#parent_menu").prop('required',true);
  } else {
    $("#parentmenurow").css('display','none');
    $("#parent_menu").prop('required',false);
  }
});


$(document).ready(function(){

  var menu_type = $("#menu_type").val();
  if(menu_type==2)
  {
    $("#parentmenurow").css('display','block');
    $("#parent_menu").prop('required',true);
  } else {
    $("#parentmenurow").css('display','none');
    $("#parent_menu").prop('required',false);
  }

//$("#parentmenurow").css('display','none');

$("#menuform").validate({
        rules: {
            menu_name: "required",
            menu_type: "required",
            menu_permission: "required",
            menu_slug: "required",
            menu_order: "required"
        },
        messages: {
            menu_name: "Name is required",
            menu_type: "Type is required",
            menu_permission: "Permission is required",
            menu_slug: "Slug is required",
            menu_order: "Order is required"

        }
    })


$('#saveMenu').click(function(){

    //$("#menuform").valid();
    if($("#menuform").valid())
    {
      var menu_name = $("#menu_name").val();
      var menu_type = $("#menu_type").val();
      var menu_permission = $("#menu_permission").val()
      var menu_slug = $("#menu_slug").val();
      var menu_order = $("#menu_order").val();
      var menu_icon = "";
      if($('#parentmenurow').css('display') == 'block'){
        var parent_menu = $("#parent_menu").val();
      } else {
        var parent_menu = 0;
      }
    
    

      $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });

      $.ajax({
            url: "{{ url('/menu/save') }}",
            method: 'post',
            data: {
               menu_name: menu_name,menu_type: menu_type,menu_permission: menu_permission,menu_slug: menu_slug,menu_order: menu_order,parent_menu: parent_menu,menu_icon: menu_icon
            },
            success: function(result){
               if(result==1){

                // Swal.fire(
                //   'Saved!',
                //   'Menu saved',
                //   'success'
                // )

                // var menu_name = $("#menu_name").val('');
                // var menu_type = $("#menu_type").val('');
                // var menu_permission = $("#menu_permission").val('')
                // var menu_slug = $("#menu_slug").val('');
                // var menu_order = $("#menu_order").val('');
                // $("#parentmenurow").css('display','none');

                var APP_URL = {!! json_encode(url('/')) !!};
                Swal.fire({title: "Saved", text: "Menu Saved.", type: "success"}).then(

                  function(){ 
                       window.location.href = APP_URL+'/menu';
                   }
                );
               }
            }});
  }

});




$('#updateMenu').click(function(){

    //$("#menuform").valid();
    if($("#editmenuform").valid())
    {
      var menu_id = $("#menu_id").val();
      var menu_name = $("#menu_name").val();
      var menu_type = $("#menu_type").val();
      var menu_permission = $("#menu_permission").val()
      var menu_slug = $("#menu_slug").val();
      var menu_order = $("#menu_order").val();
      if($('#parentmenurow').css('display') == 'block'){
        var parent_menu = $("#parent_menu").val();
      } else {
        var parent_menu = 0;
      }
    
    

      $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });

      $.ajax({
            url: "{{ url('/menu/update') }}",
            method: 'post',
            data: {
               menu_id: menu_id,menu_name: menu_name,menu_type: menu_type,menu_permission: menu_permission,menu_slug: menu_slug,menu_order: menu_order,parent_menu: parent_menu
            },
            success: function(result){
               if(result==1){
                  var APP_URL = {!! json_encode(url('/')) !!};
                  console.log(APP_URL);
                  Swal.fire({title: "Good job", text: "Menu Updated !", type: "success"}).then(

                    function(){ 
                         window.location.href = APP_URL+'/menu';
                     }
                    );
               }
            }});
  }

});


function format ( d ) {
    // `d` is the original data object for the row
  
   // var div = '';
   div = $('<table class="table table-striped table-bordered" style="width:100%;"><thead><tr><th>Sl No.</th><th>Name</th><th>Slug</th><th>Order</th><th>Status</th><th>Actions</th></tr></thead><tbody id='+d+'>');

   $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

 var mid = d; 
 $.ajax({
    url: "{{ url('/menu/submenu') }}",
    method: 'post',
    data: {
       menuid: mid
    },
    success: function(result){
       
       var objres = JSON.parse(result);
       var newRowContent = '';
       var i=1;
       var status= '';
       $.each(objres, function(key,value) {
        
        if(value.active_status==0) { status = "Active"; } else { status = "Disabled"; }
        newRowContent += '<tr><td>'+i+'</td><td>'+value.menu_name+'</td><td>'+value.menu_slug+'</td><td>'+value.menu_order+'</td><td>'+status+'</td><td><a  href="menu/edit/'+value.menu_id+'" title="Edit" style="text-decoration: none; color: inherit;"><i class="far fa-edit"></i></a>&nbsp; <a id='+value.menu_id+' href="javascript:void(0)" title="Delete" style="text-decoration: none; color: inherit;" class="deletemenu"><i class="far fa-trash-alt"></i></a></td><tr>';
        i=i+1;
       
      }); 
       $(div).append(newRowContent);
       $(div).append('</tbody></table><div/>');
    }});

    

    return div;

}
    
var table = $('#example').DataTable();   

$('#example tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
        var menu_id = $(this).closest('tr')[0].id;
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(menu_id) ).show();
            tr.addClass('shown');
        }
    } );


$('#example tbody').on( 'click', 'a.deletemenu', function () {


 Swal.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.value) {


      $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

       var mid = $(this).attr("id"); 
       $.ajax({
          url: "{{ url('/menu/delete') }}",
          method: 'post',
          data: {
             menuid: mid
          },
          success: function(result){
             if(result==1){

              $("#menustatus_"+mid).text('Disabled');

              var APP_URL = {!! json_encode(url('/')) !!};
              Swal.fire({title: "Deleted", text: "Menu has been deleted.", type: "success"}).then(

                function(){ 
                     window.location.href = APP_URL+'/menu';
                 }
              );
             }
          }});


    
    Swal.fire(
      'Disabled!',
      'Menu has disabled.',
      'success'
    )
  }
  
 


  
})

      
});



});

</script>
@include('includes.footer')