@extends('layouts.cloudwidget')

@section('content')

@if ($errors->any())
  <div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
    </ul>
  </div><br />
@endif
<div class="dash-main-body">
<div class="dash-main-form">

  
  <form  name="cloudwidget" id="cloudwidget" method="post" >

    @csrf
      <div class="col-lg-12 row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Name <span style="color: red;">*</span>
                  </label> 
              <input type="text" name="cl_name" id="cl_name" autocomplete="cl_name">
              <span class="emsg hidden">Please enter a valid name</span>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Email <span style="color: red;">*</span>
                  </label> 
              <input type="text" name="email" id="email" autocomplete="email" onchange="checkemailexist()">
              <span style="color: red; font-size: 14px;" id="email_err"></span>
            </div>
      </div>
      <div class="col-lg-12 row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Website URL <span style="color: red;">*</span>
                  </label> 
              <input type="url" placeholder="https://example.com" pattern="https://.*" name="company_url" id="company_url" autocomplete="company_url" onchange="checkwebsiteurl()">
              <span id="url_err" style="color: red;"></span>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Phone <span style="color: red;">*</span>
                  </label> 
              <input type="text" name="phone" id="phone" autocomplete="phone" onchange="Validate()">
              <span id="spnError" style="color: Red; display: none; font-size: 14px;">Invalid phone number</span>
            </div>
      </div>
      
  <div class="col-lg-12 row">    
      <div class="col-lg-12">
        <span align="center" id="progresscloud" style="color: green; display: none;">Processing..</span>
            <span id="buttonarea">
              <input type="button" class="dash-main-form-btn cloudwid_btn" value="Submit">
              <a href="{{ url('/home') }}"><input type="button" class="dash-main-form-btn cloud_cancel" value="Cancel" ></a>
            </span>
            
      </div>
  </div>
</form>
</div>
</div>

<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
          
          <span id="listbutton">
            <a class="btn btn-primary" href="{{ url('cloud/uploadwidget') }}" style="text-decoration: none; float: right; margin-right: 35px;">Widget List</a>
          </span>
          

          <span id="linktoggle" style="display: none;"><a id="widgetCode" class="btn btn-primary" href="" target="_blank" style="text-decoration: none;">Open widget&nbsp;<i class="fa fa-external-link"></i></a>
          </span>
          <br>
          <span id="textpart" style="display: none;">Copy and paste below code to your html page</span>
          <code><span id="embedcode" class="iframetext"></span></code>
          <!-- <span id="copyButton" style="display: none;"><p><button class="btn btn-primary js-copybtn" data-clipboard-text="">Copy Code&nbsp;<i class="fa fa-clipboard"></i></button></p></span> -->
    </div>
  </div>
</div>


@endsection


