@extends('layouts.list')

@section('content')

@if ($errors->any())
  <div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
    </ul>
  </div><br />
@endif
<div class="dash-main-body">
<div class="dash-main-form">
  <form action="{{ url('/permissions/savepermissions') }}" method="post">
     @csrf
        <div class="col-lg-12 row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                    <label>
                          Roles
                    </label> 
                <input type="text" name="name" id="name" value="{{ $users->role_name }}" readonly="">
                <input type="hidden" name="roleid" id="roleid" value="{{ $users->role_id }}" readonly="">
                <input type="hidden" name="userid" id="userid" value="{{ $users->id }}" readonly="">
              </div>

              <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                <br>
                    <label>
                          Select Permissions
                    </label> 
                    <ul style="list-style: none;">
                      <li><input type="checkbox" id="select_all" /> Select all</li>
                      <ul style="list-style: none;">
                        <?php $i=1; foreach ($permissions as $key => $value) { ?>
                        <li><input type="checkbox" class="checkbox" name="asperms[]" value="{{ $value->id }}" <?php if(!empty($assignPermissionsArr)){if(in_array($value->id, $assignPermissionsArr)){ ?> checked=""  <?php }} ?>>&nbsp;{{ $value->name }} </li>
                        <?php $i++; } ?>
                      </ul>
                    </ul>
                    <!-- <?php $i=1; foreach ($permissions as $key => $value) { ?>

                      <input type="checkbox" name="asperms[]" value="{{ $value->id }}" <?php if(!empty($assignPermissionsArr)){if(in_array($value->id, $assignPermissionsArr)){ ?> checked=""  <?php }} ?>>&nbsp;{{ $value->name }} <br>

                    <?php $i++; } ?> -->
               
              </div>
        </div>
        
    <div class="col-lg-6 row">    
        <div class="col-lg-6">
              <input type="submit" value="Submit" style="float: left;">
        </div>
    </div>
  </form>
</div>
</div>

@endsection

<script type="text/javascript">

</script>
