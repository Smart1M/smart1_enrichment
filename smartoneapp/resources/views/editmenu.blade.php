@extends('layouts.menulist')

@section('content')

@if ($errors->any())
  <div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
    </ul>
  </div><br />
@endif
<div class="dash-main-body">
<div class="dash-main-form">
  <form id="editmenuform" name="editmenuform" action="{{ url('menu/update/'.$menu_det->menu_id) }}" method="post">
    @csrf

      <input type="hidden" name="menu_id" id="menu_id" value="{{ $menu_det->menu_id }}">
      <div class="col-lg-12 row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Name <span style="color: red;">*</span>
                  </label> 
              <input type="text" name="menu_name" id="menu_name" required autocomplete="menu_name" value="{{ $menu_det->menu_name }}">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Type <span style="color: red;">*</span>
                  </label> 

              <input type="hidden" name="menu_type" id="menu_type" value="{{ $menu_det->menu_type }}">
               <select name="menu_type_dis" id="menu_type_dis" required="" style="width: 100%; height: 30px;" disabled="">
                  <option value="">Select Type</option>
                      <option value="1" <?php if($menu_det->menu_type==1){ ?>selected=""<?php } ?>>Parent</option>
                      <option value="2" <?php if($menu_det->menu_type==2){ ?>selected=""<?php } ?>>Sub Menu</option>
                </select>
            </div>
      </div>
      
      <div class="col-lg-12 row" id="parentmenurow" <?php if($menu_det->parent_id!=0){ ?> style="display: block;" <?php } ?>>
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                  <label>
                        Parent Menu <span style="color: red;">*</span>
                  </label> 
                <select name="parent_menu" id="parent_menu" style="width: 100%; height: 30px;">
                  <option value="">Choose Parent Menu</option>
                  @foreach($parent_menu as $key => $val)
                      <option value="{{ $val->menu_id }}" <?php if($menu_det->parent_id==$val->menu_id) { ?>selected=""<?php } ?>>{{ $val->menu_name }}</option>
                  @endforeach
                </select>
            </div>
      </div>
    
      <div class="col-lg-12 row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Menu Permission <span style="color: red;">*</span>
                  </label> 
                <select name="menu_permission" id="menu_permission" required="" style="width: 100%; height: 30px;">
                  <option value="">Choose Permission</option>
                  @foreach($permissions as $k => $v)
                      <option value="{{ $v->id }}" <?php if($menu_det->menu_perm_name==$v->id) { ?>selected=""<?php } ?>>{{ $v->name }}</option>
                  @endforeach
                </select>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Slug <span style="color: red;">*</span>
                  </label> 
                <input type="text" name="menu_slug" id="menu_slug" required="" autocomplete="menu_slug" value="{{ $menu_det->menu_slug }}">
            </div>
      </div>
      <div class="col-lg-12 row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Order <span style="color: red;">*</span>
                  </label> 
              <input type="text" name="menu_order" id="menu_order" required autocomplete="menu_order" value="{{ $menu_det->menu_order }}">
            </div>
            <!-- <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Icon <span style="color: grey;">(<a href="https://fontawesome.com/v4.7.0/icons/" target="_blank" style="text-decoration: none;">Font awesome icon class name <i class="fa fa-external-link" aria-hidden="true"></i></a>)</span>
                  </label> 
              <input type="text" name="menu_icon" id="menu_icon" autocomplete="menu_icon">
            </div> -->
      </div>
      
  <div class="col-lg-12 row">    
      <div class="col-lg-12">
            <input type="button" value="Submit" name="updateMenu" id="updateMenu" style="float: left;">
            <a href="{{ url('/menu') }}"><input type="button" class="dash-main-form-btn" value="Cancel" style="float: left;" ></a>
      </div>
  </div>
</form>
</div>
</div>

@endsection

<script type="text/javascript">

</script>
