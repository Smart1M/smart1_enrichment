@extends('layouts.googlemapdatalist')

@section('content')

<!-- @if ($errors->any())
  <div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
    </ul>
  </div><br />
@endif -->

@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif

@if (session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
@endif
<div class="dash-main-body">
<div class="dash-main-form">
  <form action="{{ url('googledata/importfile') }}" method="post" name="googleexportadd" enctype="multipart/form-data">
    @csrf
      <div class="col-lg-12 row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                  <label>
                        Upload file (CSV) <span style="color: red;">*</span>
                  </label> 
              <input type="file" name="google_export" id="google_export" onchange="return fileValidation()">
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-12 pt-1">
              <input type="checkbox" name="updateKnack" id="updateKnack">&nbsp;Add clients to Knack</input>
            </div>
      </div>
      
      
  <div class="col-lg-12 row">    
      <div class="col-lg-12" id="button_area">
            <input type="submit" class="dash-main-form-btn" id="upload_submit" value="Submit">
            <a href="{{ url('/company') }}"><input type="button" class="dash-main-form-btn" value="Cancel" ></a>
      </div>
      <div class="col-lg-12" id="alert_msg" style="display: none;">
        <p>Processing.. Please wait</p>
      </div>
  </div>
</form>
</div>
</div>

<div class="container">
<a href="{{ url('uploads/sample/sample-csv-sheet.csv') }}" style="text-decoration: none;" class="btn btn-default">Sample Sheet&nbsp; <i class="fas fa-download"></i></a>

<a href="{{ url('/company') }}" style="text-decoration: none; float: right;" class="btn btn-default"><i class="fa fa-arrow-left"></i>&nbsp;Company List</a>

</div>

@endsection

<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script type="text/javascript">

function fileValidation(){
    var fileInput = document.getElementById('google_export');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.csv)$/i;
    if(!allowedExtensions.exec(filePath)){
        alert('Please upload file having extension .csv only.');
        fileInput.value = '';
        return false;
    }else{

    }
}

// $(document).ready(function(){
//   $("#upload_submit").on('click',function(){
//     $("#button_area").hide();
//     $("#alert_msg").css('display','block');
//   });
// });

</script>
