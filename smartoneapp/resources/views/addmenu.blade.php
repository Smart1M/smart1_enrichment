@extends('layouts.menulist')

@section('content')

@if ($errors->any())
  <div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
    </ul>
  </div><br />
@endif
<div class="dash-main-body">
<div class="dash-main-form">
  <form id="menuform" name="menuform" action="{{ url('menu/save') }}" method="post">
    @csrf
      <div class="col-lg-12 row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Name <span style="color: red;">*</span>
                  </label> 
              <input type="text" name="menu_name" id="menu_name" required autocomplete="menu_name">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Type <span style="color: red;">*</span>
                  </label> 
               <select name="menu_type" id="menu_type" required="" style="width: 100%; height: 30px;">
                  <option value="">Select Type</option>
                      <option value="1">Parent</option>
                      <option value="2">Sub Menu</option>
                </select>
            </div>
      </div>
      <div class="col-lg-12 row" id="parentmenurow">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                  <label>
                        Parent Menu <span style="color: red;">*</span>
                  </label> 
                <select name="parent_menu" id="parent_menu" style="width: 100%; height: 30px;">
                  <option value="">Choose Parent Menu</option>
                  @foreach($parent_menu as $key => $val)
                      <option value="{{ $val->menu_id }}">{{ $val->menu_name }}</option>
                  @endforeach
                </select>
            </div>
      </div>
      <div class="col-lg-12 row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Menu Permission <span style="color: red;">*</span>
                  </label> 
                <select name="menu_permission" id="menu_permission" required="" style="width: 100%; height: 30px;">
                  <option value="">Choose Permission</option>
                  @foreach($permissions as $k => $v)
                      <option value="{{ $v->id }}">{{ $v->name }}</option>
                  @endforeach
                </select>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Slug <span style="color: red;">*</span>
                  </label> 
                <input type="text" name="menu_slug" id="menu_slug" required="" autocomplete="menu_slug">
            </div>
      </div>
      <div class="col-lg-12 row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Order <span style="color: red;">*</span>
                  </label> 
              <input type="text" name="menu_order" id="menu_order" required autocomplete="menu_order">
            </div>
            <!-- <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Icon <span style="color: grey;">(<a href="https://fontawesome.com/v4.7.0/icons/" target="_blank" style="text-decoration: none;">Font awesome icon class name <i class="fa fa-external-link" aria-hidden="true"></i></a>)</span>
                  </label> 
              <input type="text" name="menu_icon" id="menu_icon" autocomplete="menu_icon">
            </div> -->
      </div>
      
  <div class="col-lg-12 row">    
      <div class="col-lg-12">
            <input type="button" value="Submit" name="saveMenu" id="saveMenu" style="float: left;">
            <a href="{{ url('/menu') }}"><input type="button" class="dash-main-form-btn" value="Cancel" style="float: left;" ></a>
      </div>
  </div>
</form>
</div>
</div>

@endsection

<script type="text/javascript">

</script>
