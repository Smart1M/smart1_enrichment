@extends('layouts.customerlist')

@section('content')


<p>
@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
        
        <ul id="horizontal-list">
            <li>
                <a href="{{ url('customers/add') }}" >
                <button type="button" class="btn btn-primary" name="create" class="">Customer <i class="fa fa-plus"></i></button>
                </a> 
            </li>
        </ul> 

        <table id="example" class="table table-striped table-bordered" style="width:100%;">
        <thead>
        <tr>
            <th>Sl.No</th>
            <th>Name</th>
            <th>Company</th>
            <th>DOB</th>
            <th>Email</th>
            <th>Mobile</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>

        <?php $i=1; foreach ($customer as $key => $value) { ?>
        <tr>
            <td>{{ $i }} </td>
            <td>@if ($value->fname){{ $value->fname }}@endif</td>
            <td>{{ $value->cname }}</td>
            <td>{{ $value->dob }}</td>
            <td>{{ $value->email }}</td>
            <td>{{ $value->mob }}</td>
            <td><span id="status_{{ $value->cust_id }}">@if($value->cstatus==0) Active @else Inactive @endif </span></td>
            <td><a  href="{{ url('customers/edit/'.$value->cust_id) }}" title="Edit" style="text-decoration: none; color: inherit;"><i class="far fa-edit"></i></a>&nbsp; <a id="{{ $value->cust_id }}" href="javascript:void(0)" title="Delete" style="text-decoration: none; color: inherit;" class="deletecustomer"><i class="far fa-trash-alt"></i></a></td>
        </tr>

        <?php $i++; } ?>

        </tbody>
        </table>
            
        </div>
    </div>
</div>
</p>



@endsection

