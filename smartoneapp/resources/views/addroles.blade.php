@extends('layouts.list')

@section('content')

@if ($errors->any())
  <div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
    </ul>
  </div><br />
@endif
<div class="dash-main-body">
<div class="dash-main-form">
  <form action="{{ url('/roles/save') }}" method="post" name="roleadd">
     @csrf
        <div class="col-lg-12 row">
              <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                    <label>
                          Role <span style="color: red;">*</span>
                    </label> 
                <input type="text" name="name" id="name" >
              </div>
        </div>
        
    <div class="col-lg-12 row">    
        <div class="col-lg-12">
              <input type="submit" class="dash-main-form-btn" value="Submit" style="float: left;">
              <a href="{{ url('/roles') }}"><input type="button" class="dash-main-form-btn" value="Cancel" style="float: left;" ></a>
        </div>
    </div>
  </form>
</div>
</div>

@endsection

<script type="text/javascript">

</script>
