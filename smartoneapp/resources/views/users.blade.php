@extends('layouts.userlist')

@section('content')


<p>
@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
        
        <ul id="horizontal-list">
            @can('add users')
            <li>
                <a href="{{ url('users/add') }}" >
                <button type="button" class="btn btn-primary" name="create" class="">User <i class="fa fa-plus"></i></button>
                </a> 
            </li>
            @endcan
        </ul> 

        <table id="example" class="table table-striped table-bordered" style="width:100%;">
        <thead>
        <tr>
            <th>Sl.No</th>
            <th>Name</th>
            <th>Email</th>
            <th>Mobile</th>
            <th>Role</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>

        <?php $i=1; foreach ($users as $key => $value) { ?>
        <tr>
            <td>{{ $i }}</td>
            <td>{{ $value->name }}</td>
            <td>{{ $value->email }}</td>
            <td>{{ $value->mobile }}</td>
            <td>{{ $value->rolename }}</td>
            <td><span id="status_{{ $value->id }}">@if($value->ustatus==0) Active @else Inactive @endif </span></td>
            <td>@can('edit users')<a  href="{{ url('users/edit/'.$value->id) }}" title="Edit" style="text-decoration: none; color: inherit;"><i class="far fa-edit"></i></a>@endcan @can('delete users')&nbsp; <a id="{{ $value->id }}" href="javascript:void(0)" title="Delete" style="text-decoration: none; color: inherit;" class="deleteusers"><i class="far fa-trash-alt"></i></a>@endcan&nbsp;@can('assign permission')<a  href="{{ url('permissions/assignpermission/'.$value->id) }}" title="Assign Permission" style="text-decoration: none; color: inherit;"><i class="fa fa-link"></i></a>@endcan</td>
        </tr>

        <?php $i++; } ?>

        </tbody>
        </table>
            
        </div>
    </div>
</div>
</p>



@endsection

