@extends('layouts.userlist')

@section('content')

@if ($errors->any())
  <div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
    </ul>
  </div><br />
@endif

@if (session('error_msg'))
    <div class="alert alert-danger">
        {{ session('error_msg') }}
    </div>
@endif
<div class="dash-main-body">
<div class="dash-main-form">
  <form action="{{ url('/users/update/'.$users->id) }}" method="post" name="useredit" >
    @csrf
      <div class="col-lg-12 row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Name <span style="color: red;">*</span>
                  </label>
              <input type="text" name="name" id="name"  autocomplete="name" value="{{ $users->name }}">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Role <span style="color: red;">*</span>
                  </label>
              <select name="role_id" id="role_id" style="width: 100%;" >
                <option value="">Select a Role</option>
                <?php foreach ($roles as $key => $value) { ?>
                 <option value="{{ $value->id }}" <?php if($value->id==$users->role_id) { ?>selected=""<?php } ?> >{{ $value->name }}</option>
               <?php } ?>
              </select>
            </div>
      </div>
      <div class="col-lg-12 row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Email <span style="color: red;">*</span>
                  </label>
              <input type="text"  name="email" id="email" value="{{ $users->email }}"  autocomplete="email" onchange="checkemailexist('{{ $users->id }}')">
              <span style="color: red; font-size: 14px;" id="email_err"></span>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Mobile <span style="color: red;">*</span>
                  </label>
              <input type="text" name="mobile" id="mobile" autocomplete="Mobile" value="{{ $users->mobile }}" onchange="Validate()">
              <span id="spnError" style="color: Red; display: none">*Invalid phone number</span>
            </div>
      </div>
      <?php if(Auth::user()->id==$users->id){ ?>

        <div class="col-lg-12 row">

            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Password <span style="color: red;">*</span>
                  </label>
              <input type="password" name="password" id="password" style="width: 100%;"  autocomplete="new-password" value="{{ $users->rawpassword }}">
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <label>
                        Confirm password <span style="color: red;">*</span>
                  </label>
              <input id="password-confirm" type="password"  name="password_confirmation"  autocomplete="new-password" style="width: 100%" value="{{ $users->rawpassword }}">
            </div>
      </div>

      <?php } ?>

  <div class="col-lg-12 row">
      <div class="col-lg-12">
            <input type="submit" class="dash-main-form-btn" value="Submit">
            <a href="{{ url('/users') }}"><input type="button" class="dash-main-form-btn" value="Cancel" ></a>
      </div>
  </div>
</form>
</div>
</div>

@endsection

<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
<script type="text/javascript">
// $(document).ready(function() {

//     var regExp = /[a-z]/i;
//     $('#mobile').on('keydown keyup', function(e) {
//       var value = String.fromCharCode(e.which) || e.key;

//       // No letters
//       if (regExp.test(value)) {
//         e.preventDefault();
//         return false;
//       }
//     });
// });
function Validate() {
    var isValid = false;
    var regex = /^[0-9-+()'']*$/;
    isValid = regex.test(document.getElementById("mobile").value);
    document.getElementById("spnError").style.display = !isValid ? "block" : "none";

    if(isValid==false)
    {
      $("#mobile").val("");
    }
    return isValid;
}
function checkemailexist(userid)
{
  var mail = document.getElementById('email').value;
  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
  {

    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

       $.ajax({
          url: "{{ url('/users/editcheckemail') }}",
          method: 'post',
          data: {
             email: mail,userid:userid
          },
          success: function(result){

             if(result=='1'){

                $("#email_err").text('Email Id already exist');
                $("#email").val("");


             } else {
                $("#email_err").text('');
             }
          }
        });

  } else{
    ///alert("You have entered an invalid email address!")
    $("#email_err").text('You have entered an invalid email address!');
    $("#email").val("");
  }
}
</script>
