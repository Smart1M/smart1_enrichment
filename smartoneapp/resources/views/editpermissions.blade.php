@extends('layouts.permission')

@section('content')

@if ($errors->any())
  <div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
    </ul>
  </div><br />
@endif
<div class="dash-main-body">
<div class="dash-main-form">
  <form action="{{ url('/permissions/update/'.$permissions->id) }}" method="post" name="permissionedit">
     @csrf
        <div class="col-lg-12 row">
              <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                    <label>
                          Permission <span style="color: red;">*</span>
                    </label> 
                <input type="text" name="name" id="name" value="{{ $permissions->name }}" >
              </div>
        </div>
        
    <div class="col-lg-6 row">    
        <div class="col-lg-6">
              <input type="submit" value="Submit" style="float: left;">
        </div>
    </div>
  </form>
</div>
</div>

@endsection

<script type="text/javascript">

</script>
